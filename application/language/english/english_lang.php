<?php

$lang['language'] = array(
    'language' => 'english',
    'button_view_detail' => 'View detail',



    'txt_main_info_title' => 'Main information',
    'txt_main_info_title_small' => '',
    'txt_input_title_page' => 'Title',
    'txt_input_title_vn_page' => 'Title',
    'txt_input_base_domain' => 'Base URL',
    'txt_header_home_page' => 'Text Header',
    'txt_header_home_vn_page' => 'Text Header (VN)',
    'txt_header_member_page' => 'Text Member',
    'txt_header_member_vn_page' => 'Text Member (VN)',

);