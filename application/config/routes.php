<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'admin/login';
$route['ApiSyc/createUser'] = 'ApiSyc/createUser';
$route['ApiSyc/updateUser'] = 'ApiSyc/updateUser';

$route['admin/list-upload-file'] = 'admin/listUploadFile';
$route['admin/upload-file'] = 'admin/uploadFile';
$route['admin/upload-file/search'] = 'admin/searchUploadFile';


$route['captcha/refresh'] = 'contact/refreshCaptcha';
//About
$route['introduce/(:any)'] = 'introduce/show/$1';

//Business
$route['business/(:any)'] = 'business/show/$1';

//Project
$route['project/(:any)'] = 'project/show/$1';
$route['project/detail/(:any)'] = 'project/showDetail/$1';

//Partner - Customer
$route['customer-partner'] = 'partner/show';


$route['career-job'] = 'careerjob/show';
$route['career-job/detail/(:any)'] = 'careerjob/showDetail/$1';
$route['career-job/apply/(:any)'] = 'careerjob/showApply/$1';
$route['career-job/(:any)'] = 'careerjob/showSub/$1';

//contact
$route['contact'] = 'contact/show';
$route['contact/sendContact'] = 'contact/sendContact';

//News
$route['news-media'] = 'news/show';
$route['news-media/(:any)'] = 'news/showDetail/$1';
$route['more/(:any)'] = 'news/readMore/$1';
$route['language/(:any)'] = 'home/switchLanguage/$1';
//////////////////////////////////////////////////ADMIN
#region MAIN INFO
//$route['admin/main-info'] = 'admin/mainInfo';
$route['admin/main-info/create'] = 'admin/mainInfoCreate';
$route['admin/main-info/edit/(:any)'] = 'admin/mainInfoEdit/$1';
$route['admin/main-info/detail/(:any)'] = 'admin/mainInfoDetail/$1';
#endregion

#region MAIN MENU
//$route['admin/main-info'] = 'admin/mainInfo';
$route['admin/main-menu/create'] = 'admin/mainMenuCreate';
$route['admin/main-menu/edit/(:any)'] = 'admin/mainMenuEdit/$1';
$route['admin/main-menu/detail/(:any)'] = 'admin/mainMenuDetail/$1';
$route['admin/main-menu/list'] = 'admin/mainMenuList';
$route['admin/main-menu/search'] = 'admin/mainMenuSearch';
#endregion

#region MAIN SLIDE
$route['admin/main-banner/create'] = 'admin/mainBannerCreate';
$route['admin/main-banner/edit/(:any)'] = 'admin/mainBannerEdit/$1';
$route['admin/main-banner/detail/(:any)'] = 'admin/mainBannerDetail/$1';
$route['admin/main-banner/list'] = 'admin/mainBannerList';
$route['admin/main-banner/search'] = 'admin/mainBannerSearch';
#endregion

#region PROJECT
$route['admin/project/create'] = 'admin/projectCreate';
$route['admin/project/edit/(:any)'] = 'admin/projectEdit/$1';
$route['admin/project/detail/(:any)'] = 'admin/projectDetail/$1';
$route['admin/project/list'] = 'admin/projectList';
$route['admin/project/search'] = 'admin/projectSearch';
#endregion

#region PROJECT DETAIL
$route['admin/project-detail/create'] = 'admin/projectDetailCreate';
$route['admin/project-detail/edit/(:any)'] = 'admin/projectDetailEdit/$1';
$route['admin/project-detail/detail/(:any)'] = 'admin/projectDetailDetail/$1';
$route['admin/project-detail/list'] = 'admin/projectDetailList';
$route['admin/project-detail/search'] = 'admin/projectDetailSearch';
#endregion

#region BUSINESS
$route['admin/business/create'] = 'admin/businessCreate';
$route['admin/business/edit/(:any)'] = 'admin/businessEdit/$1';
$route['admin/business/detail/(:any)'] = 'admin/businessDetail/$1';
$route['admin/business/list'] = 'admin/businessList';
$route['admin/business/search'] = 'admin/businessSearch';
#endregion

#region NEWS
$route['admin/news/create'] = 'admin/newsCreate';
$route['admin/news/edit/(:any)'] = 'admin/newsEdit/$1';
$route['admin/news/detail/(:any)'] = 'admin/newsDetail/$1';
$route['admin/news/list'] = 'admin/newsList';
$route['admin/news/setting'] = 'admin/newsSetting';
$route['admin/news/search'] = 'admin/newsSearch';
#endregion

#region PARTNER GLOBAL
$route['admin/partner-global/create'] = 'admin/partnerGlobalCreate';
$route['admin/partner-global/edit/(:any)'] = 'admin/partnerGlobalEdit/$1';
$route['admin/partner-global/detail/(:any)'] = 'admin/partnerGlobalDetail/$1';
$route['admin/partner-global/list'] = 'admin/partnerGlobalList';
$route['admin/partner-global/search'] = 'admin/partnerGlobalSearch';
#endregion

#region PARTNER CUSTOMER
$route['admin/partner-customer/create'] = 'admin/partnerCustomerCreate';
$route['admin/partner-customer/edit/(:any)'] = 'admin/partnerCustomerEdit/$1';
$route['admin/partner-customer/detail/(:any)'] = 'admin/partnerCustomerDetail/$1';
$route['admin/partner-customer/list'] = 'admin/partnerCustomerList';
$route['admin/partner-customer/search'] = 'admin/partnerCustomerSearch';
#endregion

#region PARTNER
$route['admin/partner/create'] = 'admin/partnerPartnerCreate';
$route['admin/partner/edit/(:any)'] = 'admin/partnerPartnerEdit/$1';
$route['admin/partner/detail/(:any)'] = 'admin/partnerPartnerDetail/$1';
$route['admin/partner/list'] = 'admin/partnerPartnerList';
$route['admin/partner/search'] = 'admin/partnerPartnerSearch';
#endregion


#region INTRODUCE
$route['admin/introduce/create'] = 'admin/introduceCreate';
$route['admin/introduce/edit/(:any)'] = 'admin/introduceEdit/$1';
$route['admin/introduce/detail/(:any)'] = 'admin/introduceDetail/$1';
$route['admin/introduce/list'] = 'admin/introduceList';
$route['admin/introduce/search'] = 'admin/introduceSearch';
#endregion

#region INTRODUCE ABOUT
$route['admin/introduce-about/edit/(:any)'] = 'admin/introduceAboutEdit/$1';
$route['admin/introduce-about/detail/(:any)'] = 'admin/introduceAboutDetail/$1';
#endregion

#region INTRODUCE ABOUT AWARDS
$route['admin/introduce-about-awards/edit/(:any)'] = 'admin/introduceAboutAwardsEdit/$1';
$route['admin/introduce-about-awards/detail/(:any)'] = 'admin/introduceAboutAwardsDetail/$1';
#endregion

#region INTRODUCE ABOUT CORE
$route['admin/introduce-about-core/edit/(:any)'] = 'admin/introduceAboutCoreEdit/$1';
$route['admin/introduce-about-core/detail/(:any)'] = 'admin/introduceAboutCoreDetail/$1';
#endregion

#region INTRODUCE ABOUT AWARDS ARTICLE
$route['admin/introduce-about-awards-article/edit/(:any)'] = 'admin/introduceAboutAwardsArticleEdit/$1';
$route['admin/introduce-about-awards-article/create'] = 'admin/introduceAboutAwardsArticleCreate';
$route['admin/introduce-about-awards-article/detail/(:any)'] = 'admin/introduceAboutAwardsArticleDetail/$1';
$route['admin/introduce-about-awards-article/list'] = 'admin/introduceAboutAwardsArticleList';
$route['admin/introduce-about-awards-article/search'] = 'admin/introduceAboutAwardsArticleSearch';
#endregion

#region INTRODUCE ABOUT CORE ARTICLE
$route['admin/introduce-about-core-article/edit/(:any)'] = 'admin/introduceAboutCoreArticleEdit/$1';
$route['admin/introduce-about-core-article/create'] = 'admin/introduceAboutCoreArticleCreate';
$route['admin/introduce-about-core-article/detail/(:any)'] = 'admin/introduceAboutCoreArticleDetail/$1';
$route['admin/introduce-about-core-article/list'] = 'admin/introduceAboutCoreArticleList';
$route['admin/introduce-about-core-article/search'] = 'admin/introduceAboutCoreArticleSearch';
#endregion

#region INTRODUCE ABOUT NETWORK
$route['admin/introduce-about-network/edit/(:any)'] = 'admin/introduceAboutNetworkEdit/$1';
$route['admin/introduce-about-network/create'] = 'admin/introduceAboutNetworkCreate';
$route['admin/introduce-about-network/detail/(:any)'] = 'admin/introduceAboutNetworkDetail/$1';
$route['admin/introduce-about-network/list'] = 'admin/introduceAboutNetworkList';
$route['admin/introduce-about-network/search'] = 'admin/introduceAboutNetworkSearch';
#endregion

#region INTRODUCE ABOUT SOCIAL
$route['admin/introduce-about-social/edit/(:any)'] = 'admin/introduceAboutSocialEdit/$1';
$route['admin/introduce-about-social/detail/(:any)'] = 'admin/introduceAboutSocialDetail/$1';
#endregion

#region INTRODUCE ABOUT SYSTEM MANAGEMENT
$route['admin/introduce-about-system-management/edit/(:any)'] = 'admin/introduceAboutSystemManagementEdit/$1';
$route['admin/introduce-about-system-management/detail/(:any)'] = 'admin/introduceAboutSystemManagementDetail/$1';
#endregion

#region INTRODUCE ABOUT SYSTEM MANAGEMENT ARTICLE
$route['admin/introduce-about-system-management-article/edit/(:any)'] = 'admin/introduceAboutSystemManagementArticleEdit/$1';
$route['admin/introduce-about-system-management-article/create'] = 'admin/introduceAboutSystemManagementArticleCreate';
$route['admin/introduce-about-system-management-article/detail/(:any)'] = 'admin/introduceAboutSystemManagementArticleDetail/$1';
$route['admin/introduce-about-system-management-article/list'] = 'admin/introduceAboutSystemManagementArticleList';
$route['admin/introduce-about-system-management-article/search'] = 'admin/introduceAboutSystemManagementArticleSearch';
#endregion

#region INTRODUCE ABOUT MODEL CULTURE
$route['admin/introduce-model-culture/edit/(:any)'] = 'admin/introduceModelCultureEdit/$1';
$route['admin/introduce-model-culture/detail/(:any)'] = 'admin/introduceModelCultureDetail/$1';
#endregion

#region INTRODUCE ABOUT MODEL CULTURE MEMBER
$route['admin/introduce-model-culture-member/edit/(:any)'] = 'admin/introduceModelCultureMemberEdit/$1';
$route['admin/introduce-model-culture-member/create'] = 'admin/introduceModelCultureMemberCreate';
$route['admin/introduce-model-culture-member/detail/(:any)'] = 'admin/introduceModelCultureMemberDetail/$1';
$route['admin/introduce-model-culture-member/list'] = 'admin/introduceModelCultureMemberList';
$route['admin/introduce-model-culture-member/search'] = 'admin/introduceModelCultureMemberSearch';
#endregion

#region INTRODUCE ABOUT MODEL STORIES
$route['admin/introduce-model-stories/edit/(:any)'] = 'admin/introduceModelStoriesEdit/$1';
$route['admin/introduce-model-stories/detail/(:any)'] = 'admin/introduceModelStoriesDetail/$1';
#endregion

#region INTRODUCE ABOUT MODEL STORIES MEMBER
$route['admin/introduce-model-stories-member/edit/(:any)'] = 'admin/introduceModelStoriesMemberEdit/$1';
$route['admin/introduce-model-stories-member/create'] = 'admin/introduceModelStoriesMemberCreate';
$route['admin/introduce-model-stories-member/detail/(:any)'] = 'admin/introduceModelStoriesMemberDetail/$1';
$route['admin/introduce-model-stories-member/list'] = 'admin/introduceModelStoriesMemberList';
$route['admin/introduce-model-stories-member/search'] = 'admin/introduceModelStoriesMemberSearch';
#endregion

#region INTRODUCE NEWS
$route['admin/introduce-news/edit/(:any)'] = 'admin/introduceNewsEdit/$1';
$route['admin/introduce-news/create'] = 'admin/introduceNewsCreate';
$route['admin/introduce-news/detail/(:any)'] = 'admin/introduceNewsDetail/$1';
$route['admin/introduce-news/list'] = 'admin/introduceNewsList';
$route['admin/introduce-news/search'] = 'admin/introduceNewsSearch';
#endregion

#region NEWS DOWNLOAD
$route['admin/news-download/create'] = 'admin/newsDownloadCreate';
$route['admin/news-download/edit/(:any)'] = 'admin/newsDownloadEdit/$1';
$route['admin/news-download/detail/(:any)'] = 'admin/newsDownloadDetail/$1';
$route['admin/news-download/list'] = 'admin/newsDownloadList';
$route['admin/news-download/search'] = 'admin/newsDownloadSearch';
#endregion

#region NEWS INTRODUCE
$route['admin/news-introduce/create'] = 'admin/newsIntroduceCreate';
$route['admin/news-introduce/edit/(:any)'] = 'admin/newsIntroduceEdit/$1';
$route['admin/news-introduce/detail/(:any)'] = 'admin/newsIntroduceDetail/$1';
$route['admin/news-introduce/list'] = 'admin/newsIntroduceList';
$route['admin/news-introduce/search'] = 'admin/newsIntroduceSearch';
#endregion

#region CAREER JOB
$route['admin/career-job/create'] = 'admin/careerJobCreate';
$route['admin/career-job/edit/(:any)'] = 'admin/careerJobEdit/$1';
$route['admin/career-job/detail/(:any)'] = 'admin/careerJobDetail/$1';
$route['admin/career-job/list'] = 'admin/careerJobList';
$route['admin/career-job/search'] = 'admin/careerJobSearch';
#endregion

#region NEWS SETTING
$route['admin/new-setting/create'] = 'admin/newSettingCreate';
$route['admin/new-setting/edit/(:any)'] = 'admin/newSettingEdit/$1';
$route['admin/new-setting/detail/(:any)'] = 'admin/newSettingDetail/$1';
$route['admin/new-setting/list'] = 'admin/newSettingList';
$route['admin/new-setting/search'] = 'admin/newSettingSearch';
#endregion

#region CONTACT
$route['admin/contact/create'] = 'admin/contactCreate';
$route['admin/contact/edit/(:any)'] = 'admin/contactEdit/$1';
$route['admin/contact/detail/(:any)'] = 'admin/contactDetail/$1';
#endregion

#region INTRODUCE MEMBER CULTURE
$route['admin/introduce-member-culture/edit/(:any)'] = 'admin/introduceMemberCultureEdit/$1';
$route['admin/introduce-member-culture/detail/(:any)'] = 'admin/introduceMemberCultureDetail/$1';
#endregion

#region INTRODUCE MEMBER STORIES
$route['admin/introduce-member-stories/edit/(:any)'] = 'admin/introduceMemberStoriesEdit/$1';
$route['admin/introduce-member-stories/detail/(:any)'] = 'admin/introduceMemberStoriesDetail/$1';
#endregion

#region INTRODUCE MEMBER STORIES ARTICLE
$route['admin/introduce-member-culture-member/edit/(:any)'] = 'admin/introduceMemberCultureMemberEdit/$1';
$route['admin/introduce-member-culture-member/create'] = 'admin/introduceMemberCultureMemberCreate';
$route['admin/introduce-member-culture-member/detail/(:any)'] = 'admin/introduceMemberCultureMemberDetail/$1';
$route['admin/introduce-member-culture-member/list'] = 'admin/introduceMemberCultureMemberList';
$route['admin/introduce-member-culture-member/search'] = 'admin/introduceMemberCultureMemberSearch';
#endregion

#region INTRODUCE MEMBER STORIES MEMBER
$route['admin/introduce-member-stories-member/edit/(:any)'] = 'admin/introduceMemberStoriesMemberEdit/$1';
$route['admin/introduce-member-stories-member/create'] = 'admin/introduceMemberStoriesMemberCreate';
$route['admin/introduce-member-stories-member/detail/(:any)'] = 'admin/introduceMemberStoriesMemberDetail/$1';
$route['admin/introduce-member-stories-member/list'] = 'admin/introduceMemberStoriesMemberList';
$route['admin/introduce-member-stories-member/search'] = 'admin/introduceMemberStoriesMemberSearch';
#endregion

#region INTRODUCE REVENUE
$route['admin/introduce-revenue/edit/(:any)'] = 'admin/introduceRevenueEdit/$1';
$route['admin/introduce-revenue/detail/(:any)'] = 'admin/introduceRevenueDetail/$1';
#endregion

#region --------------------------- SAVE ----------------------------------

#region MAIN SLIDE
$route['admin/main-banner/save'] = 'save/saveMainSlideImage';
$route['admin/main-banner/active-inactive'] = 'save/activeOrInactiveMainImage';
$route['admin/main-banner/approve-deny'] = 'save/approveOrDenyMainImage';
$route['admin/main-banner/delete'] = 'save/deleteMainImage';
#endregion

#region PROJECT
$route['admin/project/save'] = 'save/saveProject';
$route['admin/project/active-inactive'] = 'save/activeOrInactiveProject';
$route['admin/project/approve-deny'] = 'save/approveOrDenyProject';
$route['admin/project/delete'] = 'save/deleteProject';
#endregion

#region PROJECT
$route['admin/project-detail/save'] = 'save/saveProjectDetail';
$route['admin/project-detail/active-inactive'] = 'save/activeOrInactiveProjectDetail';
$route['admin/project-detail/approve-deny'] = 'save/approveOrDenyProjectDetail';
$route['admin/project-detail/delete'] = 'save/deleteProjectDetail';
#endregion

#region BUSINESS
$route['admin/business/save'] = 'save/saveBusiness';
$route['admin/business/active-inactive'] = 'save/activeOrInactiveBusiness';
$route['admin/business/approve-deny'] = 'save/approveOrDenyBusiness';
$route['admin/business/delete'] = 'save/deleteBusiness';
#endregion

#region NEWS
$route['admin/news/save'] = 'save/saveNews';
$route['admin/news/active-inactive'] = 'save/activeOrInactiveNews';
$route['admin/news/approve-deny'] = 'save/approveOrDenyNews';
$route['admin/news/delete'] = 'save/deleteNews';
#endregion

#region PARTNER GLOBAL
$route['admin/partner-global/save'] = 'save/savePartnerGlobal';
$route['admin/partner-global/active-inactive'] = 'save/activeOrInactivePartnerGlobal';
#endregion

#region PARTNER CUSTOMER
$route['admin/partner-customer/save'] = 'save/savePartnerCustomer';
$route['admin/partner-customer/active-inactive'] = 'save/activeOrInactivePartnerCustomer';
$route['admin/partner-customer/approve-deny'] = 'save/approveOrDenyPartnerCustomer';
$route['admin/partner-customer/delete'] = 'save/deletePartnerCustomer';
#endregion

#region PARTNER
$route['admin/partner/save'] = 'save/savePartner';
$route['admin/partner/active-inactive'] = 'save/activeOrInactivePartner';
    $route['admin/partner/approve-deny'] = 'save/approveOrDenyPartner';
$route['admin/partner/delete'] = 'save/deletePartner';
#endregion

#region INTRODUCE
$route['admin/introduce/save'] = 'save/saveIntroduce';
$route['admin/introduce/active-inactive'] = 'save/activeOrInactiveIntroduce';
$route['admin/introduce/approve-deny'] = 'save/approveOrDenyIntroduce';
$route['admin/introduce/delete'] = 'save/deleteIntroduce';
#endregion

#region INTRODUCE ABOUT
$route['admin/introduce-about/save'] = 'save/saveIntroduceAbout';
$route['admin/introduce-about/active-inactive'] = 'save/activeOrInactiveIntroduceAbout';
$route['admin/introduce-about/approve-deny'] = 'save/approveOrDenyIntroduceAbout';
#endregion

#region INTRODUCE ABOUT AWARDS
$route['admin/introduce-about-awards/save'] = 'save/saveIntroduceAboutAwards';
$route['admin/introduce-about-awards/active-inactive'] = 'save/activeOrInactiveIntroduceAboutAwards';
$route['admin/introduce-about-awards/approve-deny'] = 'save/approveOrDenyIntroduceAboutAwards';
#endregion

#region INTRODUCE ABOUT CORE
$route['admin/introduce-about-core/save'] = 'save/saveIntroduceAboutCore';
$route['admin/introduce-about-core/active-inactive'] = 'save/activeOrInactiveIntroduceAboutCore';
$route['admin/introduce-about-core/approve-deny'] = 'save/approveOrDenyIntroduceAboutCore';
#endregion

#region INTRODUCE ABOUT AWARDS ARTICLE
$route['admin/introduce-about-awards-article/save'] = 'save/saveIntroduceAboutAwardsArticle';
$route['admin/introduce-about-awards-article/active-inactive'] = 'save/activeOrInactiveIntroduceAboutAwardsArticle';
$route['admin/introduce-about-awards-article/approve-deny'] = 'save/approveOrDenyIntroduceAboutAwardsArticle';
$route['admin/introduce-about-awards-article/delete'] = 'save/deleteIntroduceAboutAwardsArticle';
#endregion

#region INTRODUCE ABOUT CORE ARTICLE
$route['admin/introduce-about-core-article/save'] = 'save/saveIntroduceAboutCoreArticle';
$route['admin/introduce-about-core-article/active-inactive'] = 'save/activeOrInactiveIntroduceAboutCoreArticle';
$route['admin/introduce-about-core-article/approve-deny'] = 'save/approveOrDenyIntroduceAboutCoreArticle';
$route['admin/introduce-about-core-article/delete'] = 'save/deleteIntroduceAboutCoreArticle';
#endregion

#region INTRODUCE ABOUT NETWORK
$route['admin/introduce-about-network/save'] = 'save/saveIntroduceAboutNetwork';
$route['admin/introduce-about-network/active-inactive'] = 'save/activeOrInactiveIntroduceAboutNetwork';
$route['admin/introduce-about-network/approve-deny'] = 'save/approveOrDenyIntroduceAboutNetwork';
$route['admin/introduce-about-network/delete'] = 'save/deleteIntroduceAboutNetwork';
#endregion

#region INTRODUCE ABOUT SOCIAL
$route['admin/introduce-about-social/save'] = 'save/saveIntroduceAboutSocial';
$route['admin/introduce-about-social/active-inactive'] = 'save/activeOrInactiveIntroduceAboutSocial';
$route['admin/introduce-about-social/approve-deny'] = 'save/approveOrDenyIntroduceAboutSocial';
#endregion

#region INTRODUCE ABOUT SYSTEM MANAGEMENT
$route['admin/introduce-about-system-management/save'] = 'save/saveIntroduceAboutSystemManagement';
$route['admin/introduce-about-system-management/active-inactive'] = 'save/activeOrInactiveIntroduceAboutSystemManagement';
$route['admin/introduce-about-system-management/approve-deny'] = 'save/approveOrDenyIntroduceAboutSystemManagement';
#endregion

#region INTRODUCE ABOUT SYSTEM MANAGEMENT ARTICLE
$route['admin/introduce-about-system-management-article/save'] = 'save/saveIntroduceAboutSystemManagementArticle';
$route['admin/introduce-about-system-management-article/active-inactive'] = 'save/activeOrInactiveIntroduceAboutSystemManagementArticle';
$route['admin/introduce-about-system-management-article/approve-deny'] = 'save/approveOrDenyIntroduceAboutSystemManagementArticle';
$route['admin/introduce-about-system-management-article/delete'] = 'save/deleteIntroduceAboutAwardsArticle';
#endregion

#region INTRODUCE MODEL CULTURE
$route['admin/introduce-model-culture/save'] = 'save/saveIntroduceModelCulture';
$route['admin/introduce-model-culture/active-inactive'] = 'save/activeOrInactiveIntroduceModelCulture';
$route['admin/introduce-model-culture/approve-deny'] = 'save/approveOrDenyIntroduceModelCulture';
#endregion

#region INTRODUCE MODEL CULTURE MEMBER
$route['admin/introduce-model-culture-member/save'] = 'save/saveIntroduceModelCultureMember';
$route['admin/introduce-model-culture-member/active-inactive'] = 'save/activeOrInactiveIntroduceModelCultureMember';
$route['admin/introduce-model-culture-member/approve-deny'] = 'save/approveOrDenyIntroduceModelCultureMember';
$route['admin/introduce-model-culture-member/delete'] = 'save/deleteIntroduceModelCultureMember';
#endregion

#region INTRODUCE MODEL STORIES
$route['admin/introduce-model-stories/save'] = 'save/saveIntroduceModelStories';
$route['admin/introduce-model-stories/active-inactive'] = 'save/activeOrInactiveIntroduceModelStories';
$route['admin/introduce-model-stories/approve-deny'] = 'save/approveOrDenyIntroduceModelStories';
#endregion

#region INTRODUCE MODEL STORIES MEMBER
$route['admin/introduce-model-stories-member/save'] = 'save/saveIntroduceModelStoriesMember';
$route['admin/introduce-model-stories-member/active-inactive'] = 'save/activeOrInactiveIntroduceModelStoriesMember';
$route['admin/introduce-model-stories-member/approve-deny'] = 'save/approveOrDenyIntroduceModelStoriesMember';
$route['admin/introduce-model-stories-member/delete'] = 'save/deleteIntroduceModelStoriesMember';
#endregion

#region INTRODUCE NEWS
$route['admin/introduce-news/save'] = 'save/saveIntroduceNews';
$route['admin/introduce-news/active-inactive'] = 'save/activeOrInactiveIntroduceNews';
$route['admin/introduce-news/approve-deny'] = 'save/approveOrDenyIntroduceNews';
$route['admin/introduce-news/delete'] = 'save/deleteIntroduceNews';
#endregion

#region NEWS DOWNLOAD
$route['admin/news-download/save'] = 'save/saveNewsDownload';
$route['admin/news-download/active-inactive'] = 'save/activeOrInactiveNewsDownload';
$route['admin/news-download/approve-deny'] = 'save/approveOrDenyNewsDownload';
$route['admin/news-download/delete'] = 'save/deleteNewsDownload';
#endregion

$route['admin/home'] = 'admin/home';

#region MAIN INFO
$route['admin/main-info/save'] = 'save/saveMainInfo';
#endregion

#region MAIN MENU
$route['admin/main-menu/save'] = 'save/saveMainMenu';
$route['admin/main-menu/active-inactive'] = 'save/activeOrInactiveMainMenu';
$route['admin/main-menu/delete'] = 'save/deleteMainMenu';
#endregion

#region NEWS INTRODUCE
$route['admin/news-introduce/save'] = 'save/saveNewsIntroduce';
$route['admin/news-introduce/active-inactive'] = 'save/activeOrInactiveNewsIntroduce';
$route['admin/news-introduce/approve-deny'] = 'save/approveOrDenyNewsIntroduce';
$route['admin/news-introduce/delete'] = 'save/deleteNewsIntroduce';
#endregion

#region CAREER JOB
$route['admin/career-job/save'] = 'save/saveCareerJob';
$route['admin/career-job/active-inactive'] = 'save/activeOrInactiveCareerJob';
$route['admin/career-job/approve-deny'] = 'save/approveOrDenyCareerJob';
$route['admin/career-job/delete'] = 'save/deleteCareerJob';
#endregion

#region NEWS SETTING
$route['admin/new-setting/save'] = 'save/saveNewSetting';
$route['admin/new-setting/active-inactive'] = 'save/activeOrInactiveNewSetting';
#endregion

#region CONTACT
$route['admin/contact/save'] = 'save/saveContact';
#endregion

#region INTRODUCE MEMBER CULTURE
$route['admin/introduce-member-culture/save'] = 'save/saveIntroduceMemberCulture';
$route['admin/introduce-member-culture/active-inactive'] = 'save/activeOrInactiveIntroduceMemberCulture';
$route['admin/introduce-member-culture/approve-deny'] = 'save/approveOrDenyIntroduceMemberCulture';
#endregion

#region INTRODUCE MEMBER CULTURE MEMBER
$route['admin/introduce-member-culture-member/save'] = 'save/saveIntroduceMemberCultureMember';
$route['admin/introduce-member-culture-member/active-inactive'] = 'save/activeOrInactiveIntroduceMemberCultureMember';
$route['admin/introduce-member-culture-member/approve-deny'] = 'save/approveOrDenyIntroduceMemberCultureMember';
$route['admin/introduce-member-culture-member/delete'] = 'save/deleteIntroduceMemberCultureMember';
#endregion

#region INTRODUCE MEMBER STORIES
$route['admin/introduce-member-stories/save'] = 'save/saveIntroduceMemberStories';
$route['admin/introduce-member-stories/active-inactive'] = 'save/activeOrInactiveIntroduceMemberStories';
$route['admin/introduce-member-stories/approve-deny'] = 'save/approveOrDenyIntroduceMemberStories';
#endregion

#region INTRODUCE MEMBER STORIES MEMBER
$route['admin/introduce-member-stories-member/save'] = 'save/saveIntroduceMemberStoriesMember';
$route['admin/introduce-member-stories-member/active-inactive'] = 'save/activeOrInactiveIntroduceMemberStoriesMember';
$route['admin/introduce-member-stories-member/approve-deny'] = 'save/approveOrDenyIntroduceMemberStoriesMember';
$route['admin/introduce-member-stories-member/delete'] = 'save/deleteIntroduceMemberStoriesMember';
#endregion

#region INTRODUCE REVENUE
$route['admin/introduce-revenue/save'] = 'save/saveIntroduceRevenue';
$route['admin/introduce-revenue/active-inactive'] = 'save/activeOrInactiveIntroduceRevenue';
$route['admin/introduce-revenue/approve-deny'] = 'save/approveOrDenyIntroduceRevenue';
#endregion

#endregion
