<?php
/**
 * Created by PhpStorm.
 * User: harry.long
 * Date: 6/29/2017
 * Time: 9:49 AM
 */
class Utils
{
    function getFieldData($array, $field, $idField = null)
    {
        $_out = array();

        if (is_array($array)) {
            if ($idField == null) {
                foreach ($array as $value) {
                    $_out[] = trim($value[$field]);
                }
            } else {
                foreach ($array as $value) {
                    if(is_array($value[$field]) || is_object($value[$field])){
                        $_out[$value[$idField]] = $value[$field];
                    }else{
                        $_out[$value[$idField]] = trim($value[$field]);
                    }
                }
            }
            return $_out;
        } else {
            return false;
        }
    }

    function getFieldDataArray($array, $idField = null)
    {
        $_out = array();

        if (is_array($array)) {
            if ($idField == null) {
                foreach ($array as $value) {
                    $_out[] = trim($value);
                }
            } else {
                foreach ($array as $value) {
                    $_out[$value[$idField]] = $value;
                }
            }
            return $_out;
        } else {
            return false;
        }
    }

    function filterDocumentByCategory($documents = array()){
        $result = array();
        if(count($documents) > 0){
            for($i = 0;$i < count($documents);$i++){
                $result[$documents[$i]['category_id']][] = $documents[$i];
            }
        }
        return $result;
    }

    function saveImage($base64_string, $output_file)
    {
        // echo $output_file; die;
        $ifp = fopen($output_file, 'wb');
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return $output_file;
    }

    public function convertDateTimeToFormatDB($date_time)// d/m/Y h:ia
    {
        $date_time = trim($date_time);
        $str = explode(" ", $date_time);
        if (count($str) > 0 && count($str) == 2) {

            $date = $str[0];
            $time = $str[1];
            $result_date = self::convertDateDB($date);
            $result_time = self::convertTimeDB($time);
            return $result_date . ' ' . $result_time;
        } else {
            return '';
        }
    }

    public function convertDateDB($date)// d/m/Y
    {
        $str = explode("/", $date);
        $date_value = $str[0] . '-' . $str[1] . '-' . $str[2];
        if ($date_value == '' || $date_value == '0000-00-00') {
            return '';
        } else {
            return date('Y-m-d', strtotime($date_value));
        }
    }

    public function convertTimeDB($time)// h:ia
    {
        $time = trim($time);
        $str = explode(":", $time);
        $result = '';
        if (count($str) > 0 && count($str) == 2) {
            $hour = $str[0];
            $min_me = $str[1];
            if ($hour > 0 && $hour < 13) {
                if ($hour == 12) {
                    $hour = 0;
                }
                $me = substr($min_me, -2);
                $min = substr($min_me, 0, 2);
                if ($me == 'am') {
                    $result = $hour . ':' . $min . ':00';
                } else {
                    $result = ($hour + 12) . ':' . $min . ':00';
                }
                return $result;
            } else {
                return $result;
            }
        } else {
            return $result;
        }
    }

    public static function convertDateDisplay($date)
    {
        if ($date == '' || $date == '0000-00-00 00:00:00' || $date == '0000-00-00') {
            return '';
        } else {
            return date('d/m/Y', strtotime($date));
        }
    }

    public static function convertDateTimeDisplay($date_time)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {
            return date('d/m/Y h:ia', strtotime($date_time));
        }
    }

    public static function convertDateToString($date){
        $str = explode("-", $date);
        $date_value = $str[0] . ' tháng ' . $str[1] . ' năm ' . $str[2];
        return $date_value;
    }
	
	public static function convertDateToString2($date){
		$str = explode(" ", $date);
        $strDate = explode("-", $str[0]);
        $date_value = $strDate [2] . ' tháng ' . $strDate [1] . ' năm ' . $strDate [0];
        return $date_value;
    }

    public static function checkParamPost($dataPost,$fieldRequire){
        $flag = true;
        $field = '';
        for($i = 0 ;$i < count($fieldRequire);$i++){
            if(!(array_key_exists($fieldRequire[$i],$dataPost))){
                $flag = false;
                $field = $fieldRequire[$i];
            }
        }
        return array(
            'status'=>$flag,
            'field'=>$field//str_replace('_',' ',$field),
        );
    }
    public static function checkAuthenticationMobileStation($parameters,$authorizationRequest){
        $parameters_decode = json_decode($parameters,true);
        if(time() - $parameters_decode['timestamp'] > 600){
            return array(
                'error_code' => 202,
                'description' => 'Authentication token is expired'
            );
        }
        $tokenStandard = hash_hmac('sha1','WEBCORP', $parameters.TOKEN);

        if($authorizationRequest == $tokenStandard){
            return array(
                'error_code' => 200,
                'description' => 'Success'
            );
        }
        return array(
            'error_code' => 203,
            'description' => 'Authorization token failed'
        );
    }
    public static function generalNewArrayForKey($array = array(),$key = ''){
        $result = array();
        for($i = 0 ;$i < count($array);$i++){
            $result[] = $array[$i][$key];
        }
        return $result;
    }
    public static function sendEmail($to, $sub, $content)
    {
        $ci =& get_instance();
        $config = $ci->config->item('email_setting');

        $ci->load->library('email');
        $ci->email->initialize($config);
        $ci->email->set_newline("\r\n");
        $ci->email->from($ci->config->item('sender_email'), 'CNS System');
        $ci->email->to($to);

//        $bcc[] = 'dinhduongtin35b@gmail.com';
//
//        if(count($bcc)>0) {
//            $ci->email->bcc($bcc);
//        }

        $ci->email->subject($sub);
        $ci->email->message($content);
        return $ci->email->send() ? TRUE : $ci->email->print_debugger();
    }
    public function sendEmailMultiAttachFile($to = array(),$cc = array(),$bcc = array(), $sub = '', $content = '' ,$arrFile = array(),$sender_name = 'CNS', $reply, $reply_name = '')
    {
        // var_dump($to); die;
        $this->_ci =& get_instance();

        $config = $this->_ci->config->item('email_setting');

        $this->_ci->load->library('email');
        $this->_ci->email->clear(true);
        $this->_ci->email->initialize($config);
        $this->_ci->email->set_newline("\r\n");
        $this->_ci->email->from($this->_ci->config->item('sender_email'), $sender_name);
        if(count($to)>0) {
             for ($i = 0; $i < count($to); $i++) {
                 $this->_ci->email->to($to[$i]);
             }
        }else{
            return false;
        }
        if(count($cc)>0) {
            for ($i = 0; $i < count($cc); $i++) {
                $this->_ci->email->cc($cc[$i]);
            }
        }

        if(count($bcc)>0) {
            $this->_ci->email->bcc($bcc);
        }

        $this->_ci->email->subject($sub);
        $this->_ci->email->message($content);
        if(count($arrFile) > 0 ){
            for($i=0;$i<count($arrFile);$i++) {
                if(isset($arrFile[$i]['file']) && $arrFile[$i]['file'] != '') {
                    if (isset($arrFile[$i]['file_name']) && $arrFile[$i]['file_name'] != '') {
                        $this->_ci->email->attach($arrFile[$i]['file'], 'attachment', $arrFile[$i]['file_name']);
                    } else {
                        $this->_ci->email->attach($arrFile[$i]['file']);
                    }
                }
            }
        }
        $this->_ci->email->reply_to($reply, $reply_name);
        return $this->_ci->email->send() ? TRUE : FALSE;
        // $this->_ci->email->send();
        // echo $this->_ci->email->print_debugger(); die;

    }
    public function refresh(){
        $ci =& get_instance();

        $captcha = create_captcha($ci->config->item("config_captcha"));

        // Unset previous captcha and store new captcha word
        $ci->session->unset_userdata('captchaCode');
        $ci->session->set_userdata('captchaCode',$captcha['word']);

        // Display captcha image
        return $captcha['image'];
    }
}