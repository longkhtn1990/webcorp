<?php
class Common
{
    function generateSelectOption($list = array(),$disabled = array(),$selected = null){
        $result = '';
        if(count($list) > 0){
            foreach($list AS $value => $text){
                $selected_attr = '';
                $disabled_attr = '';
                if($value == $selected){
                    $selected_attr = ' selected="selected" ';
                }
                if(array_search($value,$disabled) !== false){
                    $disabled_attr = ' disabled ';
                }
                $result .= '<option value="'.$value.'" '.$selected_attr. $disabled_attr.'>'.$text.'</option>';
            }
        }
        return $result;
    }
    function generateSelectOptionArray($list = array(),$disabled = array(),$selected = null,$field = ''){
        $result = '';
        if(count($list) > 0){
            foreach($list AS $value => $text){
                $selected_attr = '';
                $disabled_attr = '';
                if($text['id'] == $selected){
                    $selected_attr = ' selected="selected" ';
                }
                if(array_search($text['id'],$disabled) !== false){
                    $disabled_attr = ' disabled ';
                }
                $result .= '<option value="'.$text['id'].'" '.$selected_attr. $disabled_attr.'>'.$text[$field].'</option>';
            }
        }
        return $result;
    }
}