<?php

/**
 * Created by PhpStorm.
 * User: harry.long
 * Date: 9/8/2017
 * Time: 11:17 AM
 */
class checkData
{
    public function checkTypeIntroduce($page){
        $list = array(
            'about-us' => 'about-us',
            'about-revenue' => 'news',
            'about-leadership' => 'news',
            'about-operational-model' => 'about-model'
        );
        return $list[$page];
    }
}