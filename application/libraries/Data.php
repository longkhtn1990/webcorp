<?php

class Data
{
    private $domain = '';
    private $mainInfoData = array();

    public function countTotalView(){
        // Lấy giá trị session.
        $CI =& get_instance();
        $CI->load->model('main_model'); 
        $check_view = $CI->session->userdata(SESSION_COUNT);
        $data = $CI->main_model->get_main_info_with_id(1);

        if( empty( $check_view ) ){
            $CI->session->set_userdata(SESSION_COUNT, 1);
            $CI->main_model->update_main_info(1,array(
                'total_view' => $data['total_view'] + 1,
            ));
        }   
        if($data['week_count_total'] == date('W')){
            if( empty( $check_view ) ){
                $CI->session->set_userdata(SESSION_COUNT, 1);
                $CI->main_model->update_main_info(1,array(
                    'total_view_week' => $data['total_view_week'] + 1,
                ));
            }
        }else{
            if( empty( $check_view ) ){
                $CI->session->set_userdata(SESSION_COUNT, 1);
                $CI->main_model->update_main_info(1,array(
                    'total_view_week' => 1,
                    'week_count_total'=>date('W')
                ));
            }
        }
    }
    public function mainInfo($page = '')
    {
        $data = array();
        $t = 0;
        $CI =& get_instance();
        $CI->load->model('main_model');
        $CI->load->model('news_model');
        $CI->load->model('project_model');
        $CI->load->model('business_model');
        $CI->load->model('introduce_model');
        $CI->load->library('utils');
        $this->countTotalView();
        $currentLanguage = $CI->session->userdata('language');
        $mainInfo = $CI->main_model->get_data_display();

        if ($mainInfo != false) {
            $this->domain = $mainInfo[$t]['base_domain'];
            $this->mainInfoData = $mainInfo[$t];

            $data['title'] = $currentLanguage == 'vietnamese' ? $mainInfo[$t]['title_vn'] : $mainInfo[$t]['title'];
            $data['favicon'] = $this->domain . $mainInfo[$t]['favicon'];
            $data['logo'] = $this->domain . $mainInfo[$t]['logo'];
            $data['link'] = $this->domain;
            $data['mainHeader'] = array(
                'home' => array(
                    'link' => $this->domain,
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_header_home_vn'] : $mainInfo[$t]['text_header_home']
                ),
                'member' => array(
                    'link' => VPDT_CNS,
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_header_member_vn'] : $mainInfo[$t]['text_header_member']
                ),
                'customerContact' => array(
                    'link' => $this->domain."contact",
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_header_contact_vn'] : $mainInfo[$t]['text_header_contact']
                ),
            );
            $data['mainFooter'] = array(
                'connecting' => array(
                    'title' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_connecting_footer_vn'] : $mainInfo[$t]['text_connecting_footer'],
                    'facebook' => $mainInfo[$t]['url_facebook'] != '' ? $mainInfo[$t]['url_facebook'] : '',
                    'youtube' => $mainInfo[$t]['url_youtube'] != '' ? $mainInfo[$t]['url_youtube'] : '',
                    'googlePlus' => $mainInfo[$t]['url_google_plus'] != '' ? $mainInfo[$t]['url_google_plus'] : '',
                    'totalView' => array(
                        'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_total_view_vn'] : $mainInfo[$t]['text_total_view'],
                        'number' => number_format($mainInfo[$t]['total_view']),
                        'unit' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['view_unit_vn'] : $mainInfo[$t]['view_unit'],
                    ),
                    'weekView' => array(
                        'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_total_view_week_vn'] : $mainInfo[$t]['text_total_view_week'],
                        'number' => number_format($mainInfo[$t]['total_view_week']),
                        'unit' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['view_unit_vn'] : $mainInfo[$t]['view_unit'],
                    ),
                ),
                'copyright' => array(
                    'pre-name' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['copyright_pre_text_vn'] : $mainInfo[$t]['copyright_pre_text'],
                    'name' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['copyright_text_vn'] : $mainInfo[$t]['copyright_text'],
                ),
                'career-job' => array(
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_career_job_vn'] : $mainInfo[$t]['text_career_job'],
                    'link' => 'career-job'
                ),
                'customer-contact' => array(
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_customer_contact_vn'] : $mainInfo[$t]['text_customer_contact'],
                    'link' => base_url().'contact'
                ),
            );

        } else {
            $data = array(
                'title' => 'Tổng công ty công nghiệp Sài Gòn',
                'favicon' => base_url() . 'assets/images/favicon.ico',
                'logo' => base_url() . 'assets/images/logo.png',
                'link' => base_url()
            );
            $data['mainHeader'] = array(
                'home' => array(
                    'link' => base_url(),
                    'text' => 'Trang chủ'
                ),
                'member' => array(
                    'link' => VPDT_CNS,
                    'text' => 'Thành viên'
                ),
                'customerContact' => array(
                    'link' => base_url().'contact',
                    'text' => 'Liên hệ'
                ),
            );
            $data['mainFooter'] = array(
                'connecting' => array(
                    'title' => 'Kết nối với chúng tôi',
                    'facebook' => '',
                    'youtube' => '',
                    'googlePlus' => '',
                    'totalView' => array(
                        'text' => 'Tổng lượt truy cập',
                        'number' => number_format(0),
                        'unit' => 'lượt'
                    ),
                    'weekView' => array(
                        'text' => 'Lượt truy cập trong tuần',
                        'number' => number_format(0),
                        'unit' => 'lượt'
                    ),
                ),
                'copyright' => array(
                    'pre-name' => 'Ủy ban Nhân dân TP.Hồ Chí Minh',
                    'name' => 'Tổng Công ty Công Nghiệp Sài Gòn - Trách nhiệm hữu hạn Một Thành Viên'
                ),
                'career-job' => array(
                    'text' => '',
                    'link' => ''
                ),
                'customer-contact' => array(
                    'text' => '',
                    'link' => ''
                ),
            );
        }
        $mainMenu = $CI->main_model->get_data_menu_display();
        if ($mainMenu != false) {
            for ($i = 0; $i < count($mainMenu); $i++) {
                $data['mainMenu'][$i]['text'] = $currentLanguage == 'vietnamese' ? $mainMenu[$i]['title_vn'] : $mainMenu[$i]['title'];
                if ($mainInfo[$t]['base_domain'] != '') {
                    $data['mainMenu'][$i]['link'] = $mainInfo[$t]['base_domain'] . $mainMenu[$i]['type'] . ($mainMenu[$i]['default'] != '' ? '/' . $mainMenu[$i]['default'] : '');
                    if ($page == $mainMenu[$i]['default'] && $page != '') {
                        $data['mainMenu'][$i]['active'] = true;
                    } else {
                        $data['mainMenu'][$i]['active'] = false;
                    }
                } else {
                    $data['mainMenu'][$i]['link'] = base_url() . $mainMenu[$i]['type'] . ($mainMenu[$i]['default'] != '' ? '/' . $mainMenu[$i]['title_vn'] : '');
                }
            }
        }
        /*$data['mainMenu'] = array(
            array(
                'text' => 'giới thiệu',
                'link' => base_url() . 'introduce/about-us',
                'active' => false
            ),
            array(
                'text' => 'lĩnh vực hoạt động',
                'link' => base_url() . 'business/technology-food',
                'active' => false
            ),
            array(
                'text' => 'dự án',
                'link' => base_url() . 'project/industrial-clusters',
                'active' => false
            ),
            array(
                'text' => 'khách hàng và đối tác',
                'link' => base_url() . 'customer-partner',
                'active' => false
            ),
            array(
                'text' => 'tin tức',
                'link' => base_url() . 'news-media',
                'active' => false
            ),
        );*/
        $new_model = $CI->news_model->get_list_category_display();
        $data_new_setting = $CI->utils->getFieldData($new_model,$currentLanguage == 'vietnamese'?'title_vn':'title','id');

        $introduce = $CI->introduce_model->get_data_introduce_display();
        $data['mainFooter']['col-1'] = array(
            'text' => 'Giới thiệu',
            'item' => array()
        );
        foreach ($introduce AS $index => $item){
            $data['mainFooter']['col-1']['item'][] = array(
                'text' => ucwords(mb_strtolower($currentLanguage == 'vietnamese' ?  $item['title_vn'] : $item['title'])),
                'link' => base_url() . 'introduce/'.$item['alias']
            );
        }

        $business = $CI->business_model->get_data_business_display();
        $data['mainFooter']['col-2'] =array(
            'text' => 'Lĩnh vực kinh doanh',
            'item' => array()
        );
        foreach ($business AS $index => $item){
	    if($item['position_order'] != -1) {
		$text = mb_convert_case($currentLanguage == 'vietnamese' ? $item['title_vn'] : $item['title'],MB_CASE_TITLE,'UTF-8');
                $data['mainFooter']['col-2']['item'][] = array(
                    'text' => str_replace("Cntt","CNTT",$text),
                    'link' => base_url() . 'business/' . $item['alias']
                );
            }
        }

        $project = $CI->project_model->get_data_projects_display();
        $data['mainFooter']['col-3'] = array(
            'text' => 'Dự án',
            'item' => array()
        );
        foreach ($project AS $index => $item){
            $data['mainFooter']['col-3']['item'][] = array(
                'text' => ucwords(mb_strtolower($currentLanguage == 'vietnamese' ?  $item['title_vn'] : $item['title'])),
                'link' => base_url() . 'project/'.$item['alias']
            );
        }

        $data['mainFooter']['col-4'] = array(
            'text' => 'Tin Tức',
            'item' => array(
                array(
                    'text' => $data_new_setting['company'],
                    'link' => base_url() . 'more/company'
                ),
                array(
                    'text' => $data_new_setting['market'],
                    'link' => base_url() . 'more/market'
                ),
                array(
                    'text' => $data_new_setting['general'],
                    'link' => base_url() . 'more/general'
                ),
                array(
                    'text' => $data_new_setting['newspaper'],
                    'link' => base_url() . 'more/newspaper'
                )
            )
        );
//        $data['mainFooter'] = array(
//            'connecting' => array(
//                'title' => 'Kết nối với chúng tôi',
//                'facebook' => 'ss',
//                'youtube' => 'ss',
//                'googlePlus' => 'ss',
//                'totalView' => array(
//                    'text' => 'Tổng lượt truy cập',
//                    'number' => number_format(56667),
//                    'unit' => 'lượt'
//                ),
//                'weekView' => array(
//                    'text' => 'Lượt truy cập trong tuần',
//                    'number' => number_format(566),
//                    'unit' => 'lượt'
//                ),
//            ),
//            'copyright' => array(
//                'pre-name' => 'Ủy ban Nhân dân TP.Hồ Chí Minh',
//                'name' => 'Tổng Công ty Công Nghiệp Sài Gòn - Trách nhiệm hữu hạn Một Thành Viên'
//            ),
//            'career-job' => array(
//                'text' => '',
//                'link' => ''
//            ),
//            'customer-contact' => array(
//                'text' => '',
//                'link' => ''
//            ),
//        );
        return $data;
    }

    public function homePageInfo()
    {
        $data = self::mainInfo();
        $data['mainBanner'] = array();
        $CI =& get_instance();
        $CI->load->model('main_model');
        $CI->load->model('news_model');
        $CI->load->model('business_model');
        $CI->load->library('utils');
        $currentLanguage = $CI->session->userdata('language');
        $list_category = $CI->news_model->get_list_category_display();
        $list_category = $CI->utils->getFieldDataArray($list_category, 'id');

        #region BANNER
        $mainBanner = $CI->main_model->get_data_banner_display();
        if ($mainBanner != false) {
            for ($i = 0; $i < count($mainBanner); $i++) {
                if ($currentLanguage == 'vietnamese') {
                    $title = $mainBanner[$i]['title_vn'];
                    $description = $mainBanner[$i]['description_vn'];
                } else {
                    $title = $mainBanner[$i]['title'];
                    $description = $mainBanner[$i]['description'];
                }
                $data['mainBanner'][] = array(
                    'image' => $this->domain . $mainBanner[$i]['image'],
                    'link' => $mainBanner[$i]['link_news'],
                    'title' => $title,
                    'description' => $description
                );
            }
        }
        #endregion

        #region BUSINESS
        $data['business'] = array();
        $mainBusiness = $CI->business_model->get_data_business_limit(6);
        if ($mainBusiness != false) {
            if ($currentLanguage == 'vietnamese') {
                $data['business']['title'] = $this->mainInfoData['text_main_business_vn'];
            } else {
                $data['business']['title'] = $this->mainInfoData['text_main_business'];
            }
            for ($i = 0; $i < count($mainBusiness); $i++) {
                if ($currentLanguage == 'vietnamese') {
                    $title = $mainBusiness[$i]['title_vn'];
                } else {
                    $title = $mainBusiness[$i]['title'];
                }
                $data['business']['item'][] = array(
                    'image' => $this->domain . $mainBusiness[$i]['image'],
                    'link' => ($mainBusiness[$i]['position_order'] != -1 ? 'business/' . $mainBusiness[$i]['alias'] : "javascript:void(0)"),
                    'text' => $title,
                    'position'=>$mainBusiness[$i]['position_order']
                );
            }
            usort($data['business']['item'], function ($a, $b) { return strnatcmp($a['position'], $b['position']); });
        }
        #endregion

        $data['market'] = array(
            'display' => false,
            'title' => '',
            'main' => array(),
            'extra' => array(),
            'moreText' => ''
        );
        $data['typical'] = array(
            'display' => false,
            'title' => '',
            'main' => array(),
            'extra' => array(),
            'moreText' => ''
        );

        if ($list_category['market']['status'] == 'active') {
            $market = $CI->news_model->get_data_market_news_display();
            if ($market != false) {
                $data['market']['title'] = $currentLanguage == 'vietnamese' ? $list_category['market']['title_vn'] : $list_category['market']['title'];
                $data['market']['display'] = true;
                $data['market']['moreText'] = $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more';
                $data['market']['main'] = array(
                    'link' => $this->domain . 'news-media/' . $market[0]['id'],
                    'image' => $this->domain . $market[0]['image'],
                    'title' => $currentLanguage == 'vietnamese' ? $market[0]['title_vn'] : $market[0]['title'],
                    'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($market[0]['date_display']) : date('d M Y', strtotime($market[0]['date_display'])),
                    'content' => $currentLanguage == 'vietnamese' ? $market[0]['description_vn'] : $market[0]['description_vn'],
                );
                if (count($market) > 1) {
                    for ($i = 1; $i < count($market) && $i < 4; $i++) {
                        $data['market']['extra'][] = array(
                            'link' => $this->domain . 'news-media/' . $market[$i]['id'],
                            'title' => $currentLanguage == 'vietnamese' ? $market[$i]['title_vn'] : $market[$i]['title'],
                            'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($market[$i]['date_display']) : date('d M Y', strtotime($market[$i]['date_display'])),
                            'content' => $currentLanguage == 'vietnamese' ? $market[$i]['description_vn'] : $market[$i]['description_vn'],
                        );
                    }
                }
            }
        }

        if ($list_category['highlights']['status'] == 'active') {
            $highlights = $CI->news_model->get_data_highlights_news_display();
            if ($highlights != false) {
                $data['typical']['display'] = true;
                $data['typical']['title'] = $currentLanguage == 'vietnamese' ? $list_category['highlights']['title_vn'] : $list_category['highlights']['title'];
                $data['typical']['moreText'] = $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more';
                $data['typical']['main'] = array(
                    'link' => $this->domain . 'news-media/' . $highlights[0]['id'],
                    'image' => $this->domain . $highlights[0]['image'],
                    'title' => $currentLanguage == 'vietnamese' ? $highlights[0]['title_vn'] : $highlights[0]['title'],
                    'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($highlights[0]['date_display']) : date('d M Y', strtotime($highlights[0]['date_display'])),
                    'content' => $currentLanguage == 'vietnamese' ? $highlights[0]['description_vn'] : $highlights[0]['description_vn'],
                );
                if (count($highlights) > 1) {
                    for ($i = 1; $i < count($highlights) && $i < 4; $i++) {
                        $data['typical']['extra'][] = array(
                            'link' => $this->domain . 'news-media/' . $highlights[$i]['id'],
                            'title' => $currentLanguage == 'vietnamese' ? $highlights[$i]['title_vn'] : $highlights[$i]['title'],
                            'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($highlights[$i]['date_display']) : date('d M Y', strtotime($highlights[$i]['date_display'])),
                            'content' => $currentLanguage == 'vietnamese' ? $highlights[$i]['description_vn'] : $highlights[$i]['description_vn'],
                        );
                    }
                }
            }
        }
        return $data;
    }

    ////////////////////// ABOUT ////////////////////
    public function aboutMenuInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('main_model');
        $currentLanguage = $CI->session->userdata('language');
        $menuData = $CI->main_model->get_data_introduce_display();
        if ($menuData != false) {
            $data['banner'] = array(
                'image' => base_url() . 'assets/images/upload/banner-about.jpg',
                'title' => 'Thông tin doanh nghiệp',
                'description' => 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên',
            );
            for ($i = 0; $i < count($menuData); $i++) {
                if ($currentLanguage == 'vietnamese') {
                    $title = $menuData[$i]['title_vn'];
                    $description = $menuData[$i]['description_vn'] != '' ? $menuData[$i]['description_vn'] : '';
                } else {
                    $title = $menuData[$i]['title'];
                    $description = $menuData[$i]['description'] != '' ? $menuData[$i]['description'] : '';
                }
                $data['menu'][$i] = array(
                    'text' => $title,
                    'link' => $this->domain . 'introduce/' . $menuData[$i]['alias'],
                    'active' => false,
                    'type' => $menuData[$i]['type'] != '' ? $menuData[$i]['type'] : 'about-us'
                );
                if ($page == $menuData[$i]['alias']) {
                    $data['menu'][$i]['active'] = true;
                    $data['currentType'] = $menuData[$i]['type'] != '' ? $menuData[$i]['type'] : 'about-us';
                    $data['banner'] = array(
                        'image' => $this->domain . $menuData[$i]['image'],
                        'title' => $title,
                        'description' => $description != '' ? $description : 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên'
                    );
                }
            }
        }
        return $data;
    }

    public function aboutUsInfo($data)
    {

        $CI =& get_instance();
        $CI->load->model('introduce_model');
        $currentLanguage = $CI->session->userdata('language');
        //ABOUT
        $dataAbout = $CI->introduce_model->get_data_about_display();
        if (count($dataAbout) > 0 && $dataAbout != false) {
            $data['content']['about']['title'] = $currentLanguage == 'vietnamese' ? $dataAbout['title_vn'] : $dataAbout['title'];
            $data['content']['about']['content'] = $currentLanguage == 'vietnamese' ? $dataAbout['content_vn'] : $dataAbout['content'];
        }
        //ABOUT CORE
        $dataAboutCore = $CI->introduce_model->get_data_about_core_display();
        if (count($dataAboutCore) > 0 && $dataAboutCore != false) {
            $data['content']['about-core']['title'] = $currentLanguage == 'vietnamese' ? $dataAboutCore['title_vn'] : $dataAboutCore['title'];
            if (count($dataAboutCore['articles']) > 0) {
                for ($i = 0; $i < count($dataAboutCore['articles']); $i++) {
                    $data['content']['about-core']['article-item'][$i]['title'] = $currentLanguage == 'vietnamese' ? $dataAboutCore['articles'][$i]['title_vn'] : $dataAboutCore['articles'][$i]['title'];
                    $data['content']['about-core']['article-item'][$i]['info'] = $currentLanguage == 'vietnamese' ? $dataAboutCore['articles'][$i]['content_vn'] : $dataAboutCore['articles'][$i]['content'];
                }
                $data['content']['about-core']['banner_image'] = '../'.$dataAboutCore['banner_image'];
            }
        }
        //SOCIAL
        $dataAboutSocial = $CI->introduce_model->get_data_about_social_display();
        if (count($dataAboutSocial) > 0 && $dataAboutSocial != false) {
            $data['content']['social-responsibility']['title'] = $currentLanguage == 'vietnamese' ? $dataAboutSocial['title_vn'] : $dataAboutSocial['title'];
            $data['content']['social-responsibility']['content'] = $currentLanguage == 'vietnamese' ? $dataAboutSocial['content_vn'] : $dataAboutSocial['content'];
            $data['content']['social-responsibility']['banner_image'] = '../'.$dataAboutSocial['banner_image'];
        }
        //OUR-NETWORK
        $dataAboutNetwork = $CI->introduce_model->get_data_about_our_network_display();
        if (count($dataAboutNetwork) && $dataAboutNetwork != false) {
            for ($i = 0; $i < count($dataAboutNetwork); $i++) {
                $data['content']['our-network'][] = array(
                    'title' => $currentLanguage == 'vietnamese' ? $dataAboutNetwork[$i]['title_vn'] : $dataAboutNetwork[$i]['title'],
                    'content' => $currentLanguage == 'vietnamese' ? $dataAboutNetwork[$i]['content_vn'] : $dataAboutNetwork[$i]['content'],
                    'item-1' => array(
                        'image' => $this->domain . $dataAboutNetwork[$i]['image_1'],
                        'number' => number_format($dataAboutNetwork[$i]['number_1']),
                        'text' => $currentLanguage == 'vietnamese' ? $dataAboutNetwork[$i]['text_1_vn'] : $dataAboutNetwork[$i]['text_1'],
                        'unit'=>$dataAboutNetwork[$i]['unit_2']
                    ),
                    'item-2' => array(
                        'image' => $this->domain . $dataAboutNetwork[$i]['image_2'],
                        'number' => number_format($dataAboutNetwork[$i]['number_2']),
                        'text' => $currentLanguage == 'vietnamese' ? $dataAboutNetwork[$i]['text_2_vn'] : $dataAboutNetwork[$i]['text_2'],
                        'unit'=>$dataAboutNetwork[$i]['unit']
                    )
                );
            }
        }
        //SYSTEM-MANAGEMENT
        $dataAboutSystem = $CI->introduce_model->get_data_about_system_management_display();
        if (count($dataAboutSystem) > 0 && $dataAboutSystem != false) {
            $data['content']['system-management']['title'] = $currentLanguage == 'vietnamese' ? $dataAboutSystem['title_vn'] : $dataAboutSystem['title'];
            $data['content']['system-management']['content'] = $currentLanguage == 'vietnamese' ? $dataAboutSystem['content_vn'] : $dataAboutSystem['content'];
            $data['content']['system-management']['banner_image'] = '../'.$dataAboutSystem['banner_image'];
                if (count($dataAboutSystem['articles']) > 0) {
                    for ($i = 0; $i < count($dataAboutSystem['articles']); $i++) {
                        $data['content']['system-management']['items'][$i]['image'] = $this->domain . $dataAboutSystem['articles'][$i]['image'];
                        $data['content']['system-management']['items'][$i]['title'] = $currentLanguage == 'vietnamese' ? $dataAboutSystem['articles'][$i]['title_vn'] : $dataAboutSystem['articles'][$i]['title'];
                        $data['content']['system-management']['items'][$i]['content'] = $currentLanguage == 'vietnamese' ? $dataAboutSystem['articles'][$i]['content_vn'] : $dataAboutSystem['articles'][$i]['content'];
                    }
                }
        }
        //AWARDS
        $dataAboutAwards = $CI->introduce_model->get_data_about_awards_display();
        if (count($dataAboutAwards) > 0 && $dataAboutAwards != false) {
            $data['content']['awards']['title'] = $currentLanguage == 'vietnamese' ? $dataAboutAwards['title_vn'] : $dataAboutAwards['title'];
            if (count($dataAboutAwards['articles']) > 0) {
                for ($i = 0; $i < count($dataAboutAwards['articles']); $i++) {
                    $data['content']['awards']['items'][$i]['image'] = $this->domain . $dataAboutAwards['articles'][$i]['image'];
                    $data['content']['awards']['items'][$i]['title'] = $currentLanguage == 'vietnamese' ? $dataAboutAwards['articles'][$i]['title_vn'] : $dataAboutAwards['articles'][$i]['title'];
                    $data['content']['awards']['items'][$i]['content'] = $currentLanguage == 'vietnamese' ? $dataAboutAwards['articles'][$i]['content_vn'] : $dataAboutAwards['articles'][$i]['content'];
                }
            }
        }
        return $data;
    }

    public function aboutNewsInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('introduce_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataIntroduce = $CI->introduce_model->get_data_news_display($page);
        $data['content'] = array('news' => array(),'title'=>'');
        if (count($dataIntroduce) > 0) {
            for($i = 0 ; $i < count($dataIntroduce) ; $i ++){
//                $data['content']['news'][]['title'] = $currentLanguage == 'vietnamese' ? $dataIntroduce['title_vn'] : $dataIntroduce['title'];
                $data['content']['news'][] = array(
                    'title' => $currentLanguage == 'vietnamese' ? $dataIntroduce[$i]['title_vn'] : $dataIntroduce[$i]['title'],
                    'content' => $currentLanguage == 'vietnamese' ? $dataIntroduce[$i]['content_vn'] : $dataIntroduce[$i]['content'],
                    'image'=>$this->domain .$dataIntroduce[$i]['image']
                );
                $data['content']['title'] = $currentLanguage == 'vietnamese' ? $dataIntroduce[$i]['introduce_title_vn'] : $dataIntroduce[$i]['introduce_title'];
            }
        }
        return $data;
    }
    public function aboutRevenue($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('introduce_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataIntroduce = $CI->introduce_model->get_data_revenue_display($page);
        $data['content'] = array('news' => array(),'title'=>'');
        if (count($dataIntroduce) > 0) {
            for($i = 0 ; $i < count($dataIntroduce) ; $i ++){
                $data['content']['news'][]['title'] = $currentLanguage == 'vietnamese' ? $dataIntroduce['title_vn'] : $dataIntroduce['title'];
                $data['content']['news'][] = array(
                    'title' => $currentLanguage == 'vietnamese' ? $dataIntroduce[$i]['title_vn'] : $dataIntroduce[$i]['title'],
                    'content' => $currentLanguage == 'vietnamese' ? $dataIntroduce[$i]['content_vn'] : $dataIntroduce[$i]['content'],
                    'image'=>$this->domain .$dataIntroduce[$i]['image']
                );
            }
        }
        return $data;
    }

    public function aboutModelInfo($data)
    {
        $CI =& get_instance();
        $CI->load->model('introduce_model');
        $currentLanguage = $CI->session->userdata('language');
        //CULTURE
        $dataCulture = $CI->introduce_model->get_data_model_culture_display();
        if (count($dataCulture) > 0 && $dataCulture != false) {
            $data['content']['culture']['title'] = $currentLanguage == 'vietnamese' ? $dataCulture['title_vn'] : $dataCulture['title'];
            $data['content']['culture']['content'] = $currentLanguage == 'vietnamese' ? $dataCulture['content_vn'] : $dataCulture['content'];
            if (count($dataCulture['member']) > 0) {
                for ($i = 0; $i < count($dataCulture['member']); $i++) {
                    $data['content']['culture']['member'][$i]['image'] = $this->domain . $dataCulture['member'][$i]['image'];
                    $data['content']['culture']['member'][$i]['name'] = $currentLanguage == 'vietnamese' ? $dataCulture['member'][$i]['name_vn'] : $dataCulture['member'][$i]['name'];
                    $data['content']['culture']['member'][$i]['position'] = $currentLanguage == 'vietnamese' ? $dataCulture['member'][$i]['position_vn'] : $dataCulture['member'][$i]['position'];
                    $data['content']['culture']['member'][$i]['info'] = $currentLanguage == 'vietnamese' ? $dataCulture['member'][$i]['info_vn'] : $dataCulture['member'][$i]['info'];
                }
            }
        }

        $dataStories = $CI->introduce_model->get_data_model_stories_display();
        if (count($dataStories) > 0 && $dataStories != false) {
            $data['content']['stories']['title'] = $currentLanguage == 'vietnamese' ? $dataStories['title_vn'] : $dataStories['title'];
            if (count($dataStories['member']) > 0) {
                for ($i = 0; $i < count($dataStories['member']); $i++) {
                    $data['content']['stories']['member'][$i]['image'] = $this->domain . $dataStories['member'][$i]['image'];
                    $data['content']['stories']['member'][$i]['name'] = $currentLanguage == 'vietnamese' ? $dataStories['member'][$i]['name_vn'] : $dataStories['member'][$i]['name'];
                    $data['content']['stories']['member'][$i]['position'] = $currentLanguage == 'vietnamese' ? $dataStories['member'][$i]['position_vn'] : $dataStories['member'][$i]['position'];
                    $data['content']['stories']['member'][$i]['info'] = $currentLanguage == 'vietnamese' ? $dataStories['member'][$i]['info_vn'] : $dataStories['member'][$i]['info'];
                }
            }
        }

		//CULTURE MEMBER
		$dataCulture = $CI->introduce_model->get_data_member_culture_display();
		if (count($dataCulture) > 0 && $dataCulture != false) {
			$data['content']['culture_member']['title'] = $currentLanguage == 'vietnamese' ? $dataCulture['title_vn'] : $dataCulture['title'];
			$data['content']['culture_member']['content'] = $currentLanguage == 'vietnamese' ? $dataCulture['content_vn'] : $dataCulture['content'];
			if (count($dataCulture['member']) > 0) {
				for ($i = 0; $i < count($dataCulture['member']); $i++) {
					$data['content']['culture_member']['member'][$i]['image'] = $this->domain . $dataCulture['member'][$i]['image'];
					$data['content']['culture_member']['member'][$i]['name'] = $currentLanguage == 'vietnamese' ? $dataCulture['member'][$i]['name_vn'] : $dataCulture['member'][$i]['name'];
					$data['content']['culture_member']['member'][$i]['position'] = $currentLanguage == 'vietnamese' ? $dataCulture['member'][$i]['position_vn'] : $dataCulture['member'][$i]['position'];
					$data['content']['culture_member']['member'][$i]['info'] = $currentLanguage == 'vietnamese' ? $dataCulture['member'][$i]['info_vn'] : $dataCulture['member'][$i]['info'];
				}
			}
		}

		$dataStories = $CI->introduce_model->get_data_member_stories_display();
		if (count($dataStories) > 0 && $dataStories != false) {
			$data['content']['stories_member']['title'] = $currentLanguage == 'vietnamese' ? $dataStories['title_vn'] : $dataStories['title'];
			$data['content']['stories_member']['content'] = $currentLanguage == 'vietnamese' ? $dataStories['content_vn'] : $dataStories['content'];
		}
        return $data;
    }

    public function aboutMemberInfo($data)
    {
        $CI =& get_instance();
        $CI->load->model('introduce_model');
        $currentLanguage = $CI->session->userdata('language');
        //CULTURE
        $dataCulture = $CI->introduce_model->get_data_member_culture_display();
        if (count($dataCulture) > 0 && $dataCulture != false) {
            $data['content']['culture']['title'] = $currentLanguage == 'vietnamese' ? $dataCulture['title_vn'] : $dataCulture['title'];
            $data['content']['culture']['content'] = $currentLanguage == 'vietnamese' ? $dataCulture['content_vn'] : $dataCulture['content'];
            if (count($dataCulture['member']) > 0) {
                for ($i = 0; $i < count($dataCulture['member']); $i++) {
                    $data['content']['culture']['member'][$i]['image'] = $this->domain . $dataCulture['member'][$i]['image'];
                    $data['content']['culture']['member'][$i]['name'] = $currentLanguage == 'vietnamese' ? $dataCulture['member'][$i]['name_vn'] : $dataCulture['member'][$i]['name'];
                    $data['content']['culture']['member'][$i]['position'] = $currentLanguage == 'vietnamese' ? $dataCulture['member'][$i]['position_vn'] : $dataCulture['member'][$i]['position'];
                    $data['content']['culture']['member'][$i]['info'] = $currentLanguage == 'vietnamese' ? $dataCulture['member'][$i]['info_vn'] : $dataCulture['member'][$i]['info'];
                }
            }
        }

        $dataStories = $CI->introduce_model->get_data_member_stories_display();
        if (count($dataStories) > 0 && $dataStories != false) {
            $data['content']['stories']['title'] = $currentLanguage == 'vietnamese' ? $dataStories['title_vn'] : $dataStories['title'];
            if (count($dataStories['member']) > 0) {
                for ($i = 0; $i < count($dataStories['member']); $i++) {
                    $data['content']['stories']['member'][$i]['image'] = $this->domain . $dataStories['member'][$i]['image'];
                    $data['content']['stories']['member'][$i]['name'] = $currentLanguage == 'vietnamese' ? $dataStories['member'][$i]['name_vn'] : $dataStories['member'][$i]['name'];
                    $data['content']['stories']['member'][$i]['position'] = $currentLanguage == 'vietnamese' ? $dataStories['member'][$i]['position_vn'] : $dataStories['member'][$i]['position'];
                    $data['content']['stories']['member'][$i]['info'] = $currentLanguage == 'vietnamese' ? $dataStories['member'][$i]['info_vn'] : $dataStories['member'][$i]['info'];
                }
            }
        }
        return $data;
    }
    ///////////////////// END ABOUT ////////////////////

    //////////////////// BUSINESS /////////////////////
    public function businessMenuInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('business_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataBusiness = $CI->business_model->get_data_business_limit();

        if (count($dataBusiness) > 0 && $dataBusiness != false) {
            $data['banner'] = array(
                'image' => $this->domain . 'assets/images/upload/business-solution.jpg',
                'title' => ''
            );
            for ($i = 0; $i < count($dataBusiness); $i++) {
                $data['menu'][$i]['text'] = $currentLanguage == 'vietnamese' ? $dataBusiness[$i]['title_vn'] : $dataBusiness[$i]['title'];
                $data['menu'][$i]['link'] = $this->domain . 'business/' . $dataBusiness[$i]['alias'];
                if ($dataBusiness[$i]['alias'] == $page) {
                    $data['menu'][$i]['active'] = true;
                    $data['banner'] = array(
                        'image' => $this->domain . $dataBusiness[$i]['banner_image'],
                        'title' => $currentLanguage == 'vietnamese' ? $dataBusiness[$i]['title_vn'] : $dataBusiness[$i]['title']
                    );
                } else {
                    $data['menu'][$i]['active'] = false;
                }
            }
        }
        return $data;
    }

    public function businessContentInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('business_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataBusinessContent = $CI->business_model->get_data_business_content_display($page);

        if (count($dataBusinessContent) > 0 && $dataBusinessContent != false) {
            $data['content']['title'] = $currentLanguage == 'vietnamese' ? $dataBusinessContent['title_vn'] : $dataBusinessContent['title'];
            $data['content']['content'] = $currentLanguage == 'vietnamese' ? $dataBusinessContent['content_vn'] : $dataBusinessContent['content'];
        }

        return $data;
    }

    /////////////////// END BUSINESS //////////////////


    //////////////////// PROJECT //////////////////////
    public function projectMenuInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('project_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataProject = $CI->project_model->get_data_projects_display();

        if (count($dataProject) > 0 && $dataProject != false) {
            for ($i = 0; $i < count($dataProject); $i++) {
                if(!array_key_exists('banner',$data)) {
                    $data['banner'] = array(
                        'image' => $this->domain . 'assets/images/upload/business-solution.jpg',
                        'title' => 'dự án',
                        'description' => 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên'
                    );
                }
                $data['menu'][$i]['text'] = $currentLanguage == 'vietnamese' ? $dataProject[$i]['title_vn'] : $dataProject[$i]['title'];
                $data['menu'][$i]['link'] = $this->domain . 'project/' . $dataProject[$i]['alias'];
                if ($dataProject[$i]['alias'] == $page) {
                    $data['menu'][$i]['active'] = true;
                    $data['banner'] = array(
                        'image' => $this->domain . $dataProject[$i]['banner_image'],
                        'title' => $currentLanguage == 'vietnamese' ? $dataProject[$i]['title_vn'] : $dataProject[$i]['title'],
                        'description' => $currentLanguage == 'vietnamese' ? $dataProject[$i]['description_vn'] : $dataProject[$i]['description']
                    );
                } else {
                    $data['menu'][$i]['active'] = false;
                }
            }
        }

        return $data;
    }

    public function projectDetailMenuInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('project_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataProject = $CI->project_model->get_data_projects_display();
        $dataPage = $CI->project_model->get_data_project_menu_detail_display($page);

        if (count($dataProject) > 0 && $dataProject != false) {
            for ($i = 0; $i < count($dataProject); $i++) {
                if(!array_key_exists('banner',$data)) {
                    $data['banner'] = array(
//                    'image' => $this->domain . 'assets/images/upload/business-solution.jpg',
                        'image' => $this->domain . $dataProject[$i]['banner_image'],
                        'title' => 'dự án',
                        'description' => 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên'
                    );
                }
                $data['menu'][$i]['text'] = $currentLanguage == 'vietnamese' ? $dataProject[$i]['title_vn'] : $dataProject[$i]['title'];
                $data['menu'][$i]['link'] = $this->domain . 'project/' . $dataProject[$i]['alias'];
                if ($dataProject[$i]['alias'] == $dataPage['alias']) {
                    $data['menu'][$i]['active'] = true;
                    $data['banner'] = array(
                        'image' => $this->domain . $dataProject[$i]['banner_image'],
                        'title' => $currentLanguage == 'vietnamese' ? $dataProject[$i]['title_vn'] : $dataProject[$i]['title'],
                        'description' => $currentLanguage == 'vietnamese' ? $dataProject[$i]['description_vn'] : $dataProject[$i]['description']
                    );
                } else {
                    $data['menu'][$i]['active'] = false;
                }
            }
        }

        return $data;
    }

    public function projectContentInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('project_model');
        $CI->load->model('project_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataProjectList = $CI->project_model->get_data_project_list_display($page);
        if ($currentLanguage == 'vietnamese') {
            $CI->lang->load('vietnamese', 'vietnamese');
        } else {
            $CI->lang->load('english', 'english');
        }
        $language = $CI->lang->line('language');

        if (count($dataProjectList) > 0 && $dataProjectList != false) {
            $data['content']['title'] = $currentLanguage == 'vietnamese' ? $dataProjectList[0]['project_title_vn'] : $dataProjectList[0]['project_title'];
            for ($i = 0; $i < count($dataProjectList); $i++) {
                $data['content']['items'][$i] = array(
                    'day' => date('d', strtotime($dataProjectList[$i]['date_display'])),
                    'month' => date('m', strtotime($dataProjectList[$i]['date_display'])),
                    'year' => date('Y', strtotime($dataProjectList[$i]['date_display'])),
                    'image' => $this->domain . $dataProjectList[$i]['image'],
                    'name' => $currentLanguage == 'vietnamese' ? $dataProjectList[$i]['title_vn'] : $dataProjectList[$i]['title'],
                    'link' => $this->domain . 'project/detail/' . $dataProjectList[$i]['id'],
                    'text_button' => $language['button_view_detail']
                );
            }
        }
        return $data;
    }

    public function projectDetailContentInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('project_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataProjectDetail = $CI->project_model->get_data_project_detail_display($page);
        if (count($dataProjectDetail) > 0 && $dataProjectDetail != false) {
            $data['content']['title'] = $currentLanguage == 'vietnamese' ? $dataProjectDetail['title_vn'] : $dataProjectDetail['title'];
            $data['content']['content'] = $currentLanguage == 'vietnamese' ? $dataProjectDetail['content_vn'] : $dataProjectDetail['content'];
        }
        return $data;
    }

    /////////////////// END PROJECT ///////////////////


    //////////////////// PARTNER /////////////////////
    public function partnerMenuInfo($data)
    {

        $CI =& get_instance();
        $CI->load->model('partner_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataPartner = $CI->partner_model->get_data_partner_banner_display();

        if (count($dataPartner) > 0 && $dataPartner != false) {
            $data['banner']['image'] = $this->domain . $dataPartner['image'];
            $data['banner']['title'] = $currentLanguage == 'vietnamese' ? $dataPartner['title_vn'] : $dataPartner['title'];
            $data['banner']['description'] = $currentLanguage == 'vietnamese' ? $dataPartner['description_vn'] : $dataPartner['description'];
        }
        return $data;
    }

    public function partnerContentInfo($data)
    {
        $CI =& get_instance();
        $CI->load->model('partner_model');

        $data['content'] = array(
            'global-partnership' => array(
                'title' => '',
                'image' => '',
                'content' => ''
            ),
            'customer' => array(
                'title' => 'Khách hàng',
                'items' => array()
            ),
            'partner' => array(
                'title' => 'Đối tác',
                'items' => array()
            )
        );
        $partnerCustomer = $CI->partner_model->get_data_partner_customer_all('','active','approved_2');
        for ($i = 0; $i < count($partnerCustomer); $i++) {
            if($partnerCustomer[$i]['image'] != '' && $partnerCustomer[$i]['image'] != null){
                $data['content']['customer']['items'][] = $partnerCustomer[$i]['image'];
            }
        }


        $partnerPartner = $CI->partner_model->get_data_partner_partner_all('','active','approved_2');
        for ($i = 0; $i < count($partnerPartner); $i++) {
            if($partnerPartner[$i]['image'] != '' && $partnerPartner[$i]['image'] != null){
                $data['content']['partner']['items'][] = $partnerPartner[$i]['image'];
            }
        }

        $CI =& get_instance();
        $CI->load->model('partner_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataPartnerGlobal = $CI->partner_model->get_data_partner_global_display();

        if (count($dataPartnerGlobal) > 0 && $dataPartnerGlobal != false) {
            $data['content']['global-partnership']['title'] = $currentLanguage == 'vietnamese' ? $dataPartnerGlobal['title_vn'] : $dataPartnerGlobal['title'];
            $data['content']['global-partnership']['image'] = $dataPartnerGlobal['image'];
            $data['content']['global-partnership']['content'] = $currentLanguage == 'vietnamese' ? $dataPartnerGlobal['content_vn'] : $dataPartnerGlobal['content'];
        }


        return $data;
    }

    /////////////////// END PARTNER //////////////////


    public function newsMenuInfo($data)
    {
        $data['banner'] = array(
            'image' => base_url() . 'assets/images/upload/theway-bg.jpg',
            'title' => 'Tin tức',
            'description' => 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên'
        );
        return $data;
    }

    public function newsMenuDetailInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('news_model');
        $dataPage = $CI->news_model->get_data_news_with_id($page);
        $data['banner'] = array(
            'image' => $this->domain . $dataPage['banner_image'],
            'title' => 'Tin tức',
            'description' => 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên'
        );
        return $data;
    }

    public function newsContentInfo($data)
    {

        $CI =& get_instance();
        $CI->load->model('news_model');
        $CI->load->library('utils');
        $currentLanguage = $CI->session->userdata('language');
        $list_category = $CI->news_model->get_list_category_display();
        $list_category = $CI->utils->getFieldDataArray($list_category, 'id');

        $data['content'] = array(
            'new-update' => array(
                'link' => '',
                'title' => '',
                'slogan' => ''
            ),
            'news-company' => array(
                'title' => '',
                'thumbnail' => array(),
                'list-news' => array(),
                'text-more' => $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more',
            ),
            'news-market' => array(
                'title' => '',
                'thumbnail' => array(),
                'list-news' => array(),
                'text-more' => $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more',
            ),
            'news-general' => array(
                'title' => '',
                'thumbnail' => array(),
                'list-news' => array(),
                'text-more' => $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more',
            ),
            'newspaper' => array(
                'title' => '',
                'list-news' => array(),
                'text-more' => $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more',
            ),
            'introduce' => array(
                'title' => '',
                'video-url' => '',
                'list-news' => array(),
            ),
            'download-information' => array(
                'title' => '',
                'list-news' => array(),
                'text-more' => $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more',
            )
        );

        if ($list_category != false) {
            /*NEWS UPDATE*/
            if ($list_category['news_update']['status'] == 'active') {
                $latest_news = $CI->news_model->get_data_latest_news_display();
                if ($latest_news != false) {
                    $data['content']['new-update']['title'] = $currentLanguage == 'vietnamese' ? $latest_news['title_vn'] : $latest_news['title'];
                    $data['content']['new-update']['link'] = $this->domain . 'news-media/' . $latest_news['id'];
                    $data['content']['new-update']['slogan'] = $currentLanguage == 'vietnamese' ? 'Tin tức mới nhất' : 'News update';
                }
            }
            /*COMPANY*/
            if ($list_category['company']['status'] == 'active') {
                $company = $CI->news_model->get_data_company_news_display();
                if ($company != false) {
                    $data['content']['news-company']['title'] = $currentLanguage == 'vietnamese' ? $list_category['company']['title_vn'] : $list_category['company']['title'];
                    $data['content']['news-company']['thumbnail'] = array(
                        'link' => $this->domain . 'news-media/' . $company[0]['id'],
                        'image' => $this->domain . $company[0]['image'],
                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($company[0]['date_display']) : date('d M Y', strtotime($company[0]['date_display'])),
                        'title' => $currentLanguage == 'vietnamese' ? $company[0]['title_vn'] : $company[0]['title'],
                        'info' => $currentLanguage == 'vietnamese' ? $company[0]['description_vn'] : $company[0]['description_vn'],
                    );
                    if (count($company) > 1) {
                        for ($i = 1; $i < count($company); $i++) {
                            $data['content']['news-company']['list-news'][] = array(
                                'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($company[$i]['date_display']) : date('d M Y', strtotime($company[$i]['date_display'])),
                                'link' => $this->domain . 'news-media/' . $company[$i]['id'],
                                'title' => $currentLanguage == 'vietnamese' ? $company[$i]['title_vn'] : $company[$i]['title']
                            );
                        }
                    } else {
                        $data['content']['news-company']['list-news'] = array();
                    }
                }
            }
            /*MARKET*/
            if ($list_category['market']['status'] == 'active') {
                $market = $CI->news_model->get_data_market_news_display();
                if ($market != false) {
                    $data['content']['news-market']['title'] = $currentLanguage == 'vietnamese' ? $list_category['market']['title_vn'] : $list_category['market']['title'];
                    $data['content']['news-market']['thumbnail'] = array(
                        'link' => $this->domain . 'news-media/' . $market[0]['id'],
                        'image' => $this->domain . $market[0]['image'],
                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($market[0]['date_display']) : date('d M Y', strtotime($market[0]['date_display'])),
                        'title' => $currentLanguage == 'vietnamese' ? $market[0]['title_vn'] : $market[0]['title'],
                        'info' => $currentLanguage == 'vietnamese' ? $market[0]['description_vn'] : $market[0]['description_vn'],
                    );
                    if (count($market) > 1) {
                        for ($i = 1; $i < count($market); $i++) {
                            $data['content']['news-market']['list-news'][] = array(
                                'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($market[$i]['date_display']) : date('d M Y', strtotime($market[$i]['date_display'])),
                                'link' => $this->domain . 'news-media/' . $market[$i]['id'],
                                'title' => $currentLanguage == 'vietnamese' ? $market[$i]['title_vn'] : $market[$i]['title']
                            );
                        }
                    } else {
                        $data['content']['news-market']['list-news'] = array();
                    }
                }
            }
            /*GENERAL*/
            if ($list_category['general']['status'] == 'active') {
                $general = $CI->news_model->get_data_general_news_display();
                if ($general != false) {
                    $data['content']['news-general']['title'] = $currentLanguage == 'vietnamese' ? $list_category['general']['title_vn'] : $list_category['general']['title'];
                    $data['content']['news-general']['thumbnail'] = array(
                        'link' => $this->domain . 'news-media/' . $general[0]['id'],
                        'image' => $this->domain . $general[0]['image'],
                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($general[0]['date_display']) : date('d M Y', strtotime($general[0]['date_display'])),
                        'title' => $currentLanguage == 'vietnamese' ? $general[0]['title_vn'] : $general[0]['title'],
                        'info' => $currentLanguage == 'vietnamese' ? $general[0]['description_vn'] : $general[0]['description_vn'],
                    );
                    if (count($general) > 1) {
                        for ($i = 1; $i < count($general); $i++) {
                            $data['content']['news-general']['list-news'][] = array(
                                'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($general[$i]['date_display']) : date('d M Y', strtotime($general[$i]['date_display'])),
                                'link' => $this->domain . 'news-media/' . $general[$i]['id'],
                                'title' => $currentLanguage == 'vietnamese' ? $general[$i]['title_vn'] : $general[$i]['title']
                            );
                        }
                    } else {
                        $data['content']['news-general']['list-news'] = array();
                    }
                }
            }
            /*NEWSPAPER*/
            if ($list_category['newspaper']['status'] == 'active') {
                $newspaper = $CI->news_model->get_data_newspaper_news_display();
                if ($newspaper != false) {
                    $data['content']['newspaper']['title'] = $currentLanguage == 'vietnamese' ? $list_category['newspaper']['title_vn'] : $list_category['newspaper']['title'];
                    if (count($newspaper) > 0) {
                        for ($i = 0; $i < count($newspaper); $i++) {
                            $data['content']['newspaper']['list-news'][] = array(
                                'link' => $this->domain . 'news-media/' . $newspaper[$i]['id'],
                                'title' => $currentLanguage == 'vietnamese' ? $newspaper[$i]['title_vn'] : $newspaper[$i]['title']
                            );
                        }
                    } else {
                        $data['content']['newspaper']['list-news'] = array();
                    }
                }
            }
            /*INTRODUCE*/
            if ($list_category['introduce']['status'] == 'active') {
                $introduce = $CI->news_model->get_data_introduce_news_display();
                if ($introduce != false) {
                    $data['content']['introduce']['title'] = $currentLanguage == 'vietnamese' ? $list_category['introduce']['title_vn'] : $list_category['introduce']['title'];
                    $data['content']['introduce']['video-url'] = $currentLanguage == 'vietnamese' ? 'https://www.youtube.com/embed/' . $introduce[0]['youtube_url_vn'] : 'https://www.youtube.com/embed/' . $introduce[0]['youtube_url'];
                    if (count($introduce) > 1) {
                        for ($i = 1; $i < count($introduce); $i++) {
                            $data['content']['introduce']['list-news'][] = array(
                                'link' => $currentLanguage == 'vietnamese' ? 'https://www.youtube.com/watch?v=' . $introduce[$i]['youtube_url_vn'] : 'https://www.youtube.com/watch?v=' . $introduce[$i]['youtube_url'],
                                'title' => $currentLanguage == 'vietnamese' ? $introduce[$i]['title_vn'] : $introduce[$i]['title']
                            );
                        }
                    } else {
                        $data['content']['introduce']['list-news'] = array();
                    }
                }
            }
            /*DOWNLOAD*/
            if ($list_category['download-information']['status'] == 'active') {
                $download = $CI->news_model->get_data_download_news_display();
                if ($download != false) {
                    $data['content']['download-information']['title'] = $currentLanguage == 'vietnamese' ? $list_category['download-information']['title_vn'] : $list_category['download-information']['title'];
                    for ($i = 0; $i < count($download); $i++) {
                        $data['content']['download-information']['list-news'][] = array(
                            'link' => $download[$i][$download[$i]['option_display']] != null && $download[$i][$download[$i]['option_display']] != '' ? $download[$i][$download[$i]['option_display']] : ($download[$i]['link'] != null && $download[$i]['link'] != '' ? $download[$i]['link'] : $this->domain . $download[$i]['file']),
                            'title' => $currentLanguage == 'vietnamese' ? $download[$i]['title_vn'] : $download[$i]['title']
                        );
                    }
                }
            }
        }
        return $data;
    }
    public function readMoreContentInfo($data,$page,$paging)
    {

        $CI =& get_instance();
        $CI->load->model('news_model');
        $CI->load->library('utils');
        $currentLanguage = $CI->session->userdata('language');
        $list_category = $CI->news_model->get_list_category_display();
        $list_category = $CI->utils->getFieldDataArray($list_category, 'id');

        $data['list'] = array(
            'title' => '',
            'list' => array(),
        );
        $limit = $CI->config->item('paging_new');
        $offset = $paging * $CI->config->item('paging_new') -  $CI->config->item('paging_new');
        $data['limit'] = $limit;
        $data['page'] = $paging;
        if ($list_category != false) {
            switch ($page){
                case "company":
                    if ($list_category['company']['status'] == 'active') {
                        $company = $CI->news_model->get_data_company_news_no_limit_display($limit,$offset);
                        $totalCompany = $CI->news_model->get_data_company_news_count_display();
                        $data['total_page'] = $totalCompany;
                        if ($company != false) {
                            $data['list']['title'] = $currentLanguage == 'vietnamese' ? $list_category['company']['title_vn'] : $list_category['company']['title'];
                            if (count($company) > 1) {
                                for ($i = 0; $i < count($company); $i++) {
                                    $data['list']['list'][] = array(
                                        'link' => $this->domain . 'news-media/' . $company[$i]['id'],
                                        'image' => $this->domain . $company[$i]['image'],
                                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($company[$i]['date_display']) : date('d M Y', strtotime($company[$i]['date_display'])),
                                        'title' => $currentLanguage == 'vietnamese' ? $company[$i]['title_vn'] : $company[$i]['title'],
                                        'info' => $currentLanguage == 'vietnamese' ? $company[$i]['description_vn'] : $company[$i]['description_vn'],
                                    );
                                }
                            } else {
                                $data['list']['list'] = array();
                            }
                        }
                    }
                    break;
                case "market":
                    if ($list_category['market']['status'] == 'active') {
                        $market = $CI->news_model->get_data_market_news_no_limit_display($limit,$offset);
                        $totalMarket = $CI->news_model->get_data_market_news_count_display();
                        $data['total_page'] = $totalMarket;
                        if ($market != false) {
                            $data['list']['title'] = $currentLanguage == 'vietnamese' ? $list_category['market']['title_vn'] : $list_category['market']['title'];
                            if (count($market) > 1) {
                                for ($i = 1; $i < count($market); $i++) {
                                    $data['list']['list'][] = array(
                                        'link' => $this->domain . 'news-media/' . $market[$i]['id'],
                                        'image' => $this->domain . $market[$i]['image'],
                                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($market[$i]['date_display']) : date('d M Y', strtotime($market[$i]['date_display'])),
                                        'title' => $currentLanguage == 'vietnamese' ? $market[$i]['title_vn'] : $market[$i]['title'],
                                        'info' => $currentLanguage == 'vietnamese' ? $market[$i]['description_vn'] : $market[$i]['description_vn'],
                                    );
                                }
                            } else {
                                $data['list']['list'] = array();
                            }
                        }
                    }
                    break;
                case "general":
                    if ($list_category['general']['status'] == 'active') {
                        $general = $CI->news_model->get_data_general_news_no_limit_display($limit,$offset);
                        $totalGeneral = $CI->news_model->get_data_general_news_count_display();
                        $data['total_page'] = $totalGeneral;
                        if ($general != false) {
                            $data['list']['title'] = $currentLanguage == 'vietnamese' ? $list_category['general']['title_vn'] : $list_category['general']['title'];

                            if (count($general) > 1) {
                                for ($i = 1; $i < count($general); $i++) {
                                    $data['list']['list'][] = array(
                                        'link' => $this->domain . 'news-media/' . $general[$i]['id'],
                                        'image' => $this->domain . $general[$i]['image'],
                                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($general[$i]['date_display']) : date('d M Y', strtotime($general[$i]['date_display'])),
                                        'title' => $currentLanguage == 'vietnamese' ? $general[$i]['title_vn'] : $general[$i]['title'],
                                        'info' => $currentLanguage == 'vietnamese' ? $general[$i]['description_vn'] : $general[$i]['description_vn'],
                                    );
                                }
                            } else {
                                $data['list']['list'] = array();
                            }
                        }
                    }
                    break;
                case "newspaper":
                    if ($list_category['newspaper']['status'] == 'active') {
                        $newspaper = $CI->news_model->get_data_newspaper_news_no_limit_display($limit,$offset);
                        $totalNewspaper = $CI->news_model->get_data_newspaper_news_count_display();
                        $data['total_page'] = $totalNewspaper;
                        if ($newspaper != false) {
                            $data['list']['title'] = $currentLanguage == 'vietnamese' ? $list_category['newspaper']['title_vn'] : $list_category['newspaper']['title'];
                            if (count($newspaper) > 0) {
                                for ($i = 0; $i < count($newspaper); $i++) {
                                    $data['list']['list'][] = array(
                                        'link' => $this->domain . 'news-media/' . $newspaper[$i]['id'],
                                        'image' => $this->domain . $newspaper[$i]['image'],
                                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($newspaper[$i]['date_display']) : date('d M Y', strtotime($newspaper[$i]['date_display'])),
                                        'title' => $currentLanguage == 'vietnamese' ? $newspaper[$i]['title_vn'] : $newspaper[$i]['title'],
                                        'info' => $currentLanguage == 'vietnamese' ? $newspaper[$i]['description_vn'] : $newspaper[$i]['description_vn'],
                                    );
                                }
                            } else {
                                $data['list']['list'] = array();
                            }
                        }
                    }
                    break;
            }

            /*INTRODUCE*/
            if ($list_category['introduce']['status'] == 'active') {
                $introduce = $CI->news_model->get_data_introduce_news_display();
                if ($introduce != false) {
                    $data['content']['introduce']['title'] = $currentLanguage == 'vietnamese' ? $list_category['introduce']['title_vn'] : $list_category['introduce']['title'];
                    $data['content']['introduce']['video-url'] = $currentLanguage == 'vietnamese' ? 'https://www.youtube.com/embed/' . $introduce[0]['youtube_url_vn'] : 'https://www.youtube.com/embed/' . $introduce[0]['youtube_url'];
                    if (count($introduce) > 1) {
                        for ($i = 1; $i < count($introduce); $i++) {
                            $data['content']['introduce']['list-news'][] = array(
                                'link' => $currentLanguage == 'vietnamese' ? 'https://www.youtube.com/watch?v=' . $introduce[$i]['youtube_url_vn'] : 'https://www.youtube.com/watch?v=' . $introduce[$i]['youtube_url'],
                                'title' => $currentLanguage == 'vietnamese' ? $introduce[$i]['title_vn'] : $introduce[$i]['title']
                            );
                        }
                    } else {
                        $data['content']['introduce']['list-news'] = array();
                    }
                }
            }
            /*DOWNLOAD*/
            if ($list_category['download-information']['status'] == 'active') {
                $download = $CI->news_model->get_data_download_news_display();
                if ($download != false) {
                    $data['content']['download-information']['title'] = $currentLanguage == 'vietnamese' ? $list_category['download-information']['title_vn'] : $list_category['download-information']['title'];
                    for ($i = 0; $i < count($download); $i++) {
                        $data['content']['download-information']['list-news'][] = array(
                            'link' => $download[$i][$download[$i]['option_display']] != null && $download[$i][$download[$i]['option_display']] != '' ? $download[$i][$download[$i]['option_display']] : ($download[$i]['link'] != null && $download[$i]['link'] != '' ? $download[$i]['link'] : $this->domain . $download[$i]['file']),
                            'title' => $currentLanguage == 'vietnamese' ? $download[$i]['title_vn'] : $download[$i]['title']
                        );
                    }
                }
            }
            /*NEWSPAPER*/
            if ($list_category['newspaper']['status'] == 'active') {
                $newspaper = $CI->news_model->get_data_newspaper_news_display();
                if ($newspaper != false) {
                    $data['content']['newspaper']['title'] = $currentLanguage == 'vietnamese' ? $list_category['newspaper']['title_vn'] : $list_category['newspaper']['title'];
                    if (count($newspaper) > 0) {
                        for ($i = 0; $i < count($newspaper); $i++) {
                            $data['content']['newspaper']['list-news'][] = array(
                                'link' => $this->domain . 'news-media/' . $newspaper[$i]['id'],
                                'title' => $currentLanguage == 'vietnamese' ? $newspaper[$i]['title_vn'] : $newspaper[$i]['title']
                            );
                        }
                    } else {
                        $data['content']['newspaper']['list-news'] = array();
                    }
                }
            }
            //tin moi nhat
            $dataLast = $CI->news_model->get_data_latest_news(1);
            if($dataLast != false){
                $data['new_last']['title'] = 'Tin tức mới nhất';
                $data['new_last']['item'] = array(
                    'link' => $this->domain . 'news-media/' . $dataLast[0]['id'],
                    'image' => $this->domain . $dataLast[0]['image'],
                    'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($dataLast[0]['date_display']) : date('d M Y', strtotime($dataLast[0]['date_display'])),
                    'title' => $currentLanguage == 'vietnamese' ? $dataLast[0]['title_vn'] : $dataLast[0]['title'],
                    'info' => $currentLanguage == 'vietnamese' ? $dataLast[0]['description_vn'] : $dataLast[0]['description_vn'],
                );
            }
        }
        return $data;
    }
    public function newDetailContentInfo($data, $id)
    {
        $CI =& get_instance();
        $CI->load->model('News_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataNewDetail = $CI->News_model->get_data_news_with_id_by_new($id);
        if (count($dataNewDetail) > 0 && $dataNewDetail != false) {
            $data['content']['title'] = $currentLanguage == 'vietnamese' ? $dataNewDetail['title_vn'] : $dataNewDetail['title'];
            $data['content']['content'] = $currentLanguage == 'vietnamese' ? $dataNewDetail['content_vn'] : $dataNewDetail['content'];
            $data['content']['date'] = strtotime($dataNewDetail['date_display']);
//            $data['content']['date'] = $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($dataNewDetail['date_display']) : date('d m Y', strtotime($dataNewDetail['date_display']));
            $person = ($dataNewDetail['modified_by']!= '' && $dataNewDetail['modified_by']!= null ? $dataNewDetail['edit_name'] : $dataNewDetail['create_name']);
            $data['content']['person'] = $currentLanguage == 'vietnamese' ? "Nguồn : ".$person : 'Source'.$person;
        }
        $dataNew = $CI->news_model->get_data_latest_news();
        $data['list'] = array();
        if($dataNew) {
            for ($i = 0; $i < count($dataNew); $i++) {
                $data['list'][] = array(
                    'id' => $dataNew[$i]['id'],
                    'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($dataNew[$i]['date_display']) : date('d M Y', strtotime($dataNew[$i]['date_display'])),
                    'title' => $currentLanguage == 'vietnamese' ? $dataNew[$i]['title_vn'] : $dataNew[$i]['title'],
                    'content' =>
                        $currentLanguage == 'vietnamese'
                            ? strlen($dataNew[$i]['content_vn']) > 73 ? substr($dataNew[$i]['content_vn'], 0, 73) . ' ... .' : $dataNew[$i]['content_vn']
                            : strlen($dataNew[$i]['content']) > 73 ? substr($dataNew[$i]['content'], 0, 73) . ' ... .' : $dataNew[$i]['content'],
                );
            }
        }

        $dataHighlight = $CI->news_model->get_data_highligh_news();
        $data['list_highlight'] = array();
        if($dataHighlight) {
            for ($i = 0; $i < count($dataHighlight); $i++) {
                $data['list_highlight'][] = array(
                    'id' => $dataHighlight[$i]['id'],
                    'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString2($dataHighlight[$i]['date_display']) : date('d M Y', strtotime($dataNew[$i]['date_display'])),
                    'title' => $currentLanguage == 'vietnamese' ? $dataHighlight[$i]['title_vn'] : $dataHighlight[$i]['title'],
                    'content' =>
                        $currentLanguage == 'vietnamese'
                            ? strlen($dataHighlight[$i]['content_vn']) > 73 ? substr($dataHighlight[$i]['content_vn'], 0, 73) . ' ... .' : $dataNew[$i]['content_vn']
                            : strlen($dataHighlight[$i]['content']) > 73 ? substr($dataHighlight[$i]['content'], 0, 73) . ' ... .' : $dataNew[$i]['content'],
                );
            }
        }
        return $data;
    }

    //////// CONTACT ///////
    public function contactMenuInfo($data){
        $data['banner'] = array(
            'image' => $this->domain.'assets/images/upload/banner-contact.jpg',
            'title' => "LIÊN HỆ",
            'description' => "Tổng Công ty Công Nghiệp Sài Gòn - Trách nhiệm hữu hạn Một Thành Viên"
        );
        return $data;
    }
    public function contactContentInfo($data){
        $CI =& get_instance();
        $CI->load->model('contact_model');
        $user = $CI->session->userdata(SESSION_LOGIN);
        $contact = $CI->contact_model->get_data_contact_display();
        if(count($contact) > 0){
            $data['content']['banner'] = $this->domain .$contact[0]['banner'];
            $data['content']['title'] = $contact[0][$user['language'] == 'english' ? 'name' : 'name_vn'];
            $data['content']['title_company'] =  $contact[0][$user['language'] == 'english' ? 'title' : 'title_vn'];
            $data['content']['address'] =  $contact[0][$user['language'] == 'english' ? 'address' : 'address_vn'];
            $data['content']['tel'] =  $contact[0]['tel'];
            $data['content']['fax'] = $contact[0]['fax'];
            $data['content']['mail'] = $contact[0]['mail'];
            $data['content']['website'] = $contact[0]['website'];
        }else {
            $data['content']['banner'] = $this->domain ."assets/images/upload/banner-contact.jpg";
            $data['content']['title'] = "Văn Phòng Của CNS";
            $data['content']['title_company'] = "Tổng công ty Công nghiệp Sài Gòn - TNHH một thành viên ";
            $data['content']['address'] = "58 - 60 Nguyễn Tất Thành, Phường 12, Quận 4, TP. Hồ chí Minh";
            $data['content']['tel'] = "(84-28) 38 255 999";
            $data['content']['fax'] = "(84-28) 38 263 666 - 38 255 858";
            $data['content']['mail'] = "cns@cns.com.vn";
            $data['content']['website'] = "http://www.cns.com.vn";
        }
        return $data;
    }
    //////// CAREER JOB ///////
    public function careerJobBannerInfo($data){
        $CI =& get_instance();
        $CI->load->model('career_job_model');
        $dataCJ = $CI->career_job_model->get_data_career_job_by_id(0);
        if($dataCJ){
            $data['banner'] = array(
                'image' => $this->domain . $dataCJ['banner'],
                'title' => "Tuyển dụng",
                'description' => "Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên"
            );
        }else {
            $data['banner'] = array(
                'image' => $this->domain . 'assets/images/upload/banner-career.jpg',
                'title' => "Tuyển dụng",
                'description' => "Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên"
            );
        }
        return $data;
    }
    public function careerJobMenuInfo($data){
        $CI =& get_instance();
        $CI->load->model('career_job_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataCJ = $CI->career_job_model->get_data_career_job_menu_display();
        $data['career_job_menu'] = array();
        $data['career_job_menu'][] = array(
            'name'=>($currentLanguage == 'vietnamese' ? 'Tất Cả' : 'All').' ',
            'link'=>base_url().'career-job'
        );
        $total = 0;
        foreach ($dataCJ AS $index => $item){
            $data['career_job_menu'][] = array(
                'name'=>$item['position'],
                'number'=>($item['num'] != null && $item['num'] != '') ? $item['num'] : 0,
                'link'=>base_url().'career-job/'.$item['alias']
            );
            $total += $item['num'];
        }
         $data['career_job_menu'][0]['number'] = $total;
        return $data;
    }
    public function careerJobContentInfo($data,$type){
        $CI =& get_instance();
        $CI->load->model('career_job_model');
        $CI->load->library('utils');
        $dataCJ = $CI->career_job_model->get_data_career_job_display(array('type'=>$type));
        $data['content'] = array();
        foreach ($dataCJ AS $index => $item){
            $data['content'][] = array(
                'id'=>$item['id'],
                'position'=>$item['position'],
                'quantity'=>$item['quantity'],
                'workplace'=>$item['workplace'],
                'date_expired'=>$CI->utils->convertDateDisplay($item['date_expired']),
                'action'=>'<div><a class="btn btn-secondary" href="'.base_url().'career-job/detail/'.$item['id'].'">Ứng tuyển</a></div>',
            );
        }
        return $data;
    }
    public function careerJobContentDetailInfo($data,$id){
        $CI =& get_instance();
        $CI->load->model('career_job_model');
        $data['detail'] = $CI->career_job_model->get_data_career_job_by_id($id);

        return $data;
    }
}
