<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL


        //LIBRARY
        $this->load->library('data');
    }

    public function index()
    {
        if (!isset($this->session->userdata['language'])) {
            $this->session->set_userdata('language', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $this->session->userdata['language'];
            $this->lang->load($this->session->userdata['language'], $this->session->userdata['language']);
        }
        $data = $this->data->homePageInfo();
        $this->load->view('index', $data);
    }
    public function switchLanguage($language){
        $this->session->set_userdata('language', $language);
        redirect($_SERVER['HTTP_REFERER']);
    }
}
