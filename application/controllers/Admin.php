<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    private $domain = '';
    private $list_status_option = array(
        '' => '',
        'draft' => 'Draft',
        'waiting' => 'Waiting',
        'active' => 'Active',
        'inactive' => 'Inactive'
    );
    private $list_status_approve_option = array(
        '' => '',
        'need_approve' => 'Need Approve',
        'approved_1' => 'Approved 1',
        'approved_2' => 'Approved 2',
        'denied' => 'Denied'
    );
    private $list_position_business = array(
        '' => '',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '-1' => '-1',
    );
    private $list_type_news = array(
        '' => '',
        'company' => 'News Company',
        'market' => 'News Market',
        'general' => 'News General',
        'newspaper' => 'Newspaper'
    );
    private $list_highlights = array(
        '0' => 'None',
        '1' => 'Highlights'
    );
    private $list_type_main_menu = array(
        '' => '',
        'introduce' => 'Introduce',
        'business' => 'Business',
        'project' => 'Project',
        'customer-partner' => 'Customer partner',
        'news-media' => 'News media'
    );
    private  $option_display_file = array(
                        'file'=>'File',
                        'link'=>'Link'
                    );

    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL
        $this->load->model('main_model');
        $this->load->model('project_model');
        $this->load->model('business_model');
        $this->load->model('news_model');
        $this->load->model('partner_model');
        $this->load->model('introduce_model');
        $this->load->model('introduce_about_model');
        $this->load->model('introduce_about_awards_model');
        $this->load->model('introduce_about_awards_article_model');
        $this->load->model('introduce_about_core_model');
        $this->load->model('introduce_about_core_article_model');
        $this->load->model('introduce_about_network_model');
        $this->load->model('introduce_about_social_model');
        $this->load->model('introduce_about_system_management_model');
        $this->load->model('introduce_about_system_management_article_model');
        $this->load->model('introduce_model_culture_model');
        $this->load->model('introduce_model_culture_member_model');
        $this->load->model('introduce_model_stories_model');
        $this->load->model('introduce_model_stories_member_model');
        $this->load->model('introduce_news_model');
        $this->load->model('user_model');
        $this->load->model('career_job_model');
        $this->load->model('contact_model');

		$this->load->model('introduce_member_culture_model');
		$this->load->model('introduce_member_culture_member_model');
		$this->load->model('introduce_member_stories_model');
		$this->load->model('introduce_member_stories_member_model');
		$this->load->model('introduce_revenue_model');
        //LIBRARY
        $this->load->library('data');
        $this->load->library('common');
        $this->load->library('utils');
        $info = $this->main_model->get_data_display();
        $this->domain = $info[0]['base_domain'];
    }

    public function mainInfo()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language')
        );
        $this->load->view('admin/main_info', $data);
    }


    #region MAIN SLIDE
    public function mainBannerCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];

        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied'))
            )
        );
        $this->load->view('admin/banner_slide/create', $data);
    }

    public function mainBannerEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];

        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->main_model->get_data_banner_slide_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/banner_slide/create', $data);
    }

    public function mainBannerDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->main_model->get_data_banner_slide_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'main-slide\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBannerSlideImage(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBannerSlideImage(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveBannerSlideImage(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBannerSlideImage(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBannerSlideImage(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBannerSlideImage(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveBannerSlideImage(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/banner_slide/detail', $data);
    }

    public function mainBannerList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/banner_slide/list', $data);
    }

    public function mainBannerSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->main_model->get_data_banner_slide_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'main-banner\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteBannerSlideImage(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region PROJECT
    public function projectCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied'))
            )
        );
        $this->load->view('admin/project/create', $data);
    }

    public function projectEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->project_model->get_data_project_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/project/create', $data);
    }

    public function projectDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->project_model->get_data_project_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'project\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProject(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProject(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveProject(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProject(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProject(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProject(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveProject(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/project/detail', $data);
    }

    public function projectList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/project/list', $data);
    }

    public function projectSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->project_model->get_data_project_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'project\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteProject(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['banner_image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region PROJECT DETAIL
    public function projectDetailCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $project = $this->project_model->get_data_project_all();
        $projectData = $this->utils->getFieldData($project,'title','id');
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_project' => $this->common->generateSelectOption($projectData)
            )
        );
        $this->load->view('admin/project_detail/create', $data);
    }

    public function projectDetailEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $project = $this->project_model->get_data_project_all();
        $projectData = $this->utils->getFieldData($project,'title','id');

        $dataBannerSlide = $this->project_model->get_data_project_detail_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_project'] = $this->common->generateSelectOption($projectData,array(),$dataBannerSlide['project_id']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/project_detail/create', $data);
    }

    public function projectDetailDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }
        $project = $this->project_model->get_data_project_all();
        $projectData = $this->utils->getFieldData($project,'title','id');

        $dataBannerSlide = $this->project_model->get_data_project_detail_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'project-detail\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['project'] = $projectData[$dataBannerSlide['project_id']];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProjectDetail(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProjectDetail(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveProjectDetail(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProjectDetail(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProjectDetail(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveProjectDetail(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveProjectDetail(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/project_detail/detail', $data);
    }

    public function projectDetailList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/project_detail/list', $data);
    }

    public function projectDetailSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->project_model->get_data_project_detail_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'project-detail\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteProjectDetail(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region BUSINESS
    public function businessCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_business)
            )
        );
        $this->load->view('admin/business/create', $data);
    }

    public function businessEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->business_model->get_data_business_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_position'] = $this->common->generateSelectOption($this->list_position_business, array(), $dataBannerSlide['position_order']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/business/create', $data);
    }

    public function businessDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->business_model->get_data_business_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'business\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBusiness(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBusiness(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveBusiness(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBusiness(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBusiness(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveBusiness(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveBusiness(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/business/detail', $data);
    }

    public function businessList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/business/list', $data);
    }

    public function businessSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->business_model->get_data_business_all($title, $status, $status_approve);
        if (count($dataSlide) > 0) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'business\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteBusiness(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region NEWS
    public function newsCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_type' => $this->common->generateSelectOption($this->list_type_news),
                'option_highlight' => $this->common->generateSelectOption($this->list_highlights)
            )
        );
        $this->load->view('admin/news/create', $data);
    }

    public function newsEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->news_model->get_data_news_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_type'] = $this->common->generateSelectOption($this->list_type_news, array(), $dataBannerSlide['type']);
        $dataBannerSlide['option_highlight'] = $this->common->generateSelectOption($this->list_highlights, array(), $dataBannerSlide['highlights']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/news/create', $data);
    }

    public function newsDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions'],true);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->news_model->get_data_news_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'news\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['type'] = $this->list_type_news[$dataBannerSlide['type']];
        $dataBannerSlide['highlights'] = $this->list_highlights[$dataBannerSlide['highlights']];
        if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNews(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNews(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveNews(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNews(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNews(\'approved_2\')">Approve 2</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/news/detail', $data);
    }

    public function newsList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/news/list', $data);
    }

    public function newsSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->news_model->get_data_news_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'news\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_2' && $dataSlide[$i]['status_approve'] != 'approved_1'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteNews(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'date_display' => $this->utils->convertDateDisplay($dataSlide[$i]['date_display']),
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'type' => $this->list_type_news[$dataSlide[$i]['type']],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }

    public function newsSetting()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $listSetting = $this->news_model->get_all_news_setting();
        $listSetting = $this->utils->getFieldDataArray($listSetting,'');
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_type' => $this->common->generateSelectOption($this->list_type_news),
                'option_highlight' => $this->common->generateSelectOption($this->list_highlights),
                'data_setting' => $listSetting
            )
        );
        $this->load->view('admin/news/setting', $data);
    }
    #endregion

    #region PARTNER GLOBAL
    /*public function partnerGlobalCreate($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_business)
            )
        );
        $this->load->view('admin/partner_global/create', $data);
    }*/

    public function partnerGlobalEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->partner_model->get_data_partner_global_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_position'] = $this->common->generateSelectOption($this->list_position_business, array(), $dataBannerSlide['position_order']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/partner_global/create', $data);
    }

    public function partnerGlobalDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->partner_model->get_data_partner_global_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'partner-global\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/partner_global/detail', $data);
    }

    /*public function partnerGlobalList($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/business/list', $data);
    }

    public function partnerGlobalSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->business_model->get_data_business_all($title, $status, $status_approve);
        if (count($dataSlide) > 0) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'business\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteBusiness(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }*/
    #endregion

    #region PARTNER CUSTOMER
    public function partnerCustomerCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_business)
            )
        );
        $this->load->view('admin/partner_customer/create', $data);
    }

    public function partnerCustomerEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->partner_model->get_data_partner_customer_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_position'] = $this->common->generateSelectOption($this->list_position_business, array(), $dataBannerSlide['position_order']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/partner_customer/create', $data);
    }

    public function partnerCustomerDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->partner_model->get_data_partner_customer_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'partner-customer\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartnerCustomer(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartnerCustomer(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approvePartnerCustomer(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartnerCustomer(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartnerCustomer(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartnerCustomer(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approvePartnerCustomer(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/partner_customer/detail', $data);
    }

    public function partnerCustomerList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/partner_customer/list', $data);
    }

    public function partnerCustomerSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->partner_model->get_data_partner_customer_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'partner-customer\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deletePartnerCustomer(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region PARTNER
    public function partnerPartnerCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_business)
            )
        );
        $this->load->view('admin/partner/create', $data);
    }

    public function partnerPartnerEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->partner_model->get_data_partner_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_position'] = $this->common->generateSelectOption($this->list_position_business, array(), $dataBannerSlide['position_order']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/partner/create', $data);
    }

    public function partnerPartnerDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->partner_model->get_data_partner_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'partner\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartner(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartner(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approvePartner(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartner(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartner(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approvePartner(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approvePartner(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/partner/detail', $data);
    }

    public function partnerPartnerList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/partner/list', $data);
    }

        public function partnerPartnerSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->partner_model->get_data_partner_partner_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'partner\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deletePartner(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region INTRODUCE
    public function introduceCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_introduce)
            )
        );
        $this->load->view('admin/introduce/create', $data);
    }

    public function introduceEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model->get_data_introduce_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce/create', $data);
    }

    public function introduceDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model->get_data_introduce_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduce(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduce(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduce(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduce(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduce(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduce(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduce(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce/detail', $data);
    }

    public function introduceList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/introduce/list', $data);
    }

    public function introduceSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->introduce_model->get_data_introduce_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteintroduce(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $title = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title'].'</a>';
                $title_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title_vn'].'</a>';
                $data[] = array(
                    'title' => $title,
                    'title_vn' => $title_vn,
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region INTRODUCE ABOUT
    public function introduceAboutEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_model->get_data_introduce_about_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about/create', $data);
    }

    public function introduceAboutDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_model->get_data_introduce_about_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-about\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAbout(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAbout(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAbout(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAbout(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAbout(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAbout(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAbout(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about/detail', $data);
    }
    #endregion

    #region INTRODUCE ABOUT AWARDS
    public function introduceAboutAwardsEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_awards_model->get_data_introduce_about_awards_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_awards/create', $data);
    }

    public function introduceAboutAwardsDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_awards_model->get_data_introduce_about_awards_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-about-awards\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwards(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwards(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutAwards(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwards(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwards(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwards(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutAwards(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_awards/detail', $data);
    }
    #endregion

    #region INTRODUCE ABOUT AWARDS ARTICLE
    public function introduceAboutAwardsArticleEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_awards_article_model->get_data_introduce_about_awards_article_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_awards_article/create', $data);
    }

    public function introduceAboutAwardsArticleCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_introduce)
            )
        );
        $this->load->view('admin/introduce_about_awards_article/create', $data);
    }

    public function introduceAboutAwardsArticleDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_awards_article_model->get_data_introduce_about_awards_article_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-about-awards-article\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwardsArticle(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwardsArticle(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutAwardsArticle(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwardsArticle(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwardsArticle(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutAwardsArticle(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutAwardsArticle(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_awards_article/detail', $data);
    }

    public function introduceAboutAwardsArticleList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/introduce_about_awards_article/list', $data);
    }

    public function introduceAboutAwardsArticleSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->introduce_about_awards_article_model->get_data_introduce_about_awards_article_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce-about-awards-article\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteIntroduceAboutAwardsArticle(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $title = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-about-awards-article\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title'].'</a>';
                $title_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-about-awards-article\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title_vn'].'</a>';
                $data[] = array(
                    'title' => $title,
                    'title_vn' => $title_vn,
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region INTRODUCE ABOUT CORE
    public function introduceAboutCoreEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_core_model->get_data_introduce_about_core_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_core/create', $data);
    }

    public function introduceAboutCoreDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_core_model->get_data_introduce_about_core_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-about-core\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCore(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCore(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutCore(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCore(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCore(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCore(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutCore(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_core/detail', $data);
    }
    #endregion

    #region INTRODUCE ABOUT SOCIAL
    public function introduceAboutSocialEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_social_model->get_data_introduce_about_social_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_social/create', $data);
    }

    public function introduceAboutSocialDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_social_model->get_data_introduce_about_social_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-about-social\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSocial(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSocial(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutSocial(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSocial(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSocial(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSocial(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutSocial(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_social/detail', $data);
    }
    #endregion

    #region INTRODUCE ABOUT SYSTEM MANAGEMENT
    public function introduceAboutSystemManagementEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_system_management_model->get_data_introduce_about_system_management_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_system_management/create', $data);
    }

    public function introduceAboutSystemManagementDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_system_management_model->get_data_introduce_about_system_management_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-about-system-management\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagement(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagement(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagement(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagement(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagement(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagement(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagement(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_system_management/detail', $data);
    }
    #endregion

    #region INTRODUCE SYSTEM MANAGEMENT ARTICLE
    public function introduceAboutSystemManagementArticleEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_system_management_article_model->get_data_introduce_about_system_management_article_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_system_management_article/create', $data);
    }

    public function introduceAboutSystemManagementArticleCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_introduce)
            )
        );
        $this->load->view('admin/introduce_about_system_management_article/create', $data);
    }

    public function introduceAboutSystemManagementArticleDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_system_management_article_model->get_data_introduce_about_system_management_article_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-about-system-management-article\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagementArticle(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagementArticle(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagementArticle(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagementArticle(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagementArticle(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagementArticle(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutSystemManagementArticle(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_system_management_article/detail', $data);
    }

    public function introduceAboutSystemManagementArticleList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/introduce_about_system_management_article/list', $data);
    }

        public function introduceAboutSystemManagementArticleSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->introduce_about_system_management_article_model->get_data_introduce_about_system_management_article_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce-about-system-management-article\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteIntroduceAboutSystemManagementArticle(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $title = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-about-system-management-article\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title'].'</a>';
                $title_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-about-system-management-article\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title_vn'].'</a>';
                $data[] = array(
                    'title' => $title,
                    'title_vn' => $title_vn,
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region INTRODUCE MODEL CULTURE
    public function introduceModelCultureEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_culture_model->get_data_introduce_model_culture_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_culture/create', $data);
    }

    public function introduceModelCultureDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_culture_model->get_data_introduce_model_culture_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-model-culture\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCulture(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCulture(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceModelCulture(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCulture(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCulture(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCulture(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceModelCulture(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_culture/detail', $data);
    }
    #endregion

    #region INTRODUCE MODEL CULTURE MEMBER
    public function introduceModelCultureMemberEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_culture_member_model->get_data_introduce_model_culture_member_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_culture_member/create', $data);
    }

    public function introduceModelCultureMemberCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_introduce)
            )
        );
        $this->load->view('admin/introduce_model_culture_member/create', $data);
    }

    public function introduceModelCultureMemberDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_culture_member_model->get_data_introduce_model_culture_member_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-model-culture-member\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCultureMember(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCultureMember(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceModelCultureMember(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCultureMember(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCultureMember(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelCultureMember(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceModelCultureMember(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_culture_member/detail', $data);
    }

    public function introduceModelCultureMemberList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/introduce_model_culture_member/list', $data);
    }

    public function introduceModelCultureMemberSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $name = $parameters['name'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->introduce_model_culture_member_model->get_data_introduce_model_culture_member_model_all($name, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce-model-culture-member\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteIntroduceModelCultureMember(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $name_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-model-culture-member\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['name_vn'].'</a>';
                $data[] = array(
                    'name_vn' => $name_vn,
                    'position_vn' => $dataSlide[$i]['position_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region INTRODUCE MODEL STORIES
    public function introduceModelStoriesEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_stories_model->get_data_introduce_model_stories_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_stories/create', $data);
    }

    public function introduceModelStoriesDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_stories_model->get_data_introduce_model_stories_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-model-stories\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStories(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStories(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceModelStories(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStories(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStories(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStories(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceModelStories(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_stories/detail', $data);
    }
    #endregion

    #region INTRODUCE ABOUT CORE ARTICLE
    public function introduceAboutCoreArticleEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_core_article_model->get_data_introduce_about_core_article_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_core_article/create', $data);
    }

    public function introduceAboutCoreArticleCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied'))
            )
        );
        $this->load->view('admin/introduce_about_core_article/create', $data);
    }

    public function introduceAboutCoreArticleDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_core_article_model->get_data_introduce_about_core_article_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-about-core-article\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCoreArticle(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCoreArticle(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutCoreArticle(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCoreArticle(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCoreArticle(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutCoreArticle(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutCoreArticle(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_core_article/detail', $data);
    }

    public function introduceAboutCoreArticleList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/introduce_about_core_article/list', $data);
    }

    public function introduceAboutCoreArticleSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->introduce_about_core_article_model->get_data_introduce_about_core_article_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce-about-core-article\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteIntroduceAboutCoreArticle(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $title = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-about-core-article\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title'].'</a>';
                $title_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-about-core-article\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title_vn'].'</a>';
                $data[] = array(
                    'title' => $title,
                    'title_vn' => $title_vn,
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region INTRODUCE ABOUT NETWORK
    public function introduceAboutNetworkEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_network_model->get_data_introduce_about_network_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_position'] = $this->common->generateSelectOption($this->list_position_business, array(), $dataBannerSlide['position_order']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_network/create', $data);
    }

    public function introduceAboutNetworkCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied'))
            )
        );
        $this->load->view('admin/introduce_about_network/create', $data);
    }

    public function introduceAboutNetworkDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_network_model->get_data_introduce_about_network_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-about-network\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutNetwork(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutNetwork(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutNetwork(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutNetwork(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutNetwork(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceAboutNetwork(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceAboutNetwork(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_network/detail', $data);
    }

    public function introduceAboutNetworkList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/introduce_about_network/list', $data);
    }

    public function introduceAboutNetworkSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->introduce_about_network_model->get_data_introduce_about_network_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce-about-network\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteIntroduceAboutNetwork(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $title = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-about-network\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title'].'</a>';
                $title_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-about-network\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title_vn'].'</a>';
                $data[] = array(
                    'title' => $title,
                    'title_vn' => $title_vn,
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image_1' => '<img src="'.$this->domain.$dataSlide[$i]['image_1'].'" style="max-width: 135px; max-height: 80px;" />',
                    'image_2' => '<img src="'.$this->domain.$dataSlide[$i]['image_1'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region INTRODUCE MODEL STORIES MEMBER
    public function introduceModelStoriesMemberEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_stories_member_model->get_data_introduce_model_stories_member_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_position'] = $this->common->generateSelectOption($this->list_position_business, array(), $dataBannerSlide['position_order']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_stories_member/create', $data);
    }

    public function introduceModelStoriesMemberCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied'))
            )
        );
        $this->load->view('admin/introduce_model_stories_member/create', $data);
    }

    public function introduceModelStoriesMemberDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_stories_member_model->get_data_introduce_model_stories_member_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-model-stories-member\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStoriesMember(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStoriesMember(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceModelStoriesMember(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStoriesMember(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStoriesMember(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceModelStoriesMember(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceModelStoriesMember(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_stories_member/detail', $data);
    }

    public function introduceModelStoriesMemberList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/introduce_model_stories_member/list', $data);
    }

    public function introduceModelStoriesMemberSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->introduce_model_stories_member_model->get_data_introduce_model_stories_member_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce-model-stories-member\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteIntroduceModelStoriesMember(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $title = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-model-stories-member\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['name'].'</a>';
                $title_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-model-stories-member\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['name_vn'].'</a>';
                $data[] = array(
                    'name' => $title,
                    'name_vn' => $title_vn,
                    'position' => $dataSlide[$i]['position'],
                    'position_vn' => $dataSlide[$i]['position_vn'],
                    'info' => $dataSlide[$i]['info'],
                    'info_vn' => $dataSlide[$i]['info_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region INTRODUCE NEWS
    public function introduceNewsEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_news_model->get_data_introduce_news_with_id($record);


        $list_introduce_option = $this->introduce_model->get_data_introduce_all();
        $dataBannerSlide['option_introduce'] = $this->common->generateSelectOptionArray($list_introduce_option, array(),$dataBannerSlide['introduce_id'],'title_vn');
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['image'] = $this->domain.$dataBannerSlide['image'];
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_news/create', $data);
    }

    public function introduceNewsCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied'))
            )
        );
        $this->load->view('admin/introduce_news/create', $data);
    }

    public function introduceNewsDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_news_model->get_data_introduce_news_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        $dataBannerSlide['image'] = $this->domain.$dataBannerSlide['image'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-news\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceNews(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceNews(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceNews(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceNews(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceNews(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceNews(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceNews(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_news/detail', $data);
    }

    public function introduceNewsList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/introduce_news/list', $data);
    }

    public function introduceNewsSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->introduce_news_model->get_data_introduce_news_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce-news\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteIntroduceNews(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $title = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-news\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title'].'</a>';
                $title_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-news\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title_vn'].'</a>';
                $data[] = array(
                    'introduce' => $dataSlide[$i]['introduce_title_vn'],
                    'title' => $title,
                    'title_vn' => $title_vn,
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region NEWS DOWNLOAD
    public function newsDownloadCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
            )
        );
        $this->load->view('admin/news_download/create', $data);
    }

    public function newsDownloadEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }
        $dataBannerSlide = $this->news_model->get_news_download_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['option_display'] = $this->common->generateSelectOption($this->option_display_file ,array(), $dataBannerSlide['option_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/news_download/create', $data);
    }

    public function newsDownloadDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->news_model->get_news_download_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'news-download\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['option_display'] = $this->option_display_file[$dataBannerSlide['option_display']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsDownload(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsDownload(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveNewsDownload(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsDownload(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsDownload(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsDownload(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveNewsDownload(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        $dataBannerSlide['type'] = $this->list_type_news[$dataBannerSlide['type']];
        $nameFile = explode('/',$dataBannerSlide['file']);
        $dataBannerSlide['file'] = "<a href='".$this->domain .$dataBannerSlide['file']."'>".$nameFile[count($nameFile) - 1]."</a>";
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/news_download/detail', $data);
    }

    public function newsDownloadList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/news_download/list', $data);
    }

    public function newsDownloadSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->news_model->get_data_news_download_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'news-download\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteNewsDownload(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region MAIN INFO
    public function mainInfoEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->main_model->get_main_info_with_id($record);
        $dataBannerSlide['image_logo'] = $this->domain . $dataBannerSlide['logo'];
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/main_info/create', $data);
    }

    public function mainInfoDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->main_model->get_main_info_with_id($record);
        $dataBannerSlide['image_logo'] = $this->domain . $dataBannerSlide['logo'];

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/main_info/detail', $data);
    }
    #endregion

    #region MAIN MENU
    public function mainMenuEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->main_model->get_main_menu_with_id($record);

        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/main_menu/create', $data);
    }

    public function mainMenuDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->main_model->get_main_menu_with_id($record);

        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/main_menu/detail', $data);
    }

    public function mainMenuCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
            )
        );
        $this->load->view('admin/main_menu/create', $data);
    }

    public function mainMenuList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
            )
        );
        $this->load->view('admin/main_menu/list', $data);
    }

    public function mainMenuSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];

        $dataSlide = $this->main_model->get_data_main_menu_all($title, $status);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'main-menu\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                $button .= '<button type="button" class="btn red btn-circle" onclick="deleteMainMenu(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'type' => $this->list_type_main_menu[$dataSlide[$i]['type']],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region NEWS INTRODUCE
    public function newsIntroduceCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
            )
        );
        $this->load->view('admin/news_introduce/create', $data);
    }

    public function newsIntroduceEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->news_model->get_news_introduce_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/news_introduce/create', $data);
    }

    public function newsIntroduceDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->news_model->get_news_introduce_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'news-introduce\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsIntroduce(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsIntroduce(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveNewsIntroduce(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsIntroduce(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsIntroduce(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveNewsIntroduce(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveNewsIntroduce(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        $dataBannerSlide['type'] = $this->list_type_news[$dataBannerSlide['type']];
        $dataBannerSlide['youtube_url'] = "<a target='_blank' href='https://www.youtube.com/watch?v=".$dataBannerSlide['youtube_url']."'>".$dataBannerSlide['youtube_url']."</a>";
        $dataBannerSlide['youtube_url_vn'] = "<a target='_blank' href='https://www.youtube.com/watch?v=".$dataBannerSlide['youtube_url_vn']."'>".$dataBannerSlide['youtube_url_vn']."</a>";

        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/news_introduce/detail', $data);
    }

    public function newsIntroduceList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/news_introduce/list', $data);
    }

    public function newsIntroduceSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->news_model->get_data_news_introduce_all($title, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'news-introduce\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteNewsIntroduce(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'youtube_url' => "<a target='_blank' href='https://www.youtube.com/watch?v=".$dataSlide[$i]['youtube_url']."'>".$dataSlide[$i]['youtube_url']."</a>",
                    'youtube_url_vn' => "<a target='_blank' href='https://www.youtube.com/watch?v=".$dataSlide[$i]['youtube_url_vn']."'>".$dataSlide[$i]['youtube_url_vn']."</a>",
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region CAREER JOB
    public function careerJobCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
            )
        );
        $this->load->view('admin/career_job/create', $data);
    }

    public function careerJobEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->career_job_model->get_data_career_job_by_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_expired'] = $this->utils->convertDateDisplay($dataBannerSlide['date_expired']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner'];
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/career_job/create', $data);
    }

    public function careerJobDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->career_job_model->get_data_career_job_by_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        if($dataBannerSlide['status'] == 'draft') {
            $dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'news-introduce\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
        }
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveCareerJob(\'approved_2\')">Approve 2</button>';
            }else if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveCareerJob(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveCareerJob(\'denied\')">Deny</button>';
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveCareerJob(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_2,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'approved_1') {
                $dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveCareerJob(\'approved_2\')">Approve 2</button>';
            }
        }else if(in_array(APPROVED_1,$permissions)) {
            if ($dataBannerSlide['status_approve'] == 'need_approve') {
                $dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveCareerJob(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveCareerJob(\'denied\')">Deny</button>';
            }
        }
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        $dataBannerSlide['type'] = $this->list_type_news[$dataBannerSlide['type']];
        $dataBannerSlide['youtube_url'] = "<a target='_blank' href='https://www.youtube.com/watch?v=".$dataBannerSlide['youtube_url']."'>".$dataBannerSlide['youtube_url']."</a>";
        $dataBannerSlide['youtube_url_vn'] = "<a target='_blank' href='https://www.youtube.com/watch?v=".$dataBannerSlide['youtube_url_vn']."'>".$dataBannerSlide['youtube_url_vn']."</a>";
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner'];

        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/career_job/detail', $data);
    }

    public function careerJobList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/career_job/list', $data);
    }

    public function careerJobSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $position = $parameters['position'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->career_job_model->get_data_career_job_all($position, $status, $status_approve);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'career-job\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteCareerJob(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'position' => $dataSlide[$i]['position'],
                    'quantity' => $dataSlide[$i]['quantity'],
                    'workplace' => $dataSlide[$i]['workplace'],
                    'date_expired' => $dataSlide[$i]['date_expired'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region NEWS SETTING
    public function newSettingCreate()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
            )
        );
        $this->load->view('admin/new_setting/create', $data);
    }

    public function newSettingEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->news_model->get_news_setting_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/new_setting/create', $data);
    }

    public function newSettingDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        $permissions = json_decode($userData['permissions']);
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->news_model->get_news_setting_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/new_setting/detail', $data);
    }

    public function newSettingList()
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option)
            )
        );
        $this->load->view('admin/new_setting/list', $data);
    }

    public function newSettingSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $link = $parameters['link'];
        $status = $parameters['status'];
        $title = $parameters['title'];

        $dataSlide = $this->news_model->get_data_news_setting_all($title, $status, $link);
        if ($dataSlide) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'new-setting\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                $data[] = array(
                    'id' => $dataSlide[$i]['id'],
                    'link' => $dataSlide[$i]['id'],
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region CONTACT
    public function contactEdit($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->contact_model->get_data_contact_with_id($record);
        $dataBannerSlide['banner'] = $this->domain . $dataBannerSlide['banner'];
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/contact/create', $data);
    }

    public function contactDetail($record)
    {
        $userData = $this->checkLogin();
        $selectLanguage = $userData['language'];
        $user = $userData['id'];
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->contact_model->get_data_contact_with_id($record);
        $dataBannerSlide['banner'] = $this->domain . $dataBannerSlide['banner'];
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/contact/detail', $data);
    }
    #endregion

	#region INTRODUCE MEMBER CULTURE
	public function introduceMemberCultureEdit($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_member_culture_model->get_data_introduce_member_culture_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_member_culture/create', $data);
	}

	public function introduceMemberCultureDetail($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		$permissions = json_decode($userData['permissions']);
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_member_culture_model->get_data_introduce_member_culture_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
		$dataBannerSlide['status_value'] = $dataBannerSlide['status'];
		if($dataBannerSlide['status'] == 'draft') {
			$dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-member-culture\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
		}
		$dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
		$dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
		if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCulture(\'approved_2\')">Approve 2</button>';
			}else if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCulture(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceMemberCulture(\'denied\')">Deny</button>';
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCulture(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCulture(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_1,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCulture(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceMemberCulture(\'denied\')">Deny</button>';
			}
		}
		$dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
		if ($dataBannerSlide['approver'] != '') {
			$userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
			if (count($userData) > 0 && $userData != false) {
				$dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
			}
		}

		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_member_culture/detail', $data);
	}
	#endregion

	#region INTRODUCE MEMBER CULTURE MEMBER
	public function introduceMemberCultureMemberEdit($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_member_culture_member_model->get_data_introduce_member_culture_member_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_member_culture_member/create', $data);
	}

	public function introduceMemberCultureMemberCreate()
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => array(
				'id' => null,
				'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
				'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
				'option_position' => $this->common->generateSelectOption($this->list_position_introduce)
			)
		);
		$this->load->view('admin/introduce_member_culture_member/create', $data);
	}

	public function introduceMemberCultureMemberDetail($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		$permissions = json_decode($userData['permissions']);
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_member_culture_member_model->get_data_introduce_member_culture_member_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
		$dataBannerSlide['status_value'] = $dataBannerSlide['status'];
		if($dataBannerSlide['status'] == 'draft') {
			$dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-member-culture-member\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
		}
		$dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
		$dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
		if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCultureMember(\'approved_2\')">Approve 2</button>';
			}else if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCultureMember(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceMemberCultureMember(\'denied\')">Deny</button>';
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCultureMember(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCultureMember(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_1,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberCultureMember(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceMemberCultureMember(\'denied\')">Deny</button>';
			}
		}
		$dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
		if ($dataBannerSlide['approver'] != '') {
			$userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
			if (count($userData) > 0 && $userData != false) {
				$dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
			}
		}

		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_member_culture_member/detail', $data);
	}

	public function introduceMemberCultureMemberList()
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'search' => array(
				'option_status' => $this->common->generateSelectOption($this->list_status_option),
				'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
			)
		);
		$this->load->view('admin/introduce_member_culture_member/list', $data);
	}

	public function introduceMemberCultureMemberSearch()
	{
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$currentLanguage = $parameters['current_language'];
		$name = $parameters['name'];
		$status = $parameters['status'];
		$status_approve = $parameters['status_approve'];

		$dataSlide = $this->introduce_member_culture_member_model->get_data_introduce_member_culture_member_model_all($name, $status, $status_approve);
		if ($dataSlide) {
			$data = array();
			for ($i = 0; $i < count($dataSlide); $i++) {
				$button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce-member-culture-member\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
				if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
					$button .= '<button type="button" class="btn red btn-circle" onclick="deleteIntroduceMemberCultureMember(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
				}
				$name_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-member-culture-member\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['name_vn'].'</a>';
				$data[] = array(
					'name_vn' => $name_vn,
					'position_vn' => $dataSlide[$i]['position_vn'],
					'status' => $this->list_status_option[$dataSlide[$i]['status']],
					'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
					'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
					'action' => $button
				);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array());
		}
	}
	#endregion

	#region INTRODUCE MEMBER STORIES
	public function introduceMemberStoriesEdit($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_member_stories_model->get_data_introduce_member_stories_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_member_stories/create', $data);
	}

	public function introduceMemberStoriesDetail($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		$permissions = json_decode($userData['permissions']);
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_member_stories_model->get_data_introduce_member_stories_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
		$dataBannerSlide['status_value'] = $dataBannerSlide['status'];
		if($dataBannerSlide['status'] == 'draft') {
			$dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-member-stories\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
		}
		$dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
		$dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
		if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStories(\'approved_2\')">Approve 2</button>';
			}else if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStories(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceMemberStories(\'denied\')">Deny</button>';
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStories(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStories(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_1,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStories(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceMemberStories(\'denied\')">Deny</button>';
			}
		}
		$dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
		if ($dataBannerSlide['approver'] != '') {
			$userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
			if (count($userData) > 0 && $userData != false) {
				$dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
			}
		}

		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_member_stories/detail', $data);
	}
	#endregion

	#region INTRODUCE MEMBER STORIES MEMBER
	public function introduceMemberStoriesMemberEdit($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_member_stories_member_model->get_data_introduce_member_stories_member_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['option_position'] = $this->common->generateSelectOption($this->list_position_business, array(), $dataBannerSlide['position_order']);
		$dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_member_stories_member/create', $data);
	}

	public function introduceMemberStoriesMemberCreate()
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => array(
				'id' => null,
				'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
				'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied'))
			)
		);
		$this->load->view('admin/introduce_member_stories_member/create', $data);
	}

	public function introduceMemberStoriesMemberDetail($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		$permissions = json_decode($userData['permissions']);
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_member_stories_member_model->get_data_introduce_member_stories_member_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
		$dataBannerSlide['status_value'] = $dataBannerSlide['status'];
		if($dataBannerSlide['status'] == 'draft') {
			$dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-member-stories-member\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
		}
		$dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
		$dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
		if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStoriesMember(\'approved_2\')">Approve 2</button>';
			}else if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStoriesMember(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceMemberStoriesMember(\'denied\')">Deny</button>';
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStoriesMember(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStoriesMember(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_1,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceMemberStoriesMember(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceMemberStoriesMember(\'denied\')">Deny</button>';
			}
		}
		$dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
		if ($dataBannerSlide['approver'] != '') {
			$userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
			if (count($userData) > 0 && $userData != false) {
				$dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
			}
		}

		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_member_stories_member/detail', $data);
	}

	public function introduceMemberStoriesMemberList()
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'search' => array(
				'option_status' => $this->common->generateSelectOption($this->list_status_option),
				'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
			)
		);
		$this->load->view('admin/introduce_member_stories_member/list', $data);
	}

	public function introduceMemberStoriesMemberSearch()
	{
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$currentLanguage = $parameters['current_language'];
		$title = $parameters['title'];
		$status = $parameters['status'];
		$status_approve = $parameters['status_approve'];

		$dataSlide = $this->introduce_member_stories_member_model->get_data_introduce_member_stories_member_all($title, $status, $status_approve);
		if ($dataSlide) {
			$data = array();
			for ($i = 0; $i < count($dataSlide); $i++) {
				$button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce-member-stories-member\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
				if($dataSlide[$i]['status_approve'] != 'approved_1' && $dataSlide[$i]['status_approve'] != 'approved_2'){
					$button .= '<button type="button" class="btn red btn-circle" onclick="deleteIntroduceMemberStoriesMember(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
				}
				$title = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-member-stories-member\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['name'].'</a>';
				$title_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce-member-stories-member\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['name_vn'].'</a>';
				$data[] = array(
					'name' => $title,
					'name_vn' => $title_vn,
					'position' => $dataSlide[$i]['position'],
					'position_vn' => $dataSlide[$i]['position_vn'],
					'info' => $dataSlide[$i]['info'],
					'info_vn' => $dataSlide[$i]['info_vn'],
					'status' => $this->list_status_option[$dataSlide[$i]['status']],
					'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
					'action' => $button
				);
			}
			echo json_encode($data);
		} else {
			echo json_encode(array());
		}
	}
	#endregion

	#region INTRODUCE REVENUE
	public function introduceRevenueEdit($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_revenue_model->get_data_introduce_revenue_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_revenue/create', $data);
	}

	public function introduceRevenueDetail($record)
	{
		$userData = $this->checkLogin();
		$selectLanguage = $userData['language'];
		$user = $userData['id'];
		$permissions = json_decode($userData['permissions']);
		if (!isset($selectLanguage)) {
			$this->session->set_userdata('lang', $this->config->item('language'));
			$this->currentLanguage = $this->config->item('language');
			$this->lang->load($this->config->item('language'), $this->config->item('language'));
		} else {
			$this->currentLanguage = $selectLanguage;
			$this->lang->load($selectLanguage, $selectLanguage);
		}

		$dataBannerSlide = $this->introduce_revenue_model->get_data_introduce_revenue_with_id($record);
		$dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
		$dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
		$dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
		$dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
		$dataBannerSlide['status_value'] = $dataBannerSlide['status'];
		if($dataBannerSlide['status'] == 'draft') {
			$dataBannerSlide['html_btn_activation_process'] = '<button type="button" class="btn green-haze btn-circle btn-outline" onclick="activationProcess(\'introduce-revenue\',\'' . $dataBannerSlide['id'] . '\')">Activation process</button>';
		}
		$dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
		$dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
		if(in_array(APPROVED_1,$permissions) && in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceRevenue(\'approved_2\')">Approve 2</button>';
			}else if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceRevenue(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceRevenue(\'denied\')">Deny</button>';
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceRevenue(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_2,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'approved_1') {
				$dataBannerSlide['html_btn_approve_2'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceRevenue(\'approved_2\')">Approve 2</button>';
			}
		}else if(in_array(APPROVED_1,$permissions)) {
			if ($dataBannerSlide['status_approve'] == 'need_approve') {
				$dataBannerSlide['html_btn_approve_1'] = '<button type="button" class="btn blue-hoki btn-circle btn-outline" onclick="approveIntroduceRevenue(\'approved_1\')">Approve 1</button>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="approveIntroduceRevenue(\'denied\')">Deny</button>';
			}
		}
		$dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
		if ($dataBannerSlide['approver'] != '') {
			$userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
			if (count($userData) > 0 && $userData != false) {
				$dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
			}
		}

		$data = array(
			'domain' => $this->domain,
			'language' => $this->lang->line('language'),
			'current_user' => $user,
			'info' => $dataBannerSlide
		);
		$this->load->view('admin/introduce_revenue/detail', $data);
	}
	#endregion


    public function home($selectLanguage, $user,$token)
    {
        $this->session->set_userdata(SESSION_LOGIN, null);
        $this->loginSession($selectLanguage,$user,$token);
        $userData = $this->checkLogin();

        if($userData) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));

            $data = array(
                'domain' => $this->domain,
                'language' => $this->lang->line('language'),
            );
            $this->load->view('admin/home', $data);
        }else{
            $data = array();
            $this->load->view('admin/login', $data);
        }
    }
    public function setSessionUser($id){
        $user = $this->session->userdata(SESSION_LOGIN);
        if(!$user){
            $user = $this->main_model->get_info_user_with_id($id);
            $this->session->set_userdata(SESSION_LOGIN, $user);
        }
        return $user;
    }
    public function loginSession($selectLanguage,$user,$token){
        if($token == md5($user.TOKEN)){
            $userData = $this->main_model->get_info_user_with_id($user);
            if($userData) {
                $userData['language'] = $selectLanguage;
                $this->session->set_userdata(SESSION_LOGIN, $userData);
            }else{
                $this->session->set_userdata(SESSION_LOGIN, null);
            }
        }else{
            $this->session->set_userdata(SESSION_LOGIN, null);
        }
    }
    public function checkLogin(){
        if($_SERVER['REQUEST_URI'] != '/login') {
            $userData = $this->session->userdata(SESSION_LOGIN);
            if (!$userData) {
                redirect($this->config->item('base_url').'/login');
            }else{
                return $userData;
            }
        }
    }
    public function login(){
        if($_POST){
            redirect($this->config->item('base_url').'/admin/home/');
        }else{
            $data = array(
                'domain' => $this->domain,
                'language' => $this->lang->line('language'),
            );
            $this->load->view('admin/login', $data);
        }
    }

    public function activationProcess(){
        $parameters = $this->input->post('parameters');
        $type = $parameters['type'];
        $listMail = $this->user_model->get_list_user_permission(APPROVED_1);
        $data = array(
            'status' => true,
            'message' => 'Success',
            'error_code' => 200
        );
        switch ($type){
            case "news":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->news_model->update_news($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "main-slide":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->main_model->update_banner_slide_image($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "project":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->project_model->update_project($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "project-detail":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->project_model->update_project_detail($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "business":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->business_model->update_business($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "partner-customer":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->partner_model->update_partner_customer($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "partner":
                break;
            case "partner-global":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->partner_model->update_partner_global($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_model->update_introduce($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-about":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_model->update_introduce_about($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-about-awards":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_awards_model->update_introduce_about_awards($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-about-awards-article":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_awards_article_model->update_introduce_about_awards_article($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-about-core":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_core_model->update_introduce_about_core($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-about-social":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_social_model->update_introduce_about_social($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-about-system-management":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_system_management_model->update_introduce_about_system_management($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-model-culture":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_model_culture_model->update_introduce_model_culture($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-model-stories":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_model_stories_model->update_introduce_model_stories($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-about-core-article":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_core_article_model->update_introduce_about_core_article($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-about-network":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_network_model->update_introduce_about_network($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-model-stories-member":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_model_stories_member_model->update_introduce_model_stories_member($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "introduce-news":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_news_model->update_introduce_news($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "news-download":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->news_model->update_news_download($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
            case "news-introduce":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->news_model->update_news_introduce($parameters['record_id'],array('status'=>'waiting'));
                }
                break;
        }
        echo json_encode($data);
    }

    public function uploadFile(){
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language')
        );
        $target_dir = "assets/images/uploads/";
        $target_file = $target_dir . $_FILES["Fichier1"]["name"];
        $flag = false;
        $data['error'] = '';
        if($_FILES){
//            $path_parts = pathinfo($_FILES["Fichier1"]["name"]);
//            $extension = $path_parts['extension'];
            if (move_uploaded_file($_FILES["Fichier1"]["tmp_name"], $target_file)) {
                $flag = true;
            } else {
                $flag = false;
                $data['error'] = "Sorry, there was an error uploading your file.";
            }
        }
        if($flag){
            redirect(base_url()."admin/list-upload-file");
        }else {
            $this->load->view('admin/upload-file/uploads-file', $data);
        }
    }
    public function listUploadFile(){
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language')
        );
        $this->load->view('admin/upload-file/list-uploads-file', $data);
    }
    public function searchUploadFile(){
        $dir    = 'assets/images/uploads/';
        $files = scandir($dir);
        $data = array();
        for($i = 2 ; $i < count($files) ; $i++){
            $type = "Undefine";
            if(strpos($files[$i],'.jpg') !== false){
                $type = 'jpg';
            }else if(strpos($files[$i],'.doc') !== false){
                $type = 'doc';
            }else if(strpos($files[$i],'.docx') !== false){
                $type = 'docx';
            }else if(strpos($files[$i],'.xls') !== false){
                $type = 'xls';
            }else if(strpos($files[$i],'.xlsx') !== false){
                $type = 'xlsx';
            }else if(strpos($files[$i],'.ppt') !== false){
                $type = 'ppt';
            }else if(strpos($files[$i],'.pptx') !== false){
                $type = 'pptx';
            }else if(strpos($files[$i],'.pdf') !== false){
                $type = 'pdf';
            }else if(strpos($files[$i],'.swf') !== false){
                $type = 'swf';
            }else if(strpos($files[$i],'.png') !== false){
                $type = 'png';
            }else if(strpos($files[$i],'.jpeg') !== false) {
                $type = 'jpeg';
            }else if(strpos($files[$i],'.gif') !== false) {
                $type = 'gif';
            }
            $data[] = array(
                'link'=>$dir.$files[$i],
                'type'=>$type,
                'image'=>(in_array($type,array('png','jpeg','gif','jpg')))?'<img style="max-width: 120px;" src="/'.$dir.$files[$i].'"/>':'',
            );
        }
        echo json_encode($data);
    }
}
