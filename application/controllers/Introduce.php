<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Introduce extends CI_Controller
{
    public function show($page)
    {
        $this->load->library('data');
        //$this->load->library('checkdata');
        $data = $this->data->mainInfo();
        $data = $this->data->aboutMenuInfo($data,$page);
        $data['currentPage'] = $page;
        $type = $data['currentType'];
        switch ($type) {
            case 'about-news':
                $data = $this->data->aboutNewsInfo($data,$page);
                break;
			case 'about-revenues':
				$data = $this->data->aboutRevenue($data,$page);
				break;
            case 'about-model':
                $data = $this->data->aboutModelInfo($data);
                break;
            case 'about-member':
                $data = $this->data->aboutMemberInfo($data);
                break;
            case 'about-us':
            default:
                $data = $this->data->aboutUsInfo($data);
                break;
        }
        $this->load->view('_partial/content/introduce/show', $data);
    }
}
