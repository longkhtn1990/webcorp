<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller
{
    public function show($page)
    {
        $this->load->library('data');
        //$this->load->library('checkdata');
        $data = $this->data->mainInfo();
        $data = $this->data->projectMenuInfo($data,$page);
        $data['currentPage'] = $page;
        $data = $this->data->projectContentInfo($data,$page);
        $this->load->view('_partial/content/project/show', $data);
    }

    public function showDetail($page)
    {
        $this->load->library('data');
        //$this->load->library('checkdata');
        $data = $this->data->mainInfo();
        $data = $this->data->projectDetailMenuInfo($data,$page);
        $data['currentPage'] = $page;
        $data = $this->data->projectDetailContentInfo($data,$page);
        $this->load->view('_partial/content/project/show-detail', $data);
    }
}
