<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL
        $this->load->model('user_model');
        //LIBRARY
        $this->load->library('data');
        $this->load->library('common');
        $this->load->library('utils');
        $this->load->helper('captcha');
    }
    public function show()
    {
        $this->load->library('data');
        //$this->load->library('checkdata');
        $data = $this->data->mainInfo();
        $data = $this->data->contactMenuInfo($data);
        $data = $this->data->contactContentInfo($data);

        $captcha = create_captcha($this->config->item("config_captcha"));
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaCode');
        $this->session->set_userdata('captchaCode',$captcha['word']);

        $data['captcha'] = $captcha;
        $this->load->view('_partial/content/contact/show', $data);
    }
    public function sendContact(){
        $parameters = $this->input->post('parameters');
        $data = array(
            'status' => true,
            'message' => 'Success',
            'error_code' => 200
        );
//        $mail = array('dinhduongtin35b@gmail.com');
        $inputCaptcha = $parameters['captcha'];
        $sessCaptcha = $this->session->userdata('captchaCode');
        if($inputCaptcha === $sessCaptcha) {
            $mail = array($this->config->item('email_contact'));
            $sub = 'Liên hệ mới';
            $content = 'Có 1 liên hệ mới:<br>';
            $content .= 'Tiêu đề liên hệ:' . $parameters['title'] . '<br>';
            $content .= 'Email của bạn:' . $parameters['email'] . '<br>';
            $content .= 'Số điện thoại của bạn:' . $parameters['phone'] . '<br>';
            $content .= 'Nội dung liên hệ:' . $parameters['content'] . '<br>';
            $send = $this->utils->sendEmail($mail, $sub, $content);
            if ($send !== TRUE) {
                $data = array(
                    'status' => false,
                    'message' => $send,
                    'error_code' => 202
                );
            }
        }else{
            $data = array(
                'status' => false,
                'message' => 'Captcha wrong.',
                'error_code' => 207
            );
        }
        echo json_encode($data);
    }
    public function refreshCaptcha(){
        echo $this->utils->refresh();
    }
}