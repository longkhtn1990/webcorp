<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Save extends CI_Controller
{
    private $approve_2 = 'approved_1';
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL
        $this->load->model('main_model');
        $this->load->model('project_model');
        $this->load->model('business_model');
        $this->load->model('news_model');
        $this->load->model('partner_model');
        $this->load->model('introduce_model');
        $this->load->model('introduce_about_model');
        $this->load->model('introduce_about_awards_model');
        $this->load->model('introduce_about_awards_article_model');
        $this->load->model('introduce_about_core_model');
        $this->load->model('introduce_about_core_article_model');
        $this->load->model('introduce_about_network_model');
        $this->load->model('introduce_about_social_model');
        $this->load->model('introduce_about_system_management_model');
        $this->load->model('introduce_about_system_management_article_model');
        $this->load->model('introduce_model_culture_model');
        $this->load->model('introduce_model_culture_member_model');
        $this->load->model('introduce_model_stories_model');
        $this->load->model('introduce_model_stories_member_model');
        $this->load->model('introduce_news_model');
        $this->load->model('user_model');
        $this->load->model('career_job_model');
        $this->load->model('contact_model');

		$this->load->model('introduce_member_culture_model');
		$this->load->model('introduce_member_culture_member_model');
		$this->load->model('introduce_member_stories_model');
		$this->load->model('introduce_member_stories_member_model');
		$this->load->model('introduce_revenue_model');
        //LIBRARY
        $this->load->library('data');
        $this->load->library('common');
        $this->load->library('utils');
    }

    public function mainInfo($selectLanguage,$user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }
        $data = array(
            'domain' => 'http://webcorp.dev',
            'language' => $this->lang->line('language')
        );
        $this->load->view('admin/main_info', $data);
    }


    #region MAIN SLIDE
    public function saveMainSlideImage()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,11)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $banner_slide_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->main_model->update_banner_slide_image($banner_slide_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $banner_slide_id = $this->main_model->save_banner_slide_image($data);
            }

            if($banner_slide_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'banner-'.$banner_slide_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'banner-'.$banner_slide_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->main_model->update_banner_slide_image($banner_slide_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $banner_slide_id));die();
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveMainImage(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->main_model->update_banner_slide_image($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyMainImage(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->main_model->update_banner_slide_image($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('main-slide', $recordId);
            }
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteMainImage(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->main_model->update_banner_slide_image($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region PROJECT
    public function saveProject()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,8)] = $parameter_value;
            }
            $image_value = $data['banner_image_file_value'];
            $image_type = $data['banner_image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['banner_image_file_value']);
            unset($data['banner_image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->project_model->update_project($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->project_model->save_project($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'project-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'project-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->project_model->update_project($project_id,array('banner_image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveProject(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->project_model->update_project($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyProject(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->project_model->update_project($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('project', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteProject(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->project_model->update_project($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region PROJECT DETAIL
    public function saveProjectDetail()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,15)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->project_model->update_project_detail($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->project_model->save_project_detail($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'project-detail-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'project-detail-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->project_model->update_project_detail($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveProjectDetail(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->project_model->update_project_detail($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyProjectDetail(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->project_model->update_project_detail($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('project-detail', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteProjectDetail(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->project_model->update_project_detail($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region BUSINESS
    public function saveBusiness()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,9)] = $parameter_value;
            }
            $banner_image_value = $data['banner_image_file_value'];
            $banner_image_type = $data['banner_image_file_type'];
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['banner_image_file_value']);
            unset($data['banner_image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->business_model->update_business($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->business_model->save_business($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'business-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'business-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->business_model->update_business($project_id,array('image' => $link_image));
            }
            if($project_id != '' && $banner_image_value != null && $banner_image_type != null){
                if($banner_image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $banner_image_value);
                    $image_name = 'business-banner-'.$project_id.'.png' ;
                }else if($banner_image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $banner_image_value);
                    $image_name = 'business-banner-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->business_model->update_business($project_id,array('banner_image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveBusiness(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->business_model->update_business($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyBusiness(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->business_model->update_business($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('business', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteBusiness(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->business_model->update_business($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region NEWS
    public function saveNews()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,5)] = $parameter_value;
            }
            $banner_image_value = $data['banner_image_file_value'];
            $banner_image_type = $data['banner_image_file_type'];
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['banner_image_file_value']);
            unset($data['banner_image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->news_model->update_news($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->news_model->save_news($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'news-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'news-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->news_model->update_news($project_id,array('image' => $link_image));
            }
            if($project_id != '' && $banner_image_value != null && $banner_image_type != null){
                if($banner_image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $banner_image_value);
                    $image_name = 'news-banner-'.$project_id.'.png' ;
                }else if($banner_image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $banner_image_value);
                    $image_name = 'news-banner-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->news_model->update_news($project_id,array('banner_image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveNews(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->news_model->update_news($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyNews(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->news_model->update_news($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('news', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteNews(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->news_model->update_news($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region PARTNER GLOBAL
    public function savePartnerGlobal()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,15)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->partner_model->update_partner_global($data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->partner_model->save_partner_global($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'partner-global-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'partner-global-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->partner_model->update_partner_global(array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactivePartnerGlobal(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->partner_model->update_partner_global($data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    #endregion

    #region PARTNER CUSTOMER
    public function savePartnerCustomer()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,17)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['banner_image_file_value']);
            unset($data['banner_image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->partner_model->update_partner_customer($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->partner_model->save_partner_customer($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'partner-customer-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'partner-customer-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->partner_model->update_partner_customer($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactivePartnerCustomer(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->partner_model->update_partner_customer($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyPartnerCustomer(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->partner_model->update_partner_customer($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('partner-customer', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deletePartnerCustomer(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->partner_model->update_partner_customer($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region PARTNER
    public function savePartner()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,8)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->partner_model->update_partner($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->partner_model->save_partner($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'partner-partner-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'partner-partner-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->partner_model->update_partner($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactivePartner(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->partner_model->update_partner($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyPartner(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->partner_model->update_partner($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('partner', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deletePartner(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->partner_model->update_partner($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region INTRODUCE
    public function saveIntroduce()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,10)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_model->update_introduce($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_model->save_introduce($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'introduce-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'introduce-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_model->update_introduce($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduce(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_model->update_introduce($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduce(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_model->update_introduce($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteIntroduce(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->introduce_model->update_introduce($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region introduce about
    public function saveIntroduceAbout(){
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,16)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_model->update_introduce_about($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_model->save_introduce_about($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceAbout(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_about_model->update_introduce_about($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceAbout(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_about_model->update_introduce_about($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-about', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region introduce about awards
    public function saveIntroduceAboutAwards()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,23)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_awards_model->update_introduce_about_awards($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_awards_model->save_introduce_about_awards($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceAboutAwards(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_about_awards_model->update_introduce_about_awards($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceAboutAwards(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_about_awards_model->update_introduce_about_awards($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-about-awards', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion saveIntroduceAbout

    #region introduce about awards article
    public function saveIntroduceAboutAwardsArticle()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,31)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_awards_article_model->update_introduce_about_awards_article($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_awards_article_model->save_introduce_about_awards_article($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'introduce-aaa-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'introduce-aaa-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_about_awards_article_model->update_introduce_about_awards_article($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceAboutAwardsArticle(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_about_awards_article_model->update_introduce_about_awards_article($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceAboutAwardsArticle(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_about_awards_article_model->update_introduce_about_awards_article($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-about-awards-article', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteIntroduceAboutAwardsArticle(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->introduce_about_awards_article_model->update_introduce_about_awards_article($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion saveIntroduceAbout

    #region introduce about core
    public function saveIntroduceAboutCore()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,21)] = $parameter_value;
            }
            $banner_image_value = $data['banner_image_file_value'];
            $banner_image_type = $data['banner_image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['banner_image_file_value']);
            unset($data['banner_image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_core_model->update_introduce_about_core($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_core_model->save_introduce_about_core($data);
            }

            if($project_id != '' && $banner_image_value != null && $banner_image_type != null){
                if($banner_image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $banner_image_value);
                    $image_name = 'introduce-bout-core-banner-'.$project_id.'.png' ;
                }else if($banner_image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $banner_image_value);
                    $image_name = 'introduce-bout-core-banner-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_about_core_model->update_introduce_about_core($project_id,array('banner_image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceAboutCore(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_about_core_model->update_introduce_about_core($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceAboutCore(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_about_core_model->update_introduce_about_core($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-about-core', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion saveIntroduceAbout

    #region introduce about social
    public function saveIntroduceAboutSocial()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,23)] = $parameter_value;
            }
            $banner_image_value = $data['banner_image_file_value'];
            $banner_image_type = $data['banner_image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['banner_image_file_value']);
            unset($data['banner_image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_social_model->update_introduce_about_social($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_social_model->save_introduce_about_social($data);
            }
            if($project_id != '' && $banner_image_value != null && $banner_image_type != null){
                if($banner_image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $banner_image_value);
                    $image_name = 'introduce-about-social-banner-'.$project_id.'.png' ;
                }else if($banner_image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $banner_image_value);
                    $image_name = 'introduce-about-social-banner-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_about_social_model->update_introduce_about_social($project_id,array('banner_image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceAboutSocial(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_about_social_model->update_introduce_about_social($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceAboutSocial(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_about_social_model->update_introduce_about_social($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-about-social', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion saveIntroduceAbout

    #region introduce about system management
    public function saveIntroduceAboutSystemManagement()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,34)] = $parameter_value;
            }
            $banner_image_value = $data['banner_image_file_value'];
            $banner_image_type = $data['banner_image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['banner_image_file_value']);
            unset($data['banner_image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_system_management_model->update_introduce_about_system_management($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_system_management_model->save_introduce_about_system_management($data);
            }
            if($project_id != '' && $banner_image_value != null && $banner_image_type != null){
                if($banner_image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $banner_image_value);
                    $image_name = 'introduce-about-system-management-banner-'.$project_id.'.png' ;
                }else if($banner_image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $banner_image_value);
                    $image_name = 'introduce-about-system-management-banner-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_about_system_management_model->update_introduce_about_system_management($project_id,array('banner_image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceAboutSystemManagement(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_about_system_management_model->update_introduce_about_system_management($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceAboutSystemManagement(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_about_system_management_model->update_introduce_about_system_management($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-about-system-management', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion saveIntroduceAbout

    #region introduce about system management article
    public function saveIntroduceAboutSystemManagementArticle()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,42)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_system_management_article_model->update_introduce_about_system_management_article($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_system_management_article_model->save_introduce_about_system_management_article($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'introduce-asma-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'introduce-asma-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_about_system_management_article_model->update_introduce_about_system_management_article($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceAboutSystemManagementArticle(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_about_system_management_article_model->update_introduce_about_system_management_article($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceAboutSystemManagementArticle(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_about_system_management_article_model->update_introduce_about_system_management_article($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-about-system-management-article', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteIntroduceAboutSystemManagementArticle(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->introduce_about_system_management_article_model->update_introduce_about_system_management_article($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion saveIntroduceAbout

    #region introduce model culture
    public function saveIntroduceModelCulture()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,24)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_model_culture_model->update_introduce_model_culture($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_model_culture_model->save_introduce_model_culture($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceModelCulture(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_model_culture_model->update_introduce_model_culture($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceModelCulture(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_model_culture_model->update_introduce_model_culture($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-model-culture', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion saveIntroduceAbout

    #region introduce model culture member
    public function saveIntroduceModelCultureMember()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,31)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_model_culture_member_model->update_introduce_model_culture_member($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_model_culture_member_model->save_introduce_model_culture_member($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'introduce-asma-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'introduce-asma-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_model_culture_member_model->update_introduce_model_culture_member($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceModelCultureMember(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_model_culture_member_model->update_introduce_model_culture_member($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceModelCultureMember(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_model_culture_member_model->update_introduce_model_culture_member($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-model-culture-member', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteIntroduceModelCultureMember(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->introduce_model_culture_member_model->update_introduce_model_culture_member($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion saveIntroduceAbout

    #region introduce model stories
    public function saveIntroduceModelStories()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,24)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_model_stories_model->update_introduce_model_stories($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_model_stories_model->save_introduce_model_stories($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceModelStories(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_model_stories_model->update_introduce_model_stories($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceModelStories(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_model_stories_model->update_introduce_model_stories($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-model-stories', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion saveIntroduceAbout

    #region introduce core article
    public function saveIntroduceAboutCoreArticle()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,29)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_core_article_model->update_introduce_about_core_article($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_core_article_model->save_introduce_about_core_article($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceAboutCoreArticle(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_about_core_article_model->update_introduce_about_core_article($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceAboutCoreArticle(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_about_core_article_model->update_introduce_about_core_article($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-about-core-article', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteIntroduceAboutCoreArticle(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->introduce_about_core_article_model->update_introduce_about_core_article($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region introduce about network
    public function saveIntroduceAboutNetwork()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,24)] = $parameter_value;
            }
            $image_1_value = $data['image_1_file_value'];
            $image_1_type = $data['image_1_file_type'];
            $image_2_value = $data['image_2_file_value'];
            $image_2_type = $data['image_2_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_1_file_value']);
            unset($data['image_1_file_type']);
            unset($data['image_2_file_value']);
            unset($data['image_2_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_network_model->update_introduce_about_network($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_network_model->save_introduce_about_network($data);
            }

            if($project_id != '' && $image_1_type != null && $image_1_value != null){
                if($image_1_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_1_value);
                    $image_name = 'introduce-about-network-1-'.$project_id.'.png' ;
                }else if($image_1_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_1_value);
                    $image_name = 'introduce-about-network-1-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_about_network_model->update_introduce_about_network($project_id,array('image_1' => $link_image));
            }
            if($project_id != '' && $image_2_type != null && $image_2_value != null){
                if($image_2_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_2_value);
                    $image_name = 'introduce-about-network-2-'.$project_id.'.png' ;
                }else if($image_2_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_2_value);
                    $image_name = 'introduce-about-network-2-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_about_network_model->update_introduce_about_network($project_id,array('image_2' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceAboutNetwork(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_about_network_model->update_introduce_about_network($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceAboutNetwork(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_about_network_model->update_introduce_about_network($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-about-network', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteIntroduceAboutNetwork(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->introduce_about_network_model->update_introduce_about_network($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region introduce model stories member
    public function saveIntroduceModelStoriesMember()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,31)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_model_stories_member_model->update_introduce_model_stories_member($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_model_stories_member_model->save_introduce_model_stories_member($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'introduce-model-stories-member-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'introduce-model-stories-member-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_model_stories_member_model->update_introduce_model_stories_member($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceModelStoriesMember(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_model_stories_member_model->update_introduce_model_stories_member($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceModelStoriesMember(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_model_stories_member_model->update_introduce_model_stories_member($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-model-stories-member', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteIntroduceModelStoriesMember(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->introduce_model_stories_member_model->update_introduce_model_stories_member($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region introduce news
    public function saveIntroduceNews()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,15)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_news_model->update_introduce_news($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_news_model->save_introduce_news($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'introduce-news-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'introduce-news-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_news_model->update_introduce_news($project_id,array('image' => $link_image));
            }

            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduceNews(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_news_model->update_introduce_news($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduceNews(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approve' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status']
            );
            $this->introduce_news_model->update_introduce_news($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('introduce-news', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteIntroduceNews(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->introduce_news_model->update_introduce_news($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region NEWS DOWNLOAD
    public function saveNewsDownload()
    {
        $parameters = json_decode($this->input->post('parameters'),true);
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,14)] = $parameter_value;
            }
            $currentUser = $data['current_user'];

            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);

            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->news_model->update_news_download($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->news_model->save_news_download($data);
            }

            if($project_id != '' && count($_FILES) > 0){
                $uploadDir = 'assets/file/new-download/';
                $fileName = '';
                foreach($_FILES as $file){
                    if(move_uploaded_file($file['tmp_name'], $uploadDir .basename($file['name']))){
                        $files[] = array(
                            'file' => $uploadDir .$file['name'],
                            'file_name' => $file['name']
                        );
                        $fileName = $file['name'];
                    }
                }
                if($fileName != '') {
                    $this->news_model->update_news_download($project_id, array('file' => $uploadDir . $fileName));
                }
            }

            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveNewsDownload(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->news_model->update_news_download($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyNewsDownload(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status_approve']
            );
            $this->news_model->update_news_download($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('news-download', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteNewsDownload(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->news_model->update_news_download($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region MAIN INFO
    public function saveMainInfo(){
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,10)] = $parameter_value;
            }
            $image_value = $data['logo_file_value'];
            $image_type = $data['logo_file_type'];
            $currentUser = $data['current_user'];
            unset($data['logo_file_value']);
            unset($data['logo_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                $this->main_model->update_main_info($project_id,$data);
            }else{
                $project_id = $this->main_model->save_main_info($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'logo.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'logo.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/' . $image_name;
                $link_image = 'assets/images/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->main_model->update_main_info($project_id,array('logo' => $link_image));
            }

            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion

    #region MAIN MENU
    public function saveMainMenu(){
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,10)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];

            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);

            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                $this->main_model->update_main_menu($project_id,$data);
            }else{
                $project_id = $this->main_model->save_main_menu($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'main-menu-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'main-menu-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->main_model->update_main_menu($project_id,array('image' => $link_image));
            }

            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    public function deleteMainMenu(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'deleted' => 1
            );
            $this->main_model->update_main_menu($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    public function activeOrInactiveMainMenu(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'status' => $parameters['status']
            );
            $this->main_model->update_main_menu($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region NEWS INTRODUCE
    public function saveNewsIntroduce()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,15)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);

            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->news_model->update_news_introduce($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->news_model->save_news_introduce($data);
            }

            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveNewsIntroduce(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->news_model->update_news_introduce($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyNewsIntroduce(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status_approve']
            );
            $this->news_model->update_news_introduce($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('news-introduce', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteNewsIntroduce(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->news_model->update_news_introduce($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region CAREER JOB
    public function saveCareerJob()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,11)] = $parameter_value;
            }
            $banner_value = $data['banner_file_value'];
            $banner_type = $data['banner_file_type'];
            $currentUser = $data['current_user'];
            unset($data['banner_file_value']);
            unset($data['banner_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_expired'] = $this->utils->convertDateDB($data['date_expired']);

            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->career_job_model->update_career_job($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->career_job_model->save_career_job($data);
            }
            if($project_id != '' && $banner_type != null && $banner_value != null){
                if($banner_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $banner_value);
                    $image_name = 'banner-career-'.$project_id.'.png' ;
                }else if($banner_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $banner_value);
                    $image_name = 'banner-career-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->career_job_model->update_career_job($project_id,array('banner' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveCareerJob(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->career_job_model->update_career_job($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyCareerJob(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approved' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status_approve']
            );
            $this->career_job_model->update_career_job($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            if($parameters['status'] == $this->approve_2) {
                $this->sendingApprove2('news-introduce', $recordId);
            }
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteCareerJob(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->career_job_model->update_career_job($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region NEW SETTING
    public function saveNewSetting()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,12)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);

            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                $data['id'] = $data['link'];
                unset($data['link']);
                $this->news_model->update_news_setting($project_id,$data);
            }else{
                $data['id'] = $data['link'];
                unset($data['link']);
                $project_id = $this->news_model->save_news_setting($data);
                if($project_id !== false){
                    $project_id = $data['id'];
                }
            }

            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveNewSetting(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'status' => $parameters['status']
            );
            $this->news_model->update_news_setting($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region CONTACT
    public function saveContact(){
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,8)] = $parameter_value;
            }
            $banner_value = $data['banner_file_value'];
            $banner_type = $data['banner_file_type'];
            unset($data['banner_file_value']);
            unset($data['banner_file_type']);
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                $this->contact_model->update_contact($project_id,$data);
            }else{
                $project_id = $this->contact_model->save_contact($data);
            }
            if($project_id != '' && $banner_type != null && $banner_value != null){
                if($banner_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $banner_value);
                    $image_name = 'banner-contact'.$project_id.'.png' ;
                }else if($banner_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $banner_value);
                    $image_name = 'banner-contact'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->contact_model->update_contact($project_id,array('banner' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion


	#region introduce member culture
	public function saveIntroduceMemberCulture()
	{
		$parameters = $this->input->post('parameters');
		$parameters = $this->utils->getFieldData($parameters,'value','name');
		if(count($parameters) > 0){
			$data = array();
			foreach($parameters AS $parameter_key =>  $parameter_value){
				$data[substr($parameter_key,24)] = $parameter_value;
			}
			$currentUser = $data['current_user'];
			unset($data['current_user']);
			unset($data['current_language']);
			$data['modified_by'] = $currentUser;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$data['date_display'] = $this->utils->convertDateDB($data['date_display']);
			if($data['id'] != '' && $data['id'] != null){
				$project_id = $data['id'];
				if($data['date_approved'] == null || $data['date_approved'] == '') {
					if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
						$data['date_approved'] = date('Y-m-d H:i:s');
					}
				}
				$this->introduce_member_culture_model->update_introduce_member_culture($project_id,$data);
			}else{
				$data['created_by'] = $currentUser;
				$data['date_entered'] = date('Y-m-d H:i:s');
				$project_id = $this->introduce_member_culture_model->save_introduce_member_culture($data);
			}
			echo json_encode(array('error_code' => 1,'record' => $project_id));
		}else{
			echo json_encode(array('error_code' => 2,'record' => null));
		}
	}

	public function activeOrInactiveIntroduceMemberCulture(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'status' => $parameters['status']
			);
			$this->introduce_member_culture_model->update_introduce_member_culture($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}

	public function approveOrDenyIntroduceMemberCulture(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'date_approved' => date('Y-m-d H:i:s'),
				'approver' => $currentUser,
				'status_approve' => $parameters['status']
			);
			$this->introduce_member_culture_model->update_introduce_member_culture($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			if($parameters['status'] == $this->approve_2) {
				$this->sendingApprove2('introduce-member-culture', $recordId);
			}
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}
	#endregion saveIntroduceAbout

	#region introduce member culture member
	public function saveIntroduceMemberCultureMember()
	{
		$parameters = $this->input->post('parameters');
		$parameters = $this->utils->getFieldData($parameters,'value','name');
		if(count($parameters) > 0){
			$data = array();
			foreach($parameters AS $parameter_key =>  $parameter_value){
				$data[substr($parameter_key,31)] = $parameter_value;
			}
			$image_value = $data['image_file_value'];
			$image_type = $data['image_file_type'];
			$currentUser = $data['current_user'];
			unset($data['image_file_value']);
			unset($data['image_file_type']);
			unset($data['current_user']);
			unset($data['current_language']);
			$data['modified_by'] = $currentUser;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$data['date_display'] = $this->utils->convertDateDB($data['date_display']);
			if($data['id'] != '' && $data['id'] != null){
				$project_id = $data['id'];
				if($data['date_approved'] == null || $data['date_approved'] == '') {
					if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
						$data['date_approved'] = date('Y-m-d H:i:s');
					}
				}
				$this->introduce_member_culture_member_model->update_introduce_member_culture_member($project_id,$data);
			}else{
				$data['created_by'] = $currentUser;
				$data['date_entered'] = date('Y-m-d H:i:s');
				$project_id = $this->introduce_member_culture_member_model->save_introduce_member_culture_member($data);
			}

			if($project_id != '' && $image_type != null && $image_value != null){
				if($image_type == 'image/png'){
					$image = str_replace('data:image/png;base64,', '', $image_value);
					$image_name = 'introduce-asma-'.$project_id.'.png' ;
				}else if($image_type == 'image/jpeg'){
					$image = str_replace('data:image/jpeg;base64,', '', $image_value);
					$image_name = 'introduce-asma-'.$project_id.'.jpg' ;
				}else{
					$image = null;
					$image_name = null;
				}
				$img = str_replace(' ', '+', $image);
				$url_save = APPPATH . '/../assets/images/upload/' . $image_name;
				$link_image = 'assets/images/upload/' . $image_name;

				$this->utils->saveImage($img,$url_save);
				$this->introduce_member_culture_member_model->update_introduce_member_culture_member($project_id,array('image' => $link_image));
			}
			echo json_encode(array('error_code' => 1,'record' => $project_id));
		}else{
			echo json_encode(array('error_code' => 2,'record' => null));
		}
	}

	public function activeOrInactiveIntroduceMemberCultureMember(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'status' => $parameters['status']
			);
			$this->introduce_member_culture_member_model->update_introduce_member_culture_member($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}

	public function approveOrDenyIntroduceMemberCultureMember(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'date_approved' => date('Y-m-d H:i:s'),
				'approver' => $currentUser,
				'status_approve' => $parameters['status']
			);
			$this->introduce_member_culture_member_model->update_introduce_member_culture_member($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			if($parameters['status'] == $this->approve_2) {
				$this->sendingApprove2('introduce-member-culture-member', $recordId);
			}
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}

	public function deleteIntroduceMemberCultureMember(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'deleted' => 1
			);
			$this->introduce_member_culture_member_model->update_introduce_member_culture_member($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			die();
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}
	#endregion saveIntroduceAbout

	#region introduce member stories
	public function saveIntroduceMemberStories()
	{
		$parameters = $this->input->post('parameters');
		$parameters = $this->utils->getFieldData($parameters,'value','name');
		if(count($parameters) > 0){
			$data = array();
			foreach($parameters AS $parameter_key =>  $parameter_value){
				$data[substr($parameter_key,24)] = $parameter_value;
			}
			$currentUser = $data['current_user'];
			unset($data['current_user']);
			unset($data['current_language']);
			$data['modified_by'] = $currentUser;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$data['date_display'] = $this->utils->convertDateDB($data['date_display']);
			if($data['id'] != '' && $data['id'] != null){
				$project_id = $data['id'];
				if($data['date_approved'] == null || $data['date_approved'] == '') {
					if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
						$data['date_approved'] = date('Y-m-d H:i:s');
					}
				}
				$this->introduce_member_stories_model->update_introduce_member_stories($project_id,$data);
			}else{
				$data['created_by'] = $currentUser;
				$data['date_entered'] = date('Y-m-d H:i:s');
				$project_id = $this->introduce_member_stories_model->save_introduce_member_stories($data);
			}
			echo json_encode(array('error_code' => 1,'record' => $project_id));
		}else{
			echo json_encode(array('error_code' => 2,'record' => null));
		}
	}

	public function activeOrInactiveIntroduceMemberStories(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'status' => $parameters['status']
			);
			$this->introduce_member_stories_model->update_introduce_member_stories($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}

	public function approveOrDenyIntroduceMemberStories(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'date_approved' => date('Y-m-d H:i:s'),
				'approver' => $currentUser,
				'status_approve' => $parameters['status']
			);
			$this->introduce_member_stories_model->update_introduce_member_stories($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			if($parameters['status'] == $this->approve_2) {
				$this->sendingApprove2('introduce-member-stories', $recordId);
			}
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}
	#endregion saveIntroduceAbout

	#region introduce member stories member
	public function saveIntroduceMemberStoriesMember()
	{
		$parameters = $this->input->post('parameters');
		$parameters = $this->utils->getFieldData($parameters,'value','name');
		if(count($parameters) > 0){
			$data = array();
			foreach($parameters AS $parameter_key =>  $parameter_value){
				$data[substr($parameter_key,31)] = $parameter_value;
			}
			$image_value = $data['image_file_value'];
			$image_type = $data['image_file_type'];
			$currentUser = $data['current_user'];
			unset($data['image_file_value']);
			unset($data['image_file_type']);
			unset($data['current_user']);
			unset($data['current_language']);
			$data['modified_by'] = $currentUser;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$data['date_display'] = $this->utils->convertDateDB($data['date_display']);
			if($data['id'] != '' && $data['id'] != null){
				$project_id = $data['id'];
				if($data['date_approved'] == null || $data['date_approved'] == '') {
					if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
						$data['date_approved'] = date('Y-m-d H:i:s');
					}
				}
				$this->introduce_member_stories_member_model->update_introduce_member_stories_member($project_id,$data);
			}else{
				$data['created_by'] = $currentUser;
				$data['date_entered'] = date('Y-m-d H:i:s');
				$project_id = $this->introduce_member_stories_member_model->save_introduce_member_stories_member($data);
			}

			if($project_id != '' && $image_type != null && $image_value != null){
				if($image_type == 'image/png'){
					$image = str_replace('data:image/png;base64,', '', $image_value);
					$image_name = 'introduce-member-stories-member-'.$project_id.'.png' ;
				}else if($image_type == 'image/jpeg'){
					$image = str_replace('data:image/jpeg;base64,', '', $image_value);
					$image_name = 'introduce-member-stories-member-'.$project_id.'.jpg' ;
				}else{
					$image = null;
					$image_name = null;
				}
				$img = str_replace(' ', '+', $image);
				$url_save = APPPATH . '/../assets/images/upload/' . $image_name;
				$link_image = 'assets/images/upload/' . $image_name;

				$this->utils->saveImage($img,$url_save);
				$this->introduce_member_stories_member_model->update_introduce_member_stories_member($project_id,array('image' => $link_image));
			}
			echo json_encode(array('error_code' => 1,'record' => $project_id));
		}else{
			echo json_encode(array('error_code' => 2,'record' => null));
		}
	}

	public function activeOrInactiveIntroduceMemberStoriesMember(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'status' => $parameters['status']
			);
			$this->introduce_member_stories_member_model->update_introduce_member_stories_member($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}

	public function approveOrDenyIntroduceMemberStoriesMember(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'date_approved' => date('Y-m-d H:i:s'),
				'approver' => $currentUser,
				'status_approve' => $parameters['status']
			);
			$this->introduce_member_stories_member_model->update_introduce_member_stories_member($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			if($parameters['status'] == $this->approve_2) {
				$this->sendingApprove2('introduce-member-stories-member', $recordId);
			}
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}

	public function deleteIntroduceMemberStoriesMember(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'deleted' => 1
			);
			$this->introduce_member_stories_member_model->update_introduce_member_stories_member($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			die();
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}
	#endregion

	#region introduce revenue
	public function saveIntroduceRevenue()
	{
		$parameters = $this->input->post('parameters');
		$parameters = $this->utils->getFieldData($parameters,'value','name');
		if(count($parameters) > 0){
			$data = array();
			foreach($parameters AS $parameter_key =>  $parameter_value){
				$data[substr($parameter_key,24)] = $parameter_value;
			}
			$currentUser = $data['current_user'];
			unset($data['current_user']);
			unset($data['current_language']);
			$data['modified_by'] = $currentUser;
			$data['date_modified'] = date('Y-m-d H:i:s');
			$data['date_display'] = $this->utils->convertDateDB($data['date_display']);
			if($data['id'] != '' && $data['id'] != null){
				$project_id = $data['id'];
				if($data['date_approved'] == null || $data['date_approved'] == '') {
					if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
						$data['date_approved'] = date('Y-m-d H:i:s');
					}
				}
				$this->introduce_revenue_model->update_introduce_revenue($project_id,$data);
			}else{
				$data['created_by'] = $currentUser;
				$data['date_entered'] = date('Y-m-d H:i:s');
				$project_id = $this->introduce_revenue_model->save_introduce_revenue($data);
			}
			echo json_encode(array('error_code' => 1,'record' => $project_id));
		}else{
			echo json_encode(array('error_code' => 2,'record' => null));
		}
	}

	public function activeOrInactiveIntroduceRevenue(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'status' => $parameters['status']
			);
			$this->introduce_revenue_model->update_introduce_revenue($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}

	public function approveOrDenyIntroduceRevenue(){
		$parameters = $this->input->post('parameters');
		$currentUser = $parameters['current_user'];
		$recordId = $parameters['record_id'];
		if($recordId != ''){
			$data = array(
				'modified_by' => $currentUser,
				'date_modified' => date('Y-m-d H:i:s'),
				'date_approved' => date('Y-m-d H:i:s'),
				'approver' => $currentUser,
				'status_approve' => $parameters['status']
			);
			$this->introduce_revenue_model->update_introduce_revenue($recordId,$data);
			echo json_encode(array('error_code' => 1,'record' => $recordId));
			if($parameters['status'] == $this->approve_2) {
				$this->sendingApprove2('introduce-member-culture', $recordId);
			}
			return;
		}
		echo json_encode(array('error_code' => 2,'record' => null));
	}
	#endregion saveIntroduceAbout

    public function sendingApprove2($module,$id){
        $listMail = $this->user_model->get_list_user_permission(APPROVED_2);
        $data = array(
            'status' => true,
            'message' => 'Success',
            'error_code' => 200
        );
        $send = true;
        switch ($module){
            case "news":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->news_model->update_news($id,array('status'=>'active'));
                }
                break;
            case "main-slide":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->main_model->update_banner_slide_image($id,array('status'=>'active'));
                }
                break;
            case "project":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->project_model->update_project($id,array('status'=>'active'));
                }
                break;
            case "project-detail":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->project_model->update_project_detail($id,array('status'=>'active'));
                }
                break;
            case "business":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->business_model->update_business($id,array('status'=>'active'));
                }
                break;
            case "partner-customer":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->partner_model->update_partner_customer($id,array('status'=>'active'));
                }
                break;
            case "partner":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->partner_model->update_partner($id,array('status'=>'active'));
                }
                break;
            case "partner-global":
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->partner_model->update_partner_global($id,array('status'=>'active'));
                }
                break;
            case "introduce":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_model->update_introduce($id,array('status'=>'active'));
                }
                break;
            case "introduce-about":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_model->update_introduce_about($id,array('status'=>'active'));
                }
                break;
            case "introduce-about-awards":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_awards_model->update_introduce_about_awards($id,array('status'=>'active'));
                }
                break;
            case "introduce-about-awards-article":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_awards_article_model->update_introduce_about_awards_article($id,array('status'=>'active'));
                }
                break;
            case "introduce-about-core":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_core_model->update_introduce_about_core($id,array('status'=>'active'));
                }
                break;
            case "introduce-about-social":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_social_model->update_introduce_about_social($id,array('status'=>'active'));
                }
                break;
            case "introduce-about-system-management":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_system_management_model->update_introduce_about_system_management($id,array('status'=>'active'));
                }
                break;
            case "introduce-model-culture":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_model_culture_model->update_introduce_model_culture($id,array('status'=>'active'));
                }
                break;
            case "introduce-model-stories":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_model_stories_model->update_introduce_model_stories($id,array('status'=>'active'));
                }
                break;
            case "introduce-about-core-article":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_core_article_model->update_introduce_about_core_article($id,array('status'=>'active'));
                }
                break;
            case "introduce-about-network":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_about_network_model->update_introduce_about_network($id,array('status'=>'active'));
                }
                break;
            case "introduce-model-stories-member":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_model_stories_member_model->update_introduce_model_stories_member($id,array('status'=>'active'));
                }
                break;
            case "introduce-news":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->introduce_news_model->update_introduce_news($id,array('status'=>'active'));
                }
                break;
            case "news-download":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->news_model->update_news_download($id,array('status'=>'active'));
                }
                break;
            case "news-introduce":
                $list = $this->utils->generalNewArrayForKey($listMail,'email');
                $sub = 'Bài viết mới cần duyệt';
                $content = 'Có 1 bài viết mới cần duyệt.';
                $send = $this->utils->sendEmail($list,$sub,$content);
                if(!$send) {
                    $data = array(
                        'status' => $send,
                        'message' => 'Send fail',
                        'error_code' => 202
                    );
                }else{
                    $this->news_model->update_news_introduce($id,array('status'=>'active'));
                }
                break;
        }
        if(!$send) {
            $data = array(
                'status' => $send,
                'message' => 'Send fail',
                'error_code' => 202
            );
        }
        return $data;
    }
}
