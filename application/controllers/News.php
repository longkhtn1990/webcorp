<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL

        //LIBRARY
        $this->load->library('data');
        //$this->load->library('checkdata');
    }

    public function show()
    {
        $data = $this->data->mainInfo();
        $data = $this->data->newsMenuInfo($data);
        $data['currentPage'] = 'news-media';
        $data = $this->data->newsContentInfo($data);
        $this->load->view('_partial/content/news/show', $data);
    }

    public function showList()
    {
        $data = $this->data->mainInfo();
        $data = $this->data->newsMenuInfo($data);
        $data['currentPage'] = 'news-media';
        $data = $this->data->newsContentInfo($data);
        $this->load->view('_partial/content/news/show', $data);
    }

    public function showDetail($page)
    {
        $data = $this->data->mainInfo();
        $data = $this->data->newsMenuDetailInfo($data,$page);
        $data['currentPage'] = $page;
        $data = $this->data->newDetailContentInfo($data,$page);
        $this->load->view('_partial/content/news/show-detail', $data);
    }

    public function readMore($page)
    {
        $paging = $this->input->get('page');
        if($paging <= 0){
            $paging = 1;
        }
        $data = $this->data->mainInfo();
        $data = $this->data->newsMenuInfo($data,$page);
        $data['currentPage'] = $page;
        $data = $this->data->readMoreContentInfo($data,$page,$paging);
        $this->load->view('_partial/content/news/read-more', $data);
    }

}
