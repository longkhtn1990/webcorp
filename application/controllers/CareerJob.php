<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CareerJob extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL
        $this->load->model('user_model');
        //LIBRARY
        $this->load->library('data');
        $this->load->library('common');
        $this->load->library('utils');
    }
    public function show()
    {
        $this->load->library('data');
        //$this->load->library('checkdata');
        $data = $this->data->mainInfo();
        $data = $this->data->careerJobBannerInfo($data);
        $data = $this->data->careerJobMenuInfo($data);
        $data = $this->data->careerJobContentInfo($data,null);
        $this->load->view('_partial/content/careerjob/show', $data);
    }
    public function showSub($type)
    {
        $data = $this->data->mainInfo();
        $data = $this->data->careerJobBannerInfo($data);
        $data = $this->data->careerJobMenuInfo($data);
        $data = $this->data->careerJobContentInfo($data,$type);
        $this->load->view('_partial/content/careerjob/show', $data);
    }
    public function showDetail($id)
    {
        $data = $this->data->mainInfo();
        $data = $this->data->careerJobBannerInfo($data);
        $data = $this->data->careerJobMenuInfo($data);
        $data = $this->data->careerJobContentDetailInfo($data,$id);
        $this->load->view('_partial/content/careerjob/show-detail', $data);
    }
    public function showApply($id)
    {
        $data = $this->data->mainInfo();
        $data = $this->data->careerJobBannerInfo($data);
        $data = $this->data->careerJobMenuInfo($data);
        $data = $this->data->careerJobContentDetailInfo($data,$id);

        if($_POST){
            $uploadDir = 'assets/file/career-job/';
            $error = true;
            foreach($_FILES as $file){
                if(move_uploaded_file($file['tmp_name'], $uploadDir .basename($file['name']))){
                    $files[] = array(
                        'file' => $uploadDir .$file['name'],
                        'file_name' => $file['name']
                    );
                }
                else{
                    $error = false;
                }
            }
            if($error) {
                //$to = array(),$cc = array(),$bcc = array(), $sub = '', $content = '' ,$arrFile = array(),$sender_name = 'CNS', $reply, $reply_name = ''
                $to = array($this->config->item('email_contact'));
                $sub = 'Có Ứng Viên Mới';
                $content = 'Thông tin ứng viên :' .
                    '<br>Họ và tên : ' . $_POST['name'] .
                    '<br>Email : ' . $_POST['email'] .
                    '<br>Số điện thoại : ' . $_POST['tel'];
                $arrFile = $files;
                $sender_name = 'CNS';
                $reply = 'CNS System';
                $reply_name = 'CNS System';
                $mail = $this->utils->sendEmailMultiAttachFile($to, array(), array(), $sub, $content, $arrFile, $sender_name, $reply, $reply_name);
                if ($mail) {
                    $data['send_info_career_job'] = true;
                } else {
                    $data['send_info_career_job'] = false;
                }
            }else{
                $data['send_info_career_job'] = false;
            }
        }
        $this->load->view('_partial/content/careerjob/show-apply', $data);
    }
}