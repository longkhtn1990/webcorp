<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller
{
    public function show($page)
    {
        $this->load->library('data');
        //$this->load->library('checkdata');
        $data = $this->data->mainInfo();
        $data = $this->data->businessMenuInfo($data,$page);
        $data['currentPage'] = $page;
        $data = $this->data->businessContentInfo($data,$page);
        $this->load->view('_partial/content/business/show', $data);
    }
}
