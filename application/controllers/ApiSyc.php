<?php
/**
 * Created by PhpStorm.
 * User: duong
 * Date: 28-11-2017
 * Time: 16:49
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiSyc extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL
        $this->load->model('user_model');
        //LIBRARY
        $this->load->library('data');
        $this->load->library('common');
        $this->load->library('utils');
    }
    public function createUser(){
        $data = array(
            'status' => true,
            'message' => 'Success',
            'error_code' => 200
        );
        $input = $this->input->raw_input_stream;
        $inputData = json_decode(trim($input), true);

        if($inputData){
            $authenticationRequest = $this->input->get_request_header('Authentication', TRUE);
            $result = $this->utils->checkAuthenticationMobileStation($input,$authenticationRequest);
            if($result['error_code'] == 1) {
                $check = $this->utils->checkParamPost($inputData, array('username','first_name','last_name','email','timestamp'));
                if ($check['status']) {
                    $checkUserName = $this->user_model->get_data_user_by_user_name($inputData['username']);
                    if(!$checkUserName){
                        $dataUpdate = $inputData;
                        unset($dataUpdate['timestamp']);
                        unset($dataUpdate['token']);
                        $insert = $this->user_model->save_user($dataUpdate);
                        if($insert){
                            $data['id'] = $insert;
                        }else{
                            $data = array(
                                'status' => true,
                                'description' => 'Insert fail',
                                'code' => 206
                            );
                        }
                    }else{
                        $data = array(
                            'status' => true,
                            'description' => 'User exist',
                            'code' => 205
                        );
                    }
                }else {
                    $data = array(
                        'error_code' => 0,
                        'description' => 'Missing parameter ' . $check['field'],
                    );
                }
            }else{
                $data = array(
                    'status' => false,
                    'error_code' => $result['error_code'],
                    'description' => $result['description'],
                );
            }
        }else{
            $data = array(
                'status' => true,
                'description' => 'Post data wrong',
                'code' => 200
            );
        }
        echo json_encode($data);
    }
    public function updateUser(){
        $data = array(
            'status' => true,
            'message' => 'Success',
            'error_code' => 200
        );
        $input = $this->input->raw_input_stream;
        $inputData = json_decode(trim($input), true);

        if($inputData){
            $authenticationRequest = $this->input->get_request_header('Authentication', TRUE);
            $result = $this->utils->checkAuthenticationMobileStation($input,$authenticationRequest);
            if($result['error_code'] == 200) {
                $check = $this->utils->checkParamPost($inputData, array('username','first_name','last_name','email','timestamp'));
                if ($check['status']) {
                    $checkUserName = $this->user_model->get_data_user_by_user_name($inputData['username']);
                    if($checkUserName){
                        $dataUpdate = $inputData;
                        unset($dataUpdate['timestamp']);
                        unset($dataUpdate['token']);
                        unset($dataUpdate['id']);
                        $insert = $this->user_model->update_user($inputData['id'],$dataUpdate);
                        if($insert){
                            $data['id'] = $inputData['id'];
                        }else{
                            $data = array(
                                'status' => true,
                                'description' => 'Insert fail',
                                'code' => 206
                            );
                        }
                    }else{
                        $data = array(
                            'status' => true,
                            'description' => 'User not exist',
                            'code' => 205
                        );
                    }
                }else {
                    $data = array(
                        'error_code' => 0,
                        'description' => 'Missing parameter ' . $check['field'],
                    );
                }
            }else{
                $data = array(
                    'status' => false,
                    'error_code' => $result['error_code'],
                    'description' => $result['description'],
                );
            }
        }else{
            $data = array(
                'status' => true,
                'description' => 'Post data wrong',
                'code' => 200
            );
        }
        echo json_encode($data);
    }
}