<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Partner extends CI_Controller
{
    public function show()
    {
        $this->load->library('data');
        //$this->load->library('checkdata');
        $data = $this->data->mainInfo();
        $data = $this->data->partnerMenuInfo($data);
        $data = $this->data->partnerContentInfo($data);
        $this->load->view('_partial/content/partner/show', $data);
    }

}