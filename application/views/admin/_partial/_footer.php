<!--[if lt IE 9]>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/respond.min.js"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/excanvas.min.js"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo $domain; ?>assets/admin/assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?php echo $domain; ?>assets/admin/assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/admin/assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<script src="<?php echo $domain; ?>assets/admin/spinner/Spinner.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_main_slide.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_project.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_project_detail.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_business.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_news.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_partner.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_partner_global.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_partner_customer.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_about.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_about_awards.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_about_awards_article.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_about_core.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_about_core_article.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_about_social.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_about_network.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_about_system_management.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_model_culture.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_model_stories.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_model_stories_member.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_news.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_news_download.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_main_info.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_main_menu.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_news_introduce.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_career_job.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_new_setting.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_contact.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_about_system_management_article.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_model_culture_member.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_upload_file.js" type="text/javascript"></script>

<script src="<?php echo $domain; ?>assets/js/admin_introduce_member_stories.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_member_stories_member.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_member_culture.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_member_culture_member.js" type="text/javascript"></script>
<script src="<?php echo $domain; ?>assets/js/admin_introduce_revenue.js" type="text/javascript"></script>

<script type="text/javascript">
    var spinner = new Spinner();
    var domain = 'http://webcorp.xampp';
</script>
