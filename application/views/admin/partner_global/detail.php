<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Partner Global'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px"></div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Detail'?></span>
                                </div>
                                <div class="btn-set pull-right">
                                    <?php if(isset($info['status_value']) && $info['status_value'] == 'active' ): ?>
                                        <button type="button" class="btn red-mint btn-circle btn-outline" onclick="activePartnerGlobal('inactive')">Inactive</button>
                                    <?php else : ?>
                                        <button type="button" class="btn green-haze btn-circle btn-outline" onclick="activePartnerGlobal('active')">Active</button>
                                    <?php endif;?>
                                    <button type="button" class="btn yellow-mint btn-circle btn-outline" onclick="changeIframeToEdit('partner-global','<?php echo isset($info['id']) ? $info['id'] : ''; ?>')">Edit</button>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="partner_global_form" name="partner_global_form" role="form">
                                    <input type="hidden" name="partner_global_current_user" id="partner_global_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="partner_global_current_language" id="partner_global_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="partner_global_id" id="partner_global_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title']) ? $info['title'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Status:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['status']) ? $info['status'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Approver:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['approver']) ? $info['approver'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Status approve:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['status_approve']) ? $info['status_approve'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Approved date:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['date_approved']) ? $info['date_approved'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Display date:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['date_display']) ? $info['date_display'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Image:</label>
                                                <div class="col-md-9">
                                                    <img src="<?php echo isset($info['image']) ? $info['image'] : ''; ?>" style="max-width: 200px; max-height: 145px;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-1 bold">content:</label>
                                                <div class="col-md-11">
                                                    <?php echo isset($info['content']) ? $info['content'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-1 bold">content (VN):</label>
                                                <div class="col-md-11">
                                                    <?php echo isset($info['content_vn']) ? $info['content_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>