<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->

                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->

                <div class="panel panel-default">
                    <div class="row" style="margin: 20px 5px 5px 5px">
                        <div class="col-md-3">
                            <a href="/admin/main-banner/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Main Banner</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/main-info/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Main Info</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/main-menu/list/">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Main Menu</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/list-upload-file/">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Upload file</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="row" style="margin: 20px 5px 5px 5px">
                        <div class="col-md-3">
                            <a href="/admin/project/list/">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Project</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/project-detail/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Project Detail</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="row" style="margin: 20px 5px 5px 5px">
                        <div class="col-md-3">
                            <a href="/admin/partner-global/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Partner Global</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/partner-customer/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Partner Customer</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/partner/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Partner Partner</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="row" style="margin: 20px 5px 5px 5px">
                        <div class="col-md-3">
                            <a href="/admin/news/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">News</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/news-download/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">News Download</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/news-introduce/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">News Introduce</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="row" style="margin: 20px 5px 5px 5px">
                        <div class="col-md-3">
                            <a href="/admin/business/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Business</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/career-job/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Career Job</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/new-setting/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">New Setting</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/contact/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Contact</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="row" style="margin: 20px 5px 5px 5px">
                        <div class="col-md-3">
                            <a href="/admin/introduce/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-about/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce About</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-about-awards/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce About Awards</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-about-core/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce About Core</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-about-awards-article/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce About Awards Article</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-about-core-article/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce About Core Article</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-about-network/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce About Network</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-about-social/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce About Social</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-about-system-management/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce About System Management</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-model-culture/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce Model Culture</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-model-stories/detail/1">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce Model Stories</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-model-stories-member/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce Model Stories Member</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-news/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce News</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-about-system-management-article/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce About System Management Article</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-md-3">
                            <a href="/admin/introduce-model-culture-member/list">
                                <div class="panel minimal panel-default">
                                    <div class="panel-heading clearfix">
                                        <div class="panel-title">Introduce Model Culture Member</div>
                                    </div>
                                </div>
                            </a>
                        </div>


						<div class="col-md-3">
							<a href="/admin/introduce-member-culture/detail/1">
								<div class="panel minimal panel-default">
									<div class="panel-heading clearfix">
										<div class="panel-title">Introduce Member Culture</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3">
							<a href="/admin/introduce-member-culture-member/list">
								<div class="panel minimal panel-default">
									<div class="panel-heading clearfix">
										<div class="panel-title">Introduce Member Culture Member</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3">
							<a href="/admin/introduce-member-stories/detail/1">
								<div class="panel minimal panel-default">
									<div class="panel-heading clearfix">
										<div class="panel-title">Introduce Member Stories</div>
									</div>
								</div>
							</a>
						</div>
						<div class="col-md-3">
							<a href="/admin/introduce-revenue/detail/1">
								<div class="panel minimal panel-default">
									<div class="panel-heading clearfix">
										<div class="panel-title">Introduce Revenue</div>
									</div>
								</div>
							</a>
						</div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>
