<!DOCTYPE html>
<html class="" lang="en" xmlns="http://www.w3.org/1999/html">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Main Info'; ?>
                    <small><?php //echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <!--    <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduceAbout')">List view</button> -->
                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                            <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail('main-info','<?php echo $info['id']; ?>')">Detail view</button>
                        <?php endif;?>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Create'?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="main_info_form" name="main_info_form" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="main_info_current_user" id="main_info_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="main_info_current_language" id="main_info_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="main_info_id" id="main_info_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_title" name="main_info_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_title_vn" name="main_info_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_header_home" name="main_info_text_header_home" value="<?php echo isset($info['text_header_home']) ? $info['text_header_home'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text header home'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_header_home_vn" name="main_info_text_header_home_vn" value="<?php echo isset($info['text_header_home_vn']) ? $info['text_header_home_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text header home (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_header_member" name="main_info_text_header_member" value="<?php echo isset($info['text_header_member']) ? $info['text_header_member'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text header member'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_header_member_vn" name="main_info_text_header_member_vn" value="<?php echo isset($info['text_header_member_vn']) ? $info['text_header_member_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text header member (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_header_contact" name="main_info_text_header_contact" value="<?php echo isset($info['text_header_contact']) ? $info['text_header_contact'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text header contact'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_header_contact_vn" name="main_info_text_header_contact_vn" value="<?php echo isset($info['text_header_contact_vn']) ? $info['text_header_contact_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text header contact (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_connecting_footer" name="main_info_text_connecting_footer" value="<?php echo isset($info['text_connecting_footer']) ? $info['text_connecting_footer'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text connecting footer'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_connecting_footer_vn" name="main_info_text_connecting_footer_vn" value="<?php echo isset($info['text_connecting_footer_vn']) ? $info['text_connecting_footer_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text connecting footer (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_url_facebook" name="main_info_url_facebook" value="<?php echo isset($info['url_facebook']) ? $info['url_facebook'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'URL Facebook'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_url_youtube" name="main_info_url_youtube" value="<?php echo isset($info['url_youtube']) ? $info['url_youtube'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'URL Youtube'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_url_google_plus" name="main_info_url_google_plus" value="<?php echo isset($info['url_google_plus']) ? $info['url_google_plus'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'URL Google Plus'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_base_domain" name="main_info_base_domain" value="<?php echo isset($info['base_domain']) ? $info['base_domain'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Base Domain'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_copyright_pre_text" name="main_info_copyright_pre_text" value="<?php echo isset($info['copyright_pre_text']) ? $info['copyright_pre_text'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Copy right pre text'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_copyright_pre_text_vn" name="main_info_copyright_pre_text_vn" value="<?php echo isset($info['copyright_pre_text_vn']) ? $info['copyright_pre_text_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Copy right pre text (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_copyright_text" name="main_info_copyright_text" value="<?php echo isset($info['copyright_text']) ? $info['copyright_text'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Copy right text'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_copyright_text_vn" name="main_info_copyright_text_vn" value="<?php echo isset($info['copyright_text_vn']) ? $info['copyright_text_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Copy right text (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_career_job" name="main_info_text_career_job" value="<?php echo isset($info['text_career_job']) ? $info['text_career_job'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Career Job'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_career_job_vn" name="main_info_text_career_job_vn" value="<?php echo isset($info['text_career_job_vn']) ? $info['text_career_job_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Career Job (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_customer_contact" name="main_info_text_customer_contact" value="<?php echo isset($info['text_customer_contact']) ? $info['text_customer_contact'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Customer Contact'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_customer_contact_vn" name="main_info_text_customer_contact_vn" value="<?php echo isset($info['text_customer_contact_vn']) ? $info['text_customer_contact_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Customer Contact (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_view_unit" name="main_info_view_unit" value="<?php echo isset($info['view_unit']) ? $info['view_unit'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'View Unit'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_view_unit_vn" name="main_info_view_unit_vn" value="<?php echo isset($info['view_unit_vn']) ? $info['view_unit_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'View Unit (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_total_view" name="main_info_text_total_view" value="<?php echo isset($info['text_total_view']) ? $info['text_total_view'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Total View'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_total_view_vn" name="main_info_text_total_view_vn" value="<?php echo isset($info['text_total_view_vn']) ? $info['text_total_view_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Total View (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_total_view_week" name="main_info_text_total_view_week" value="<?php echo isset($info['text_total_view_week']) ? $info['text_total_view_week'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Total View Week'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_total_view_week_vn" name="main_info_text_total_view_week_vn" value="<?php echo isset($info['text_total_view_week_vn']) ? $info['text_total_view_week_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Total View Week (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_main_business" name="main_info_text_main_business" value="<?php echo isset($info['text_main_business']) ? $info['text_main_business'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Main Business'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_info_text_main_business_vn" name="main_info_text_main_business_vn" value="<?php echo isset($info['text_main_business_vn']) ? $info['text_main_business_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Text Main Business (VN)'; ?></label>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label class="control-label col-md-3">Image</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 260px; height: 114px;">
                                                        <img src="<?php echo isset($info['image_logo']) ? $info['image_logo'] : ''; ?>" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 260px; max-height: 114px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="main_info_logo" id="main_info_logo">
                                                            <input type="hidden" id="validate_main_info_logo" value="<?php echo isset($info['logo']) ? '1' : '0'; ?>">
                                                        </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger">NOTE!</span> Image width : 130px - height : 57 <br/>(extension : jpg, jpeg, png)</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder" style="margin-top: 100px">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <button type="button" class="btn default" onclick="changeIframeToList('main-info')">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>