<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Main Info'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <!-- <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduce-about')">List view</button> -->
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Detail'?></span>
                                </div>
                                <div class="btn-set pull-right">
                                    <button type="button" class="btn yellow-mint btn-circle btn-outline" onclick="changeIframeToEdit('main-info','<?php echo isset($info['id']) ? $info['id'] : ''; ?>')">Edit</button>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="introduce_form" name="main_info_form" role="form">
                                    <input type="hidden" name="main_info_current_user" id="main_info_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="main_info_current_language" id="main_info_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="main_info_id" id="main_info_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title']) ? $info['title'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text header home:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['text_header_home']) ? $info['text_header_home'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text header home (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['text_header_home_vn']) ? $info['text_header_home_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text header member:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['text_header_member']) ? $info['text_header_member'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text header member (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['text_header_member_vn']) ? $info['text_header_member_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text header contact:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_header_contact']) ? $info['text_header_contact'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text header contact (VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_header_contact_vn']) ? $info['text_header_contact_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text connecting footer:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_connecting_footer']) ? $info['text_connecting_footer'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text connecting footer (VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_connecting_footer_vn']) ? $info['text_connecting_footer_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">URL Facebook:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['url_facebook']) ? $info['url_facebook'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">URL Youtube:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['url_youtube']) ? $info['url_youtube'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">URL Google Plus:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['url_google_plus']) ? $info['url_google_plus'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Base Domain:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['base_domain']) ? $info['base_domain'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Total View:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['total_view']) ? $info['total_view'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Total View Week:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['total_view_week']) ? $info['total_view_week'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Copy right pre text:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['copyright_pre_text']) ? $info['copyright_pre_text'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Copy right pre text (VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['copyright_pre_text_vn']) ? $info['copyright_pre_text_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Copy right text:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['copyright_text']) ? $info['copyright_text'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Copy right text (VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['copyright_text_vn']) ? $info['copyright_text_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Career Job:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_career_job']) ? $info['text_career_job'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Career Job (VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_career_job_vn']) ? $info['text_career_job_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Customer Contact:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_customer_contact']) ? $info['text_customer_contact'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Customer Contact (VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_customer_contact_vn']) ? $info['text_customer_contact_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">View Unit:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['view_unit']) ? $info['view_unit'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">View Unit (VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['view_unit_vn']) ? $info['view_unit_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Total View:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_total_view']) ? $info['text_total_view'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Total View (VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_total_view_vn']) ? $info['text_total_view_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Total View Week:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_total_view_week']) ? $info['text_total_view_week'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Total View Week (VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_total_view_week_vn']) ? $info['text_total_view_week_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Main Business:</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_main_business']) ? $info['text_main_business'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Text Main Business(VN):</label>
                                                <div class="col-md-9">
                                                    <?php echo isset($info['text_main_business_vn']) ? $info['text_main_business_vn'] : ''; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Logo :</label>
                                                <div class="col-md-9">
                                                    <img src="<?php echo isset($info['image_logo']) ? $info['image_logo'] : ''; ?>" style="max-width: 400px; max-height: 104px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>