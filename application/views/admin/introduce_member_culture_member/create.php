<!DOCTYPE html>
<html class="" lang="en" xmlns="http://www.w3.org/1999/html">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Introduce About Awards Article'; ?>
                    <small><?php //echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduce-member-culture-member')">List view</button>
                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                            <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail('introduce-member-culture-member','<?php echo $info['id']; ?>')">Detail view</button>
                        <?php endif;?>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Create'?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="introduce_member_culture_member_form" name="introduce_member_culture_member_form" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="introduce_model_culture_member_current_user" id="introduce_model_culture_member_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="introduce_model_culture_member_current_language" id="introduce_model_culture_member_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="introduce_model_culture_member_id" id="introduce_model_culture_member_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_model_culture_member_name" name="introduce_model_culture_member_name" value="<?php echo isset($info['name']) ? $info['name'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_model_culture_member_name_vn" name="introduce_model_culture_member_name_vn" value="<?php echo isset($info['name_vn']) ? $info['name_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_model_culture_member_position" name="introduce_model_culture_member_position" value="<?php echo isset($info['position']) ? $info['position'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Position'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_model_culture_member_position_vn" name="introduce_model_culture_member_position_vn" value="<?php echo isset($info['position_vn']) ? $info['position_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Position (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <select class="form-control" id="introduce_model_culture_member_status" name="introduce_model_culture_member_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-3">Image</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 300px; height: 193px;">
                                                        <img src="<?php echo isset($info['image']) ? $domain.$info['image'] : ''; ?>" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 217px; max-height: 185px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="introduce_model_culture_member_image" id="introduce_model_culture_member_image">
                                                            <input type="hidden" id="validate_introduce_model_culture_member_image" value="<?php echo isset($info['image']) ? '1' : '0'; ?>">
                                                        </span>
                                                        <a href="javascript:void(0);" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger">NOTE!</span> Image width : 1200px - height : 1200px <br/>(extension : jpg, jpeg, png)</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder" style="margin-top: 270px">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <button type="button" class="btn default" onclick="changeIframeToList('introduce-model-culture-member')">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>
