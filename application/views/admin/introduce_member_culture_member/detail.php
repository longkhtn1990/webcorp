<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Introduce Member Culture Member'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduce-member-culture-member')">List view</button>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Detail'?></span>
                                </div>
                                <div class="btn-set pull-right">
                                    <?php echo $info['html_btn_activation_process']; ?>
                                    <?php echo $info['html_btn_approve_1']; ?>
                                    <?php echo $info['html_btn_approve_2']; ?>
                                    <?php if(isset($info['status_value']) && $info['status_value'] == 'active' ): ?>
                                        <button type="button" class="btn red-mint btn-circle btn-outline" onclick="activeIntroduceModelCultureMember('inactive')">Inactive</button>
                                    <?php else : ?>
                                        <button type="button" class="btn green-haze btn-circle btn-outline" onclick="activeIntroduceModelCultureMember('active')">Active</button>
                                    <?php endif;?>
                                    <button type="button" class="btn yellow-mint btn-circle btn-outline" onclick="changeIframeToEdit('introduce-member-culture-member','<?php echo isset($info['id']) ? $info['id'] : ''; ?>')">Edit</button>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="introduce_model_culture_member_form" name="introduce_model_culture_member_form" role="form">
                                    <input type="hidden" name="introduce_model_culture_member_current_user" id="introduce_model_culture_member_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="introduce_model_culture_member_current_language" id="introduce_model_culture_member_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="introduce_model_culture_member_id" id="introduce_model_culture_member_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Name:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['name']) ? $info['name'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Name (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['name_vn']) ? $info['name_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Position:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['position']) ? $info['position'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Position (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['position_vn']) ? $info['position_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Approver:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['approver']) ? $info['approver'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Status approve:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['status_approve']) ? $info['status_approve'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Approved date:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['date_approved']) ? $info['date_approved'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Status:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['status']) ? $info['status'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Image:</label>
                                                <div class="col-md-9">
                                                    <img src="<?php echo isset($info['image']) ? $info['image'] : ''; ?>" style="max-width: 300px; max-height: 193px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>
