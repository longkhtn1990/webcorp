<!DOCTYPE html>
<html class="" lang="en" xmlns="http://www.w3.org/1999/html">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Introduce About Awards Article'; ?>
                    <small><?php //echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduce-about-awards-article')">List view</button>
                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                            <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail('introduce-about-awards-article','<?php echo $info['id']; ?>')">Detail view</button>
                        <?php endif;?>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Create'?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="introduce_about_awards_article_form" name="introduce_about_awards_article_form" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="introduce_about_awards_article_current_user" id="introduce_about_awards_article_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="introduce_about_awards_article_current_language" id="introduce_about_awards_article_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="introduce_about_awards_article_id" id="introduce_about_awards_article_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_awards_article_title" name="introduce_about_awards_article_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_awards_article_title_vn" name="introduce_about_awards_article_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <select class="form-control" id="introduce_about_awards_article_status" name="introduce_about_awards_article_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control date-picker" id="introduce_about_awards_article_date_display" name="introduce_about_awards_article_date_display" value="<?php echo isset($info['date_display']) ? $info['date_display'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Display date'; ?></label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-3">Image</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 300px; height: 193px;">
                                                        <img src="<?php echo isset($info['image']) ? $domain.$info['image'] : ''; ?>" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 300px; max-height: 193px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="introduce_about_awards_article_image" id="introduce_about_awards_article_image">
                                                            <input type="hidden" id="validate_introduce_about_awards_article_image" value="<?php echo isset($info['image']) ? '1' : '0'; ?>">
                                                        </span>
                                                        <a href="javascript:void(0);" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger">NOTE!</span> Image width : 300px - height : 193 <br/>(extension : jpg, jpeg, png)</div>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-12">
                                            <textarea class="form-control ckeditor" id="introduce_about_awards_article_content" name="introduce_about_awards_article_content" cols="80" rows="3"><?php echo isset($info['content']) ? $info['content'] : ''; ?></textarea>
                                            <label for="header_home_page"><?php echo 'Content'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-12">
                                            <textarea class="form-control ckeditor" id="introduce_about_awards_article_content_vn" name="introduce_about_awards_article_content_vn" cols="80" rows="3"><?php echo isset($info['content_vn']) ? $info['content_vn'] : ''; ?></textarea>
                                            <label for="header_home_vn_page"><?php echo 'Content (VN)'; ?></label>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder" style="margin-top: 500px">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <button type="button" class="btn default" onclick="changeIframeToList('introduce-about-awards-article')">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>