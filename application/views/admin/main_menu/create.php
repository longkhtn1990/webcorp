<!DOCTYPE html>
<html class="" lang="en" xmlns="http://www.w3.org/1999/html">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Main Info'; ?>
                    <small><?php //echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <!--    <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduceAbout')">List view</button> -->
                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                            <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail('main-info','<?php echo $info['id']; ?>')">Detail view</button>
                        <?php endif;?>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Create'?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="main_menu_form" name="main_menu_form" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="main_menu_current_user" id="main_menu_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="main_menu_current_language" id="main_menu_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="main_menu_id" id="main_menu_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_menu_title" name="main_menu_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_menu_title_vn" name="main_menu_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_menu_type" name="main_menu_type" value="<?php echo isset($info['type']) ? $info['type'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Type'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <select class="form-control" id="main_menu_status" name="main_menu_status"><?php echo isset($info['position']) ? $info['position'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Position'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_menu_description" name="main_menu_description" value="<?php echo isset($info['description']) ? $info['description'] : ''; ?>">
                                            <label for="header_home_page"><?php echo 'Description'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="main_menu_description_vn" name="main_menu_description_vn" value="<?php echo isset($info['description_vn']) ? $info['description_vn'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Description (VN)'; ?></label>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label class="control-label col-md-3">Image</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 270px; height: 160px;">
                                                        <img src="<?php echo isset($info['image']) ? $this->domain.$info['image'] : ''; ?>" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 270px; max-height: 160px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="main_menu_image" id="main_menu_image">
                                                            <input type="hidden" id="validate_main_menu_image" value="<?php echo isset($info['image']) ? '1' : '0'; ?>">
                                                        </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger">NOTE!</span> Image width : 1349px - height : 800px <br/>(extension : jpg, jpeg, png)
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder" style="margin-top: 100px">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <button type="button" class="btn default" onclick="changeIframeToList('main-info')">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>