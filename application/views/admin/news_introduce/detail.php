<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'news Introduce'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('news-introduce')">List view</button>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Detail'?></span>
                                </div>
                                <div class="btn-set pull-right">
                                    <?php echo $info['html_btn_activation_process']; ?>
                                    <?php echo $info['html_btn_approve_1']; ?>
                                    <?php echo $info['html_btn_approve_2']; ?>
                                    <?php if(isset($info['status_value']) && $info['status_value'] == 'active' ): ?>
                                        <button type="button" class="btn red-mint btn-circle btn-outline" onclick="activeNewsIntroduce('inactive')">Inactive</button>
                                    <?php else : ?>
                                        <button type="button" class="btn green-haze btn-circle btn-outline" onclick="activeNewsIntroduce('active')">Active</button>
                                    <?php endif;?>
                                    <button type="button" class="btn yellow-mint btn-circle btn-outline" onclick="changeIframeToEdit('news-introduce','<?php echo isset($info['id']) ? $info['id'] : ''; ?>')">Edit</button>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="news_introduce_form" name="news_introduce_form" role="form">
                                    <input type="hidden" name="news_introduce_current_user" id="news_introduce_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="news_introduce_current_language" id="news_introduce_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="news_introduce_id" id="news_introduce_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title']) ? $info['title'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Link:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['youtube_url']) ? $info['youtube_url'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Link (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['youtube_url_vn']) ? $info['youtube_url_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Status:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['status']) ? $info['status'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Approver:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['approver']) ? $info['approver'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Status approve:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['status_approve']) ? $info['status_approve'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Approved date:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['date_approved']) ? $info['date_approved'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Display date:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['date_display']) ? $info['date_display'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>