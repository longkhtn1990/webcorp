<!DOCTYPE html>
<html class="" lang="en" xmlns="http://www.w3.org/1999/html">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'News Introduce'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('news-introduce')">List view</button>
                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                            <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail('news-introduce','<?php echo $info['id']; ?>')">Detail view</button>
                        <?php endif;?>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo isset($info['id']) ? 'Edit' : 'Create'; ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="news_introduce_form" name="news_introduce_form" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="news_introduce_current_user" id="news_introduce_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="news_introduce_current_language" id="news_introduce_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="news_introduce_id" id="news_introduce_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="news_introduce_title" name="news_introduce_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="news_introduce_title_vn" name="news_introduce_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="news_introduce_youtube_url" name="news_introduce_youtube_url" value="<?php echo isset($info['youtube_url']) ? $info['youtube_url'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Link'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="news_introduce_youtube_url_vn" name="news_introduce_youtube_url_vn" value="<?php echo isset($info['youtube_url_vn']) ? $info['youtube_url_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Link (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <select class="form-control" id="news_introduce_status" name="news_introduce_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control date-picker" id="news_introduce_date_display" name="news_introduce_date_display" value="<?php echo isset($info['date_display']) ? $info['date_display'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Display date'; ?></label>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder" style="margin-top: 100px">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <button type="button" class="btn default" onclick="changeIframeToList('news-introduce')">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>