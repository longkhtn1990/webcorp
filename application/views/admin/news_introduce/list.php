<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'News Introduce'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToCreate('news-introduce')">Create</button>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Filter'?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="news_introduce_form" name="news_introduce_form" role="form">
                                    <input type="hidden" name="news_introduce_current_user" id="news_introduce_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="news_introduce_current_language" id="news_introduce_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="news_introduce_title" name="news_introduce_title" value="">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <select class="form-control" id="news_introduce_status" name="news_introduce_status"><?php echo isset($search['option_status']) ? $search['option_status'] : ''; ?></select>
                                            <label for="page_title_vn"><?php echo 'Status'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <select class="form-control" id="news_introduce_status_approve" name="news_introduce_status_approve"><?php echo isset($search['option_status_approve']) ? $search['option_status_approve'] : '';?></select>
                                            <label for="header_home_page"><?php echo 'Status Approve'; ?></label>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder">
                                        <button type="button" class="btn blue" onclick="searchNewsIntroduce()">Search</button>
                                        <button type="button" class="btn default" onclick="clearDataSearchNewsIntroduce()">Clear</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red">
                                    <i class="icon-settings font-red"></i>
                                    <span class="caption-subject bold uppercase"> List </span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body table-both-scroll">
                                <table class="table table-striped table-bordered table-hover order-column" id="news_introduce_list_view">
                                    <thead>
                                    <tr>
                                        <th width="15%">Title</th>
                                        <th width="15%">Title (VN)</th>
                                        <th width="10%">Link</th>
                                        <th width="10%">Link (VN)</th>
                                        <th width="10%">Status</th>
                                        <th width="15%">Status Approve</th>
                                        <th width="15%">&nbsp;</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>