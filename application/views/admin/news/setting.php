<!DOCTYPE html>
<html class="" lang="en" xmlns="http://www.w3.org/1999/html">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'News'; ?>
                    <small><?php echo 'Setting'; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('news')">List view</button>
                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                            <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToCreate('news')">Create news</button>
                        <?php endif;?>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Setting'; ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="news_form" name="news_form" role="form">
                                    <div class="form-body">
                                        <!--New News-->
                                        <div class="form-group form-md-line-input col-md-2">
                                            <?php echo 'Tin tức mới'; ?>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_1_title" name="setting_1_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_1_title_vn" name="setting_1_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-2">
                                            <select class="form-control" id="setting_1_status" name="setting_1_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <!--News Company-->
                                        <div class="form-group form-md-line-input col-md-2">
                                            <?php echo 'Tin công ty'; ?>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_2_title" name="setting_2_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_2_title_vn" name="setting_2_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-2">
                                            <select class="form-control" id="setting_2_status" name="setting_2_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <!--News Company-->
                                        <div class="form-group form-md-line-input col-md-2">
                                            <?php echo 'Tin thị trường'; ?>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_3_title" name="setting_3_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_3_title_vn" name="setting_3_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-2">
                                            <select class="form-control" id="setting_3_status" name="setting_3_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <!--News Company-->
                                        <div class="form-group form-md-line-input col-md-2">
                                            <?php echo 'Tin tổng hợp'; ?>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_4_title" name="setting_4_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_4_title_vn" name="setting_4_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-2">
                                            <select class="form-control" id="setting_4_status" name="setting_4_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <!--News Company-->
                                        <div class="form-group form-md-line-input col-md-2">
                                            <?php echo 'Thông cáo báo chí'; ?>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_5_title" name="setting_5_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_5_title_vn" name="setting_5_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-2">
                                            <select class="form-control" id="setting_5_status" name="setting_5_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <!--News Company-->
                                        <div class="form-group form-md-line-input col-md-2">
                                            <?php echo 'Video giới thiệu'; ?>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_6_title" name="setting_6_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_6_title_vn" name="setting_6_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-2">
                                            <select class="form-control" id="setting_6_status" name="setting_6_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <!--Mẫu download-->
                                        <div class="form-group form-md-line-input col-md-2">
                                            <?php echo 'Mẫu download'; ?>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_7_title" name="setting_7_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="setting_7_title_vn" name="setting_7_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-2">
                                            <select class="form-control" id="setting_7_status" name="setting_7_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder" style="margin-top: 500px">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <button type="button" class="btn default" onclick="changeIframeToList('news')">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>