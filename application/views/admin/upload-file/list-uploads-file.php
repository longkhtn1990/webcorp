<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Upload file'; ?>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <a type="button" class="btn btn-info btn-circle" href="/admin/upload-file">Upload</a>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red">
                                    <i class="icon-settings font-red"></i>
                                    <span class="caption-subject bold uppercase"> List </span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body table-both-scroll">
                                <table class="table table-striped table-bordered table-hover order-column" id="upload_file_list_view" style="min-height: 300px">
                                    <thead>
                                        <tr>
                                            <th>link</th>
                                            <th width="10%">type</th>
                                            <th width="25%">Image</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>