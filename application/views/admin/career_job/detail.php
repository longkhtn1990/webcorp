<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Career Job'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('career-job')">List view</button>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light">
                            <?php if(isset($info['id'])){ if($info['id'] != 0){ ?>
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase"><?php echo 'Detail'?></span>
                                    </div>
                                    <div class="btn-set pull-right">
                                        <?php echo $info['html_btn_activation_process']; ?>
                                        <?php echo $info['html_btn_approve_1']; ?>
                                        <?php echo $info['html_btn_approve_2']; ?>
                                        <?php if(isset($info['status_value']) && $info['status_value'] == 'active' ): ?>
                                            <button type="button" class="btn red-mint btn-circle btn-outline" onclick="activeCareerJob('inactive')">Inactive</button>
                                        <?php else : ?>
                                            <button type="button" class="btn green-haze btn-circle btn-outline" onclick="activeCareerJob('active')">Active</button>
                                        <?php endif;?>
                                        <button type="button" class="btn yellow-mint btn-circle btn-outline" onclick="changeIframeToEdit('career-job','<?php echo isset($info['id']) ? $info['id'] : ''; ?>')">Edit</button>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form id="career_job_form" name="career_job_form" role="form">
                                        <input type="hidden" name="career_job_current_user" id="career_job_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                        <input type="hidden" name="career_job_current_language" id="career_job_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                        <input type="hidden" name="career_job_id" id="career_job_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                        <div class="form-body">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Position:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><?php echo isset($info['position']) ? $info['position'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Quantity:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><?php echo isset($info['quantity']) ? $info['quantity'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Workplace:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><?php echo isset($info['workplace']) ? $info['workplace'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Alias:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><?php echo isset($info['alias']) ? $info['alias'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label col-md-1 bold">Content:</label>
                                                    <div class="col-md-11">
                                                        <p class="form-control-static"><?php echo isset($info['content']) ? $info['content'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Status:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><?php echo isset($info['status']) ? $info['status'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Approver:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><?php echo isset($info['approver']) ? $info['approver'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Status approve:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><?php echo isset($info['status_approve']) ? $info['status_approve'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Approved date:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><?php echo isset($info['date_approved']) ? $info['date_approved'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            <?php }else{ ?>
                                <div class="portlet-title">
                                    <div class="caption font-red-sunglo">
                                        <i class="icon-settings font-red-sunglo"></i>
                                        <span class="caption-subject bold uppercase"><?php echo 'Detail'?></span>
                                    </div>
                                    <div class="btn-set pull-right">
                                        <button type="button" class="btn yellow-mint btn-circle btn-outline" onclick="changeIframeToEdit('career-job','<?php echo isset($info['id']) ? $info['id'] : ''; ?>')">Edit</button>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <form id="career_job_form" name="career_job_form" role="form">
                                        <input type="hidden" name="career_job_current_user" id="career_job_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                        <input type="hidden" name="career_job_current_language" id="career_job_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                        <input type="hidden" name="career_job_id" id="career_job_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                        <div class="form-body">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Position:</label>
                                                    <div class="col-md-9">
                                                        <p class="form-control-static"><?php echo isset($info['position']) ? $info['position'] : ''; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label col-md-3 bold">Banner:</label>
                                                    <div class="col-md-9">
                                                        <img src="<?php echo isset($info['banner_image']) ? $info['banner_image'] : ''; ?>" style="max-width: 400px; max-height: 160px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            <?php }} ?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>