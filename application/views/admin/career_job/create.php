<!DOCTYPE html>
<html class="" lang="en" xmlns="http://www.w3.org/1999/html">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Career Job'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('career-job')">List view</button>
                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                            <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail('career-job','<?php echo $info['id']; ?>')">Detail view</button>
                        <?php endif;?>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo isset($info['id']) ? 'Edit' : 'Create'; ?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <?php if( (isset($info['id']) && $info['id'] != 0) || !isset($info['id'])){ ?>
                                    <form id="career_job_form" name="career_job_form" role="form" enctype="multipart/form-data">
                                        <input type="hidden" name="career_job_current_user" id="career_job_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                        <input type="hidden" name="career_job_current_language" id="career_job_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                        <input type="hidden" name="career_job_id" id="career_job_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                        <div class="form-body">
                                            <div class="form-group form-md-line-input col-md-6">
                                                <input type="text" class="form-control" id="career_job_position" name="career_job_position" value="<?php echo isset($info['position']) ? $info['position'] : ''; ?>">
                                                <label for="career_job_position"><?php echo 'Position'; ?></label>
                                            </div>
                                            <div class="form-group form-md-line-input col-md-6">
                                                <input type="text" class="form-control" id="career_job_quantity" name="career_job_quantity" value="<?php echo isset($info['quantity']) ? $info['quantity'] : ''; ?>">
                                                <label for="career_job_quantity"><?php echo 'Quantity'; ?></label>
                                            </div>
                                            <div class="form-group form-md-line-input col-md-6">
                                                <input type="text" class="form-control" id="career_job_workplace" name="career_job_workplace" value="<?php echo isset($info['workplace']) ? $info['workplace'] : ''; ?>">
                                                <label for="career_job_workplace"><?php echo 'Workplace'; ?></label>
                                            </div>
                                            <div class="form-group form-md-line-input col-md-6">
                                                <input type="text" class="form-control date-picker" id="career_job_date_expired" name="career_job_date_expired" value="<?php echo isset($info['date_expired']) ? $info['date_expired'] : ''; ?>">
                                                <label for="career_job_date_expired"><?php echo 'Date Expired'; ?></label>
                                            </div>
                                            <div class="form-group form-md-line-input col-md-6">
                                                <input type="text" class="form-control" id="career_job_alias" name="career_job_alias" value="<?php echo isset($info['alias']) ? $info['alias'] : ''; ?>">
                                                <label for="career_job_alias"><?php echo 'Alias'; ?></label>
                                            </div>
                                            <div class="form-group form-md-line-input col-md-6">
                                                <select class="form-control" id="career_job_status" name="career_job_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                                <label for="career_job_status"><?php echo 'Status'; ?></label>
                                            </div>
                                            <div class="form-group form-md-line-input col-md-12">
                                                <textarea class="ckeditor form-control" id="career_job_content" name="career_job_content" cols="80" rows="10"><?php echo isset($info['content']) ? $info['content'] : ''; ?></textarea>
                                                <label for="career_job_content"><?php echo 'Content'; ?></label>
                                            </div>
                                        </div>
                                        <div class="form-actions noborder" style="margin-top: 200px">
                                            <button type="submit" class="btn blue">Submit</button>
                                            <button type="button" class="btn default" onclick="changeIframeToList('career-job')">Cancel</button>
                                        </div>
                                    </form>
                                <?php }else{ ?>
                                    <form id="career_job_banner_form" name="career_job_banner_form" role="form" enctype="multipart/form-data">
                                        <input type="hidden" name="career_job_current_user" id="career_job_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                        <input type="hidden" name="career_job_current_language" id="career_job_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                        <input type="hidden" name="career_job_id" id="career_job_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                        <div class="form-body">
                                            <div class="form-group form-md-line-input col-md-6">
                                                <input disabled type="text" class="form-control" id="career_job_position" name="career_job_position" value="<?php echo isset($info['position']) ? $info['position'] : ''; ?>">
                                                <label for="career_job_position"><?php echo 'Position'; ?></label>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="control-label col-md-3">Image Banner</label>
                                                <div class="col-md-9">
                                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <div class="fileinput-new thumbnail" style="width: 400px; height: 150px;">
                                                            <img src="<?php echo isset($info['banner_image']) ? $this->domain.$info['banner_image'] : ''; ?>" alt="" /> </div>
                                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 145px;"> </div>
                                                        <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="career_job_banner" id="career_job_banner">
                                                                    <input type="hidden" id="validate_career_job_banner" value="<?php echo isset($info['banner_image']) ? '1' : '0'; ?>">
                                                                </span>
                                                            <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">NOTE!</span> Image width : 1920px - height : 470px <br/>(extension : jpg, jpeg, png)</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions noborder" style="margin-top: 100px">
                                            <button type="submit" class="btn blue">Submit</button>
                                            <button type="button" class="btn default" onclick="changeIframeToList('career-job')">Cancel</button>
                                        </div>
                                    </form>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>