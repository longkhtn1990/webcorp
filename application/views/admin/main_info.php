<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo $language['txt_main_info_title']; ?>
                    <small><?php echo $language['txt_main_info_title_small']; ?></small>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo $language['txt_main_info_title']?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form role="form">
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="page_title" placeholder="<?php echo $language['txt_input_title_page']; ?>">
                                            <label for="page_title"><?php echo $language['txt_input_title_page']; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="page_title_vn" placeholder="<?php echo $language['txt_input_title_page']; ?>">
                                            <label for="page_title_vn"><?php echo $language['txt_input_title_vn_page']; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="header_home_page" placeholder="<?php echo $language['txt_header_home_page']; ?>">
                                            <label for="header_home_page"><?php echo $language['txt_header_home_page']; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="header_home_vn_page" placeholder="<?php echo $language['txt_header_home_vn_page']; ?>">
                                            <label for="header_home_vn_page"><?php echo $language['txt_header_home_vn_page']; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="header_home_page" placeholder="<?php echo $language['txt_header_member_page']; ?>">
                                            <label for="header_home_page"><?php echo $language['txt_header_member_page']; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="header_home_vn_page" placeholder="<?php echo $language['txt_header_member_vn_page']; ?>">
                                            <label for="header_home_vn_page"><?php echo $language['txt_header_member_vn_page']; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="header_home_page" placeholder="<?php echo 'Text contact'; ?>">
                                            <label for="header_home_page"><?php echo $language['txt_header_member_page']; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="header_home_vn_page" placeholder="<?php echo 'Text contact (VN)'; ?>">
                                            <label for="header_home_vn_page"><?php echo $language['txt_header_member_vn_page']; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="base_domain" placeholder="<?php echo $language['txt_input_base_domain']; ?>">
                                            <label for="base_domain"><?php echo $language['txt_input_base_domain']; ?></label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-3">Logo CNS</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 130px; height: 57px;">
                                                        <img src="http://webcorp.dev/assets/images/logo.png" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 130px; max-height: 57px;"> </div>
                                                    <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="..."> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger">NOTE!</span> Logo width : 130px - height : 57px</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder">
                                        <button type="button" class="btn blue">Submit</button>
                                        <button type="button" class="btn default">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>