<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Introduce Member Stories'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <!-- <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduce-about')">List view</button> -->
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Detail'?></span>
                                </div>
                                <div class="btn-set pull-right">
                                    <?php echo $info['html_btn_activation_process']; ?>
                                    <?php echo $info['html_btn_approve_1']; ?>
                                    <?php echo $info['html_btn_approve_2']; ?>
                                    <?php if(isset($info['status_value']) && $info['status_value'] == 'active' ): ?>
                                        <button type="button" class="btn red-mint btn-circle btn-outline" onclick="activeIntroduceMemberStories('inactive')">Inactive</button>
                                    <?php else : ?>
                                        <button type="button" class="btn green-haze btn-circle btn-outline" onclick="activeIntroduceMemberStories('active')">Active</button>
                                    <?php endif;?>
                                    <button type="button" class="btn yellow-mint btn-circle btn-outline" onclick="changeIframeToEdit('introduce-member-stories','<?php echo isset($info['id']) ? $info['id'] : ''; ?>')">Edit</button>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="introduce_form" name="introduce_member_stories_form" role="form">
                                    <input type="hidden" name="introduce_model_stories_current_user" id="introduce_model_stories_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="introduce_model_stories_current_language" id="introduce_model_stories_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="introduce_model_stories_id" id="introduce_model_stories_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title']) ? $info['title'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Status:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['status']) ? $info['status'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Approver:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['approver']) ? $info['approver'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Status approve:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['status_approve']) ? $info['status_approve'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Approved date:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['date_approved']) ? $info['date_approved'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Display date:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['date_display']) ? $info['date_display'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-1 bold">Content:</label>
												<div class="col-md-11">
													<p style="word-wrap: break-word;"><?php echo isset($info['content']) ? $info['content'] : ''; ?></p>
												</div>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<label class="control-label col-md-1 bold">Content (VN):</label>
												<div class="col-md-11">
													<p style="word-wrap: break-word;"><?php echo isset($info['content_vn']) ? $info['content_vn'] : ''; ?></p>
												</div>
											</div>
										</div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>
