<!DOCTYPE html>
<html class="" lang="en" xmlns="http://www.w3.org/1999/html">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Introduce About Network'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduce-about-network')">List view</button>
                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                            <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail('introduce-about-network','<?php echo $info['id']; ?>')">Detail view</button>
                        <?php endif;?>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Create'?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="introduce_about_network_form" name="introduce_about_network_form" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="introduce_about_network_current_user" id="introduce_about_network_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="introduce_about_network_current_language" id="introduce_about_network_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="introduce_about_network_id" id="introduce_about_network_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_title" name="introduce_about_network_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_title_vn" name="introduce_about_network_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <select class="form-control" id="introduce_about_network_status" name="introduce_about_network_status"><?php echo isset($info['option_status']) ? $info['option_status'] : ''; ?></select>
                                            <label for="header_home_page"><?php echo 'Status'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control date-picker" id="introduce_about_network_date_display" name="introduce_about_network_date_display" value="<?php echo isset($info['date_display']) ? $info['date_display'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Display date'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_number_1" name="introduce_about_network_number_1" value="<?php echo isset($info['number_1']) ? $info['number_1'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Number office'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_text_1" name="introduce_about_network_text_1" value="<?php echo isset($info['text_1']) ? $info['text_1'] : ''; ?>">
                                            <label for="header_home_page"><?php echo 'Text office'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_text_1_vn" name="introduce_about_network_text_1_vn" value="<?php echo isset($info['text_1_vn']) ? $info['text_1_vn'] : ''; ?>">
                                            <label for="header_home_page"><?php echo 'Text office VN'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_number_2" name="introduce_about_network_number_2" value="<?php echo isset($info['number_2']) ? $info['number_2'] : ''; ?>">
                                            <label for="header_home_vn_page"><?php echo 'Number employee'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_text_2" name="introduce_about_network_text_2" value="<?php echo isset($info['text_2']) ? $info['text_2'] : ''; ?>">
                                            <label for="header_home_page"><?php echo 'Text employee'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_text_2_vn" name="introduce_about_network_text_2_vn" value="<?php echo isset($info['text_2']) ? $info['text_2'] : ''; ?>">
                                            <label for="header_home_page"><?php echo 'Text employee VN'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_unit_2" name="introduce_about_network_unit_2" value="<?php echo isset($info['unit_2']) ? $info['unit_2'] : ''; ?>">
                                            <label for="introduce_about_network_unit_2"><?php echo 'Unit 1'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="introduce_about_network_unit" name="introduce_about_network_unit" value="<?php echo isset($info['unit']) ? $info['unit'] : ''; ?>">
                                            <label for="introduce_about_network_unit"><?php echo 'Unit 2'; ?></label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-3">Image office</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 165px; height: 160px;">
                                                        <img src="<?php echo isset($info['image_1']) ? $domain.$info['image_1'] : ''; ?>" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 165px; max-height: 160px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="introduce_about_network_image_1" id="introduce_about_network_image_1">
                                                            <input type="hidden" id="validate_introduce_about_network_image_1" value="<?php echo isset($info['image_1']) ? '1' : '0'; ?>">
                                                        </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger">NOTE!</span> Image width : 62px - height : 60px <br/>(extension : jpg, jpeg, png)</div>
                                            </div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-3">Image employee</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 157px; height: 160px;">
                                                        <img src="<?php echo isset($info['image_2']) ? $domain.$info['image_2'] : ''; ?>" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 157px; max-height: 160px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="introduce_about_network_image_2" id="introduce_about_network_image_2">
                                                            <input type="hidden" id="validate_introduce_about_network_image_2" value="<?php echo isset($info['image_2']) ? '1' : '0'; ?>">
                                                        </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger">NOTE!</span> Image width : 69px - height : 70px <br/>(extension : jpg, jpeg, png)</div>
                                            </div>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-12">
                                            <textarea class="ckeditor form-control" id="introduce_about_network_content" name="introduce_about_network_content" cols="80" rows="10"><?php echo isset($info['content']) ? $info['content'] : ''; ?></textarea>
                                            <label for="header_home_page"><?php echo 'Content'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-12">
                                            <textarea class="ckeditor form-control" id="introduce_about_network_content_vn" name="introduce_about_network_content_vn" cols="80" rows="10"><?php echo isset($info['content_vn']) ? $info['content_vn'] : ''; ?></textarea>
                                            <label for="header_home_vn_page"><?php echo 'Content (VN)'; ?></label>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder" style="margin-top: 900px">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <button type="button" class="btn default" onclick="changeIframeToList('introduce-about-network')">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>