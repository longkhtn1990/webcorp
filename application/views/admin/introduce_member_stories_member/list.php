<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Introduce Member Stories Member'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToCreate('introduce-member-stories-member')">Create</button>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Filter'?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="introduce_member_stories_member_form" name="introduce_member_stories_member_form" role="form">
                                    <input type="hidden" name="introduce_model_stories_member_current_user" id="introduce_model_stories_member_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="introduce_model_stories_member_current_language" id="introduce_model_stories_member_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-4">
                                            <input type="text" class="form-control" id="introduce_model_stories_member_name" name="introduce_model_stories_member_name" value="">
                                            <label for="page_title"><?php echo 'Name'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <select class="form-control" id="introduce_model_stories_member_status" name="introduce_model_stories_member_status"><?php echo isset($search['option_status']) ? $search['option_status'] : ''; ?></select>
                                            <label for="page_title_vn"><?php echo 'Status'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-4">
                                            <select class="form-control" id="introduce_model_stories_member_status_approve" name="introduce_model_stories_member_status_approve"><?php echo isset($search['option_status_approve']) ? $search['option_status_approve'] : '';?></select>
                                            <label for="header_home_page"><?php echo 'Status Approve'; ?></label>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder">
                                        <button type="button" class="btn blue" onclick="searchIntroduceMemberStoriesMember()">Search</button>
                                        <button type="button" class="btn default" onclick="clearDataSearchIntroduceMemberStoriesMember()">Clear</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red">
                                    <i class="icon-settings font-red"></i>
                                    <span class="caption-subject bold uppercase"> List </span>
                                </div>
                                <div class="tools"> </div>
                            </div>
                            <div class="portlet-body table-both-scroll" style="height: auto">
                                <table class="table table-striped table-bordered table-hover order-column" id="introduce_member_stories_member_list_view" style="min-height: 300px">
                                    <thead>
                                    <tr>
                                        <th width="11%">Name</th>
                                        <th width="11%">Name (VN)</th>
                                        <th width="11%">Position</th>
                                        <th width="11%">Position (VN)</th>
                                        <th width="11%">Info</th>
                                        <th width="11%">Info (VN)</th>
                                        <th width="10%">Status</th>
                                        <th width="10%">Status Approve</th>
                                        <th width="14%">&nbsp;</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>
