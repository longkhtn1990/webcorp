<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Contact'; ?>
                    <small><?php echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <!-- <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduce-about')">List view</button> -->
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Detail'?></span>
                                </div>
                                <div class="btn-set pull-right">
                                    <button type="button" class="btn yellow-mint btn-circle btn-outline" onclick="changeIframeToEdit('contact','<?php echo isset($info['id']) ? $info['id'] : ''; ?>')">Edit</button>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="introduce_form" name="main_info_form" role="form">
                                    <input type="hidden" name="contact_current_user" id="contact_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="contact_current_language" id="contact_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="contact_id" id="contact_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Name:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['name']) ? $info['name'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Name (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['name_vn']) ? $info['name_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title']) ? $info['title'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Title (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Address:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['address']) ? $info['address'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Address (VN):</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['address_vn']) ? $info['address_vn'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Tel:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['tel']) ? $info['tel'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Fax:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['fax']) ? $info['fax'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Email:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['mail']) ? $info['mail'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Website:</label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"><?php echo isset($info['website']) ? $info['website'] : ''; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3 bold">Image:</label>
                                                <div class="col-md-9">
                                                    <img src="<?php echo isset($info['banner']) ? $info['banner'] : ''; ?>" style="max-width: 470px; max-height: 290px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>