<!DOCTYPE html>
<html class="" lang="en" xmlns="http://www.w3.org/1999/html">

<?php $this->load->view('admin/_partial/_header'); ?>

<body class="page-content-white">
<div class="page-wrapper">
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <!-- BEGIN PAGE HEADER-->
                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title"><?php echo 'Contact'; ?>
                    <small><?php //echo ''; ?></small>
                    <div class="btn-set pull-right" style="margin-right: 20px">
                        <!--    <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToList('introduceAbout')">List view</button> -->
                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                            <button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail('contact','<?php echo $info['id']; ?>')">Detail view</button>
                        <?php endif;?>
                    </div>
                </h1>
                <!-- END PAGE TITLE-->
                <!-- END PAGE HEADER-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="portlet light bordered">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <i class="icon-settings font-red-sunglo"></i>
                                    <span class="caption-subject bold uppercase"><?php echo 'Create'?></span>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <form id="contact_form" name="contact_form" role="form" enctype="multipart/form-data">
                                    <input type="hidden" name="contact_current_user" id="contact_current_user" value="<?php echo isset($current_user) ? $current_user : ''; ?>" />
                                    <input type="hidden" name="contact_current_language" id="contact_current_language" value="<?php echo isset($language['language']) ? $language['language'] : 'vietnamese'; ?>" />
                                    <input type="hidden" name="contact_id" id="contact_id" value="<?php echo isset($info['id']) ? $info['id'] : ''; ?>" />
                                    <div class="form-body">
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_name" name="contact_name" value="<?php echo isset($info['name']) ? $info['name'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Name'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_name_vn" name="contact_name_vn" value="<?php echo isset($info['name_vn']) ? $info['name_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Name (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_title" name="contact_title" value="<?php echo isset($info['title']) ? $info['title'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Title'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_title_vn" name="contact_title_vn" value="<?php echo isset($info['title_vn']) ? $info['title_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Title (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_address" name="contact_address" value="<?php echo isset($info['address']) ? $info['address'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Address'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_address_vn" name="contact_address_vn" value="<?php echo isset($info['address_vn']) ? $info['address_vn'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Address (VN)'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_tel" name="contact_tel" value="<?php echo isset($info['tel']) ? $info['tel'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Tel'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_fax" name="contact_fax" value="<?php echo isset($info['fax']) ? $info['fax'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Fax'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_mail" name="contact_mail" value="<?php echo isset($info['mail']) ? $info['mail'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Tel'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_website" name="contact_website" value="<?php echo isset($info['website']) ? $info['website'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Website'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_lat" name="contact_lat" value="<?php echo isset($info['lat']) ? $info['lat'] : ''; ?>">
                                            <label for="page_title"><?php echo 'Latitude'; ?></label>
                                        </div>
                                        <div class="form-group form-md-line-input col-md-6">
                                            <input type="text" class="form-control" id="contact_log" name="contact_log" value="<?php echo isset($info['log']) ? $info['log'] : ''; ?>">
                                            <label for="page_title_vn"><?php echo 'Longitude'; ?></label>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label class="control-label col-md-3">Banner</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 410px; height: 150px;">
                                                        <img src="<?php echo isset($info['banner']) ? $this->domain.$info['banner'] : ''; ?>" alt="" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 410px; max-height: 150px;"> </div>
                                                    <div>
                                                                <span class="btn default btn-file">
                                                                    <span class="fileinput-new"> Select Banner </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="contact_banner" id="contact_banner">
                                                                    <input type="hidden" id="validate_contact_banner" value="<?php echo isset($info['banner']) ? '1' : '0'; ?>">
                                                                </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger">NOTE!</span> Image width : 1920px - height : 500px <br/>(extension : jpg, jpeg, png)</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-actions noborder" style="margin-top: 100px">
                                        <button type="submit" class="btn blue">Submit</button>
                                        <?php if(isset($info['id']) && $info['id'] != ''): ?>
                                            <button type="button" class="btn btn-default" onclick="changeIframeToDetail('contact','<?php echo $info['id']; ?>')">Cancel</button>
                                        <?php endif;?>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
</div>

<?php $this->load->view('admin/_partial/_footer'); ?>
</body>

</html>