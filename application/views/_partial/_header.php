<header>
    <nav class="top-header">
        <div class="container">
            <ul class="nav navbar-nav lang-nav">
                <li <?php if($this->session->userdata('language') != 'vietnamese'){ ?>class="active" <?php } ?>>
                    <a href="/language/english">
                        <span>EN</span>
                    </a>
                </li>
                <li <?php if($this->session->userdata('language') == 'vietnamese'){ ?>class="active" <?php } ?>>
                    <a href="/language/vietnamese">
                        <span>VN</span>
                    </a>
                </li>
            </ul>
            <ul class="nav navbar-nav top-menu">
                <?php if (count($mainHeader) > 0): ?>
                    <?php if (isset($mainHeader['home'])): ?>
                        <li class="top-menu-item home-item">
                            <a href="<?php echo $mainHeader['home']['link'] != '' ? $mainHeader['home']['link'] : 'javascript:void(0);'; ?>"><?php echo $mainHeader['home']['text']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if (isset($mainHeader['member'])): ?>
                        <li class="top-menu-item member-item">
                            <a href="<?php echo $mainHeader['member']['link']!= '' ? $mainHeader['member']['link'] : 'javascript:void(0);'; ?>"><?php echo $mainHeader['member']['text']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if (isset($mainHeader['customerContact'])): ?>
                        <li class="top-menu-item contact-item">
                            <a href="<?php echo $mainHeader['customerContact']['link']!= '' ? $mainHeader['customerContact']['link'] : 'javascript:void(0);'; ?>"><?php echo $mainHeader['customerContact']['text']; ?></a>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
            </ul>
        </div>
    </nav>
    <nav class="navbar main-header">
        <div class="container">
            <button class="menu-mobile-toggle" type="button" title="MENU">
                <span class="line line-top"></span>
                <span class="line line-middle"></span>
                <span class="line line-bottom"></span>
            </button>
            <h1 class="navbar-header logo">
                <a class="navbar-brand" href="<?php echo $link != '' ? $link : base_url(); ?>">
                    <img src="<?php echo $logo != '' ? $logo : base_url().'assets/images/logo.png'; ?>" alt="CNS">
                </a>
            </h1>
            <div class="menu">
                <ul class="nav navbar-nav main-menu">
                    <?php if (count($mainMenu) > 0) : ?>
                        <?php for ($i = 0; $i < count($mainMenu); $i++): ?>
                            <li class="<?php echo $mainMenu[$i]['active'] == true ? 'active' : '';  ?>">
                                <a href="<?php echo $mainMenu[$i]['link'] != '' ? $mainMenu[$i]['link'] : 'javascript:void(0);'; ?>"><?php echo $mainMenu[$i]['text']; ?></a>
                            </li>
                        <?php endfor; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </nav>
</header>