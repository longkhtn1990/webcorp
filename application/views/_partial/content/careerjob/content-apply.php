<div class="main-content-container">
    <div class="container">
        <div class="row">
            <?php $this->load->view('_partial/content/careerjob/menu'); ?>
            <section class="col-sm-9 main-content career-section">
                <div class="section-content">
                    <div class="panel job-apply-wrap">
                        <h2 class="panel-title"><?php echo $detail['position']; ?></h2>
                        <div class="wrap-form apply-form">
                            <form method="post" name="career-job-form" enctype="multipart/form-data">
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Họ và tên</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" placeholder="" type="text" name="name" id="name">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" placeholder="" type="email" name="email" id="email">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <label class="col-sm-4 control-label">Số điện thoại</label>
                                    <div class="col-sm-8">
                                        <input class="form-control" placeholder="" type="tel" name="tel" id="tel">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-4">
                                        <label class="control-label">
                                            Hồ sơ của bạn
                                        </label>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="upload-field-wrap">
                                            <div class="upload-field">
                                                <div class="fake-field">
                                                    <span>Tại lên</span>
                                                </div>
                                                <input class="form-control real-field" type="file" name="file" id="file">
                                            </div>
                                            <p class="note-label">
                                                (File type: .doc, docx, pdf, ppt, pptx, rar, zip, 7z Max size: 5MB)
                                            </p>
                                        </div>
                                        <!-- <p class="file-upload-name">LeeLee.pdf</p> -->
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col-sm-offset-4 col-sm-8 text-right">
                                        <input type="submit" class="btn btn-submit apply-submit-btn" value="Gửi">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<div class="modal success-modal career-job-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="/assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title" id="info-popup"></h2>
                <p class="cta-block">
                    <button class="btn btn-secondary closeContactformBtn" data-dismiss="modal" aria-label="Close" type="button">Cám ơn!</button>
                </p>
            </div>
        </div>
    </div>
</div>
<?php if($send_info_career_job === true){ ?>
    <script type="text/javascript">
        $('#info-popup').html('Thông tin của bạn đã được gửi đi');
        $('.career-job-modal').modal();
    </script>
<?php }else if($send_info_career_job === false){ ?>
    <script type="text/javascript">
        $('#info-popup').html('Thông tin của bạn gửi đi bị lỗi, Hãy gửi lại.');
        $('.career-job-modal').modal();
    </script>
<?php } ?>