<!--page-container-->


    <div class="main-content-container">
        <div class="container">
            <div class="row">
                <?php $this->load->view('_partial/content/careerjob/menu'); ?>
                <section class="col-sm-9 main-content career-section">
                    <div class="section-content">
                        <div class="panel job-list-panel">
                            <h2 class="panel-title">
                                Các vị trí tuyển dụng
                                <span>(10)</span>
                            </h2>
                            <table class="table rwd-table" id="table-career-job">
                                <thead>
                                <tr>
                                    <th>
                                        Vị trí
                                    </th>
                                    <th>
                                        Số lượng
                                    </th>
                                    <th>
                                        Làm việc tại
                                    </th>
                                    <th class="text-center">
                                        Ngày hết hạn
                                    </th>
                                    <th class="text-center">
                                        Ứng tuyển ngay
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php for ($i = 0; $i < count($content); $i++): ?>
                                    <tr>
                                        <td data-th="Position">
                                            <div>
                                                <a href="<?php echo base_url().'career-job/detail/'.$content[$i]['id']; ?>">
                                                    <p class="position-title">
                                                        <?php echo $content[$i]['position']; ?>
                                                    </p>
                                                </a>
                                            </div>
                                        </td>
                                        <td data-th="Số lượng">
                                            <div>
                                                <?php echo $content[$i]['quantity']; ?>
                                            </div>
                                        </td>
                                        <td data-th="Làm việc tại">
                                            <div>
                                                <?php echo $content[$i]['workplace']; ?>
                                            </div>
                                        </td>
                                        <td class="text-center" data-th="Ngày hết hạn">
                                            <div>
                                                <?php echo $content[$i]['date_expired']; ?>
                                            </div>
                                        </td>
                                        <td class="text-center" data-th="Ứng tuyển">
                                            <?php echo $content[$i]['action']; ?>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <!-- <div class="quicklink-block">
<a class="quick-item ql-item-1" href="#" title=""></a>
<a class="quick-item ql-item-2" href="#" title=""></a>
<a class="quick-item ql-item-3" href="#" title=""></a>
</div> -->