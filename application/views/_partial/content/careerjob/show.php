<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('_partial/_head'); ?>
<link href="<?php echo base_url(); ?>assets/admin/spinner/Spinner.css" rel="stylesheet">
<body class="">
<!--mobile-menu-->
<a href="#" class="overlay sidebar-close-bg"></a>
<?php $this->load->view('_partial/_sidebar'); ?>

<!--header-->
<?php $this->load->view('_partial/_header'); ?>

<!--page-container-->
<main>
    <?php $this->load->view('_partial/content/careerjob/banner'); ?>

    <?php $this->load->view('_partial/content/careerjob/content'); ?>

</main>

<!--footer-->
<?php $this->load->view('_partial/_footer'); ?>
<script src="/assets/admin/spinner/Spinner.js"></script>
<script src="/assets/js/career_job.js"></script>

</body>
</html>