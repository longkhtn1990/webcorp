<aside class="col-sm-3 sidebar-nav career-sidebar multigroup-is-tab">
    <a class="tab-selector" href="#"></a>
    <ul class="list-group-wrap">
        <li>
            <ul class="list-group">
                <?php for ($i = 0; $i < count($career_job_menu); $i++): ?>
                    <li class="list-group-item active">
                        <a href="<?php echo $career_job_menu[$i]['link']; ?>">
                            <?php echo $career_job_menu[$i]['name']; ?>
                            <span>(<?php echo $career_job_menu[$i]['number']; ?>)</span>
                        </a>
                    </li>
                <?php endfor; ?>
            </ul>
        </li>
    </ul>
</aside>