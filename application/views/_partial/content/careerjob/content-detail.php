<div class="main-content-container">
    <div class="container">
        <div class="row">
            <?php $this->load->view('_partial/content/careerjob/menu'); ?>
            <section class="col-sm-9 main-content article-detail-section career-section">
                <div class="section-header">
                    <h2 class="section-title">Thực tập sinh</h2>
                </div>
                <div class="section-content">
                    <?php echo $detail['content']; ?>
                </div>
                <div class="section-footer">
                    <a class="btn btn-submit goto-apply-form" href="<?php echo base_url(); ?>career-job/apply/<?php echo $detail['id']; ?>">Ứng tuyển ngay</a>
                </div>
            </section>
        </div>
    </div>
</div>