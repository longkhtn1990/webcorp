<?php if($content['title'] != ''): ?>
<section class="section-block business-section">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title"><?php echo $content['title']; ?></h2>
        </div>
        <div class="section-content">
            <?php echo $content['content']; ?>
        </div>
    </div>
</section>
<?php endif;?>