<?php if ($content['global-partnership']['title'] != ''): ?>
    <section class="section-block global-partnership-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo $content['global-partnership']['title']; ?></h2>
            </div>
            <div class="section-content">
                <div class="row">
                    <div class="col-sm-6 col-sm-push-6 section-content-img">
                        <img src="<?php echo $content['global-partnership']['image']; ?>" alt="">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 section-content-text">
                        <?php echo $content['global-partnership']['content']; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ($content['partner']['title'] != ''): ?>
    <section class="section-block partnership-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo $content['partner']['title']; ?></h2>
            </div>
            <div class="section-content">
                <div class="partnership-list-block">

                    <?php if (count($content['partner']['items']) > 0): ?>
                        <?php for ($i = 0; $i < count($content['partner']['items']); $i++): ?>
                            <article class="col-xs-4 col-sm-3 partnership-item">
                                <figure class="img-wrap partnership-img-wrap">
                                    <a href="#" target="_blank">
                                        <img src="<?php echo $content['partner']['items'][$i]; ?>" alt="">
                                    </a>
                                </figure>
                            </article>
                        <?php endfor; ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ($content['customer']['title'] != ''): ?>
    <section class="section-block partnership-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo $content['customer']['title']; ?></h2>
            </div>
            <div class="section-content">
                <div class="partnership-list-block">
                    <?php if (count($content['customer']['items']) > 0): ?>
                        <?php for ($i = 0; $i < count($content['customer']['items']); $i++): ?>
                            <article class="col-xs-4 col-sm-3 partnership-item">
                                <figure class="img-wrap partnership-img-wrap">
                                    <a href="#" target="_blank">
                                        <img src="<?php echo $content['customer']['items'][$i]; ?>" alt="">
                                    </a>
                                </figure>
                            </article>
                        <?php endfor; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
