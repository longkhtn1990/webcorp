<div class="section-block news-media-section">
    <?php if ($content['new-update']['title'] != ''): ?>
        <div class="section-update">
            <div class="container">
                <a class="news-update" href="<?php echo $content['new-update']['link']; ?>"
                   title="<?php echo $content['new-update']['title']; ?>">
                    <span class="news-update-icon"><?php echo $content['new-update']['slogan']; ?></span>
                    <span class="news-update-link"><?php echo $content['new-update']['title']; ?></span>
                </a>
            </div>
        </div>
    <?php endif; ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-9 corp-news-list-block">
                <?php if ($content['news-company']['title'] != ''): ?>
                    <section class="section-block news-media-section-layout">
                        <div class="section-header">
                            <h2 class="section-title"><?php echo $content['news-company']['title']; ?></h2>
                        </div>
                        <div class="section-content">
                            <div class="news-article-slide">
                                <div class="row">
                                    <?php if ($content['news-company']['thumbnail']['title'] != ''): ?>
                                        <div class="col-sm-6 col-md-7 left-child">
                                            <article class="article-item media-style">
                                                <div class="article-item-inner">
                                                    <figure class="img-wrap media-img">
                                                        <a href="<?php echo $content['news-company']['thumbnail']['link']; ?>">
                                                            <img
                                                                src="<?php echo $content['news-company']['thumbnail']['image']; ?>"
                                                                alt="">
                                                        </a>
                                                    </figure>
													<div class="article-item-detail" style="margin-top:10px;">
                                                        <h3 class="article-item-title">
                                                            <a href="<?php echo $content['news-company']['thumbnail']['link']; ?>"><?php echo $content['news-company']['thumbnail']['title']; ?></a>
                                                        </h3>
                                                        <p class="short-info">
                                                            <?php echo $content['news-company']['thumbnail']['info']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (count($content['news-company']['list-news']) > 0): ?>
                                        <div class="col-sm-6  col-md-5 right-child">
                                            <?php for ($i = 0; $i < count($content['news-company']['list-news']) && $i < 5; $i++): ?>
                                                <article class="article-item media-style">
                                                    <div class="article-item-inner">
                                                        <div class="article-date-time">
                                                            <p>
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                <span><?php echo $content['news-company']['list-news'][$i]['calender']; ?></span>
                                                            </p>
                                                        </div>
                                                        <div class="article-item-detail">
                                                            <h3 class="article-item-title">
                                                                <a href="<?php echo $content['news-company']['list-news'][$i]['link']; ?>"><?php echo $content['news-company']['list-news'][$i]['title']; ?></a>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </article>
                                            <?php endfor; ?>

                                            <div class="clearfix">
                                                <a class="btn btn-submit see-all"
                                                   href="/more/company"><?php echo $content['news-company']['text-more'] != '' ? $content['news-company']['text-more'] : 'Read more'; ?>
                                                    <i
                                                        class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php endif; ?>

                <?php if ($content['news-market']['title'] != ''): ?>
                    <section class="section-block news-media-section-layout">
                        <div class="section-header">
                            <h2 class="section-title"><?php echo $content['news-market']['title']; ?></h2>
                        </div>
                        <div class="section-content">
                            <div class="news-article-slide">
                                <div class="row">
                                    <?php if ($content['news-market']['thumbnail']['title'] != ''): ?>
                                        <div class="col-sm-6 col-md-7 left-child">
                                            <article class="article-item media-style">
                                                <div class="article-item-inner">
                                                    <figure class="img-wrap media-img">
                                                        <a href="<?php echo $content['news-market']['thumbnail']['link']; ?>">
                                                            <img
                                                                src="<?php echo $content['news-market']['thumbnail']['image']; ?>"
                                                                alt="">
                                                        </a>
                                                    </figure>
													<div class="article-item-detail" style="margin-top:10px;">
                                                        <h3 class="article-item-title">
                                                            <a href="<?php echo $content['news-market']['thumbnail']['link']; ?>"><?php echo $content['news-market']['thumbnail']['title']; ?></a>
                                                        </h3>
                                                        <p class="short-info">
                                                            <?php echo $content['news-market']['thumbnail']['info']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (count($content['news-market']['list-news']) > 0): ?>
                                        <div class="col-sm-6  col-md-5 right-child">
                                            <?php for ($i = 0; $i < count($content['news-market']['list-news']) && $i < 5; $i++): ?>
                                                <article class="article-item media-style">
                                                    <div class="article-item-inner">
                                                        <div class="article-date-time">
                                                            <p>
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                <span><?php echo $content['news-market']['list-news'][$i]['calender']; ?></span>
                                                            </p>
                                                        </div>
                                                        <div class="article-item-detail">
                                                            <h3 class="article-item-title">
                                                                <a href="<?php echo $content['news-market']['list-news'][$i]['link']; ?>"><?php echo $content['news-market']['list-news'][$i]['title']; ?></a>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </article>
                                            <?php endfor; ?>

                                            <div class="clearfix">
                                                <a class="btn btn-submit see-all"
                                                   href="/more/market"><?php echo $content['news-market']['text-more'] != '' ? $content['news-market']['text-more'] : 'Read more'; ?>
                                                    <i
                                                        class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php endif; ?>

                <?php if ($content['news-general']['title'] != ''): ?>
                    <section class="section-block news-media-section-layout">
                        <div class="section-header">
                            <h2 class="section-title"><?php echo $content['news-general']['title']; ?></h2>
                        </div>
                        <div class="section-content">
                            <div class="news-article-slide">
                                <div class="row">
                                    <?php if ($content['news-general']['thumbnail']['title'] != ''): ?>
                                        <div class="col-sm-6 col-md-7 left-child">
                                            <article class="article-item media-style">
                                                <div class="article-item-inner">
                                                    <figure class="img-wrap media-img">
                                                        <a href="<?php echo $content['news-general']['thumbnail']['link']; ?>">
                                                            <img
                                                                src="<?php echo $content['news-general']['thumbnail']['image']; ?>"
                                                                alt="">
                                                        </a>
                                                    </figure>
													<div class="article-item-detail" style="margin-top:10px;">
                                                        <h3 class="article-item-title">
                                                            <a href="<?php echo $content['news-general']['thumbnail']['link']; ?>"><?php echo $content['news-general']['thumbnail']['title']; ?></a>
                                                        </h3>
                                                        <p class="short-info">
                                                            <?php echo $content['news-general']['thumbnail']['info']; ?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </article>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (count($content['news-general']['list-news']) > 0): ?>
                                        <div class="col-sm-6  col-md-5 right-child">
                                            <?php for ($i = 0; $i < count($content['news-general']['list-news']) && $i < 5; $i++): ?>
                                                <article class="article-item media-style">
                                                    <div class="article-item-inner">
                                                        <div class="article-date-time">
                                                            <p>
                                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                                <span><?php echo $content['news-general']['list-news'][$i]['calender']; ?></span>
                                                            </p>
                                                        </div>
                                                        <div class="article-item-detail">
                                                            <h3 class="article-item-title">
                                                                <a href="<?php echo $content['news-general']['list-news'][$i]['link']; ?>"><?php echo $content['news-general']['list-news'][$i]['title']; ?></a>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </article>
                                            <?php endfor; ?>

                                            <div class="clearfix">
                                                <a class="btn btn-submit see-all"
                                                   href="/more/general"><?php echo $content['news-general']['text-more'] != '' ? $content['news-general']['text-more'] : 'Read more'; ?>
                                                    <i
                                                        class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php endif; ?>
            </div>

            <div class="col-xs-12 col-md-3 sidebar-block">
                <?php if($content['newspaper']['title'] != '' && count($content['newspaper']['list-news']) > 0): ?>
                <div class="section-sidebar">
                    <div class="section-header">
                        <h2 class="section-title"><?php echo $content['newspaper']['title']?></h2>
                    </div>
                    <div class="block press-list-block">
                        <ul class="block-list">
                            <?php for($i=0;$i < count($content['newspaper']['list-news']) && $i < 5;$i++): ?>
                            <li class="item">
                                <a class="item-title" href="<?php echo $content['newspaper']['list-news'][$i]['link']; ?>"><?php echo $content['newspaper']['list-news'][$i]['title']; ?></a>
                            </li>
                            <?php endfor; ?>
                        </ul>
                        <a class="btn btn-submit see-all" href="/more/newspaper"><?php echo $content['newspaper']['text-more']; ?> <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($content['introduce']['title'] != ''): ?>
                <div class="section-sidebar">
                    <div class="section-header">
                        <h2 class="section-title"><?php echo $content['introduce']['title']?></h2>
                    </div>
                    <div class="block video-wrapper">
                        <!--thêm ?autoplay=0&showinfo=0&controls=0 sau Id video để ẩn control-bar-->
                        <iframe width="560" height="315" src="<?php echo $content['introduce']['video-url']; ?>?autoplay=0&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <?php if(count($content['introduce']['list-news']) > 0):?>
                    <div class="block press-list-block video-list-block">
                        <ul class="block-list">
                            <?php for($i = 0;$i < count($content['introduce']['list-news']) && $i < 5;$i++): ?>
                                <li class="item">
                                    <a target="_blank" class="item-title"  href="<?php echo $content['introduce']['list-news'][$i]['link']; ?>"><?php echo $content['introduce']['list-news'][$i]['title']; ?></a>
                                </li>
                            <?php endfor; ?>
                        </ul>
                    </div>
                    <?php endif;?>
                </div>
                <?php endif;?>

                <?php if($content['download-information']['title'] != '' && count($content['download-information']['list-news']) > 0): ?>
                    <div class="section-sidebar">
                        <div class="section-header">
                            <h2 class="section-title"><?php echo $content['download-information']['title']?></h2>
                        </div>
                        <div class="block gray-block pdf-block-list">
                            <ul class="block-list">
                                <?php for($i=0;$i < count($content['download-information']['list-news']) && $i < 5;$i++): ?>
                                    <li class="item">
                                        <a class="item-title" href="<?php echo $content['download-information']['list-news'][$i]['link']; ?>">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                            <span><?php echo $content['download-information']['list-news'][$i]['title']; ?></span>
                                        </a>
                                    </li>
                                <?php endfor; ?>
                            </ul>
                         </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>