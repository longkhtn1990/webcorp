<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('_partial/_head'); ?>

<body class="">
<!--mobile-menu-->
<a href="#" class="overlay sidebar-close-bg"></a>
<?php $this->load->view('_partial/_sidebar'); ?>

<!--header-->
<?php $this->load->view('_partial/_header'); ?>

<!--page-container-->
<main>
    <?php $this->load->view('_partial/content/news/banner_detail'); ?>

    <?php $this->load->view('_partial/content/news/content-detail'); ?>

</main>

<!--footer-->
<?php $this->load->view('_partial/_footer'); ?>

</body>
</html>