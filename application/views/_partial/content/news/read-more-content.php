<div class="section-block news-media-section">
    <div class="section-update">
        <div class="container">
            <a class="news-update" href="<?php echo $new_last['item']['link'];?>" title="<?php echo $new_last['title']['item']['title'];?>">
                <span class="news-update-icon"><?php echo $new_last['title'];?></span>
                <span class="news-update-link"><?php echo $new_last['item']['title'];?></span>
            </a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-9 corp-news-list-block">
                <section class="section-block news-media-section-layout">
                    <div class="section-header">
                        <h2 class="section-title"><?php echo $list['title']; ?></h2>
                    </div>
                    <div class="section-content">
                        <div class="row">
                            <?php for ($i = 0; $i < count($list['list']); $i++): ?>
                                <div class="col-sm-6">
                                    <article class="article-item media-style media-style-h2">
                                        <div class="article-item-inner">
                                            <figure class="img-wrap media-img">
                                                <a href="<?php echo $list['list'][$i]['link']; ?>">
                                                    <img src="<?php echo $list['list'][$i]['image']; ?>" alt="ITL">
                                                </a>
                                            </figure>
                                            <div class="article-item-detail" style="margin-top:10px;">
                                                <h3 class="article-item-title">
                                                    <a href="<?php echo $list['list'][$i]['link']; ?>">
                                                        <?php echo strlen($list['list'][$i]['title'])>65?substr($list['list'][$i]['title'],0,65).'... .':$list['list'][$i]['title']; ?>
                                                    </a>
                                                </h3>
                                                <p class="short-info">
                                                    <?php echo strlen($list['list'][$i]['info'])>65?substr($list['list'][$i]['info'],0,65).'... .':$list['list'][$i]['info']; ?>
                                                </p>
                                                <div class="article-footer">
                                                    <a class="btn btn-link" href="<?php echo $list['list'][$i]['link']; ?>">Xem chi tiết</a>
                                                </div>
                                            </div>
                                        </div>
                                    </article>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="seciton-footer">
                        <div class="text-right pagination-nav">
                            <ul class="pagination" data-current="<?php echo $page; ?>" data-total="<?php echo $total_page; ?>" id="paginator">
                                <?php
                                    $total = intval($total_page / $limit);
                                    if($limit * $total < $total_page){
                                        $total = $total + 1;
                                    }
                                ?>
                                <li>
                                    <a title="" href="<?php if($page <= $total && $page > 1){ echo base_url().'more/'.$currentPage.'?page='.($page-1); }else { echo "javascript:void(0);"; } ?>" ><i class="fa fa-caret-left" aria-hidden="true"></i></a>
                                </li>
                                <?php for ($i = 1; $i <= $total; $i++): ?>
                                    <li <?php if($page == $i): echo 'class="active"'; endif;?> >
                                        <a title="" href="<?php echo base_url().'more/'.$currentPage.'?page='.$i?>"><?php echo $i; ?></a>
                                    </li>
                                <?php endfor; ?>
                                <li>
                                    <a title="" href="<?php if($page <= $total && $page < $total){ echo base_url().'more/'.$currentPage.'?page='.($page+1); }else{ echo "javascript:void(0);"; } ?>" ><i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-xs-12 col-md-3 sidebar-block">
                <?php if($content['newspaper']['title'] != '' && count($content['newspaper']['list-news']) > 0): ?>
                    <div class="section-sidebar">
                        <div class="section-header">
                            <h2 class="section-title"><?php echo $content['newspaper']['title']?></h2>
                        </div>
                        <div class="block press-list-block">
                            <ul class="block-list">
                                <?php for($i=0;$i < count($content['newspaper']['list-news']) && $i < 5;$i++): ?>
                                    <li class="item">
                                        <a class="item-title" href="<?php echo $content['newspaper']['list-news'][$i]['link']; ?>"><?php echo $content['newspaper']['list-news'][$i]['title']; ?></a>
                                    </li>
                                <?php endfor; ?>
                            </ul>
                            <a class="btn btn-submit see-all" href="/more/newspaper"><?php echo $content['newspaper']['text-more']; ?> <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if($content['introduce']['title'] != ''): ?>
                    <div class="section-sidebar">
                        <div class="section-header">
                            <h2 class="section-title"><?php echo $content['introduce']['title']?></h2>
                        </div>
                        <div class="block video-wrapper">
                            <!--thêm ?autoplay=0&showinfo=0&controls=0 sau Id video để ẩn control-bar-->
                            <iframe width="560" height="315" src="<?php echo $content['introduce']['video-url']; ?>?autoplay=0&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                        <?php if(count($content['introduce']['list-news']) > 0):?>
                            <div class="block press-list-block video-list-block">
                                <ul class="block-list">
                                    <?php for($i = 0;$i < count($content['introduce']['list-news']) && $i < 5;$i++): ?>
                                        <li class="item">
                                            <a target="_blank" class="item-title"  href="<?php echo $content['introduce']['list-news'][$i]['link']; ?>"><?php echo $content['introduce']['list-news'][$i]['title']; ?></a>
                                        </li>
                                    <?php endfor; ?>
                                </ul>
                            </div>
                        <?php endif;?>
                    </div>
                <?php endif;?>

                <?php if($content['download-information']['title'] != '' && count($content['download-information']['list-news']) > 0): ?>
                    <div class="section-sidebar">
                        <div class="section-header">
                            <h2 class="section-title"><?php echo $content['download-information']['title']?></h2>
                        </div>
                        <div class="block gray-block pdf-block-list">
                            <ul class="block-list">
                                <?php for($i=0;$i < count($content['download-information']['list-news']) && $i < 5;$i++): ?>
                                    <li class="item">
                                        <a class="item-title" href="<?php echo $content['download-information']['list-news'][$i]['link']; ?>">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                            <span><?php echo $content['download-information']['list-news'][$i]['title']; ?></span>
                                        </a>
                                    </li>
                                <?php endfor; ?>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>