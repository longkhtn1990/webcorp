<?php if($content['title'] != ''): ?>
    <section class="section-block news-detail-section">
        <div class="container">
            <ol class="breadcrumb" style="position: relative;">
                <li><a href="<?php echo base_url(); ?>">Trang chủ</a></li>
                <li><a href="<?php echo base_url(); ?>news-media">Tin tức</a></li>
                <li class="active"><?php echo $content['title']; ?></li>
            </ol>
            <div class="section-content article-detail-content">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="section-header">
                            <div class="article-date-time">
                                <p class="day-time"><?php echo date('d',$content['date']); ?></p>
                                <p class="month-year-time">
                                    <span class="month-time"><?php echo date('m',$content['date']); ?></span>/<span class="year-time"><?php echo date('Y',$content['date']); ?></span>
                                </p>
                            </div>
                            <h2 class="section-title"><?php echo $content['title']; ?></h2>
                        </div>
                        <div class="section-content">
                            <?php echo $content['content']; ?>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-4 right-sidebar">
                        <h2 class="form-title">Tin tức nổi bật</h2>
                        <div class="tutorials-list-slide">
                            <ul class="block-list">
                                <?php for ($i = 0; $i < count($list_highlight); $i++) : ?>
                                    <li class="item">
                                        <a class="item-title" href="/news-media/<?php echo $list_highlight[$i]['id']; ?>"><?php echo $list_highlight[$i]['title']; ?></a>
                                        <p class="date-time"><?php echo $list_highlight[$i]['calender']; ?></p>
                                        <!--<p class="item-description">
                                            <?php /*echo $list_highlight[$i]['content']; */?>
                                        </p>-->
                                    </li>
                                <?php endfor;?>
                            </ul>
                        </div>
                        <h2 class="form-title">Tin tức mới nhất</h2>
                        <div class="tutorials-list-slide">
                            <ul class="block-list">
                                <?php for ($i = 0; $i < count($list); $i++) : ?>
                                    <li class="item">
                                        <a class="item-title" href="/news-media/<?php echo $list[$i]['id']; ?>"><?php echo $list[$i]['title']; ?></a>
                                        <p class="date-time"><?php echo $list[$i]['calender']; ?></p>
                                        <!--<p class="item-description">
                                            <?php /*echo $list[$i]['content']; */?>
                                        </p>-->
                                    </li>
                                <?php endfor;?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif;?>
