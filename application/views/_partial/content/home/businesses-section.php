<section class="section-block businesses-section">
    <div class="container">
        <div class="section-header">
            <!--<h2 class="section-title"><?php //echo isset($business['title']) ? $business['title'] : 'Các sản phẩm kinh doanh'; ?></h2>-->
        </div>
        <div class="section-content">
            <div class="row">
                <?php if (count($business['item']) > 0) : ?>
                    <?php for ($i = 0; $i < count($business['item']); $i++): ?>
                        <article class="col-sm-6 article-item business-style">
                            <div class="article-item-inner">
                                <figure class="img-wrap">
                                    <a href="<?php echo $business['item'][$i]['link'] != '' ? $business['item'][$i]['link'] : 'javascript:void(0);'; ?>">
                                        <img src="<?php echo isset($business['item'][$i]['image']) ? $business['item'][$i]['image'] : ''; ?>" alt="">
                                    </a>
                                </figure>
                                <div class="article-item-detail">
                                    <h3 class="article-item-title"><?php echo $business['item'][$i]['text'] != '' ? $business['item'][$i]['text'] : '';  ?></h3>
                                </div>
                            </div>
                        </article>
                    <?php endfor; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>