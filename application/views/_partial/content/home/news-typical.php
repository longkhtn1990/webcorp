<?php if($typical['display'] == true): ?>
    <section class="section-block news-section-layout news-section-2">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo isset($typical['title']) ? $typical['title'] : 'Thông tin thị trường'; ?></h2>
            </div>
            <div class="section-content">
                <div class="news-article-slide">
                    <div class="row">
                        <div class="col-md-6 left-child">
                            <article class="article-item news-style">
                                <div class="article-item-inner">
                                    <figure class="img-wrap">
                                        <a href="<?php echo $typical['main']['link'] != '' ? $typical['main']['link'] : 'javascript:void(0);'; ?>">
                                            <img
                                                src="<?php echo $typical['main']['image'] != '' ? $typical['main']['image'] : 'assets/images/upload/news-item-2.jpg'; ?>"
                                                alt="CNS">
                                        </a>
                                    </figure>
                                    <div class="article-date-time">
                                        <p>
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                            <span><?php echo $typical['main']['calender'] != '' ? $typical['main']['calender'] : ''; ?></span>
                                        </p>
                                    </div>
                                    <div class="article-item-detail">
                                        <h3 class="article-item-title">
                                            <a href="<?php echo $typical['main']['link'] != '' ? $typical['main']['link'] : 'javascript:void(0);'; ?>"><?php echo isset($typical['main']['title']) ? $typical['main']['title'] : ''; ?></a>
                                        </h3>
                                        <p class="short-info"><?php echo $typical['main']['content'] != '' ? $typical['main']['content'] : ''; ?>
                                            ...</p>
                                    </div>
                                </div>
                            </article>
                        </div>
                        <div class="col-md-6 right-child">

                            <?php if (count($typical['extra']) > 0): ?>
                                <?php for ($i = 0; $i < count($typical['extra']); $i++): ?>
                                    <article class="article-item news-style">
                                        <div class="article-item-inner">
                                            <div class="article-date-time">
                                                <p>
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    <span><?php echo $typical['extra'][$i]['calender'] != '' ? $typical['extra'][$i]['calender'] : ''; ?></span>
                                                </p>
                                            </div>
                                            <div class="article-item-detail">
                                                <h3 class="article-item-title">
                                                    <a href="<?php echo $typical['extra'][$i]['link'] != '' ? $typical['extra'][$i]['link'] : ''; ?>"><?php echo $typical['extra'][$i]['title'] != '' ? $typical['extra'][$i]['title'] : ''; ?></a>
                                                </h3>
                                                <p class="short-info"><?php echo $typical['extra'][$i]['content'] != '' ? $typical['extra'][$i]['content'] : ''; ?></p>
                                            </div>
                                        </div>
                                    </article>
                                <?php endfor; ?>
                            <?php endif; ?>

                            <?php if (count($typical['extra']) > 2): ?>
                            <div class="clearfix">
                                <a class="btn btn-submit see-all" href="#"><?php echo $typical['moreText'] != '' ? $typical['moreText'] : 'Read more'; ?> <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>