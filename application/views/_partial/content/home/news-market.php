<?php if($market['display'] == true): ?>
<section class="section-block news-section-layout news-section-1">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title"><?php echo isset($market['title']) ? $market['title'] : 'Thông tin thị trường'; ?></h2>
        </div>
        <div class="section-content">
            <div class="news-article-slide">
                <div class="row">
                    <div class="col-md-6 left-child">
                        <article class="article-item news-style">
                            <div class="article-item-inner">
                                <figure class="img-wrap">
                                    <a href="<?php echo $market['main']['link'] != '' ? $market['main']['link'] : 'javascript:void(0);'; ?>">
                                        <img
                                            src="<?php echo $market['main']['image'] != '' ? $market['main']['image'] : 'assets/images/upload/news-item-2.jpg'; ?>"
                                            alt="CNS">
                                    </a>
                                </figure>
                                <div class="article-date-time">
                                    <p>
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                        <span><?php echo $market['main']['calender'] != '' ? $market['main']['calender'] : ''; ?></span>
                                    </p>
                                </div>
                                <div class="article-item-detail">
                                    <h3 class="article-item-title">
                                        <a href="<?php echo $market['main']['link'] != '' ? $market['main']['link'] : 'javascript:void(0);'; ?>"><?php echo isset($market['main']['title']) ? $market['main']['title'] : ''; ?></a>
                                    </h3>
                                    <p class="short-info"><?php echo $market['main']['content'] != '' ? $market['main']['content'] : ''; ?>
                                        ...</p>
                                </div>
                            </div>
                        </article>
                    </div>
                    <div class="col-md-6 right-child">
                        <?php if (count($market['extra']) > 0): ?>
                            <?php for ($i = 0; $i < count($market['extra']); $i++): ?>
                                <article class="article-item news-style">
                                    <div class="article-item-inner">
                                        <div class="article-date-time">
                                            <p>
                                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                                <span><?php echo $market['extra'][$i]['calender'] != '' ? $market['extra'][$i]['calender'] : ''; ?></span>
                                            </p>
                                        </div>
                                        <div class="article-item-detail">
                                            <h3 class="article-item-title">
                                                <a href="<?php echo $market['extra'][$i]['link'] != '' ? $market['extra'][$i]['link'] : ''; ?>"><?php echo $market['extra'][$i]['title'] != '' ? $market['extra'][$i]['title'] : ''; ?></a>
                                            </h3>
                                            <p class="short-info"><?php echo $market['extra'][$i]['content'] != '' ? $market['extra'][$i]['content'] : ''; ?></p>
                                        </div>
                                    </div>
                                </article>
                            <?php endfor; ?>
                        <?php endif; ?>

                        <?php if(count($market['extra']) > 2): ?>
                        <div class="clearfix">
                            <a class="btn btn-submit see-all" href="#"><?php echo $market['moreText'] != '' ? $market['moreText'] : 'Read more'; ?> <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>