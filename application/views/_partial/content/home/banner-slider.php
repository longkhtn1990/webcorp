<section class="banner banner-slider">
    <?php if (count($mainBanner) > 0): ?>
        <?php for ($i = 0; $i < count($mainBanner); $i++): ?>
            <div class="banner-inner" style="background-image: url(<?php echo $mainBanner[$i]['image'] ?>);">
                <a class="view-detail-article" href="<?php echo $mainBanner[$i]['link'] ?>"></a>
                <div class="container banner-text">
                    <div class="banner-text-inner">
                        <h2 class="section-title"><?php echo $mainBanner[$i]['title'] ?></h2>
                        <p class="section-description"><?php echo $mainBanner[$i]['description'] ?></p>
                    </div>
                </div>
            </div>
        <?php endfor; ?>
    <?php endif; ?>
</section>