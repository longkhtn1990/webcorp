<section class="section-block project-section">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title"><?php $content['title'] != '' ? $content['title'] : ''; ?></h2>
        </div>
        <div class="section-content">
            <div class="row industry-news-list-block">
                <?php if (count($content['items']) > 0): ?>
                    <?php for ($i = 0; $i < count($content['items']); $i++): ?>
                        <article class="col-sm-6 col-md-3 article-item news-media-style">
                            <div class="article-item-inner">
                                <div class="article-header">
                                    <!--<div class="article-date-time">
                                        <p class="day-time"><?php /*echo $content['items'][$i]['day']; */?></p>
                                        <p class="month-year-time">
                                            <span class="month-time"><?php /*echo $content['items'][$i]['month']; */?></span>/<span class="year-time"><?php /*echo $content['items'][$i]['year']; */?></span>
                                        </p>
                                    </div>-->
                                    <figure class="img-wrap media-img-2">
                                        <a href="<?php echo $content['items'][$i]['link']; ?>">
                                            <img src="<?php echo $content['items'][$i]['image']; ?>" alt="">
                                        </a>
                                    </figure>
                                </div>
                                <div class="article-item-detail">
                                    <h3 class="article-item-title">
                                        <a href="<?php echo $content['items'][$i]['link']; ?>"><?php echo $content['items'][$i]['name']; ?></a>
                                    </h3>
                                    <a class="goto-detail" href="<?php echo $content['items'][$i]['link']; ?>" tabindex="0"><?php echo $content['items'][$i]['text_button']; ?></a>
                                </div>
                            </div>
                        </article>
                    <?php endfor; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>