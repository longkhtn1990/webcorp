<div class="sub-menu-nav sub-menu-nav-secondary">
    <nav class="navbar navbar-tab-style project-navbar">
        <ul class="nav navbar-nav">
            <?php if (count($menu) > 0): ?>
                <?php for ($i = 0; $i < count($menu); $i++): ?>
                    <li class="<?php echo $menu[$i]['active'] == true ? 'active' : ''; ?>" style="width: 50%">
                        <a href="<?php echo $menu[$i]['link'] == true ? $menu[$i]['link'] : 'javascript:void(0)'; ?>">
                            <span><?php echo $menu[$i]['text'] == true ? $menu[$i]['text'] : ''; ?></span>
                        </a>
                    </li>
                <?php endfor; ?>

            <?php endif; ?>
        </ul>
    </nav>
</div>