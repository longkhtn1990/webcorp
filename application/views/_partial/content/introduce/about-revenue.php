<?php if (count($content['news']) > 0): ?>
    <section class="section-block business-section">
        <div class="container">

			<?php for ($i = 0; $i < count($content['news']); $i++):?>
					<div class="section-header">
						<h2 class="section-title"><?php echo $content['news'][$i]['title'] != '' ? $content['news'][$i]['title'] : ''; ?></h2>
					</div>
					<div class="section-content">
						<div class="content-body">
							<div class="row">
								<div class="col-sm-12 section-content-text">
									<div>
										<?php echo $content['news'][$i]['content'] != '' ? $content['news'][$i]['content'] : ''; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php endfor; ?>
        </div>
    </section>
<?php endif; ?>
