<div id="introduce-<?php echo $currentPage != '' ? $currentPage : 'about-model'; ?>" class="introduce-content">
    <?php if ($content['culture']['title'] != ''): ?>
        <section class="section-block culture-section">
            <div class="container">
                <div class="section-header">
                    <h2 class="section-title"><?=$content['culture']['title'];?></h2>
                </div>
                <div class="section-content group-layout-container">
                    <div class="group-layout-row">
                        <?php if (count($content['culture']['member']) > 0): ?>
                        <div class="group-layout-item">
                            <figure class="img-wrap">
                                <a href="">
                                    <img src="<?php echo $content['culture']['member'][0]['image']; ?>" alt="">
                                </a>
                            </figure>
                            <div class="article-item-detail">
                                <h4 class="item-title">Ông <strong><?php echo $content['culture']['member'][0]['name'];?></strong></h4>
                                <p class="item-short-detail"><?php echo $content['culture']['member'][0]['position'];?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                    <div class="group-layout-row">
                        <?php if (count($content['culture']['member']) > 1): ?>
                            <?php for ($i = 1; $i < count($content['culture']['member']); $i++):?>
                                <div class="group-layout-item">
                                    <figure class="img-wrap">
                                        <a href="">
                                            <img src="<?php echo $content['culture']['member'][$i]['image']; ?>" alt="">
                                        </a>
                                    </figure>
                                    <div class="article-item-detail">
                                        <h4 class="item-title">Ông <strong><?php echo $content['culture']['member'][$i]['name'];?></strong></h4>
                                        <p class="item-short-detail"><?php echo $content['culture']['member'][$i]['position'];?></p>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
	<?php if ($content['culture_member']['title'] != ''): ?>
		<section class="section-block culture-section">
			<div class="container">
				<div class="section-header">
					<h2 class="section-title"><?=$content['culture_member']['title'];?></h2>
				</div>
				<div class="section-content group-layout-container">
					<div class="group-layout-row">
						<?php if (count($content['culture_member']['member']) > 0): ?>
							<div class="group-layout-item">
								<figure class="img-wrap">
									<a href="">
										<img src="<?php echo $content['culture_member']['member'][0]['image']; ?>" alt="">
									</a>
								</figure>
								<div class="article-item-detail">
									<h4 class="item-title">Ông <strong><?php echo $content['culture_member']['member'][0]['name'];?></strong></h4>
									<p class="item-short-detail"><?php echo $content['culture_member']['member'][0]['position'];?></p>
								</div>
							</div>
						<?php endif; ?>
					</div>
					<div class="group-layout-row">
						<?php if (count($content['culture_member']['member']) > 1): ?>
							<?php for ($i = 1; $i < count($content['culture_member']['member']); $i++):?>
								<div class="group-layout-item">
									<figure class="img-wrap">
										<a href="">
											<img src="<?php echo $content['culture_member']['member'][$i]['image']; ?>" alt="">
										</a>
									</figure>
									<div class="article-item-detail">
										<h4 class="item-title">Ông <strong><?php echo $content['culture_member']['member'][$i]['name'];?></strong></h4>
										<p class="item-short-detail"><?php echo $content['culture_member']['member'][$i]['position'];?></p>
									</div>
								</div>
							<?php endfor; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>
	<?php if ($content['stories_member']['title'] != ''): ?>
	<section class="section-block stories-section">
		<div class="container">
			<div class="section-header">
				<h2 class="section-title"><?php echo $content['stories_member']['title']; ?></h2>
			</div>
			<div class="section-content">
				<div class="row">
					<div class="col-md-push-0 col-md-12 awards-list-block" style="word-wrap: break-word;">
						<?php echo $content['stories_member']['content']; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
	<?php endif; ?>

    <?php if ($content['stories']['title'] != ''): ?>
    <section class="section-block stories-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo $content['stories']['title']; ?></h2>
            </div>
            <div class="section-content">
                <div class="row">
                    <div class="col-md-push-6 col-md-6 awards-list-block">
                        <div class="row memeberListSlider">
                            <?php if (count($content['stories']['member']) > 0): ?>
                            <?php $memberRow = 0; ?>
                            <?php for ($i = 0; $i < count($content['stories']['member']); $i++):?>
                                    <div class="col-xs-6 member-item">
                                        <a class="active" href="javascript:void(0)" onclick="loadInfoMember(this)">
                                            <figure class="article-img member-item-img">
                                                <img src="<?php echo $content['stories']['member'][$i]['image']; ?>"
                                                     alt="">
                                            </figure>
                                            <input type="hidden" class="member-name-hidden"
                                                   value="<?php echo $content['stories']['member'][$i]['name']; ?>"/>
                                            <input type="hidden" class="member-position-hidden"
                                                   value="<?php echo $content['stories']['member'][$i]['position']; ?>"/>
                                            <input type="hidden" class="member-info-hidden"
                                                   value="<?php echo $content['stories']['member'][$i]['info']; ?>"/>
                                        </a>
                                    </div>
                            <?php endfor; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <article class="col-md-pull-6 col-md-6 article-item member-detail-block">
                        <?php if ($content['stories']['member'][0]['image'] != ''): ?>
                        <div class="article-item-inner">
                            <figure id="member-detail-img" class="article-img member-detail-img">
                                <img src="<?php echo $content['stories']['member'][0]['image']; ?>" alt="">
                            </figure>
                            <div class="article-item-detail">
                                <h3 class="article-item-title">
                                    <span id="member-name" class="member-name"><?php echo $content['stories']['member'][0]['name'];?></span>
                                    <span class="mb-icon"> - </span>
                                    <span id="member-position" class="member-position"><?php echo $content['stories']['member'][0]['position'];?></span>
                                </h3>
                                <div id="member-info" class="short-info">
                                    <p><?php echo $content['stories']['member'][0]['info']; ?></p>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>
                    </article>
                </div>
            </div>
        </div>
</div>
</section>
<?php endif; ?>



</div>
