<!DOCTYPE html>
<html class="" lang="en">

<?php $this->load->view('_partial/_head'); ?>

<body class="">
<!--mobile-menu-->
<a href="#" class="overlay sidebar-close-bg"></a>
<?php $this->load->view('_partial/_sidebar'); ?>

<!--header-->
<?php $this->load->view('_partial/_header'); ?>

<!--page-container-->
<main>
    <?php $this->load->view('_partial/content/introduce/banner'); ?>

    <?php $this->load->view('_partial/content/introduce/menu'); ?>
    <?php if ($currentType == 'about-news'): ?>
        <?php $this->load->view('_partial/content/introduce/about-news'); ?>
    <?php endif; ?>
    <?php if ($currentType == 'about-revenues'): ?>
        <?php $this->load->view('_partial/content/introduce/about-revenue'); ?>
    <?php endif; ?>
    <?php if ($currentType == 'about-model'): ?>
        <?php $this->load->view('_partial/content/introduce/about-model'); ?>
    <?php endif; ?>
    <?php if ($currentType == 'about-member'): ?>
        <?php $this->load->view('_partial/content/introduce/about-member'); ?>
    <?php endif; ?>
    <?php if ($currentType == 'about-us'): ?>
        <?php $this->load->view('_partial/content/introduce/about-us'); ?>
    <?php endif; ?>

</main>

<!--footer-->
<?php $this->load->view('_partial/_footer'); ?>

</body>
</html>
