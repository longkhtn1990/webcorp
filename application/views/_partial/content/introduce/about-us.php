<div id="introduce-<?php echo $currentPage != '' ? $currentPage : 'about-us'; ?>" class="introduce-content">
    <?php if ($content['about']['title'] != ''): ?>
        <section class="section-block about-section">
            <div class="container">
                <div class="section-header">
                    <h2 class="section-title"><?php echo $content['about']['title'] != '' ? $content['about']['title'] : ''; ?></h2>
                </div>
                <div class="section-content">
                    <?php echo $content['about']['content'] != '' ? $content['about']['content'] : ''; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php if ($content['about-core']['title'] != ''): ?>
        <section class="section-block about-core-section" style="background-color: #cfdfed;background-size: cover;background-position: center center;background-repeat: no-repeat;background-image: url('<?php echo $content['about-core']['banner_image']; ?>');">
            <div class="container">
                <div class="container-inner">
                    <div class="col-sm-6 section-content-description">
                        <div class="section-header">
                            <h2 class="section-title"><?php echo $content['about-core']['title'] != '' ? $content['about-core']['title'] : ''; ?></h2>
                        </div>
                        <div class="section-content">
                            <?php if (count($content['about-core']['article-item']) > 0): ?>
                                <?php for ($i = 0; $i < count($content['about-core']['article-item']); $i++): ?>
                                    <article class="core-article-item">
                                        <h3 class="article-item-title"><?php echo $content['about-core']['article-item'][$i]['title']; ?></h3>
                                        <div class="short-info">
                                            <?php echo $content['about-core']['article-item'][$i]['info']; ?>
                                        </div>
                                    </article>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php if ($content['social-responsibility']['title'] != ''): ?>
        <section class="section-block social-responsibility-section">
            <div class="container">
                <div class="container-inner" style="background-image: url('<?php echo $content['social-responsibility']['banner_image']; ?>');background-position: right center;background-repeat: no-repeat;">
                    <div class="col-sm-6 section-content-description">
                        <div class="section-header">
                            <h2 class="section-title"><?php echo $content['social-responsibility']['title']; ?></h2>
                        </div>
                        <div class="section-content">
                            <?php echo $content['social-responsibility']['content'] ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php if (count($content['our-network']) > 0): ?>
        <div class="our-network-slide-wrap">
            <div class="our-network-slide ourNetworkSlide">
                <?php for ($i = 0; $i < count($content['our-network']); $i++): ?>
                    <section class="section-block our-network-section">
                        <figure class="our-network-section-img">
                            <img src="<?php echo base_url() . '/assets/images/upload/our-value-bg-' . $i . '.jpg' ?>"
                                 alt="">
                        </figure>
                        <div class="container">
                            <div class="section-content">
                                <div class="col-sm-6 col-sm-push-6 network-list-block">
                                    <div class="row network-list-group">
                                        <?php if (isset($content['our-network'][$i]['item-1'])): ?>
                                            <div class="col-xs-6 col-sm-5 network-item">
                                                <div class="network-item-img">
                                                    <img src="<?php echo $content['our-network'][$i]['item-1']['image']; ?>" alt="">
                                                </div>
                                                <div class="network-item-number">
                                                    <span ><?php echo $content['our-network'][$i]['item-1']['number']; ?></span>
                                                        <span class="text-sup-unit"><?php echo $content['our-network'][$i]['item-1']['unit']; ?></span>
                                                </div>
                                                <div class="network-item-text">
                                                    <span><?php echo $content['our-network'][$i]['item-1']['text']; ?></span>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php if (isset($content['our-network'][$i]['item-2'])): ?>
                                            <div class="col-xs-6 col-sm-5 network-item">
                                                <div class="network-item-img">
                                                    <img src="<?php echo $content['our-network'][$i]['item-2']['image']; ?>" alt="ITL">
                                                </div>
                                                <div class="network-item-number">
                                                    <span ><?php echo $content['our-network'][$i]['item-2']['number']; ?></span>
                                                    <span class="text-sup-unit"><?php echo $content['our-network'][$i]['item-2']['unit']; ?></span>
                                                </div>
                                                <div class="network-item-text">
                                                    <span><?php echo $content['our-network'][$i]['item-2']['text']; ?></span>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-sm-pull-6 section-content-description">
                                    <div class="section-content-inner">
                                        <h2 class="section-title"><?php echo $content['our-network'][$i]['title']; ?></h2>
                                        <div class="content-description-text">
                                            <?php echo $content['our-network'][$i]['content']; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php endfor; ?>
            </div>
            <div class="container nav-arrow-slide-wrap">
                <div class="slide-count-wrap">
                    <span class="current currentNumber"></span>/<span class="total totalNumber"></span>
                </div>
                <div class="nav-arrow-slide">
                    <button type="button" data-role="none" class="slick-prev slick-arrow custom-btn-prev"
                            aria-label="Previous" role="button" style="display: block;">Previous
                    </button>
                    <button type="button" data-role="none" class="slick-next slick-arrow custom-btn-next"
                            aria-label="Next"
                            role="button" style="display: block;">Next
                    </button>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($content['system-management']['title'] != ''): ?>
        <section class="section-block system-management-section">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 awards-list-block">
                        <div class="row awardListSlider">
                            <?php if (count($content['system-management']['items']) > 0): ?>
                                <?php $memberRow = 0; ?>
                                <?php for ($i = 0; $i < count($content['system-management']['items']);$i++) : ?>
                                    <div class="col-xs-6 col-sm-4 system-item">
                                        <a class="" href="javascript:void(0)" onclick="loadAwards(this)">
                                            <img class="system-management-image" src="<?php echo $content['system-management']['items'][$i]['image']; ?>">
                                            <input type="hidden" class="system-management-title" value="<?php echo $content['system-management']['items'][$i]['title']; ?>" />
                                            <input type="hidden" class="system-management-content" value='<?=$content['system-management']['items'][$i]['content']; ?>' />
                                        </a>
                                    </div>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="col-sm-6 system-management-content">
                        <div class="section-content-description">
                            <div class="section-header">
                                <h2 class="section-title"><?=$content['system-management']['title']; ?></h2>
                            </div>
                            <div class="section-content">
                                <div class="systemSlimscroll">
                                    <?=$content['system-management']['content']?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>

    <?php if ($content['awards']['title'] != ''): ?>
        <section class="section-block awards-section">
            <div class="container">
                <div class="section-header">
                    <h2 class="section-title"><?php echo $content['awards']['title']; ?></h2>
                </div>
                <div class="section-content">
<!--                    <div class="row">-->
<!--                        <div class="col-sm-push-6 col-sm-6 awards-list-block awardListSlider">-->
<!--                            --><?php //if (count($content['awards']['items']) > 0): ?>
<!--                                --><?php //$memberRow = 0; ?>
<!--                                --><?php //for ($i = 0; $i < count($content['awards']['items']);$i++) : ?>
<!--                                    --><?php //if ($memberRow == 0): ?>
<!--                                        <div class="row">-->
<!--                                    --><?php //endif; ?>
<!--                                    <div class="col-xs-6 col-sm-4 awards-item">-->
<!--                                        <a class="" href="javascript:void(0)" onclick="loadAwards(this)">-->
<!--                                            <img class="awards-image" src="--><?php //echo $content['awards']['items'][$i]['image']; ?><!--">-->
<!--                                            <input type="hidden" class="awards-title" value="--><?php //echo $content['awards']['items'][$i]['title']; ?><!--" />-->
<!--                                            <input type="hidden" class="awards-content" value='--><?//=$content['awards']['items'][$i]['content']; ?><!--' />-->
<!--                                        </a>-->
<!--                                    </div>-->
<!--                                    --><?php //$memberRow++; ?>
<!--                                    --><?php //if ($memberRow > 5): ?>
<!--                                        </div>-->
<!--                                        --><?php //$memberRow = 0; ?>
<!--                                    --><?php //endif; ?>
<!--                                --><?php //endfor; ?>
<!--                            --><?php //endif; ?>
<!--                            --><?php //if ($memberRow != 0): ?>
<!--                                </div>-->
<!--                            --><?php //endif; ?>
<!--                        </div>-->
<!--                        <article class="col-sm-pull-6 col-sm-6 article-item awards-detail-block">-->
<!--                            <div class="article-item-inner">-->
<!--                                <figure class="article-header">-->
<!--                                    <img id="awards_image" src="--><?php //echo $content['awards']['items'][0]['image']; ?><!--" alt="">-->
<!--                                </figure>-->
<!--                                <div class="article-item-detail">-->
<!--                                    <h3 id="awards_title" class="article-item-title">--><?//=$content['awards']['items'][0]['title']; ?><!--</h3>-->
<!--                                    <div id="awards_content" class="short-info">-->
<!--                                        <p>--><?//=$content['awards']['items'][0]['content']?><!--</p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </article>-->
<!--                    </div>-->
                    <div class="row">
                        <div class="col-sm-push-6 col-sm-6 awards-list-block">

                            <div class="row awardListSlider">
                            <?php if (count($content['awards']['items']) > 0): ?>
                                <?php $memberRow = 0; ?>
                                <?php for ($i = 0; $i < count($content['awards']['items']);$i++) : ?>
                                    <div class="col-xs-6 col-sm-4 awards-item">
                                        <a class="" href="javascript:void(0)" onclick="loadAwards(this)">
                                            <img class="awards-image" src="<?php echo $content['awards']['items'][$i]['image']; ?>">
                                            <input type="hidden" class="awards-title" value="<?php echo $content['awards']['items'][$i]['title']; ?>" />
                                            <input type="hidden" class="awards-content" value='<?=$content['awards']['items'][$i]['content']; ?>' />
                                        </a>
                                    </div>
                                <?php endfor; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                        <article class="col-sm-pull-6 col-sm-6 article-item awards-detail-block">
                            <div class="article-item-inner">
                                <figure class="article-header">
                                    <img id="awards_image" src="<?php echo $content['awards']['items'][0]['image']; ?>" alt="">
                                </figure>
                                <div class="article-item-detail">
                                    <h3 id="awards_title" class="article-item-title"><?=$content['awards']['items'][0]['title']; ?></h3>
                                    <div id="awards_content" class="short-info">
                                        <p><?=$content['awards']['items'][0]['content']?></p>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
</div>