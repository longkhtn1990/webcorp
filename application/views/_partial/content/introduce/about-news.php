
<?php if ($content['title'] != ''): ?>
    <section class="section-block global-partnership-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo $content['title'] != '' ? $content['title'] : ''; ?></h2>
            </div>
            <div class="section-content">
                <?php for ($i = 0; $i < count($content['news']); $i++):?>
                    <?php if ($i%2 == 0): ?>
                        <div class="section-content-block">
                            <h2 class="section-title"><?php echo $content['news'][$i]['title'] != '' ? $content['news'][$i]['title'] : ''; ?></h2>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-sm-5 section-content-img">
                                        <img src="<?php echo $content['news'][$i]['image'] != '' ? $content['news'][$i]['image'] : ''; ?>" alt="CNS">
                                    </div>
                                    <div class="col-sm-7 section-content-text">
                                        <div>
                                            <?php echo $content['news'][$i]['content'] != '' ? $content['news'][$i]['content'] : ''; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="section-content-block">
                            <h2 class="section-title"><?php echo $content['news'][$i]['title'] != '' ? $content['news'][$i]['title'] : ''; ?></h2>
                            <div class="content-body">
                                <div class="row">
                                    <div class="col-sm-5 col-sm-push-7 section-content-img">
                                        <img src="<?php echo $content['news'][$i]['image'] != '' ? $content['news'][$i]['image'] : ''; ?>" alt="CNS">
                                    </div>
                                    <div class="col-sm-7 col-sm-pull-5 section-content-text">
                                        <div>
                                            <?php echo $content['news'][$i]['content'] != '' ? $content['news'][$i]['content'] : ''; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endfor; ?>
            </div>
        </div>
    </section>
<?php endif; ?>