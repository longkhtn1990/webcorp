<main>
    <section class="section-block customer-contact-intro-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo $content['title']; ?></h2>
            </div>
            <div class="section-content customer-contact-result-section">
                <div class="row map-contact-block">
                    <div class="col-sm-5">
                        <div class="contact-result">
                            <address>
                                <div class="contact-info-wrap">
                                    <h4 class="location-title">
                                        <?php echo $content['title_company']; ?> <br>
                                    </h4>
                                    <p class="contact-info-link">
                                        <a href="https://www.google.com.br/maps/place/58+Nguy%E1%BB%85n+T%E1%BA%A5t+Th%C3%A0nh,+ph%C6%B0%E1%BB%9Dng+12,+Qu%E1%BA%ADn+4,+H%E1%BB%93+Ch%C3%AD+Minh,+Vi%E1%BB%87t+Nam/@10.765887,106.7044893,17z/data=!3m1!4b1!4m5!3m4!1s0x31752f69edb46a81:0xaec4a45f5303e9!8m2!3d10.765887!4d106.706678" target="_blank">
                                            <span><strong><?php echo $content['address']; ?></strong></span>
                                        </a>
                                    </p>
                                    <p class="contact-info-link">
                                        <span><strong>Tel:</strong> <?php echo $content['tel']; ?></span>
                                    </p>
                                    <p class="contact-info-link">
                                        <span><strong>Fax:</strong> <?php echo $content['fax']; ?></span>
                                    </p>
                                    <p class="contact-info-link">
                                        <a href="mailto:cns@cns.com.vn">
                                            <span><strong>Mail:</strong> <?php echo $content['mail']; ?></span>
                                        </a>
                                    </p>
                                    <p class="contact-info-link">
                                        <a href="http://www.cns.com.vn">
                                            <span><strong>Website:</strong> <?php echo $content['website']; ?></span>
                                        </a>
                                    </p>
                                </div>
                            </address>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="map-iframe-wrap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.592093800649!2d106.70448931532921!3d10.765886992328653!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f69edb46a81%3A0xaec4a45f5303e9!2zNTggTmd1eeG7hW4gVOG6pXQgVGjDoG5oLCBwaMaw4budbmcgMTIsIFF14bqtbiA0LCBI4buTIENow60gTWluaCwgVmnhu4d0IE5hbQ!5e0!3m2!1svi!2sbr!4v1500892726416" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-block customer-contact-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title">Liên hệ</h2>
            </div>
            <div class="section-content">
                <div class="wrap-form contact-form">
                    <form id="contact_form" name="contact_form" method="post" role="form">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Tiêu đề liên hệ</label>
                                    <input class="form-control" placeholder="" value="" type="input" id="title_contact" name="title_contact">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Email của bạn</label>
                                    <input class="form-control" placeholder="" value="" type="input" id="email_contact" name="email_contact">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label">Số điện thoại của bạn</label>
                                    <input class="form-control" placeholder="" value="" type="input" id="phone_contact" name="phone_contact">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Nội dung liên hệ</label>
                                    <textarea class="form-control" placeholder="" id="content_contact" name="content_contact"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Mã Capcha</label>
                                    <input class="form-control" placeholder="" value="" type="input" id="captcha" name="captcha">
                                </div>
                            </div>
                            <div class="col-sm-6 capcha-iframe">
                                <div class="form-group">
                                    <span class="capcha-image" id="captImg">
                                        <?php echo $captcha['image']; ?>
                                    </span>
                                    <span class="capcha-refresh">
                                        <a href="javascript:void(0);" class="refreshCaptcha refresh" title="refresh"></a>
                                    </a>
                                    </span>
                                    <input type="hidden" id="code_captcha">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 call-to-action">
                                <button type="submit" class="btn blue">Submit</button><!--data-toggle="modal" data-target=".contact-success-modal" -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</main>
<div class="modal success-modal contact-success-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal" aria-label="Close" type="button">X</button>
            </div>
            <div class="modal-body text-center">
                <div class="success-img">
                    <img src="assets/images/icons/success-img.png" alt="myface">
                </div>
                <h2 class="success-title">Thông tin của bạn đã được gửi đi</h2>
                <p class="cta-block">
                    <button class="btn btn-secondary closeContactformBtn" data-dismiss="modal" aria-label="Close" type="button">Cám ơn!</button>
                </p>
            </div>
        </div>
    </div>
</div>
<?php if ($content['global-partnership']['title'] != ''): ?>
    <section class="section-block global-partnership-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo $content['global-partnership']['title']; ?></h2>
            </div>
            <div class="section-content">
                <div class="row">
                    <div class="col-sm-6 col-sm-push-6 section-content-img">
                        <img src="<?php echo $content['global-partnership']['image']; ?>" alt="">
                    </div>
                    <div class="col-sm-6 col-sm-pull-6 section-content-text">
                        <?php echo $content['global-partnership']['content']; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ($content['customer']['title'] != ''): ?>
    <section class="section-block partnership-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo $content['customer']['title']; ?></h2>
            </div>
            <div class="section-content">
                <div class="partnership-list-block">
                    <?php if (count($content['customer']['items']) > 0): ?>
                        <?php for ($i = 0; $i < count($content['customer']['items']); $i++): ?>
                            <article class="col-xs-4 col-sm-3 partnership-item">
                                <figure class="img-wrap partnership-img-wrap">
                                    <a href="#" target="_blank">
                                        <img src="<?php echo $content['customer']['items'][$i]; ?>" alt="">
                                    </a>
                                </figure>
                            </article>
                        <?php endfor; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php if ($content['partner']['title'] != ''): ?>
<section class="section-block partnership-section">
    <div class="container">
        <div class="section-header">
            <h2 class="section-title"><?php echo $content['partner']['title']; ?></h2>
        </div>
        <div class="section-content">
            <div class="partnership-list-block">

                <?php if (count($content['partner']['items']) > 0): ?>
                    <?php for ($i = 0; $i < count($content['partner']['items']); $i++): ?>
                        <article class="col-xs-4 col-sm-3 partnership-item">
                            <figure class="img-wrap partnership-img-wrap">
                                <a href="#" target="_blank">
                                    <img src="<?php echo $content['partner']['items'][$i]; ?>" alt="">
                                </a>
                            </figure>
                        </article>
                    <?php endfor; ?>
                <?php endif; ?>

            </div>
        </div>
    </div>
</section>
<?php endif; ?>