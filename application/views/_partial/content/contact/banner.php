<section class="banner about-banner">
    <div class="banner-inner" style="background-image: url(<?php echo $content['banner']; ?>);">
        <div class="container banner-text">
            <div class="banner-text-inner">
                <h2 class="section-title">
                    <?php echo $banner['title']; ?>
                </h2>
                <p class="section-description">
                    <?php echo $banner['description']; ?>
                </p>
            </div>
        </div>
    </div>
</section>