<div class="mobile-sidebar">
    <a class="sidebar-close" href="#"></a>
    <div class="sidebar-container">
        <div class="sidebar-header">
            <ul class="nav navbar-nav lang-nav">
                <li class="active">
                    <a href="#">
                        <span>EN</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span>VN</span>
                    </a>
                </li>
            </ul>
        </div>
        <div class="sidebar-body">
            <ul class="list-group sidebar-menu-list">
                <?php if (count($mainMenu) > 0) : ?>
                    <?php for ($i = 0; $i < count($mainMenu); $i++): ?>
                        <li class="menu-item <?php echo $mainMenu[$i]['active'] == true ? 'active' : '';  ?>">
                            <a href="<?php echo $mainMenu[$i]['link'] != '' ? $mainMenu[$i]['link'] : 'javascript:void(0);'; ?>"><?php echo $mainMenu[$i]['text']; ?></a>
                        </li>
                    <?php endfor; ?>
                <?php endif; ?>
            </ul>
            <ul class="list-group sidebar-menu-list sidebar-top-menu">
                <?php if (count($topHeader) > 0): ?>
                    <?php if (isset($topHeader['home'])): ?>
                        <li class="menu-item">
                            <a href="<?php echo $topHeader['home']['link'] != '' ? $topHeader['home']['link'] : 'javascript:void(0);'; ?>"><?php echo $topHeader['home']['text']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if (isset($topHeader['member'])): ?>
                        <li class="menu-item">
                            <a href="<?php echo $topHeader['member']['link']!= '' ? $topHeader['member']['link'] : 'javascript:void(0);'; ?>"><?php echo $topHeader['member']['text']; ?></a>
                        </li>
                    <?php endif; ?>
                    <?php if (isset($topHeader['customerContact'])): ?>
                        <li class="menu-item">
                            <a href="<?php echo $topHeader['customerContact']['link']!= '' ? $topHeader['customerContact']['link'] : 'javascript:void(0);'; ?>"><?php echo $topHeader['customerContact']['text']; ?></a>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>