<?php
class Career_job_model extends CI_Model
{
    public function get_data_career_job_display($param)
    {
        $sql = "SELECT career_job.*
                FROM career_job
                WHERE deleted = 0 AND status = 'active' AND status_approve = 'approved_2' ";
        if(array_key_exists('type',$param)){
            if($param['type'] != '' && $param['type'] != null){
                $sql .= " AND career_job.alias = '".$param['type']."'";
            }
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_career_job_all($position = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM career_job ";
        $where = ' WHERE deleted = 0 ';
        if($position != ''){
            $where .= ' AND position LIKE "%'.$position.'%" ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }
    public function get_data_career_job_menu_display()
    {
        $sql = "SELECT career_job.position,career_job.alias,COUNT(*) AS num
                FROM career_job
                WHERE deleted = 0 AND status = 'active' AND status_approve = 'approved_2'
                GROUP BY career_job.position ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_career_job_by_id($id)
    {
        $sql = "SELECT career_job.*
                FROM career_job
                WHERE deleted = 0 AND id = ".$id;
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function save_career_job($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('career_job', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_career_job($id = '',$data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('career_job', $data);
            return true;
        }
        return false;
    }
}