<?php
class Introduce_model extends CI_Model
{
    public function get_data_news_display($page)
    {
        $sql = "SELECT introduce.title AS introduce_title,introduce.title_vn AS introduce_title_vn,introduce_news.title,introduce_news.title_vn,introduce_news.content,introduce_news.content_vn,introduce_news.date_display,introduce_news.image
                FROM introduce_news
                INNER JOIN introduce ON introduce.id = introduce_news.introduce
                WHERE introduce_news.status = 'active'
                  AND introduce_news.status_approve = 'approved_2'
                  AND introduce_news.date_display <= '".date('Y-m-d')."'
                  AND introduce_news.deleted = 0
                  AND introduce.alias = '".$page."'
                  ORDER BY introduce_news.date_display DESC ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_revenue_display($page)
    {
        $sql = "SELECT introduce_revenue.title,introduce_revenue.title_vn,introduce_revenue.content,introduce_revenue.content_vn,introduce_revenue.date_display
                FROM introduce_revenue
                WHERE introduce_revenue.status = 'active'
                  AND introduce_revenue.status_approve = 'approved_2'
                  AND introduce_revenue.date_display <= '".date('Y-m-d')."'
                  AND introduce_revenue.deleted = 0
                  ORDER BY introduce_revenue.date_display DESC ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_about_display(){
        $sql = "SELECT introduce_about.*
                FROM introduce_about
                WHERE introduce_about.status = 'active'
                  AND introduce_about.status_approve = 'approved_2'
                  AND introduce_about.date_display <= '".date('Y-m-d')."'
                  AND introduce_about.deleted = 0
                  ORDER BY introduce_about.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_about_core_display(){
        $sql = "SELECT introduce_about_core.*
                FROM introduce_about_core
                WHERE introduce_about_core.status = 'active'
                  AND introduce_about_core.status_approve = 'approved_2'
                  AND introduce_about_core.date_display <= '".date('Y-m-d')."'
                  AND introduce_about_core.deleted = 0
                  ORDER BY introduce_about_core.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            $sql_article = "
                SELECT introduce_about_core_article.*
                FROM introduce_about_core_article
                WHERE introduce_about_core_article.status = 'active'
                  AND introduce_about_core_article.status_approve = 'approved_2'
                  AND introduce_about_core_article.date_display <= '".date('Y-m-d')."'
                  AND introduce_about_core_article.deleted = 0
                  ORDER BY introduce_about_core_article.position_order ASC ";
            $result_article = $this->db->query($sql_article)->result_array();
            if(count($result_article) > 0){
                return array(
                    'title' => $result['title'],
                    'title_vn' => $result['title_vn'],
                    'articles' => $result_article,
                    'banner_image'=>$result['banner_image']
                );
            }
        }
        return false;
    }

    public function get_data_about_social_display(){
        $sql = "SELECT introduce_about_social.*
                FROM introduce_about_social
                WHERE introduce_about_social.status = 'active'
                  AND introduce_about_social.status_approve = 'approved_2'
                  AND introduce_about_social.date_display <= '".date('Y-m-d')."'
                  AND introduce_about_social.deleted = 0
                  ORDER BY introduce_about_social.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_about_our_network_display(){
        $sql = "SELECT introduce_about_network.*
                FROM introduce_about_network
                WHERE introduce_about_network.status = 'active'
                  AND introduce_about_network.status_approve = 'approved_2'
                  AND introduce_about_network.date_display <= '".date('Y-m-d')."'
                  AND introduce_about_network.deleted = 0
                  ORDER BY introduce_about_network.position_order DESC ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_about_system_management_display(){
        $sql = "SELECT introduce_about_system_management.*
                FROM introduce_about_system_management
                WHERE introduce_about_system_management.status = 'active'
                  AND introduce_about_system_management.status_approve = 'approved_2'
                  AND introduce_about_system_management.date_display <= '".date('Y-m-d')."'
                  AND introduce_about_system_management.deleted = 0
                  ORDER BY introduce_about_system_management.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            $sql_article = "
                SELECT introduce_about_system_management_article.*
                FROM introduce_about_system_management_article
                WHERE introduce_about_system_management_article.status = 'active'
                  AND introduce_about_system_management_article.status_approve = 'approved_2'
                  AND introduce_about_system_management_article.date_display <= '".date('Y-m-d')."'
                  AND introduce_about_system_management_article.deleted = 0
                  ORDER BY introduce_about_system_management_article.position_order ASC ";
            $result_article = $this->db->query($sql_article)->result_array();
            if(count($result_article) > 0){
                return array(
                    'title' => $result['title'],
                    'title_vn' => $result['title_vn'],
                    'content' => $result['content'],
                    'content_vn' => $result['content_vn'],
                    'articles' => $result_article
                );
            }
        }
        return false;
    }

    public function get_data_about_awards_display(){
        $sql = "SELECT introduce_about_awards.*
                FROM introduce_about_awards
                WHERE introduce_about_awards.status = 'active'
                  AND introduce_about_awards.status_approve = 'approved_2'
                  AND introduce_about_awards.date_display <= '".date('Y-m-d')."'
                  AND introduce_about_awards.deleted = 0
                  ORDER BY introduce_about_awards.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            $sql_article = "
                SELECT introduce_about_awards_article.*
                FROM introduce_about_awards_article
                WHERE introduce_about_awards_article.status = 'active'
                  AND introduce_about_awards_article.status_approve = 'approved_2'
                  AND introduce_about_awards_article.date_display <= '".date('Y-m-d')."'
                  AND introduce_about_awards_article.deleted = 0
                  ORDER BY introduce_about_awards_article.position_order ASC ";
            $result_article = $this->db->query($sql_article)->result_array();
            if(count($result_article) > 0){
                return array(
                    'title' => $result['title'],
                    'title_vn' => $result['title_vn'],
                    'articles' => $result_article
                );
            }
        }
        return false;
    }

    public function get_data_model_culture_display(){
        $sql = "SELECT introduce_model_culture.*
                FROM introduce_model_culture
                WHERE introduce_model_culture.status = 'active'
                  AND introduce_model_culture.status_approve = 'approved_2'
                  AND introduce_model_culture.date_display <= '".date('Y-m-d')."'
                  AND introduce_model_culture.deleted = 0
                  ORDER BY introduce_model_culture.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            $sql_article = "
                SELECT introduce_model_culture_member.*
                FROM introduce_model_culture_member
                WHERE introduce_model_culture_member.status = 'active'
                  AND introduce_model_culture_member.status_approve = 'approved_2'
                  AND introduce_model_culture_member.date_display <= '".date('Y-m-d')."'
                  AND introduce_model_culture_member.deleted = 0
                  ORDER BY introduce_model_culture_member.position_order ASC ";
            $result_member = $this->db->query($sql_article)->result_array();
            if(count($result_member) > 0){
                return array(
                    'title' => $result['title'],
                    'title_vn' => $result['title_vn'],
                    'member' => $result_member
                );
            }
        }
        return false;
    }

    public function get_data_member_culture_display(){
        $sql = "SELECT introduce_member_culture.*
                FROM introduce_member_culture
                WHERE introduce_member_culture.status = 'active'
                  AND introduce_member_culture.status_approve = 'approved_2'
                  AND introduce_member_culture.date_display <= '".date('Y-m-d')."'
                  AND introduce_member_culture.deleted = 0
                  ORDER BY introduce_member_culture.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            $sql_article = "
                SELECT introduce_member_culture_member.*
                FROM introduce_member_culture_member
                WHERE introduce_member_culture_member.status = 'active'
                  AND introduce_member_culture_member.status_approve = 'approved_2'
                  AND introduce_member_culture_member.date_display <= '".date('Y-m-d')."'
                  AND introduce_member_culture_member.deleted = 0
                  ORDER BY introduce_member_culture_member.position_order ASC ";
            $result_member = $this->db->query($sql_article)->result_array();
            if(count($result_member) > 0){
                return array(
                    'title' => $result['title'],
                    'title_vn' => $result['title_vn'],
                    'member' => $result_member
                );
            }
        }
        return false;
    }

    public function aboutMemberInfo(){
        $sql = "SELECT introduce_model_culture.*
                FROM introduce_model_culture
                WHERE introduce_model_culture.status = 'active'
                  AND introduce_model_culture.status_approve = 'approved_2'
                  AND introduce_model_culture.date_display <= '".date('Y-m-d')."'
                  AND introduce_model_culture.deleted = 0
                  ORDER BY introduce_model_culture.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            $sql_article = "
                SELECT introduce_model_culture_member.*
                FROM introduce_model_culture_member
                WHERE introduce_model_culture_member.status = 'active'
                  AND introduce_model_culture_member.status_approve = 'approved_2'
                  AND introduce_model_culture_member.date_display <= '".date('Y-m-d')."'
                  AND introduce_model_culture_member.deleted = 0
                  ORDER BY introduce_model_culture_member.position_order ASC ";
            $result_member = $this->db->query($sql_article)->result_array();
            if(count($result_member) > 0){
                return array(
                    'title' => $result['title'],
                    'title_vn' => $result['title_vn'],
                    'member' => $result_member
                );
            }
        }
        return false;
    }

    public function get_data_model_stories_display(){
        $sql = "SELECT introduce_model_stories.*
                FROM introduce_model_stories
                WHERE introduce_model_stories.status = 'active'
                  AND introduce_model_stories.status_approve = 'approved_2'
                  AND introduce_model_stories.date_display <= '".date('Y-m-d')."'
                  AND introduce_model_stories.deleted = 0
                  ORDER BY introduce_model_stories.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            $sql_member = "
                SELECT introduce_model_stories_member.*
                FROM introduce_model_stories_member
                WHERE introduce_model_stories_member.status = 'active'
                  AND introduce_model_stories_member.status_approve = 'approved_2'
                  AND introduce_model_stories_member.date_display <= '".date('Y-m-d')."'
                  AND introduce_model_stories_member.deleted = 0
                  ORDER BY introduce_model_stories_member.position_order ASC ";
            $result_member = $this->db->query($sql_member)->result_array();
            if(count($result_member) > 0){
                return array(
                    'title' => $result['title'],
                    'title_vn' => $result['title_vn'],
                    'member' => $result_member
                );
            }
        }
        return false;
    }
    public function get_data_member_stories_display(){
        $sql = "SELECT introduce_member_stories.*
                FROM introduce_member_stories
                WHERE introduce_member_stories.status = 'active'
                  AND introduce_member_stories.status_approve = 'approved_2'
                  AND introduce_member_stories.date_display <= '".date('Y-m-d')."'
                  AND introduce_member_stories.deleted = 0
                  ORDER BY introduce_member_stories.date_display DESC ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
			return $result;
        }
        return false;
    }
    public function get_data_introduce_display(){
        $sql = "SELECT introduce.*
                FROM introduce
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND status_approve = 'approved_2' ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_introduce_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM introduce
                WHERE deleted = 0 ";
        if($title != ''){
            $sql .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $sql .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $sql .= ' AND status_approve = "'.$status_approve.'" ';
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_introduce_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM introduce
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }
    public function save_introduce($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('introduce', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_introduce($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('introduce', $data);
            return true;
        }
        return false;
    }
}
