<?php
class Project_model extends CI_Model
{
    public function get_data_projects_display()
    {
        $sql = "SELECT projects.*
                FROM projects
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND status_approve = 'approved_2' ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_project_menu_detail_display($page)
    {
        $sql = "SELECT projects.alias
                FROM project_detail
                INNER JOIN projects ON projects.id = project_detail.project_id
                WHERE project_detail.`status` = 'active'
                      AND project_detail.status_approve = 'approved_2'
                      AND project_detail.date_display < '" . date('Y-m-d') . "'
                      AND project_detail.deleted = 0
                      AND project_detail.id = '" . $page . "'
                ORDER BY project_detail.date_display DESC";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_project_list_display($page)
    {
        $sql = "SELECT projects.title AS project_title,projects.title_vn AS project_title_vn,project_detail.*
                FROM project_detail
                INNER JOIN projects ON projects.id = project_detail.project_id
                WHERE project_detail.`status` = 'active'
                      AND project_detail.status_approve = 'approved_2'
                      AND project_detail.date_display < '" . date('Y-m-d') . "'
                      AND project_detail.deleted = 0
                      AND projects.alias = '" . $page . "'
                ORDER BY project_detail.date_display DESC";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_project_detail_display($page)
    {
        $sql = "SELECT project_detail.*
                FROM project_detail
                INNER JOIN projects ON projects.id = project_detail.project_id
                WHERE project_detail.`status` = 'active'
                      AND project_detail.status_approve = 'approved_2'
                      AND project_detail.date_display < '" . date('Y-m-d') . "'
                      AND project_detail.deleted = 0
                      AND project_detail.id = '" . $page . "'";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    /////////////////////////// ADMIN
    public function get_data_project_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM projects
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function get_data_project_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM projects ";
        $where = ' WHERE deleted = 0 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function save_project($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('projects', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_project($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id);
            $this->db->update('projects', $data);
            return true;
        }
        return false;
    }


    public function get_data_project_detail_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM project_detail
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function get_data_project_detail_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM project_detail ";
        $where = ' WHERE deleted = 0 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function save_project_detail($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('project_detail', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_project_detail($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id);
            $this->db->update('project_detail', $data);
            return true;
        }
        return false;
    }
}