<?php
class Introduce_member_culture_model extends CI_Model
{
    public function get_data_introduce_member_culture_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM introduce_member_culture
                WHERE deleted = 0 ";
        if($title != ''){
            $sql .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $sql .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $sql .= ' AND status_approve = "'.$status_approve.'" ';
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_introduce_member_culture_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM introduce_member_culture
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }
    public function save_introduce_member_culture($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('introduce_member_culture', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_introduce_member_culture($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('introduce_member_culture', $data);
            return true;
        }
        return false;
    }
}
