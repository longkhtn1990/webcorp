<?php
class Contact_model extends CI_Model
{
    public function get_data_contact_display($limit = 1)
    {
        $sql = "SELECT contact.*
                FROM contact
                LIMIT ".$limit;
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_contact_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM contact
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function save_contact($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            if ($this->db->insert('contact', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_contact($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('contact', $data);
            return true;
        }
        return false;
    }
}