<?php
class User_model extends CI_Model
{
    public function get_data_user_by_id($id)
    {
        $sql = "SELECT *
                FROM users
                WHERE deleted = 0
                    AND id = '".$id."' ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_list_user_permission($per)
    {
        $sql = "SELECT *
                FROM users
                WHERE deleted = 0
                    AND permissions LIKE '%".$per."%' ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_user_by_user_name($name)
    {
        $sql = "SELECT *
                FROM users
                WHERE deleted = 0
                    AND username = '".$name."' ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function save_user($data = array())
    {
        try{
            if (count($data) > 0) {
                if ($this->db->insert('users', $data)) {
                    $insert_id = $this->db->insert_id();
                    return $insert_id;
                }
            }
            return false;
        }catch (Exception $e){
            return false;
        }
    }

    public function update_user($id = '', $data = array())
    {
        try{
            if ($id != '' && count($data) > 0) {
                unset($data['id']);
                $this->db->where('id', $id)->update('users', $data);
                return true;
            }
            return false;
        }catch (Exception $e){
            return false;
        }
    }
}