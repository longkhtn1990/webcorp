<?php
class News_model extends CI_Model
{
    public function get_list_category_display(){
        $sql = "SELECT * FROM news_setting";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_latest_news_display()
    {
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type IN ('company','market','general','newspaper')
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_company_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'company'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC
                LIMIT 6";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_market_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'market'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC
                LIMIT 6";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_general_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'general'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC
                LIMIT 6";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_newspaper_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'newspaper'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC
                LIMIT 6";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_highlights_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND highlights = 1
                    AND type IN ('company','market','general','newspaper')
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC
                LIMIT 5";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_introduce_news_display(){
        $sql = "SELECT news_introduce.*
                FROM news_introduce
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC,id DESC
                LIMIT 5";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_download_news_display(){
        $sql = "SELECT news_download.*
                FROM news_download
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND status_approve = 'approved_2'
                    AND (news_download.file IS NOT NULL OR news_download.link IS NOT NULL) 
                ORDER BY date_display DESC,id DESC
                LIMIT 5";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    /*public function get_data_news_content_display($alias)
    {
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND status_approve = 'approved'
                    AND alias = '".$alias."'";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }*/

    /////////////////////////// ADMIN
    public function get_data_news_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM news
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function get_data_news_down_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM news_download
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function get_data_news_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM news ";
        $where = ' WHERE deleted = 0 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function save_news($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('news', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_news($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('news', $data);
            return true;
        }
        return false;
    }

    public function get_all_news_setting(){
        $sql = "SELECT news_setting.*
                FROM news_setting";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function save_news_download($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('news_download', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_news_download($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('news_download', $data);
            return true;
        }
        return false;
    }

    public function get_data_news_download_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM news_download ";
        $where = ' WHERE deleted = 0 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function get_news_download_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM news_download
                    WHERE id = '" . $id . "' ";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function get_data_news_with_id_by_new($id = '')
    {
        if ($id != '') {
            $sql = "SELECT news.*,
                        CONCAT(IF(u_create.first_name IS NULL,'',u_create.first_name),' ',u_create.last_name) AS create_name,
                        CONCAT(IF(edit.first_name IS NULL,'',edit.first_name),' ',edit.last_name) AS edit_name
                    FROM news
                    INNER JOIN users AS u_create ON u_create.id = news.created_by
                    INNER JOIN users AS edit ON edit.id = news.modified_by
                    WHERE news.id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function get_data_latest_news($limit = 5)
    {
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type IN ('company','market','general','newspaper')
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC LIMIT ".$limit;
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_highligh_news($limit = 5)
    {
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type IN ('company','market','general','newspaper')
                    AND status_approve = 'approved_2'
                    AND highlights != 0
                ORDER BY date_display DESC LIMIT ".$limit;
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_news_introduce_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM news_introduce ";
        $where = ' WHERE deleted = 0 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function get_news_introduce_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM news_introduce
                    WHERE id = '" . $id . "' ";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function save_news_introduce($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('news_introduce', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_news_introduce($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('news_introduce', $data);
            return true;
        }
        return false;
    }

    public function get_data_company_news_no_limit_display($limit,$offset){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'company'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC ";
        if($limit != '' && $limit != null){
            $sql .= " LIMIT ".$limit;
        }
        if($offset != '' && $offset != null){
            $sql .= " OFFSET ".$offset;
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_market_news_no_limit_display($limit,$offset){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'market'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC ";
        if($limit != '' && $limit != null){
            $sql .= " LIMIT ".$limit;
        }
        if($offset != '' && $offset != null){
            $sql .= " OFFSET ".$offset;
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_general_news_no_limit_display($limit,$offset){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'general'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC ";
        if($limit != '' && $limit != null){
            $sql .= " LIMIT ".$limit;
        }
        if($offset != '' && $offset != null){
            $sql .= " OFFSET ".$offset;
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_newspaper_news_no_limit_display($limit,$offset){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'newspaper'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC ";
        if($limit != '' && $limit != null){
            $sql .= " LIMIT ".$limit;
        }
        if($offset != '' && $offset != null){
            $sql .= " OFFSET ".$offset;
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_company_news_count_display(){
        $sql = "SELECT *
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'company'
                    AND status_approve = 'approved_2' ";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    public function get_data_market_news_count_display(){
        $sql = "SELECT *
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'market'
                    AND status_approve = 'approved_2' ";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    public function get_data_general_news_count_display(){
        $sql = "SELECT *
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'general'
                    AND status_approve = 'approved_2' ";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }

    public function get_data_newspaper_news_count_display(){
        $sql = "SELECT *
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'newspaper'
                    AND status_approve = 'approved_2'  ";
        $result = $this->db->query($sql)->num_rows();
        return $result;
    }
    public function get_data_news_setting_all($title = '',$status = '',$link = '')
    {
        $sql = "SELECT *
                FROM news_setting ";
        $where = ' WHERE 1 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($link != ''){
            $where .= ' AND id LIKE "%'.$link.'%" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function get_news_setting_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM news_setting
                    WHERE id = '" . $id . "' ";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function save_news_setting($data = array())
    {
        if (count($data) > 0) {
            if ($this->db->insert('news_setting', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_news_setting($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('news_setting', $data);
            return true;
        }
        return false;
    }
}