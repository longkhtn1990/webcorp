<?php
class Introduce_news_model extends CI_Model
{
    public function get_data_introduce_news_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT introduce_news.*,introduce.title AS introduce_title,introduce.title_vn AS introduce_title_vn 
                FROM introduce_news
                LEFT JOIN introduce ON introduce.id = introduce_news.introduce AND introduce.deleted = 0 
                WHERE introduce_news.deleted = 0 ";
        if($title != ''){
            $sql .= ' AND ( introduce_news.title LIKE "%'.$title.'%" OR introduce_news.title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $sql .= ' AND introduce_news.status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $sql .= ' AND introduce_news.status_approve = "'.$status_approve.'" ';
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_introduce_news_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT introduce_news.*,
                            introduce.title AS introduce_title,
                            introduce.title_vn AS introduce_title_vn,
                            introduce.id AS introduce_id
                FROM introduce_news
                LEFT JOIN introduce ON introduce.id = introduce_news.introduce AND introduce.deleted = 0 
                WHERE introduce_news.id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }
    public function save_introduce_news($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('introduce_news', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_introduce_news($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('introduce_news', $data);
            return true;
        }
        return false;
    }
}