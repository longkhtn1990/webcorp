<?php
class Partner_model extends CI_Model
{
    public function get_data_partner_banner_display()
    {
        $sql = "SELECT main_menu.*
                FROM main_menu
                WHERE status = 'active' AND type = 'customer-partner' ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_partner_global_display()
    {
        $sql = "SELECT partner_global.*
                FROM partner_global
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND status_approve = 'approved_2'
                ORDER BY date_display DESC";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_partner_global_with_id($id = 1){
        $sql = "SELECT partner_global.*
                FROM partner_global
                WHERE deleted = 0 AND id = '".$id."'";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_partner_customer_with_id($id = 1){
        $sql = "SELECT partner_customer.*
                FROM partner_customer
                WHERE deleted = 0 AND id = '".$id."'";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_partner_customer_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM partner_customer ";
        $where = ' WHERE deleted = 0 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function get_data_partner_partner_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM partner_partner ";
        $where = ' WHERE deleted = 0 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function get_data_partner_with_id($id = 1){
        $sql = "SELECT partner_partner.*
                FROM partner_partner
                WHERE deleted = 0 AND id = '".$id."'";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function save_partner_global($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('partner_global', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_partner_global($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', 1)->update('partner_global', $data);
            return true;
        }
        return false;
    }

    public function save_partner_customer($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('partner_customer', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function save_partner($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('partner_partner', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_partner_customer($id = '',$data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('partner_customer', $data);
            return true;
        }
        return false;
    }

    public function update_partner($id = '',$data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('partner_partner', $data);
            return true;
        }
        return false;
    }
}