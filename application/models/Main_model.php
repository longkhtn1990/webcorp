<?php

class Main_model extends CI_Model
{
    public function get_data_display()
    {
        $sql = "SELECT * 
                FROM main_info 
                WHERE id = '1' ";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_menu_display()
    {
        $sql = "SELECT * 
                FROM main_menu 
                WHERE status = 'active' 
                ORDER BY position ASC";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_banner_display()
    {
        $sql = "SELECT *
                FROM main_banner
                WHERE status = 'active'
                AND status_approve = 'approved_2'
                AND date_display <= '" . date('Y-m-d H:i:s') . "'
                ORDER BY date_display DESC";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_business_display()
    {
        $sql = "SELECT *
                FROM business
                WHERE status = 'active'
                AND status_approve = 'approved_2'
                AND date_display <= '" . date('Y-m-d H:i:s') . "'
                ORDER BY date_display DESC";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_introduce_display()
    {
        $sql = "SELECT *
                FROM introduce
                WHERE status = 'active'
                AND status_approve = 'approved_2'
                AND date_display <= '" . date('Y-m-d H:i:s') . "'
                ORDER BY date_display DESC";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_introduce_detail_display()
    {

    }

    public function get_info_user_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM users
                    WHERE id = '" . $id . "' ";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }


    ///////////////////////// ADMIN BANNER ////////////////////////
    public function get_data_banner_slide_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM main_banner
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function get_data_banner_slide_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM main_banner ";
        $where = ' WHERE deleted = 0 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function save_banner_slide_image($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('main_banner', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_banner_slide_image($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id);
            $this->db->update('main_banner', $data);
            return true;
        }
        return false;
    }

    ///////////////////////// MAIN INFO ////////////////////////
    public function get_main_info_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM main_info
                    WHERE id = '" . $id . "' ";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }
    public function update_main_info($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('main_info', $data);
            return true;
        }
        return false;
    }
    public function save_main_info($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('main_info', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    ///////////////////////// MAIN MENU ////////////////////////
    public function get_main_menu_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM main_menu
                    WHERE id = '" . $id . "' ";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }
    public function update_main_menu($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('main_menu', $data);
            return true;
        }
        return false;
    }
    public function save_main_menu($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('main_menu', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }
    public function get_data_main_menu_all($title = '',$status = '')
    {
        $sql = "SELECT *
                FROM main_menu
                WHERE deleted = 0 ";
        if($title != ''){
            $sql .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $sql .= ' AND status = "'.$status.'" ';
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
}