<?php
class Introduce_about_core_model extends CI_Model
{
    public function get_data_introduce_about_core_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM introduce_about_core
                WHERE deleted = 0 ";
        if($title != ''){
            $sql .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $sql .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $sql .= ' AND status_approve = "'.$status_approve.'" ';
        }
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
    public function get_data_introduce_about_core_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM introduce_about_core
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }
    public function save_introduce_about_core($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('introduce_about_core', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_introduce_about_core($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', $id)->update('introduce_about_core', $data);
            return true;
        }
        return false;
    }
}