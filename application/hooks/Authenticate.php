<?php
/**
 * Created by PhpStorm.
 * User: duong
 * Date: 27-11-2017
 * Time: 21:46
 */
class Authenticate{
    protected $CI;

    public function __construct() {
        $this->CI = & get_instance();
    }

    public function check_user_login(){
        if($_SERVER['REQUEST_URI'] != '/login') {
            if (!$this->CI->session->userdata(SESSION_LOGIN)) {
                redirect($this->CI->config->item('base_url').'/login');
            }
        }
    }
}