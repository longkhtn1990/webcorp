$(document).ready(function () {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        format: 'dd/mm/yyyy',
        orientation: "left",
        autoclose: true
    });

    $("#partner_global_image").change(function () {
        if ($(this).val() == '') {
            $("#validate_partner_global_image").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_partner_global_image").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#partner_global_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            partner_global_title: {
                required: true
            },
            partner_global_title_vn: {
                required: true
            },
            partner_global_content: {
                required: true
            },
            partner_global_content_vn: {
                required: true
            },
            partner_global_status: {
                required: true
            },
            partner_global_position: {
                required: true
            },
            partner_global_alias: {
                required: true
            },
            partner_global_date_display: {
                required: true
            },
            partner_global_image: {
                extension: "jpg|jpeg|png"
            },
            partner_global_banner_image: {
                extension: "jpg|jpeg|png"
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_partner_global_image").val() == 0 || $('#partner_global_image-error').text() != '') {
                $("#partner_global_image").closest('.form-group').addClass('has-error');
                $("#partner_global_image").closest('.form-group').removeClass('has-success');
            } else {
                $("#partner_global_image").closest('.form-group').removeClass('has-error');
                $("#partner_global_image").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();

            // VALIDATE IMAGE
            if ($("#validate_partner_global_image").val() == 0 || $('#partner_global_image-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                savePartnerGlobal();
            }
        }
    });
});

function savePartnerGlobal() {
    CKEDITOR.instances.partner_global_content.updateElement();
    CKEDITOR.instances.partner_global_content_vn.updateElement();
    var parameters = $('#partner_global_form').serializeArray();
    if ($("#partner_global_image").prop("files")[0] != null && $("#partner_global_image").prop("files")[0]['size'] > 0) {
        var file = $("#partner_global_image").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'partner_global_image_file_type',
            'value': file['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'partner_global_image_file_value',
            'value': file['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'partner_global_image_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'partner_global_image_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/partner-global/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('partner-global',data['record']);
            } else {
                alert('Partner global save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Partner global save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activePartnerGlobal(status) {
    $.ajax({
        url: domain + '/admin/partner-global/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#partner_global_current_user').val(),
                'record_id': $('#partner_global_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('partner_global save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('partner_global save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}