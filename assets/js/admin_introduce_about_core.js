$(document).ready(function () {

    $("#introduce_about_core_image").change(function () {
        if ($(this).val() == '') {
            $("#validate_introduce_about_core_image").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_introduce_about_core_image").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#introduce_about_core_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            introduce_about_core_title: {
                required: true
            },
            introduce_about_core_title_vn: {
                required: true
            },
            introduce_about_core_status: {
                required: true
            },
            introduce_about_core_date_display: {
                required: true
            },
            introduce_about_core_banner_image: {
                extension: "jpg|jpeg|png"
            },
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_introduce_about_core_image").val() == 0 || $('#introduce_about_core_image-error').text() != '') {
                $("#introduce_about_core_image").closest('.form-group').addClass('has-error');
                $("#introduce_about_core_image").closest('.form-group').removeClass('has-success');
            } else {
                $("#introduce_about_core_image").closest('.form-group').removeClass('has-error');
                $("#introduce_about_core_image").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            if ($("#validate_introduce_about_core_image").val() == 0 || $('#introduce_about_core_image-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                saveIntroduceAboutCore();
            }
        }
    });
});

function saveIntroduceAboutCore() {
    var parameters = $('#introduce_about_core_form').serializeArray();
    if ($("#introduce_about_core_banner_image").prop("files")[0] != null && $("#introduce_about_core_banner_image").prop("files")[0]['size'] > 0) {
        var file_banner = $("#introduce_about_core_banner_image").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_core_banner_image_file_type',
            'value': file_banner['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_core_banner_image_file_value',
            'value': file_banner['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_core_banner_image_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_core_banner_image_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/introduce-about-core/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('introduce-about-core',data['record']);
            } else {
                alert('introduce about core save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Introduce about core save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeIntroduceAboutCore(status) {
    $.ajax({
        url: domain + '/admin/introduce-about-core/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_about_core_current_user').val(),
                'record_id': $('#introduce_about_core_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce about core save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce about core save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function approveIntroduceAboutCore(status) {
    $.ajax({
        url: domain + '/admin/introduce-about-core/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_about_core_current_user').val(),
                'record_id': $('#introduce_about_core_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce about core save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce about core save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}