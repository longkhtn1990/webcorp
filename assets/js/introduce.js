function loadInfoMember(this_selected){
    var image = $(this_selected).find('.member-item-img').find('img').attr('src');
    var name = $(this_selected).find('.member-name-hidden').val();
    var position = $(this_selected).find('.member-position-hidden').val();
    var info = $(this_selected).find('.member-info-hidden').val();

    $('#member-detail-img img').attr('src',image);
    $('#member-name').text(name);
    $('#member-position').text(position);
    $('#member-info').html(info);
}

function loadAwards(this_image){
    $('#awards_image').attr('src',$(this_image).find('.awards-image').attr('src'));
    $('#awards_title').html($(this_image).find('.awards-title').val());
    $('#awards_content').html($(this_image).find('.awards-content').val());
}