$(document).ready(function () {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        format: 'dd/mm/yyyy',
        orientation: "left",
        autoclose: true
    });

    var e = $("#career_job_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            career_job_position: {
                required: true
            },
            career_job_quantity: {
                required: true
            },
            career_job_workplace: {
                required: true
            },
            career_job_date_expired: {
                required: true
            },
            career_job_status: {
                required: true
            },
            career_job_content: {
                required: true
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            saveCareerJob("#career_job_form");
        }
    });

    $("#career_job_banner").change(function () {
        if ($(this).val() == '') {
            $("#validate_career_job_banner").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_career_job_banner").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });
    var ee = $("#career_job_banner_form"), rr = $(".alert-danger", ee), ii = $(".alert-success", ee);
    ee.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            career_job_banner: {
                extension: "jpg|jpeg|png"
            },
        },
        invalidHandler: function (e, t) {
            ii.hide();
            rr.show();
            App.scrollTo(rr, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_career_job_banner").val() == 0 || $('#career_job_banner-error').text() != '') {
                $("#career_job_banner").closest('.form-group').addClass('has-error');
                $("#career_job_banner").closest('.form-group').removeClass('has-success');
            } else {
                $("#career_job_banner").closest('.form-group').removeClass('has-error');
                $("#career_job_banner").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            ii.show();
            rr.hide();
            if ($("#validate_career_job_banner").val() == 0 || $('#career_job_banner-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                saveCareerJob('#career_job_banner_form');
            }
        }
    });

    initTableListCareerJob();

    if($('#career_job_list_view').html() != null){
        searchCareerJob();
    }
});

function initTableListCareerJob() {
    if (!jQuery().dataTable && $('#career_job_list_view').html() == null) {
        return;
    }
    var table = $('#career_job_list_view');

    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columns": [
            {'data':'position'},
            {'data':'quantity'},
            {'data':'workplace'},
            {'data':'date_expired'},
            {'data':'status'},
            {'data':'status_approve'},
            {'data':'action'}
        ],

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        // setup buttons extension: http://datatables.net/extensions/buttons/
        buttons: [],

        // scroller extension: http://datatables.net/extensions/scroller/
        scrollY: 300,
        deferRender: true,
        scroller: true,
        deferRender: true,
        //scrollX: true,
        scrollCollapse: true,

        "order": [
            [0, 'asc']
        ],
        "columnDefs": [
            {"orderable": false, "targets":[4,5]}
        ],
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
    });
}

function saveCareerJob(form) {
    if(form == '#career_job_form') {
        CKEDITOR.instances.career_job_content.updateElement();
        var parameters = $(form).serializeArray();
    }else{
        var parameters = $(form).serializeArray();
        if ($("#career_job_banner").prop("files")[0] != null && $("#career_job_banner").prop("files")[0]['size'] > 0) {
            var file_banner = $("#career_job_banner").prop("files")[0];
            parameters[Object.keys(parameters).length] = {
                'name': 'career_job_banner_file_type',
                'value': file_banner['type']
            };
            parameters[Object.keys(parameters).length] = {
                'name': 'career_job_banner_file_value',
                'value': file_banner['result']
            };
        } else {
            parameters[Object.keys(parameters).length] = {
                'name': 'career_job_banner_file_type',
                'value': null
            };
            parameters[Object.keys(parameters).length] = {
                'name': 'career_job_banner_file_value',
                'value': null
            };
        }
    }

    $.ajax({
        url: domain + '/admin/career-job/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('career-job',data['record']);
            } else {
                alert('career job save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('career job save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeCareerJob(status) {
    $.ajax({
        url: domain + '/admin/career-job/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#career_job_current_user').val(),
                'record_id': $('#career_job_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('career job save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('career job save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function approveCareerJob(status) {
    $.ajax({
        url: domain + '/admin/career-job/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#career_job_current_user').val(),
                'record_id': $('#career_job_id').val(),
                'status_approve': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('news career job error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('career job save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function searchCareerJob(){
    $.ajax({
        url: domain + '/admin/career-job/search',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#career_job_current_user').val(),
                'current_language': $('#career_job_current_language').val(),
                'position' : $('#career_job_positions').val(),
                'status' : $('#career_job_status').val(),
                'status_approve' : $('#career_job_status_approve').val()
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            $('#career_job_list_view').DataTable().clear().draw();
            if (data.length > 0) {
                $('#career_job_list_view').DataTable().rows.add(data).draw();
            }
        },
        error: function (data) {
            spinner.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function clearDataSearchCareerJob(){
    $('#career_job_title').val('');
    $('#career_job_status').val('');
    $('#career_job_status_approve').val('');
}

function deleteCareerJob(this_row,record_id){
    if(confirm('Are you sure delete?')) {
        $.ajax({
            url: domain + '/admin/career-job/delete',
            type: 'post',
            data: {
                parameters: {
                    'current_user': $('#career_job_current_user').val(),
                    'record_id': record_id
                }
            },
            dataType: 'json',
            success: function (data) {
                spinner.hide();
                $('#career_job_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            error: function (data) {
                spinner.hide();
                $('#career_job_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            beforeSend: function (jqXHR, setting) {
                spinner.show();
            }
        });
    }
}