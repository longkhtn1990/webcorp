$(document).ready(function () {
    $("#contact_banner").change(function () {
        if ($(this).val() == '') {
            $("#validate_contact_banner").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_contact_banner").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });
    var e = $("#contact_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            contact_name: {
                required: true
            },
            contact_name_vn: {
                required: true
            },
            contact_title: {
                required: true
            },
            contact_title_vn: {
                required: true
            },
            contact_address: {
                required: true
            },
            contact_address_vn: {
                required: true
            },
            contact_tel: {
                required: true
            },
            contact_fax: {
                required: true
            },
            contact_mail: {
                required: true
            },
            contact_website: {
                required: true
            },
            contact_banner: {
                extension: "jpg|jpeg|png"
            },
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_contact_banner").val() == 0 || $('#contact_banner-error').text() != '') {
                $("#contact_banner").closest('.form-group').addClass('has-error');
                $("#contact_banner").closest('.form-group').removeClass('has-success');
            } else {
                $("#contact_banner").closest('.form-group').removeClass('has-error');
                $("#contact_banner").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            if ($("#validate_contact_banner").val() == 0 || $('#contact_banner-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                saveAdminContact();
            }
        }
    });
});

function saveAdminContact() {
    var parameters = $('#contact_form').serializeArray();
    if ($("#contact_banner").prop("files")[0] != null && $("#contact_banner").prop("files")[0]['size'] > 0) {
        var file_banner = $("#contact_banner").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'contact_banner_file_type',
            'value': file_banner['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'contact_banner_file_value',
            'value': file_banner['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'contact_banner_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'contact_banner_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/contact/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('contact',data['record']);
            } else {
                alert('Contact save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Contact save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}