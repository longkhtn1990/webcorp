$(document).ready(function () {

    $("#main_menu_image").change(function () {
        if ($(this).val() == '') {
            $("#validate_main_menu_image").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_main_menu_image").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#main_menu_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            main_menu_title: {
                required: true
            },
            main_menu_title_vn: {
                required: true
            },
            main_menu_description: {
                required: true
            },
            main_menu_description_vn: {
                required: true
            },
            main_menu_type: {
                required: true
            },
            main_menu_position: {
                required: true
            },
            main_menu_image: {
                extension: "jpg|jpeg|png"
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#main_menu_image").val() == 0 || $('#main_menu_image-error').text() != '') {
                $("#main_menu_image").closest('.form-group').addClass('has-error');
                $("#main_menu_image").closest('.form-group').removeClass('has-success');
            } else {
                $("#main_menu_image").closest('.form-group').removeClass('has-error');
                $("#main_menu_image").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            // VALIDATE IMAGE
            if ($("#validate_main_menu_image").val() == 0 || $('#main_menu_image-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                saveMainMenu();
            }
        }
    });

    initTableListMainMenu();

    if($('#main_menu_list_view').html() != null){
        searchMainMenu();
    }
});

function initTableListMainMenu() {
    if (!jQuery().dataTable && $('#main_menu_list_view').html() == null) {
        return;
    }
    var table = $('#main_menu_list_view');

    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columns": [
            {'data':'title'},
            {'data':'title_vn'},
            {'data':'type'},
            {'data':'status'},
            {'data':'image'},
            {'data':'action'}
        ],

        // setup buttons extension: http://datatables.net/extensions/buttons/
        buttons: [],

        // scroller extension: http://datatables.net/extensions/scroller/
        scrollY: 300,
        deferRender: true,
        scroller: true,
        deferRender: true,
        //scrollX: true,
        scrollCollapse: true,

        "order": [
            [3, 'desc']
        ],
        "columnDefs": [
            {"orderable": false, "targets":4}
        ],
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    });
}

function searchMainMenu(){
    $.ajax({
        url: domain + '/admin/main-menu/search',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#main_menu_current_user').val(),
                'current_language': $('#main_menu_current_language').val(),
                'title' : $('#main_menu_title').val(),
                'status' : $('#main_menu_status').val(),
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            $('#main_menu_list_view').DataTable().clear().draw();
            if (data.length > 0) {
                $('#main_menu_list_view').DataTable().rows.add(data).draw();
            }
        },
        error: function (data) {
            spinner.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function clearDataSearchMainMenu(){
    $('#main_menu_title').val('');
    $('#main_menu_status').val('');
}

function saveMainMenu() {
    var parameters = $('#main_menu_form').serializeArray();
    if ($("#main_menu_image").prop("files")[0] != null && $("#main_menu_image").prop("files")[0]['size'] > 0) {
        var file = $("#main_menu_image").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'main_menu_image_file_type',
            'value': file['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'main_menu_image_file_value',
            'value': file['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'main_menu_image_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'main_menu_image_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/main-menu/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('main-menu',data['record']);
            } else {
                alert('Main menu save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Main menu save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeMainMenu(status) {
    $.ajax({
        url: domain + '/admin/main-menu/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#main_menu_current_user').val(),
                'record_id': $('#main_menu_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('Main menu save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce news save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}
function deleteMainMenu(this_row,record_id){
    if(confirm('Are you sure delete?')) {
        $.ajax({
            url: domain + '/admin/main-menu/delete',
            type: 'post',
            data: {
                parameters: {
                    'current_user': $('#main_menu_current_user').val(),
                    'record_id': record_id
                }
            },
            dataType: 'json',
            success: function (data) {
                spinner.hide();
                $('#main_menu_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            error: function (data) {
                spinner.hide();
                $('#main_menu_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            beforeSend: function (jqXHR, setting) {
                spinner.show();
            }
        });
    }
}