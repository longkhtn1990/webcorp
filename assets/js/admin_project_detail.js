$(document).ready(function () {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        format: 'dd/mm/yyyy',
        orientation: "left",
        autoclose: true
    });

    $("#project_detail_image").change(function () {
        if ($(this).val() == '') {
            $("#validate_project_detail_image").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_project_detail_image").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#project_detail_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            project_detail_title: {
                required: true
            },
            project_detail_title_vn: {
                required: true
            },
            project_detail_content: {
                required: true
            },
            project_detail_content_vn: {
                required: true
            },
            project_detail_status: {
                required: true
            },
            project_detail_alias: {
                required: true
            },
            project_detail_date_display: {
                required: true
            },
            project_detail_image: {
                extension: "jpg|jpeg|png"
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_project_detail_image").val() == 0 || $('#project_detail_image-error').text() != '') {
                $("#project_detail_image").closest('.form-group').addClass('has-error');
                $("#project_detail_image").closest('.form-group').removeClass('has-success');
            } else {
                $("#project_detail_image").closest('.form-group').removeClass('has-error');
                $("#project_detail_image").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            // VALIDATE IMAGE
            if ($("#validate_project_detail_image").val() == 0 || $('#project_detail_image-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                saveProjectDetail();
            }
        }
    });

    initTableListProjectDetail();

    if($('#project_detail_list_view').html() != null){
        searchProjectDetail();
    }
});

function initTableListProjectDetail() {
    if (!jQuery().dataTable && $('#project_detail_list_view').html() == null) {
        return;
    }
    var table = $('#project_detail_list_view');

    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columns": [
            {'data':'title'},
            {'data':'title_vn'},
            {'data':'status'},
            {'data':'status_approve'},
            {'data':'image'},
            {'data':'action'}
        ],

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        // setup buttons extension: http://datatables.net/extensions/buttons/
        buttons: [],

        // scroller extension: http://datatables.net/extensions/scroller/
        scrollY: 300,
        deferRender: true,
        scroller: true,
        deferRender: true,
        //scrollX: true,
        scrollCollapse: true,

        "order": [
            [4, 'desc']
        ],
        "columnDefs": [
            {"orderable": false, "targets":5}
        ],
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
    });
}

function saveProjectDetail() {
    CKEDITOR.instances.project_detail_content.updateElement();
    CKEDITOR.instances.project_detail_content_vn.updateElement();
    var parameters = $('#project_detail_form').serializeArray();
    if ($("#project_detail_image").prop("files")[0] != null && $("#project_detail_image").prop("files")[0]['size'] > 0) {
        var file = $("#project_detail_image").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'project_detail_image_file_type',
            'value': file['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'project_detail_image_file_value',
            'value': file['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'project_detail_image_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'project_detail_image_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/project-detail/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('project-detail',data['record']);
            } else {
                alert('Project detail save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Project detail save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeProjectDetail(status) {
    $.ajax({
        url: domain + '/admin/project-detail/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#project_detail_current_user').val(),
                'record_id': $('#project_detail_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('Project detail save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Project detail save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function approveProjectDetail(status) {
    $.ajax({
        url: domain + '/admin/project-detail/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#project_detail_current_user').val(),
                'record_id': $('#project_detail_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('Project detail save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Project detail save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function searchProjectDetail(){
    $.ajax({
        url: domain + '/admin/project-detail/search',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#project_detail_current_user').val(),
                'current_language': $('#project_detail_current_language').val(),
                'title' : $('#project_detail_title').val(),
                'status' : $('#project_detail_status').val(),
                'status_approve' : $('#project_detail_status_approve').val()
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            $('#project_detail_list_view').DataTable().clear().draw();
            if (data.length > 0) {
                $('#project_detail_list_view').DataTable().rows.add(data).draw();
            }
        },
        error: function (data) {
            spinner.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function clearDataSearchProjectDetail(){
    $('#project_detail_title').val('');
    $('#project_detail_status').val('');
    $('#project_detail_status_approve').val('');
}

function deleteProjectDetail(this_row,record_id){
    if(confirm('Are you sure delete?')) {
        $.ajax({
            url: domain + '/admin/project-detail/delete',
            type: 'post',
            data: {
                parameters: {
                    'current_user': $('#project_detail_current_user').val(),
                    'record_id': record_id
                }
            },
            dataType: 'json',
            success: function (data) {
                spinner.hide();
                $('#project_detail_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            error: function (data) {
                spinner.hide();
                $('#project_detail_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            beforeSend: function (jqXHR, setting) {
                spinner.show();
            }
        });
    }
}