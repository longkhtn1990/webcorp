$(document).ready(function () {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        format: 'dd/mm/yyyy',
        orientation: "left",
        autoclose: true
    });

    $("#news_download_file").change(function () {
        if ($(this).val() == '') {
            $("#validate_news_download_file").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_news_download_file").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#news_download_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            news_download_title: {
                required: true
            },
            news_download_title_vn: {
                required: true
            },
            news_download_status: {
                required: true
            },
            news_download_date_display: {
                required: true
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            saveNewsDownload();
        }
    });

    initTableListNewsDownload();

    if($('#news_download_list_view').html() != null){
        searchNewsDownload();
    }
    $('input[type=file]').change(function (event) {
            var tmppath = URL.createObjectURL(event.target.files[0]);
            //$("img").fadeIn("fast").attr('src',tmppath);
        console.log(tmppath);
    });
});

function initTableListNewsDownload() {
    if (!jQuery().dataTable && $('#news_download_list_view').html() == null) {
        return;
    }
    var table = $('#news_download_list_view');

    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columns": [
            {'data':'title'},
            {'data':'title_vn'},
            {'data':'status'},
            {'data':'status_approve'},
            {'data':'action'}
        ],

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        // setup buttons extension: http://datatables.net/extensions/buttons/
        buttons: [],

        // scroller extension: http://datatables.net/extensions/scroller/
        scrollY: 300,
        deferRender: true,
        scroller: true,
        deferRender: true,
        //scrollX: true,
        scrollCollapse: true,

        "order": [
            [0, 'desc']
        ],
        "columnDefs": [
            {"orderable": false, "targets":4}
        ],
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
    });
}

function saveNewsDownload() {
    var parameters = $('#news_download_form').serializeArray();
    //Lấy ra files
    var file_data = $('#news_download_file').prop('files')[0];

    //khởi tạo đối tượng form data
    var form_data = new FormData();
    //thêm files vào trong form data
    form_data.append('file', file_data);
    //sử dụng ajax post
    form_data.append('parameters', JSON.stringify(parameters) );
    $.ajax({
        url: domain + '/admin/news-download/save',
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (data) {
            data = JSON.parse(data);
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('news-download',data['record']);
            } else {
                alert('news download save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('news download save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeNewsDownload(status) {
    $.ajax({
        url: domain + '/admin/news-download/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#news_download_current_user').val(),
                'record_id': $('#news_download_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('news download save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('news download save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function approveNewsDownload(status) {
    $.ajax({
        url: domain + '/admin/news-download/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#news_download_current_user').val(),
                'record_id': $('#news_download_id').val(),
                'status_approve': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('news download save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('news download save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function searchNewsDownload(){
    $.ajax({
        url: domain + '/admin/news-download/search',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#news_download_current_user').val(),
                'current_language': $('#news_download_current_language').val(),
                'title' : $('#news_download_title').val(),
                'status' : $('#news_download_status').val(),
                'status_approve' : $('#news_download_status_approve').val()
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            $('#news_download_list_view').DataTable().clear().draw();
            if (data.length > 0) {
                $('#news_download_list_view').DataTable().rows.add(data).draw();
            }
        },
        error: function (data) {
            spinner.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function clearDataSearchNewsDownload(){
    $('#news_download_title').val('');
    $('#news_download_status').val('');
    $('#news_download_status_approve').val('');
}

function deleteNewsDownload(this_row,record_id){
    if(confirm('Are you sure delete?')) {
        $.ajax({
            url: domain + '/admin/news-download/delete',
            type: 'post',
            data: {
                parameters: {
                    'current_user': $('#news_download_current_user').val(),
                    'record_id': record_id
                }
            },
            dataType: 'json',
            success: function (data) {
                spinner.hide();
                $('#news_download_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            error: function (data) {
                spinner.hide();
                $('#news_download_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            beforeSend: function (jqXHR, setting) {
                spinner.show();
            }
        });
    }
}