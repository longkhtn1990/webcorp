$(document).ready(function () {
    $("#introduce_about_network_image_1").change(function () {
        if ($(this).val() == '') {
            $("#validate_introduce_about_network_image_1").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_introduce_about_network_image_1").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });
    $("#introduce_about_network_image_2").change(function () {
        if ($(this).val() == '') {
            $("#validate_introduce_about_network_image_2").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_introduce_about_network_image_2").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#introduce_about_network_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            introduce_about_network_title: {
                required: true
            },
            introduce_about_network_title_vn: {
                required: true
            },
            introduce_about_network_content: {
                required: true
            },
            introduce_about_network_content_vn: {
                required: true
            },
            introduce_about_network_status: {
                required: true
            },
            introduce_about_network_number_1: {
                required: true
            },
            introduce_about_network_number_2: {
                required: true
            },
            introduce_about_network_text_1: {
                required: true
            },
            introduce_about_network_text_2: {
                required: true
            },
            introduce_about_network_text_1_vn: {
                required: true
            },
            introduce_about_network_text_2_vn: {
                required: true
            },
            introduce_about_network_date_display: {
                required: true
            },
            introduce_about_network_image_1: {
                extension: "jpg|jpeg|png"
            },
            introduce_about_network_image_2: {
                extension: "jpg|jpeg|png"
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_introduce_about_network_image_1").val() == 0 || $('#introduce_about_network_image_1-error').text() != '') {
                $("#introduce_about_network_image_1").closest('.form-group').addClass('has-error');
                $("#introduce_about_network_image_1").closest('.form-group').removeClass('has-success');
            } else {
                $("#introduce_about_network_image_1").closest('.form-group').removeClass('has-error');
                $("#introduce_about_network_image_1").closest('.form-group').addClass('has-success');
            }
            if ($("#validate_introduce_about_network_image_2").val() == 0 || $('#introduce_about_network_image_2-error').text() != '') {
                $("#introduce_about_network_image_2").closest('.form-group').addClass('has-error');
                $("#introduce_about_network_image_2").closest('.form-group').removeClass('has-success');
            } else {
                $("#introduce_about_network_image_2").closest('.form-group').removeClass('has-error');
                $("#introduce_about_network_image_2").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();

            // VALIDATE IMAGE
            if ( ($("#validate_introduce_about_network_image_1").val() == 0 || $('#introduce_about_network_image_1-error').text() != '') &&
                    ($("#validate_introduce_about_network_image_2").val() == 0 || $('#introduce_about_network_image_2-error').text() != '') ) {
                App.scrollTo(r, -200);
            } else {
                saveIntroduceAboutNetwork();
            }
        }
    });

    initTableListIntroduceAboutNetwork();

    if($('#introduce_about_network_list_view').html() != null){
        searchIntroduceAboutNetwork();
    }
});

function initTableListIntroduceAboutNetwork() {
    if (!jQuery().dataTable && $('#introduce_about_network_list_view').html() == null) {
        return;
    }
    var table = $('#introduce_about_network_list_view');

    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columns": [
            {'data':'title'},
            {'data':'title_vn'},
            {'data':'status'},
            {'data':'status_approve'},
            {'data':'image_1'},
            {'data':'image_2'},
            {'data':'action'}
        ],

        // setup buttons extension: http://datatables.net/extensions/buttons/
        buttons: [],

        // scroller extension: http://datatables.net/extensions/scroller/
        scrollY: 300,
        deferRender: true,
        scroller: true,
        deferRender: true,
        //scrollX: true,
        scrollCollapse: true,

        "order": [
            [3, 'desc']
        ],
        "columnDefs": [
            {"orderable": false, "targets":[5,6]}
        ],
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    });
}

function searchIntroduceAboutNetwork(){
    $.ajax({
        url: domain + '/admin/introduce-about-network/search',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_about_network_current_user').val(),
                'current_language': $('#introduce_about_network_current_language').val(),
                'title' : $('#introduce_about_network_title').val(),
                'status' : $('#introduce_about_network_status').val(),
                'status_approve' : $('#introduce_about_network_status_approve').val()
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            $('#introduce_about_network_list_view').DataTable().clear().draw();
            if (data.length > 0) {
                $('#introduce_about_network_list_view').DataTable().rows.add(data).draw();
            }
        },
        error: function (data) {
            spinner.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function clearDataSearchIntroduceAboutNetwork(){
    $('#introduce_about_network_title').val('');
    $('#introduce_about_network_status').val('');
    $('#introduce_about_network_status_approve').val('');
}

function saveIntroduceAboutNetwork() {
    CKEDITOR.instances.introduce_about_network_content.updateElement();
    CKEDITOR.instances.introduce_about_network_content_vn.updateElement();
    var parameters = $('#introduce_about_network_form').serializeArray();
    if ($("#introduce_about_network_image_1").prop("files")[0] != null && $("#introduce_about_network_image_1").prop("files")[0]['size'] > 0) {
        var file_banner = $("#introduce_about_network_image_1").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_network_image_1_file_type',
            'value': file_banner['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_network_image_1_file_value',
            'value': file_banner['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_network_image_1_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_network_image_1_file_value',
            'value': null
        };
    }
    if ($("#introduce_about_network_image_2").prop("files")[0] != null && $("#introduce_about_network_image_2").prop("files")[0]['size'] > 0) {
        var file_banner = $("#introduce_about_network_image_2").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_network_image_2_file_type',
            'value': file_banner['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_network_image_2_file_value',
            'value': file_banner['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_network_image_2_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_network_image_2_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/introduce-about-network/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                 changeIframeToDetail('introduce-about-network',data['record']);
            } else {
                alert('Introduce about network save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Introduce about network save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeIntroduceAboutNetwork(status) {
    $.ajax({
        url: domain + '/admin/introduce-about-network/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_about_network_current_user').val(),
                'record_id': $('#introduce_about_network_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce about network save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce about network save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function approveIntroduceAboutNetwork(status) {
    $.ajax({
        url: domain + '/admin/introduce-about-network/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_about_network_current_user').val(),
                'record_id': $('#introduce_about_network_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce network article save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce about core article save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function deleteIntroduceAboutNetwork(this_row,record_id){
    if(confirm('Are you sure delete?')) {
        $.ajax({
            url: domain + '/admin/introduce-about-network/delete',
            type: 'post',
            data: {
                parameters: {
                    'current_user': $('#introduce_about_network_current_user').val(),
                    'record_id': record_id
                }
            },
            dataType: 'json',
            success: function (data) {
                spinner.hide();
                $('#introduce_about_network_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            error: function (data) {
                spinner.hide();
                $('#introduce_about_network_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            beforeSend: function (jqXHR, setting) {
                spinner.show();
            }
        });
    }
}