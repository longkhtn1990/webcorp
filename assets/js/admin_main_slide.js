$(document).ready(function () {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        format: 'dd/mm/yyyy',
        orientation: "left",
        autoclose: true
    });

    $("#main_slide_image").change(function () {
        if ($(this).val() == '') {
            $("#validate_main_slide_image").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_main_slide_image").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#main_slide_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            main_slide_title: {
                required: true
            },
            main_slide_title_vn: {
                required: true
            },
            main_slide_description: {
                required: false
            },
            main_slide_description_vn: {
                required: false
            },
            main_slide_status: {
                required: true
            },
            main_slide_link_news: {
                required: false
            },
            main_slide_date_display: {
                required: true
            },
            main_slide_image: {
                extension: "jpg|jpeg|png"
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_main_slide_image").val() == 0 || $('#main_slide_image-error').text() != '') {
                $("#main_slide_image").closest('.form-group').addClass('has-error');
                $("#main_slide_image").closest('.form-group').removeClass('has-success');
            } else {
                $("#main_slide_image").closest('.form-group').removeClass('has-error');
                $("#main_slide_image").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            // VALIDATE IMAGE
            if ($("#validate_main_slide_image").val() == 0 || $('#main_slide_image-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                saveBannerSlideImage();
            }
        }
    });

    initTableListBannerSlide();

    if($('#main_slide_list_view').html() != null){
        searchDataBannerSlideImage();
    }
});

function initTableListBannerSlide() {
    if (!jQuery().dataTable && $('#main_slide_list_view').html() == null) {
        return;
    }
    var table = $('#main_slide_list_view');

    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columns": [
            {'data':'title'},
            {'data':'title_vn'},
            {'data':'status'},
            {'data':'status_approve'},
            {'data':'image'},
            {'data':'action'}
        ],

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        // setup buttons extension: http://datatables.net/extensions/buttons/
        buttons: [],

        // scroller extension: http://datatables.net/extensions/scroller/
        scrollY: 300,
        deferRender: true,
        scroller: true,
        deferRender: true,
        scrollX: true,
        scrollCollapse: true,

        "order": [
            [4, 'desc']
        ],
        "columnDefs": [
            {"orderable": false, "targets":5}
        ],
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
    });
}

function saveBannerSlideImage() {
    var parameters = $('#main_slide_form').serializeArray();
    if ($("#main_slide_image").prop("files")[0] != null && $("#main_slide_image").prop("files")[0]['size'] > 0) {
        var file = $("#main_slide_image").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'main_slide_image_file_type',
            'value': file['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'main_slide_image_file_value',
            'value': file['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'main_slide_image_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'main_slide_image_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/main-banner/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('main-banner',data['record']);
            } else {
                alert('Main slide image save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Main slide image save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeBannerSlideImage(status) {
    $.ajax({
        url: domain + '/admin/main-banner/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#main_slide_current_user').val(),
                'record_id': $('#main_slide_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('Main slide image save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Main slide image save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function approveBannerSlideImage(status) {
    $.ajax({
        url: domain + '/admin/main-banner/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#main_slide_current_user').val(),
                'record_id': $('#main_slide_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('Main slide image save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Main slide image save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function changeIframeToDetail(type, record) {
    var url = domain + '/admin/' + type + '/detail/' + record ;
    window.open(url, "_self");
}

function changeIframeToEdit(type, record) {
    var url = domain + '/admin/' + type + '/edit/' + record ;
    window.open(url, "_self");
}

function changeIframeToCreate(type) {
    var url = domain + '/admin/' + type + '/create';
    window.open(url, "_self");
}

function changeIframeToList(type) {
    var url = domain + '/admin/' + type + '/list';
    window.open(url, "_self");
}

function searchDataBannerSlideImage(){
    $.ajax({
        url: domain + '/admin/main-banner/search',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#main_slide_current_user').val(),
                'current_language': $('#main_slide_current_language').val(),
                'title' : $('#main_slide_title').val(),
                'status' : $('#main_slide_status').val(),
                'status_approve' : $('#main_slide_status_approve').val()
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            $('#main_slide_list_view').DataTable().clear().draw();
            if (data.length > 0) {
                $('#main_slide_list_view').DataTable().rows.add(data).draw();
            }
        },
        error: function (data) {
            spinner.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function clearDataSearchBannerSlideImage(){
    $('#main_slide_title').val('');
    $('#main_slide_status').val('');
    $('#main_slide_status_approve').val('');
}

function deleteBannerSlideImage(this_row,record_id){
    if(confirm('Are you sure delete?')) {
        $.ajax({
            url: domain + '/admin/main-banner/delete',
            type: 'post',
            data: {
                parameters: {
                    'current_user': $('#main_slide_current_user').val(),
                    record_id: record_id
                }
            },
            dataType: 'json',
            success: function (data) {
                spinner.hide();
                $('#main_slide_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            error: function (data) {
                spinner.hide();
                $('#main_slide_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            beforeSend: function (jqXHR, setting) {
                spinner.show();
            }
        });
    }
}
function activationProcess(module,record_id){
    if(confirm('Are you sure activation process?')) {
        $.ajax({
            url: domain + '/admin/activationProcess',
            type: 'post',
            data: {
                parameters: {
                    'type':module,
                    'record_id': record_id
                }
            },
            dataType: 'json',
            success: function (data) {
                spinner.hide();
                if(data.status){
                    document.location.reload();
                }else{
                    alert(data.message);
                }
            },
            error: function (data) {
                spinner.hide();
            },
            beforeSend: function (jqXHR, setting) {
                spinner.show();
            }
        });
    }
}