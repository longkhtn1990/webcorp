$(document).ready(function () {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        format: 'dd/mm/yyyy',
        orientation: "left",
        autoclose: true
    });

    var e = $("#new_setting_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            new_setting_link: {
                required: true
            },
            new_setting_title: {
                required: true
            },
            new_setting_title_vn: {
                required: true
            },
            career_job_status: {
                required: true
            },
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            saveNewSetting();
        }
    });

    initTableListNewSetting();

    if($('#new_setting_list_view').html() != null){
        searchNewSetting();
    }
});

function initTableListNewSetting() {
    if (!jQuery().dataTable && $('#new_setting_list_view').html() == null) {
        return;
    }
    var table = $('#new_setting_list_view');

    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columns": [
            {'data':'link'},
            {'data':'title'},
            {'data':'title_vn'},
            {'data':'status'},
            {'data':'action'}
        ],

        // setup buttons extension: http://datatables.net/extensions/buttons/
        buttons: [],

        // scroller extension: http://datatables.net/extensions/scroller/
        scrollY: 300,
        deferRender: true,
        scroller: true,
        deferRender: true,
        //scrollX: true,
        scrollCollapse: true,

        "order": [
            [0, 'desc']
        ],
        "columnDefs": [
            {"orderable": false, "targets":[4]}
        ],
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
    });
}

function saveNewSetting() {
    var parameters = $('#new_setting_form').serializeArray();

    $.ajax({
        url: domain + '/admin/new-setting/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('new-setting',data['record']);
            } else {
                alert('New setting save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('New setting save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeNewSetting(status) {
    $.ajax({
        url: domain + '/admin/new-setting/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#new_setting_current_user').val(),
                'record_id': $('#new_setting_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('new setting save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('new setting save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}
function searchNewSetting(){
    $.ajax({
        url: domain + '/admin/new-setting/search',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#new_setting_current_user').val(),
                'current_language': $('#new_setting_current_language').val(),
                'link' : $('#new_setting_id').val(),
                'status' : $('#new_setting_status').val(),
                'title' : $('#new_setting_title').val()
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            $('#new_setting_list_view').DataTable().clear().draw();
            if (data.length > 0) {
                $('#new_setting_list_view').DataTable().rows.add(data).draw();
            }
        },
        error: function (data) {
            spinner.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function clearDataSearchNewSetting(){
    $('#new_setting_title').val('');
    $('#new_setting_status').val('');
    $('#new_setting_id').val('');
}