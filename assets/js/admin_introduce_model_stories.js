$(document).ready(function () {

    var e = $("#introduce_model_stories_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            introduce_model_stories_title: {
                required: true
            },
            introduce_model_stories_title_vn: {
                required: true
            },
            introduce_model_stories_status: {
                required: true
            },
            introduce_date_display: {
                required: true
            },
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            saveIntroduceModelStories();
        }
    });
});

function saveIntroduceModelStories() {
    var parameters = $('#introduce_model_stories_form').serializeArray();
    $.ajax({
        url: domain + '/admin/introduce-model-stories/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('introduce-model-stories',data['record']);
            } else {
                alert('introduce model stories save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Introduce model stories save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeIntroduceModelStories(status) {
    $.ajax({
        url: domain + '/admin/introduce-model-stories/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_model_stories_current_user').val(),
                'record_id': $('#introduce_model_stories_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce model stories save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce model stories save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function approveIntroduceModelStories(status) {
    $.ajax({
        url: domain + '/admin/introduce-model-stories/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_model_stories_current_user').val(),
                'record_id': $('#introduce_model_stories_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce model stories save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce model stories save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}