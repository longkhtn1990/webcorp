var spin;
$(document).ready(function () {
    spin = new Spinner();
    var e = $("#contact_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            title_contact: {
                required: true
            },
            email_contact: {
                required: true
            },
            phone_contact: {
                required: true
            },
            content_contact: {
                required: true
            },
            title_contact: {
                required: true
            },
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, 1200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            sendContact();
        }
    });
    $('.refreshCaptcha').on('click', function(){
        $.get('captcha/refresh', function(data){
            $('#captImg').html(data);
        });
    });
});
function sendContact(){
    var parameters = {
        'title':$('#title_contact').val(),
        'email':$('#email_contact').val(),
        'phone':$('#phone_contact').val(),
        'content':$('#content_contact').val()
    };
    $.ajax({
        url: domain + '/contact/sendContact',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            if(data.status){
                $('.contact-success-modal').modal();
            }else{
                alert(data.message);
            }
            spin.hide();
        },
        error: function (data) {
            alert('Send error, pls check data.');
            spin.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spin.show();
        }
    });
    // $('.contact-success-modal').modal();
}