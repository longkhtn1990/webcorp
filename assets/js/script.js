var cont_size_1 = 0.723; //380*275
var cont_size_2 = 0.614; //554*340
var cont_size_3 = 0.669; //284*190
var cont_size_4 = 0.742; //458*340
var cont_size_5 = 0.763; //262*200
var cont_size_6 = 1; //554*548
var cont_size_7 = 0.659; //205*135

$(document).ready(function() {
    $(document).on('click', '.menu-mobile-toggle', function(e) {
        e.preventDefault();
        $('body').addClass('open-sidebar');
    });
    $(document).on('click', '.sidebar-close, .sidebar-close-bg', function(e) {
        $('body').removeClass('open-sidebar');
    });
    //backt-to-top
    $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
            $('.back-top-btn').fadeIn();
        }
        else {
            $('.back-top-btn').fadeOut();
        }
    });
    $('.back-top-btn').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, 300);
    });
    //show inquiry/booking form
    $('.goto-customer-info').click(function(event) {
        event.preventDefault();
        $('.select-services-form').slideUp();
        $('.customer-info-form').slideDown();
    });
    //display contact-form
    $('.calltoContactformBtn').click(function(event) {
        event.preventDefault();
        $(this).hide();
        $('.contactForm').slideUp();
        $(this).closest('.contact-result').find('.contactForm').slideDown();
    });
    $('.closeContactformBtn').click(function(event) {
        $('.contactForm').slideUp();
        $('.calltoContactformBtn').show();
    });
    //open contact dropdown
    $(function(){
        if ($('.contact-search-block')[0]){
            var myScrollPosTop = $('.contact-search-block').offset().top;
            $('.openDropdownControl').click(function(event) {
                event.preventDefault();
                if (!$(this).hasClass('active')) {
                    $('.openDropdownControl').removeClass('active');
                    $(this).addClass('active');
                    // $('.contactSearchDropdown').slideUp();
                    $('.contactSearchDropdown').hide();
                    // $(this).closest('.contact-search-item').find('.contactSearchDropdown').slideDown();
                    $(this).closest('.contact-search-item').find('.contactSearchDropdown').show();
                }
                else {
                    $(this).removeClass('active');
                    $('.contactSearchDropdown').slideUp();
                }
            });
            $('.contact-search-dropdown-value').click(function(event){
                event.preventDefault();
                $(this).closest('.contact-search-item').find('.contact-search-dropdown-value').removeClass('active');
                $(this).addClass('active');
                $(this).closest('.contact-search-item').find('.contact-head-text').text($(this).text());
                $('.contactSearchDropdown').slideUp();
            });
            $('.contactSearchBtn').click(function(event) {
                event.preventDefault();
                $('.customerContactResult').show();
                $('html, body').animate({scrollTop: myScrollPosTop}, 300);
                $('.contactSearchDropdown').slideUp('slow');
            });
        }
    });
    //slimscroll
    $(function() {
        $('.contactSlimScroll').slimScroll({
            color: '#f8f8f8',
            height: '228px',
            size: '3px',
            opacity: 1,
            railVisible: true,
            railOpacity: 1,
            railColor: '#fff',
            alwaysVisible: true
        });
    });
    //collapse
    $('.collapse-toggle').click(function(e) {
        e.preventDefault();
        if (!$(this).closest('.collapse-toggle-nav').hasClass('show-collapse')) {
            $('.collapse-div').slideUp("slow");
            $('.collapse-toggle-nav').removeClass('show-collapse');
            $(this).closest('.collapse-toggle-nav').addClass('show-collapse');
            $(this).closest('.collapse-toggle-nav').find('.collapse-div').slideDown("slow");
        } else {
            $(this).closest('.collapse-toggle-nav').removeClass('show-collapse');
            $(this).closest('.collapse-toggle-nav').find('.collapse-div').slideUp("slow");
        }
    });
    $(function(){
        $('.collapse-toggle-nav').first().addClass('show-collapse');
        $('.collapse-toggle-nav').first().find('.collapse-div').slideDown();
    });
    //fit your video
    $('.video-wrapper').fitVids();
    //slider
    $('.banner-slider').slick({
        autoplay: true,
        autoplaySpeed: 4000,
        infinite: true,
        adaptiveWight: true,
        arrows: true,
        dots: true,
        fade: true
    });
	$('.awardListSlider').slick({
        autoplay: true,
        infinite: true,
        adaptiveWight: true,
        arrows: true,
        dots: false,
        slidesPerRow: 3,
        rows: 2,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesPerRow: 3,
                    rows: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesPerRow: 1,
                    rows: 1
                }
            }
        ]
    });
    $('.partner-list-block').slick({
        autoplay: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        adaptiveWight: true,
        arrows: true,
        dots: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 690,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.memeberListSlider').slick({
        autoplay: true,
        infinite: true,
        adaptiveWight: true,
        arrows: true,
        dots: false,
        slidesPerRow: 2,
        rows: 2,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesPerRow: 2,
                    rows: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesPerRow: 1,
                    rows: 1
                }
            }
        ]
    });
    $('.industry-news-list-block').slick({
        autoplay: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 4,
        adaptiveWight: true,
        arrows: false,
        dots: true,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 690,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.sponsor-news-inner').slick({
        autoplay: true,
        autoplaySpeed: 1,
        dots: false,
        arrows: false,
        infinite: true,
        cssEase: 'linear'
    });
    $('.sponsor-news-inner').slick('unslick');
    $('.sponsor-news-inner').slick({
        autoplay: true,
        autoplaySpeed: 10000,
        dots: false,
        arrows: false,
        infinite: true,
        fade: true,
        cssEase: 'linear'
    });
    $(function(){
        if ($('.ourNetworkSlide')[0]){
            var $currentNum = $('.currentNumber');
            var $totalNum = $('.totalNumber');
            var $slickElement = $('.ourNetworkSlide');
            $('.counter-up').counterUp({
                delay: 10, // the delay time in ms
                time: 2000 // the speed time in ms
            });
            $slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                var i = (currentSlide ? currentSlide : 0) + 1;
                $currentNum.text(i);
                $totalNum.text(slick.slideCount);
            });
            $slickElement.slick({
                autoplay: true,
                autoplaySpeed: 4000,
                infinite: true,
                adaptiveWight: true,
                arrows: false,
                dots: false
            });
            $('.custom-btn-next').click(function(){
                $slickElement.slick('slickNext');
                $('.counter-up').counterUp({
                    delay: 10, // the delay time in ms
                    time: 2000 // the speed time in ms
                });
            });
            $('.custom-btn-prev').click(function(){
                $slickElement.slick('slickPrev');
                $('.counter-up').counterUp({
                    delay: 10, // the delay time in ms
                    time: 2000 // the speed time in ms
                });
            });
        }
    });
    //is-tab
    $(function(){
        $('.tab-selector').click(function(e){
            e.preventDefault();
            if (!$(this).hasClass('drop-active')) {
                $('a').removeClass('drop-active');
            }
            if($(this).hasClass('drop-active')) {
                $(this).removeClass('drop-active');
            }
            else {
                $(this).addClass('drop-active');
            }
        });
        $('.tab-selector').text($('.is-dropdown .active a').text());
        $('.dropdown .is-dropdown li a').click(function(e){
            e.preventDefault();
            $('.tab-selector').text($(this).text());
            $('.tab-selector').removeClass('drop-active');
        });
    });
    //setsize
    function setSizes(imgClass, prop) {
        var containerW = $(imgClass).width();
        $(imgClass).height(containerW * prop);
    }
    setSizes('.business-style .img-wrap', cont_size_1);
    setSizes('.news-style .img-wrap', cont_size_2);
    setSizes('.media-img', cont_size_4);
    setSizes('.media-img-2', cont_size_4);
    setSizes('.career-img', cont_size_5);
    setSizes('.member-item-img', cont_size_6);
    setSizes('.member-detail-img', cont_size_6);
    setSizes('.partnership-img-wrap', cont_size_7);
    $(window).resize(function() {
        setSizes('.business-style .img-wrap', cont_size_1);
        setSizes('.news-style .img-wrap', cont_size_2);
        setSizes('.media-img-2', cont_size_4);
        setSizes('.career-img', cont_size_5);
        setSizes('.member-item-img', cont_size_6);
        setSizes('.member-detail-img', cont_size_6);
    });
    //rating
    $('.ratingBox').raty({
        cancel: false,
        score: function() {
            return $(this).attr('data-score');
        },
        half: true,
        targetScore: ('.myScore'),
    });
    $('.ratingBox').click(function(){
        var s = $('.myScore').val();
        tmp = '0.00';
        tmp = (+tmp + Number(s)).toFixed(1); console.log( tmp );
        $('.rateNumber').text(tmp);
    });
    $('.ratingBoxMini').raty({
        score: function() {
            return $(this).attr('data-score');
        },
        half: true,
        readOnly: true
    });
});
$(document).ready(function () {
    if ($('.navbar-tab-style')[0]){
        var myScrollPos = $('.navbar-tab-style li.active').offset().left + $('.navbar-tab-style li.active').outerWidth(true)/2 + $('.navbar-tab-style').scrollLeft() - $('.navbar-tab-style').width()/2;
        $('.navbar-tab-style').animate({scrollLeft: myScrollPos}, 500);
    }
});
