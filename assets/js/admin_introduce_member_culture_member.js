$(document).ready(function () {
    $("#introduce_model_culture_member_image").change(function () {
        if ($(this).val() == '') {
            $("#validate_introduce_model_culture_member_image").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_introduce_model_culture_member_image").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#introduce_member_culture_member_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            introduce_model_culture_member_status: {
                required: true
            },
            introduce_model_culture_member_image : {
                extension: "jpg|jpeg|png"
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_introduce_model_culture_member_image").val() == 0 || $('#introduce_model_culture_member_image-error').text() != '') {
                $("#introduce_model_culture_member_image").closest('.form-group').addClass('has-error');
                $("#introduce_model_culture_member_image").closest('.form-group').removeClass('has-success');
            } else {
                $("#introduce_model_culture_member_image").closest('.form-group').removeClass('has-error');
                $("#introduce_model_culture_member_image").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            if ($("#validate_introduce_model_culture_member_image").val() == 0 || $('#introduce_model_culture_member_image-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                saveIntroduceMemberCultureMember();
            }
        }
    });

    initTableListIntroduceMemberCultureMember();

    if($('#introduce_member_culture_member_list_view').html() != null){
        searchIntroduceMemberCultureMember();
    }
});

function initTableListIntroduceMemberCultureMember() {
    if (!jQuery().dataTable && $('#introduce_member_culture_member_list_view').html() == null) {
        return;
    }
    var table = $('#introduce_member_culture_member_list_view');

    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columns": [
            {'data':'name_vn'},
            {'data':'position_vn'},
            {'data':'status'},
            {'data':'status_approve'},
            {'data':'image'},
            {'data':'action'}
        ],

        // setup buttons extension: http://datatables.net/extensions/buttons/
        buttons: [],

        // scroller extension: http://datatables.net/extensions/scroller/
        scrollY: 300,
        deferRender: true,
        scroller: true,
        deferRender: true,
        //scrollX: true,
        scrollCollapse: true,

        "order": [
            [0, 'desc']
        ],
        "columnDefs": [
            {"orderable": false, "targets":3}
        ],
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    });
}

function searchIntroduceMemberCultureMember(){
    $.ajax({
        url: domain + '/admin/introduce-member-culture-member/search',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_model_culture_member_current_user').val(),
                'current_language': $('#introduce_model_culture_member_current_language').val(),
                'name' : $('#introduce_model_culture_member_title').val(),
                'status' : $('#introduce_model_culture_member_status').val(),
                'status_approve' : $('#introduce_model_culture_member_status_approve').val()
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            $('#introduce_member_culture_member_list_view').DataTable().clear().draw();
            if (data.length > 0) {
                $('#introduce_member_culture_member_list_view').DataTable().rows.add(data).draw();
            }
        },
        error: function (data) {
            spinner.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function clearDataSearchIntroduceMemberCultureMember(){
    $('#introduce_model_culture_member_title').val('');
    $('#introduce_model_culture_member_status').val('');
    $('#introduce_model_culture_member_status_approve').val('');
}

function saveIntroduceMemberCultureMember() {
    var parameters = $('#introduce_member_culture_member_form').serializeArray();
    if ($("#introduce_model_culture_member_image").prop("files")[0] != null && $("#introduce_model_culture_member_image").prop("files")[0]['size'] > 0) {
        var file = $("#introduce_model_culture_member_image").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_model_culture_member_image_file_type',
            'value': file['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_model_culture_member_image_file_value',
            'value': file['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_model_culture_member_image_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_model_culture_member_image_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/introduce-member-culture-member/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('introduce-member-culture-member',data['record']);
            } else {
                alert('introduce about system management article save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Introduce about system management article save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeIntroduceMemberCultureMember(status) {
    $.ajax({
        url: domain + '/admin/introduce-member-culture-member/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_model_culture_member_current_user').val(),
                'record_id': $('#introduce_model_culture_member_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce about system management article save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce about system management article save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}
function approveIntroduceMemberCultureMember(status) {
    $.ajax({
        url: domain + '/admin/introduce-member-culture-member/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_model_culture_member_current_user').val(),
                'record_id': $('#introduce_model_culture_member_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce about system management article save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce about system management article save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function deleteIntroduceMemberCultureMember(this_row,record_id){
    if(confirm('Are you sure delete?')) {
        $.ajax({
            url: domain + '/admin/introduce-member-culture-member/delete',
            type: 'post',
            data: {
                parameters: {
                    'current_user': $('#introduce_model_culture_member_current_user').val(),
                    'record_id': record_id
                }
            },
            dataType: 'json',
            success: function (data) {
                spinner.hide();
                $('#introduce_member_culture_member_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            error: function (data) {
                spinner.hide();
                $('#introduce_member_culture_member_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            beforeSend: function (jqXHR, setting) {
                spinner.show();
            }
        });
    }
}
