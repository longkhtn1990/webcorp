$(document).ready(function () {
    $("#introduce_about_system_management_banner_image").change(function () {
        if ($(this).val() == '') {
            $("#validate_introduce_about_system_management_banner_image").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_introduce_about_system_management_banner_image").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#introduce_about_system_management_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            introduce_about_system_management_title: {
                required: true
            },
            introduce_about_system_management_title_vn: {
                required: true
            },
            introduce_about_system_management_content: {
                required: true
            },
            introduce_about_system_management_content_vn: {
                required: true
            },
            introduce_about_system_management_status: {
                required: true
            },
            introduce_about_system_management_date_display: {
                required: true
            },
            introduce_about_system_management_banner_image: {
                extension: "jpg|jpeg|png"
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_introduce_about_system_management_banner_image").val() == 0 || $('#introduce_about_system_management_banner_image-error').text() != '') {
                $("#introduce_about_system_management_banner_image").closest('.form-group').addClass('has-error');
                $("#introduce_about_system_management_banner_image").closest('.form-group').removeClass('has-success');
            } else {
                $("#introduce_about_system_management_banner_image").closest('.form-group').removeClass('has-error');
                $("#introduce_about_system_management_banner_image").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            if ($("#validate_introduce_about_system_management_banner_image").val() == 0 || $('#introduce_about_system_management_banner_image-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                saveIntroduceAboutSystemManagement();
            }
        }
    });
});

function saveIntroduceAboutSystemManagement() {
    CKEDITOR.instances.introduce_about_system_management_content.updateElement();
    CKEDITOR.instances.introduce_about_system_management_content_vn.updateElement();
    var parameters = $('#introduce_about_system_management_form').serializeArray();
    if ($("#introduce_about_system_management_banner_image").prop("files")[0] != null && $("#introduce_about_system_management_banner_image").prop("files")[0]['size'] > 0) {
        var file_banner = $("#introduce_about_system_management_banner_image").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_system_management_banner_image_file_type',
            'value': file_banner['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_system_management_banner_image_file_value',
            'value': file_banner['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_system_management_banner_image_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'introduce_about_system_management_banner_image_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/introduce-about-system-management/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('introduce-about-system-management',data['record']);
            } else {
                alert('introduce about system management save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Introduce about system management save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activeIntroduceAboutSystemManagement(status) {
    $.ajax({
        url: domain + '/admin/introduce-about-system-management/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_about_system_management_current_user').val(),
                'record_id': $('#introduce_about_system_management_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce about system management save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce about system management save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function approveIntroduceAboutSystemManagement(status) {
    $.ajax({
        url: domain + '/admin/introduce-about-system-management/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#introduce_about_system_management_current_user').val(),
                'record_id': $('#introduce_about_system_management_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('introduce about system management save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('introduce about system management save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}