$(document).ready(function () {
    $('.date-picker').datepicker({
        rtl: App.isRTL(),
        format: 'dd/mm/yyyy',
        orientation: "left",
        autoclose: true
    });

    $("#partner_image").change(function () {
        if ($(this).val() == '') {
            $("#validate_partner_image").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_partner_image").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#partner_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            partner_title: {
                required: true
            },
            partner_title_vn: {
                required: true
            },
            partner_status: {
                required: true
            },
            partner_date_display: {
                required: true
            },
            partner_image: {
                extension: "jpg|jpeg|png"
            }
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_partner_image").val() == 0 || $('#partner_image-error').text() != '') {
                $("#partner_image").closest('.form-group').addClass('has-error');
                $("#partner_image").closest('.form-group').removeClass('has-success');
            } else {
                $("#partner_image").closest('.form-group').removeClass('has-error');
                $("#partner_image").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();

            // VALIDATE IMAGE
            if ($("#validate_partner_image").val() == 0 || $('#partner_image-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                savePartner();
            }
        }
    });

    initTableListPartner();

    if($('#partner_list_view').html() != null){
        searchPartner();
    }
});

function initTableListPartner() {
    if (!jQuery().dataTable && $('#partner_list_view').html() == null) {
        return;
    }
    var table = $('#partner_list_view');

    var oTable = table.dataTable({

        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
            "aria": {
                "sortAscending": ": activate to sort column ascending",
                "sortDescending": ": activate to sort column descending"
            },
            "emptyTable": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "infoEmpty": "No entries found",
            "infoFiltered": "(filtered1 from _MAX_ total entries)",
            "lengthMenu": "_MENU_ entries",
            "search": "Search:",
            "zeroRecords": "No matching records found"
        },

        "columns": [
            {'data':'title'},
            {'data':'title_vn'},
            {'data':'status'},
            {'data':'status_approve'},
            {'data':'image'},
            {'data':'action'}
        ],

        // Or you can use remote translation file
        //"language": {
        //   url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese.json'
        //},

        // setup buttons extension: http://datatables.net/extensions/buttons/
        buttons: [],

        // scroller extension: http://datatables.net/extensions/scroller/
        scrollY: 300,
        deferRender: true,
        scroller: true,
        deferRender: true,
        //scrollX: true,
        scrollCollapse: true,

        "order": [
            [4, 'desc']
        ],
        "columnDefs": [
            {"orderable": false, "targets":5}
        ],
        "lengthMenu": [
            [10, 15, 20, -1],
            [10, 15, 20, "All"] // change per page values here
        ],
        // set the initial value
        "pageLength": 10,

        "dom": "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable

        // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
        // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
        // So when dropdowns used the scrollable div should be removed.
        //"dom": "<'row' <'col-md-12'T>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",
    });
}

function savePartner() {
    var parameters = $('#partner_form').serializeArray();
    if ($("#partner_image").prop("files")[0] != null && $("#partner_image").prop("files")[0]['size'] > 0) {
        var file = $("#partner_image").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'partner_image_file_type',
            'value': file['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'partner_image_file_value',
            'value': file['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'partner_image_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'partner_image_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/partner/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('partner',data['record']);
            } else {
                alert('Partner save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Partner save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function activePartner(status) {
    $.ajax({
        url: domain + '/admin/partner/active-inactive',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#partner_current_user').val(),
                'record_id': $('#partner_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('Partner save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Partner save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function approvePartner(status) {
    $.ajax({
        url: domain + '/admin/partner/approve-deny',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#partner_current_user').val(),
                'record_id': $('#partner_id').val(),
                'status': status
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                document.location.reload();
            } else {
                alert('Partner save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Partner save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function searchPartner(){
    $.ajax({
        url: domain + '/admin/partner/search',
        type: 'post',
        data: {
            parameters: {
                'current_user': $('#partner_current_user').val(),
                'current_language': $('#partner_current_language').val(),
                'title' : $('#partner_title').val(),
                'status' : $('#partner_status').val(),
                'status_approve' : $('#partner_status_approve').val()
            }
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            $('#partner_list_view').DataTable().clear().draw();
            if (data.length > 0) {
                $('#partner_list_view').DataTable().rows.add(data).draw();
            }
        },
        error: function (data) {
            spinner.hide();
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}

function clearDataSearchPartner(){
    $('#partner_title').val('');
    $('#partner_status').val('');
    $('#partner_status_approve').val('');
}

function deletePartner(this_row,record_id){
    if(confirm('Are you sure delete?')) {
        $.ajax({
            url: domain + '/admin/partner/delete',
            type: 'post',
            data: {
                parameters: {
                    'current_user': $('#partner_current_user').val(),
                    'record_id': record_id
                }
            },
            dataType: 'json',
            success: function (data) {
                spinner.hide();
                $('#partner_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            error: function (data) {
                spinner.hide();
                $('#partner_list_view').DataTable().row($(this_row).parents('tr')).remove().draw();
            },
            beforeSend: function (jqXHR, setting) {
                spinner.show();
            }
        });
    }
}