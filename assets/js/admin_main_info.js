$(document).ready(function () {

    $("#main_info_logo").change(function () {
        if ($(this).val() == '') {
            $("#validate_main_info_logo").val(0);
            $(this).closest('.form-group').addClass('has-error');
            $(this).closest('.form-group').removeClass('has-success');
        } else {
            $("#validate_main_info_logo").val(1);
            $(this).closest('.form-group').removeClass('has-error');
            $(this).closest('.form-group').addClass('has-success');
        }
    });

    var e = $("#main_info_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            main_info_title: {
                required: true
            },
            main_info_title_vn: {
                required: true
            },
            main_info_text_header_home: {
                required: true
            },
            main_info_text_header_home_vn: {
                required: true
            },
            main_info_logo: {
                extension: "jpg|jpeg|png"
            },
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
            if ($("#validate_main_info_logo").val() == 0 || $('#introduce_main_info_logo-error').text() != '') {
                $("#main_info_logo").closest('.form-group').addClass('has-error');
                $("#main_info_logo").closest('.form-group').removeClass('has-success');
            } else {
                $("#main_info_logo").closest('.form-group').removeClass('has-error');
                $("#main_info_logo").closest('.form-group').addClass('has-success');
            }
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            if ($("#validate_introduce_image").val() == 0 || $('#introduce_image-error').text() != '') {
                App.scrollTo(r, -200);
            } else {
                saveMainInfo();
            }
        }
    });
});

function saveMainInfo() {
    var parameters = $('#main_info_form').serializeArray();
    if ($("#main_info_logo").prop("files")[0] != null && $("#main_info_logo").prop("files")[0]['size'] > 0) {
        var file = $("#main_info_logo").prop("files")[0];
        parameters[Object.keys(parameters).length] = {
            'name': 'main_info_logo_file_type',
            'value': file['type']
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'main_info_logo_file_value',
            'value': file['result']
        };
    } else {
        parameters[Object.keys(parameters).length] = {
            'name': 'main_info_logo_file_type',
            'value': null
        };
        parameters[Object.keys(parameters).length] = {
            'name': 'main_info_logo_file_value',
            'value': null
        };
    }
    $.ajax({
        url: domain + '/admin/main-info/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                changeIframeToDetail('main-info',data['record']);
            } else {
                alert('Main info save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Main info save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}