function Spinner () {

    this.init = function() {
        if($('#spinner')[0] == null) {
            var html = '<div id="spinner"><div id="spinner-background"></div><div id="slide-wrapper"><div id="slide"><div id="loading-container"><div id="loading"></div><div id="message"><img src="/assets/images/logo.png" style="height: 35px;margin-top: -10px;" /> </div></div></div><div id="align"></div></div></div>';
            $('body').append(html);
        }
    };

    this.init();

    this.show = function() {
        $('#spinner').show();
    } ;

    this.hide = function() {
        $('#spinner').hide();
    };

    this.remove = function() {
        $('#spinner').remove();
    };
}